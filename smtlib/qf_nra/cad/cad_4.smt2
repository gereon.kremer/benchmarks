(set-logic QF_NRA)
(set-info :source |

Ulrich Loup <loup@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(set-info :status sat)
(declare-fun x () Real)
(declare-fun y () Real)
(declare-fun z () Real)
(assert (= (+ (* x x x x) (* z z z z) (* y y y y) (- 3)) 0))
(assert (= x y))
(assert (= y z))
(assert (= z x))
(check-sat)
(exit)
