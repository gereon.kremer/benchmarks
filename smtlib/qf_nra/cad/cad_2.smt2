(set-logic QF_NRA)
(set-info :source |
Example from Colin's paper.

Ulrich Loup <loup@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(set-info :status sat)
(declare-fun x () Real)
(declare-fun y () Real)
(assert (= (+ (* 144 y y) (* 96 x x y) (* 9 x x x x) (* 105 x x) (* 70 x) (- 98)) 0))
(assert (= (+ (* x y y) (* 6 x y) (* x x x) (* 9 x)) 0))
(check-sat)
(exit)
