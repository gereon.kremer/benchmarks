(set-logic QF_NRA)
(set-info :source |
Ulrich Loup <loup@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(set-info :status sat)
(declare-fun x () Real)
(declare-fun y () Real)
(declare-fun z () Real)
(assert (< (+ (* x x) (* z z) (* y y) (- 2)) 0))
(assert (< (+ (* x x) (* z z) (* y y) (* (- 2) x) (- 1)) 0))
(assert (< (+ (* x x) (* z z) (* y y) (* (- 2) x) (* (- 2) y) (- 1)) 0))
(assert (= (+ (* x x) (* z z) (* y y) (* (- 2) x) (* (- 2) y) (* (- 2) z) 1) 0))
(check-sat)
(exit)
