(set-logic QF_NRA)
(set-info :source |
Ulrich Loup <loup@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(declare-fun x () Real)
(declare-fun y () Real)
(declare-fun z () Real)
(assert (and (= (+ (* x x) (* y y) (* z z) (- 2)) 0) (= (+ (* (- x 1) (- x 1)) (* y y) (* z z) (- 2)) 0) (= (+ (* (- x 1) (- x 1)) (* (- y 1) (- y 1)) (* z z) (- 2)) 0)))
(check-sat)
(exit)
