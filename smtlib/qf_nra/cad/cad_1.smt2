(set-logic QF_NRA)
(set-info :source |
Roots of a univariate polynomial having one real root being neither rational nor radical.

Ulrich Loup <loup@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(set-info :status sat)
(declare-fun x () Real)
(assert (= (+ (* x x x x x) (* 3 x x x x) (* x x x) (* (- 1) x x) (* 2 x) (- 2)) 0))
(check-sat)
(exit)
