(set-logic QF_NRA)

(declare-fun x () Real)
(declare-fun y () Real)
(assert 
(and 
    (> (+ (* x x) (* y y) (- 4)) 0)
    (= (+ x (- y) 2) 0)
    (> (+ (* x x y) (* x y y) (- 1)) 0)
    (> x 1)
    (< x 4)
    (> y 2)
    (< y 4)
)
)
(check-sat)
