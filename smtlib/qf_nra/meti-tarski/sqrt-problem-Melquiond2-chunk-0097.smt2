(set-logic QF_NRA)

(declare-fun S () Real)
(declare-fun X () Real)
(declare-fun Y () Real)
(assert 
	(and 
		(not 
			(<= 
				(* X (+ (* S (* S (* S (* S (* S 24.))))) (* X (+ (* S (* S (* S (* S 10.)))) (* X (* S (* S S))))))) 
				(* S S S S S S (- 16))
			)
		)
		(not 
			(<= 
				(* X (+ (* S (* S (* S (* S (* S (+ (/ 371507. 102400.) (* S (/ (- 288.) 125.)))))))) (* X (+ (* S (* S (* S (* S (+ (/ 669969. 81920.) (* S (/ (- 432.) 125.))))))) (* X (+ (* S (* S (* S (+ (/ 2308369. 819200.) (* S (/ (- 36.) 25.)))))) (* X (* S (* S (+ (/ 1. 8.) (* S (/ (- 18.) 125.)))))))))))) 
				(* S S S S S S (/ 149231 51200))
			)
		) 
		(> Y 1)
		(> X (/ 3 2))
		(> S 0)
		(> 2 X)
		(> (/ 33 32) Y)
	)
)
(set-info :status sat)
(check-sat)
