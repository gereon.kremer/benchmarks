(set-logic QF_NRA)

(declare-fun skoX () Real)
(declare-fun e () Real)
(declare-fun a () Real)
(assert (and (not (= a 6.)) (and (not (<= (* skoX (+ (- 279936.) (* skoX (+ (- 46656.) (* skoX (* skoX (* skoX (* skoX (+ (* e (* e (* e (* e (* e (* e (/ 29997. 2500.))))))) (* skoX (+ (* e (* e (* e (* e (* e (* e (/ (- 30003.) 5000.))))))) (* skoX (* e (* e (* e (* e (* e (* e (/ 9999. 10000.))))))))))))))))))) 559872.)) (and (<= e (/ 28245729. 10391023.)) (and (<= e (/ 49171. 18089.)) (and (<= e (/ 193. 71.)) (and (<= e (/ 21743271936. 7992538801.)) (and (<= (/ 1084483. 398959.) e) (and (<= (/ 2721. 1001.) e) (and (<= (/ 131639193503. 48427561125.) e) (and (<= 2. e) (and (not (<= e 0.)) (not (<= skoX 0.))))))))))))))
(set-info :status sat)
(check-sat)
