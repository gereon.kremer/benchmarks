(set-logic QF_NRA)

(declare-fun X () Real)
(declare-fun SQ3 () Real)
(declare-fun pi () Real)
(assert 
	(and 
		(not (<= (* SQ3 (* SQ3 (/ 1. 3.))) 0.)) 
		(not 
			(<= 
				(* X X
						(+ 
							(+ (/ 1. 2.) (* SQ3 (* SQ3 (/ (- 1.) 6.)))) 
							(* X X
									(+ 
										(* SQ3 (* SQ3 (/ 1. 72.))) 
										(* X X
												(+ 
													(* SQ3 (* SQ3 (/ (- 1.) 2160.))) 
													(* X X
															(+ 
																(* SQ3 (* SQ3 (/ 1. 120960.))) 
																(* X X
																		(+ 
																			(* SQ3 (* SQ3 (/ (- 1.) 10886400.))) 
																			(* X X (* SQ3 (* SQ3 (/ 1. 1437004800.))))
																		)
																)
															)
													)
												)
										)
									)
							)
						)
				)
				(+ 3. (* SQ3 (* SQ3 (- 1.))))
			)
		) 
		(not (<= (+ (/ (- 1.) 10000000.) (* pi (/ 1. 2.))) X))
		(not (<= pi (/ 15707963. 5000000.))) 
		(not (<= (/ 31415927. 10000000.) pi)) 
		(not (<= X 0.)) 
		(not (<= SQ3 0.))
	)
)
(set-info :status sat)
(check-sat)
