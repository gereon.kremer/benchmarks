(set-logic QF_NRA)

(declare-fun skoX () Real)
(declare-fun pi () Real)
(declare-fun skoY () Real)
(assert
	(and
		(<=
			(* skoX (* skoX (+ 3600000000000000000000000. (* skoX skoX))))
			(- 970000000000000000000000000000.)
		)
		(not (<= pi (/ 15707963. 5000000.)))
		(not (<= (/ 31415927. 10000000.) pi))
		(<= skoY (* pi (/ 1. 3.)))
		(<= (* pi (/ 1. 4.)) skoY)
		(<= skoX 120.)
		(<= 100. skoX)
	)
)
(set-info :status unsat)
(check-sat)
