(set-logic QF_NRA)

(declare-fun x () Real)
(declare-fun z () Real)
(assert (and (not (<= x 1)) (and (= (* x (* x (- 4))) (+ 3 (* z (* z (- 1))))) (and (not (<= x 0)) (not (<= z 0))))))
(set-info :status sat)
(check-sat)
