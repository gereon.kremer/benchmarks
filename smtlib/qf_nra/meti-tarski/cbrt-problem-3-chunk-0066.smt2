(set-logic QF_NRA)

(declare-fun skoY () Real)
(declare-fun skoZ () Real)
(declare-fun skoX () Real)
(assert (and (not (<= (* skoZ (+ (+ (+ 69. (* skoX 10.)) (* skoY (+ (+ 148. (* skoX 20.)) (* skoY 20.)))) (* skoZ (+ 10. (* skoY 20.))))) (+ (+ (- 32.) (* skoX (- 5.))) (* skoY (+ (+ (- 69.) (* skoX (- 10.))) (* skoY (- 10.))))))) (and (not (<= skoZ 0.)) (and (not (<= skoY 0.)) (not (<= skoX 0.))))))
(set-info :status sat)
(check-sat)
