(set-logic QF_NRA)

(declare-fun skoY () Real)
(declare-fun skoZ () Real)
(declare-fun skoX () Real)
(assert (and (<= (* skoZ (+ (+ (+ (- 69.) (* skoX (- 10.))) (* skoY (+ (+ (- 148.) (* skoX (- 20.))) (* skoY (- 20.))))) (* skoZ (+ (- 10.) (* skoY (- 20.)))))) (+ (+ 32. (* skoX 5.)) (* skoY (+ (+ 69. (* skoX 10.)) (* skoY 10.))))) (and (<= (* skoZ (+ (+ (+ (/ 88. 3.) (* skoX (+ (- 12.) (* skoX (/ (- 1.) 3.))))) (* skoY (+ (+ (/ 761. 3.) (* skoX (+ (/ 16. 3.) (* skoX (/ (- 2.) 3.))))) (* skoY (+ (+ (/ 17. 3.) (* skoX (/ (- 4.) 3.))) (* skoY (/ (- 2.) 3.))))))) (* skoZ (+ (+ (+ (/ (- 71.) 6.) (* skoX (/ (- 2.) 3.))) (* skoY (+ (+ (/ 17. 3.) (* skoX (/ (- 4.) 3.))) (* skoY (/ (- 4.) 3.))))) (* skoZ (+ (/ (- 1.) 3.) (* skoY (/ (- 2.) 3.)))))))) (+ (+ (/ 80. 3.) (* skoX (+ (/ 40. 3.) (* skoX (/ 1. 6.))))) (* skoY (+ (+ (/ (- 88.) 3.) (* skoX (+ 12. (* skoX (/ 1. 3.))))) (* skoY (+ (+ (/ 71. 6.) (* skoX (/ 2. 3.))) (* skoY (/ 1. 3.)))))))) (and (<= (* skoZ (+ (+ (+ 69. (* skoX 10.)) (* skoY (+ (+ 148. (* skoX 20.)) (* skoY 20.)))) (* skoZ (+ 10. (* skoY 20.))))) (+ (+ (- 32.) (* skoX (- 5.))) (* skoY (+ (+ (- 69.) (* skoX (- 10.))) (* skoY (- 10.)))))) (and (<= (+ (+ 1. (* skoX (- 1.))) (* skoY (- 1.))) skoZ) (and (not (<= skoZ 0.)) (and (not (<= skoY 0.)) (not (<= skoX 0.)))))))))
(set-info :status unsat)
(check-sat)
