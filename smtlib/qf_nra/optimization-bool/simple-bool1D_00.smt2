(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun x_0 () Real)
(assert
  (and
    (or (and (<= (+ x_0 (- 3)) 0) (>= (+ x_0 (- 2)) 0) ) (and (<= (+ x_0 2) 0) (>= (+ x_0 3) 0) ) )
  )
)
(check-sat)
(exit)
