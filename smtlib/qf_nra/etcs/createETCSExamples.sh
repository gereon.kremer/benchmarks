#!/bin/bash
for steps in {01..10}; do
    for distance in 500; do
	echo "Generating etcs_"$steps"_"$distance".smt2..."
        python ../generate_etcs_example_smtlib.py $steps $distance > "etcs_"$steps"_"$distance".smt2";
    done;
done;
