(set-logic QF_NRA)
(declare-fun muscore1dollarsk!0 () Real)
(declare-fun zuscore1dollarsk!1 () Real)
(declare-fun b () Real)
(declare-fun duscore1dollarsk!2 () Real)
(declare-fun vuscore1dollarsk!3 () Real)
(declare-fun z () Real)
(declare-fun m () Real)
(declare-fun d () Real)
(declare-fun v () Real)
(declare-fun ep () Real)
(declare-fun amax () Real)
(assert (>= zuscore1dollarsk!1 muscore1dollarsk!0))
(assert (<= (+ (* vuscore1dollarsk!3 vuscore1dollarsk!3)
               (* (- 1.0) duscore1dollarsk!2 duscore1dollarsk!2))
            (* 2.0 b (+ muscore1dollarsk!0 (* (- 1.0) zuscore1dollarsk!1)))))
(assert (>= vuscore1dollarsk!3 0.0))
(assert (>= duscore1dollarsk!2 0.0))
(assert (<= (+ (* v v) (* (- 1.0) d d)) (* 2.0 b (+ m (* (- 1.0) z)))))
(assert (>= v 0.0))
(assert (not (<= ep 0.0)))
(assert (not (<= b 0.0)))
(assert (not (<= amax 0.0)))
(assert (>= d 0.0))
(assert (not (<= vuscore1dollarsk!3 duscore1dollarsk!2)))
(check-sat)
