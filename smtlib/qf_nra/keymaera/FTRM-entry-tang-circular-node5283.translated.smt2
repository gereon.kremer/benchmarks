(set-logic QF_NRA)
(declare-fun r () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun h2uscore1dollarsk!0 () Real)
(declare-fun d1 () Real)
(declare-fun h1uscore1dollarsk!1 () Real)
(declare-fun d2 () Real)
(assert (>= (+ (* x1 x1) (* x2 x2)) (* r r)))
(assert (= d1 (+ x2 (* (- 1.0) h2uscore1dollarsk!0))))
(assert (= d2 (+ (* (- 1.0) x1) h1uscore1dollarsk!1)))
(assert (= (* r r) (+ (* d1 d1) (* d2 d2))))
(assert (not (= (+ (* (+ x1 (* (- 1.0) h1uscore1dollarsk!1))
                      (+ x1 (* (- 1.0) h1uscore1dollarsk!1)))
                   (* (+ x2 (* (- 1.0) h2uscore1dollarsk!0))
                      (+ x2 (* (- 1.0) h2uscore1dollarsk!0))))
                (* r r))))
(check-sat)
