(set-logic QF_NRA)
(declare-fun buscore0dollarsk!0 () Real)
(declare-fun a () Real)
(declare-fun c () Real)
(declare-fun y0 () Real)
(declare-fun x0 () Real)
(declare-fun t1uscore0dollarsk!3 () Real)
(declare-fun t2uscore0dollarsk!2 () Real)
(declare-fun tuscore0dollarsk!1 () Real)
(assert (= (* c c) (+ (* a a) (* buscore0dollarsk!0 buscore0dollarsk!0))))
(assert (= (* c c)
           (+ (* x0 x0)
              (* (+ y0 (* (- 1.0) buscore0dollarsk!0))
                 (+ y0 (* (- 1.0) buscore0dollarsk!0))))))
(assert (= (* t1uscore0dollarsk!3 y0) (+ a x0)))
(assert (= (* t2uscore0dollarsk!2 y0) (+ a (* (- 1.0) x0))))
(assert (= (* tuscore0dollarsk!1
              (+ 1.0 (* (- 1.0) t1uscore0dollarsk!3 t2uscore0dollarsk!2)))
           (+ t1uscore0dollarsk!3 t2uscore0dollarsk!2)))
(assert (not (= y0 0.0)))
(assert (not (= a 0.0)))
(assert (not (= (* buscore0dollarsk!0 tuscore0dollarsk!1) a)))
(check-sat)
