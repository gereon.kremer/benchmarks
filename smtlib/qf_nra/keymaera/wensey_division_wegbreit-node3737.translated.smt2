(set-logic QF_NRA)
(declare-fun Tol () Real)
(declare-fun duscore2dollarsk!1 () Real)
(declare-fun Q () Real)
(declare-fun buscore2dollarsk!3 () Real)
(declare-fun yuscore2dollarsk!0 () Real)
(declare-fun auscore2dollarsk!2 () Real)
(declare-fun P () Real)
(assert (>= duscore2dollarsk!1 Tol))
(assert (= (* 2.0 buscore2dollarsk!3) (* Q duscore2dollarsk!1)))
(assert (= (* auscore2dollarsk!2 duscore2dollarsk!1)
           (* 2.0 buscore2dollarsk!3 yuscore2dollarsk!0)))
(assert (= auscore2dollarsk!2 (* Q yuscore2dollarsk!0)))
(assert (<= (+ auscore2dollarsk!2 buscore2dollarsk!3) P))
(assert (not (= buscore2dollarsk!3 (* (/ 1.0 2.0) Q duscore2dollarsk!1))))
(check-sat)
