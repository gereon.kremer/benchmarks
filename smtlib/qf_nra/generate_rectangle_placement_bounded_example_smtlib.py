#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import itertools

# Example input file generator for the SMT-Solver using virtual substitution.
# @author: Florian Corzilius
# @author: Ulrich Loup
# @version: 2010-12-13

#
# Constants
#

#
# Default values
#
A = 0
n = 0
rectanglesSet = [(2,3),(2,5),(1,1),(2,2),(5,5),(4,3),(3,3),(7,1),(8,2),(2,5)]
rectangles = rectanglesSet[n]

#
# Functions
#

def noIntersection(i,j):
  formula = ""
  if j<len(rectangles)-1:
#    formula += "(and "
    formula += noIntersection(i,j+1)
    formula += " "
    formula += "(or "
    formula += "(>= (- x_" + str(i) + " (+ x_" + str(j) + " " + str(rectangles[j][0]) + ")) 0)"
    formula += " "
    formula += "(>= (- x_" + str(j) + " (+ x_" + str(i) + " " + str(rectangles[i][0]) + ")) 0)"
    formula += " "
    formula += "(>= (- y_" + str(i) + " (+ y_" + str(j) + " " + str(rectangles[j][1]) + ")) 0)"
    formula += " "
    formula += "(>= (- y_" + str(j) + " (+ y_" + str(i) + " " + str(rectangles[i][1]) + ")) 0)"
    formula += ")"
  else:
    if i<len(rectangles)-2:
#      formula += "(and "
      formula += noIntersection(i+1,i+2)
      formula += " "
      formula += "(or "
      formula += "(>= (- x_" + str(i) + " (+ x_" + str(j) + " " + str(rectangles[j][0]) + ")) 0)"
      formula += " "
      formula += "(>= (- x_" + str(j) + " (+ x_" + str(i) + " " + str(rectangles[i][0]) + ")) 0)"
      formula += " "
      formula += "(>= (- y_" + str(i) + " (+ y_" + str(j) + " " + str(rectangles[j][1]) + ")) 0)"
      formula += " "
      formula += "(>= (- y_" + str(j) + " (+ y_" + str(i) + " " + str(rectangles[i][1]) + ")) 0)"
      formula += ")"
    else:
      formula += "(or "
      formula += "(>= (- x_" + str(i) + " (+ x_" + str(j) + " " + str(rectangles[j][0]) + ")) 0)"
      formula += " "
      formula += "(>= (- x_" + str(j) + " (+ x_" + str(i) + " " + str(rectangles[i][0]) + ")) 0)"
      formula += " "
      formula += "(>= (- y_" + str(i) + " (+ y_" + str(j) + " " + str(rectangles[j][1]) + ")) 0)"
      formula += " "
      formula += "(>= (- y_" + str(j) + " (+ y_" + str(i) + " " + str(rectangles[i][1]) + ")) 0)"
      formula += ")"
  return formula

def fitsInArea(A,i,j,k,l):
  formula = ""
  if i<len(rectangles)-1:
#    formula += "(and (and "
    formula += fitsInArea1(A,i+1,j,k,l)
    formula += " "
    formula += "(>= (- " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
    formula += " "
    formula += "(>= (+ " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
#    formula += ")"
  else:
    if j<len(rectangles)-1:
#      formula += "(and (and "
      formula += fitsInArea1(A,0,j+1,k,l)
      formula += " "
      formula += "(>= (- " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
      formula += " "
      formula += "(>= (+ " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
#      formula *= ")"
    else:
      if k<len(rectangles)-1:
#        formula += "(and (and "
        formula += fitsInArea1(A,0,0,k+1,l)
        formula += " "
        formula += "(>= (- " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
        formula += " "
        formula += "(>= (+ " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
#        formula *= ")"
      else:
        if l<len(rectangles)-2:
#          formula += "(and (and "
          formula += fitsInArea1(A,0,0,0,l+1)
          formula += " "
          formula += "(>= (- " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
          formula += " "
          formula += "(>= (+ " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
#          formula *= ")"
        else:
#          formula += "(and "
          formula += "(>= (- " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
          formula += " "
          formula += "(>= (+ " + str(A) + " (* (- (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") x_" + str(j) + ") (- (+ y_" + str(k) + " " + str(rectangles[k][1]) + ") y_" + str(l) + ") ) ) 0)"
#          formula *= ")"
  return formula

def minWidthOfsuroundingRectangle(i,j):
  formula = ""
  if j<len(rectangles)-1:
#    formula += "(and"
    formula += minWidthOfsuroundingRectangle(i,j+1)
    formula += " "
    formula += isNotTheGreatestORnotTheSmallestORtheCandidateX(i,j)
#    formula += ")"
  else:
    if j==len(rectangles)-1 and i<len(rectangles)-1:
#		  formula += "(and "
		  formula += minWidthOfsuroundingRectangle(i+1,0)
		  formula += " "
		  formula += isNotTheGreatestORnotTheSmallestORtheCandidateX(i,j)
#		  formula += ")"
    else:
      formula += isNotTheGreatestORnotTheSmallestORtheCandidateX(i,j)
  return formula

def isNotTheGreatestORnotTheSmallestORtheCandidateX(i,j):
  formula = "(or "
  formula += "(= delta_x (- (+ x_" + str(j) + " " + str(rectangles[j][0]) + ") x_" + str(i) + "))"
  formula += " "
  formula += isNotTheSmallestORnotTheGreatestX(i,j,0)
  formula += ")"
  return formula

def isNotTheSmallestORnotTheGreatestX(i,j,k):
  formula = ""
  if k<len(rectangles)-1 and k!=i and (k+1!=i or k!=len(rectangles)-2):
#    formula += "(or "
    formula += isNotTheSmallestORnotTheGreatestX(i,j,k+1)
    formula += " "
    formula += "(> x_" + str(i) + " x_" + str(k) + ")"
#    formula +=")"
  else:
    if k==i and k<len(rectangles)-1:
      formula += isNotTheSmallestORnotTheGreatestX(i,j,k+1)
    else:
#      formula += "(or "
      formula += isNotTheGreatestX(j,0)
      formula += " "
      formula += "(> x_" + str(i) + " x_" + str(k) + ")"
#      formula += ")"
  return formula

def isNotTheGreatestX(i,k):
  formula = ""
  if k<len(rectangles)-1 and k!=i and (k+1!=i or k!=len(rectangles)-2):
#    formula += "(or "
    formula += isNotTheGreatestX(i,k+1)
    formula += " "
    formula += "(< (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") (+ x_" + str(k) + " " + str(rectangles[k][0]) + "))"
#    formula +=")"
  else:
    if k==i and k<len(rectangles)-1:
      formula += isNotTheGreatestX(i,k+1)
    else:
      formula += "(< (+ x_" + str(i) + " " + str(rectangles[i][0]) + ") (+ x_" + str(k) + " " + str(rectangles[k][0]) + "))"
  return formula

def minLengthOfsuroundingRectangle(i,j):
  formula = ""
  if j<len(rectangles)-1:
#    formula += "(and "
    formula += minLengthOfsuroundingRectangle(i,j+1)
    formula += " "
    formula += isNotTheGreatestORnotTheSmallestORtheCandidateY(i,j)
#    formula += ")"
  else:
    if j==len(rectangles)-1 and i<len(rectangles)-1:
#		  formula += "(and "
		  formula += minLengthOfsuroundingRectangle(i+1,0)
		  formula += " "
		  formula += isNotTheGreatestORnotTheSmallestORtheCandidateY(i,j)
#		  formula += ")"
    else:
      formula += isNotTheGreatestORnotTheSmallestORtheCandidateY(i,j)
  return formula

def isNotTheGreatestORnotTheSmallestORtheCandidateY(i,j):
  formula = "(or "
  formula += "(= delta_y (- (+ y_" + str(j) + " " + str(rectangles[j][1]) + ") y_" + str(i) + "))"
  formula += " "
  formula += isNotTheSmallestORnotTheGreatestY(i,j,0)
  formula += ")"
  return formula

def isNotTheSmallestORnotTheGreatestY(i,j,k):
  formula = ""
  if k<len(rectangles)-1 and k!=i and (k+1!=i or k!=len(rectangles)-2):
#    formula += "(or "
    formula += isNotTheSmallestORnotTheGreatestY(i,j,k+1)
    formula += " "
    formula += "(> y_" + str(i) + " y_" + str(k) + ")"
#    formula +=")"
  else:
    if k==i and k<len(rectangles)-1:
      formula += isNotTheSmallestORnotTheGreatestY(i,j,k+1)
    else:
#      formula += "(or "
      formula += isNotTheGreatestY(j,0)
      formula += " "
      formula += "(> y_" + str(i) + " y_" + str(k) + ")"
#      formula += ")"
  return formula

def isNotTheGreatestY(i,k):
  formula = ""
  if k<len(rectangles)-1 and k!=i and (k+1!=i or k!=len(rectangles)-2):
#    formula += "(or "
    formula += isNotTheGreatestY(i,k+1)
    formula += " "
    formula += "(< (+ y_" + str(i) + " " + str(rectangles[i][1]) + ") (+ y_" + str(k) + " " + str(rectangles[k][1]) + "))"
#    formula +=")"
  else:
    if k==i and k<len(rectangles)-1:
      formula += isNotTheGreatestY(i,k+1)
    else:
      formula += "(< (+ y_" + str(i) + " " + str(rectangles[i][1]) + ") (+ y_" + str(k) + " " + str(rectangles[k][1]) + "))"
  return formula

def fitsInArea2(A):
  formula = "(<= (* delta_x delta_y) " + str(A) + ")"
  return formula

#
# Generate BMC formula up to length n.
#
def formula(A,l,u):
  assert( A >= 0)
  assert( l < u)
  lower = ""
  if l < 0:
    lower = "(- " + str(abs(l)) + ")"
  else:
    lower = str(l)
  upper = ""
  if u < 0:
    upper = "(- " + str(abs(u)) + ")"
  else:
    upper = str(u)
  formula = "(set-logic QF_NRA)\n"
  formula += "(set-info :source |\n"
  formula += "Florian Corzilius <corzilius@cs.rwth-aachen.de>\n"
  formula += "\n"
  formula += "|)\n"
  formula += "(set-info :smt-lib-version 2.0)\n"
  formula += "(set-info :category \"industrial\")\n"
  for k in range(0,len(rectangles)):
    formula += "(declare-fun x_" + str(k) + " () Real)\n"
    formula += "(declare-fun y_" + str(k) + " () Real)\n"
  formula += "(declare-fun delta_x () Real)\n"
  formula += "(declare-fun delta_y () Real)\n"
  for k in range(0,len(rectangles)):
    formula += "(assert (>= x_"+str(k)+" " + lower + "))\n"
    formula += "(assert (<= x_"+str(k)+" " + upper + "))\n"
    formula += "(assert (>= y_"+str(k)+" " + lower + "))\n"
    formula += "(assert (<= y_"+str(k)+" " + upper + "))\n"
  formula += "(assert (>= delta_x " + lower + "))\n"
  formula += "(assert (<= delta_x " + upper + "))\n"
  formula += "(assert (>= delta_y " + lower + "))\n"
  formula += "(assert (<= delta_y " + upper + "))\n"
  formula += "(assert \n"
  formula += "(and "
  formula += minWidthOfsuroundingRectangle(0,0)
  formula += " "
  formula += minLengthOfsuroundingRectangle(0,0)
  formula += " "
  formula += noIntersection(0,1)
  formula += " "
  formula += fitsInArea2(A)
  formula += "))\n"
  formula += "(check-sat)\n"
  formula += "(exit)\n"
  return formula

def formulaG(A):
  assert( A >= 0 )
  formula += "vector<symbol> l =  vector<symbol>();\n"
  for k in range(0,len(rectangles)):
    formula += "symbol x_" + str(k) + " = symbol(x_" + str(k) + "); l.push_back(x_" + str(k) + ");\n"
    formula += "symbol y_" + str(k) + " = symbol(y_" + str(k) + "); l.push_back(y_" + str(k) + ");\n"
  formula += "symbol delta_x = symbol(delta_x); l.push_back(delta_x);\n"
  formula += "symbol delta_y = symbol(delta_y); l.push_back(delta_y);\n"
  formula += "vector<Constraint> = vector<Constraint>();\n"
  formula += "(and "
  formula += minWidthOfsuroundingRectangle(0,0)
  formula += " "
  formula += minLengthOfsuroundingRectangle(0,0)
  formula += " "
  formula += noIntersection(0,1)
  formula += " "
  formula += fitsInArea2(A)
  formula += "))\n"
  formula += "(check-sat)\n"
  formula += "(exit)\n"
  return formula

# Print usage information
def printUsage():
  print "Usage: python "+sys.argv[0]+" n A \n"
  print "            n: number of rectangles for the placement (>1, <11), which are from a fixed set of rectangles"
  print "            A: area where rectangles have to be placed in (>=0)"
  print( "           l: number being the lower bound of all variables" )
  print( "           u: number (>l) being the upper bound of all variables" )
  print
  print "Example: python generate_rectangle_placement_bounded_example_smtlib.py 3 50 -1000 1000"

#
# Parse input.
#
i = 0
for entry in sys.argv:
  try:
    if i == 1:
      n = int(entry)
      if ( n < 2 or n > 10 ): raise ValueError()
      rectangles = [ rectanglesSet[j] for j in range(n) ]
    elif i == 2:
      A = int(entry)
      if (not(isinstance(A,int)) or A < 0): raise ValueError()
    elif i == 3:
      l = int(entry)
      if (not(isinstance(l,int))): raise ValueError()
    elif i == 4:
      u = int(entry)
      if (not(isinstance(u,int)) or l>=u): raise ValueError()
  except ValueError:
    print( "Error:",entry, "should be an appropriate value at position %i." % i )
    printUsage()
    sys.exit(1)
  i += 1
if i != 5:
  print( "Error: Insufficient number of arguments." )
  printUsage()
  sys.exit(1)

#
# Run formula run!
#
print formula(A,l,u)
