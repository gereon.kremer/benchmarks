(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (<= (* skoY (* skoY (+ 1. (* skoY (* skoY (+ (/ (- 1.) 3.) (* skoY (* skoY (+ (/ 2. 45.) (* skoY (* skoY (+ (/ (- 127.) 40320.) (* skoY (* skoY (+ (/ 233. 1814400.) (* skoY (* skoY (+ (/ (- 743.) 239500800.) (* skoY (* skoY (+ (/ 2. 42567525.) (* skoY (* skoY (+ (/ (- 9949.) 20922789888000.) (* skoY (* skoY (+ (/ 10889. 3201186852864000.) (* skoY (* skoY (+ (/ (- 10949.) 608225502044160000.) (* skoY (* skoY (+ (/ 79. 1080769930555392000.) (* skoY (* skoY (+ (/ (- 3163.) 13488008733331292160000.) (* skoY (* skoY (+ (/ 41. 67440043666656460800000.) (* skoY (* skoY (/ (- 1.) 809280523999877529600000.)))))))))))))))))))))))))))))))))))))))))) 0.) (and (not (<= (* skoY (* skoY (+ (/ 1. 2.) (* skoY (* skoY (+ (/ (- 1.) 24.) (* skoY (* skoY (+ (/ 1. 720.) (* skoY (* skoY (+ (/ (- 1.) 40320.) (* skoY (* skoY (+ (/ 1. 3628800.) (* skoY (* skoY (+ (/ (- 1.) 479001600.) (* skoY (* skoY (+ (/ 1. 87178291200.) (* skoY (* skoY (+ (/ (- 1.) 20922789888000.) (* skoY (* skoY (+ (/ 1. 6402373705728000.) (* skoY (* skoY (+ (/ (- 1.) 2432902008176640000.) (* skoY (* skoY (/ 1. 1124000727777607680000.))))))))))))))))))))))))))))))))) 1.)) (and (<= skoY (+ (/ (- 1.) 5.) (* pi (/ 1. 2.)))) (and (not (<= pi (/ 15707963. 5000000.))) (and (not (<= (/ 31415927. 10000000.) pi)) (and (<= (/ 1. 10.) skoX) (not (<= skoY skoX)))))))))
(set-info :status unsat)
(check-sat)

