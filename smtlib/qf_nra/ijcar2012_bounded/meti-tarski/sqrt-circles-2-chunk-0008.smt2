(set-logic QF_NRA)

(declare-fun skoD () Real)
(assert (and (<= skoD 1000) (>= skoD (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (not (<= (* skoY (+ (+ 2. (* skoD 2.)) (* skoY (- 1.)))) (+ (+ 1. (* skoD (+ 2. skoD))) (* skoX skoX)))))
(set-info :status unsat)
(check-sat)

