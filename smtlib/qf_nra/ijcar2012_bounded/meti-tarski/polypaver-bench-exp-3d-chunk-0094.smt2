(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(assert (and (not (<= (* skoZ (+ (+ (+ 17010. (* skoX (+ 5880. (* skoX (+ (/ 2835. 2.) (* skoX (+ 105. (* skoX (/ 45. 8.))))))))) (* skoY (+ (+ 5880. (* skoX (+ 2835. (* skoX (+ 315. (* skoX (/ 45. 2.))))))) (* skoY (+ (+ (/ 2835. 2.) (* skoX (+ 315. (* skoX (/ 135. 4.))))) (* skoY (+ (+ 105. (* skoX (/ 45. 2.))) (* skoY (/ 45. 8.))))))))) (* skoZ (+ (+ (+ 2940. (* skoX (+ (/ 2835. 2.) (* skoX (+ (/ 315. 2.) (* skoX (/ 45. 4.))))))) (* skoY (+ (+ (/ 2835. 2.) (* skoX (+ 315. (* skoX (/ 135. 4.))))) (* skoY (+ (+ (/ 315. 2.) (* skoX (/ 135. 4.))) (* skoY (/ 45. 4.))))))) (* skoZ (+ (+ (+ (/ 945. 2.) (* skoX (+ 105. (* skoX (/ 45. 4.))))) (* skoY (+ (+ 105. (* skoX (/ 45. 2.))) (* skoY (/ 45. 4.))))) (* skoZ (+ (+ (+ (/ 105. 4.) (* skoX (/ 45. 8.))) (* skoY (/ 45. 8.))) (* skoZ (/ 9. 8.)))))))))) (+ (+ (- 26460.) (* skoX (+ (- 17010.) (* skoX (+ (- 2940.) (* skoX (+ (/ (- 945.) 2.) (* skoX (+ (/ (- 105.) 4.) (* skoX (/ (- 9.) 8.))))))))))) (* skoY (+ (+ (- 17010.) (* skoX (+ (- 5880.) (* skoX (+ (/ (- 2835.) 2.) (* skoX (+ (- 105.) (* skoX (/ (- 45.) 8.))))))))) (* skoY (+ (+ (- 2940.) (* skoX (+ (/ (- 2835.) 2.) (* skoX (+ (/ (- 315.) 2.) (* skoX (/ (- 45.) 4.))))))) (* skoY (+ (+ (/ (- 945.) 2.) (* skoX (+ (- 105.) (* skoX (/ (- 45.) 4.))))) (* skoY (+ (+ (/ (- 105.) 4.) (* skoX (/ (- 45.) 8.))) (* skoY (/ (- 9.) 8.))))))))))))) (and (<= 0. skoX) (and (<= 0. skoY) (and (<= 0. skoZ) (and (<= skoX 1.) (and (<= skoY 1.) (and (<= skoZ 1.) (and (<= skoZ (+ (+ 2. (* skoX (- 1.))) (* skoY (- 1.)))) (<= (+ (+ 2. (* skoX (- 1.))) (* skoY (- 1.))) skoZ))))))))))
(set-info :status sat)
(check-sat)

