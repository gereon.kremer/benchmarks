(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (* skoY (* skoY (+ (- 1.) (* skoY (* skoY (+ (/ 1. 3.) (* skoY (* skoY (+ (/ (- 2.) 45.) (* skoY (* skoY (+ (/ 1. 315.) (* skoY (* skoY (+ (/ (- 2.) 14175.) (* skoY (* skoY (+ (/ 2047. 479001600.) (* skoY (* skoY (+ (/ (- 1.) 10762752.) (* skoY (* skoY (+ (/ 30827. 20922789888000.) (* skoY (* skoY (+ (/ (- 54647.) 3201186852864000.) (* skoY (* skoY (+ (/ 8441. 57926238289920000.) (* skoY (* skoY (+ (/ (- 2.) 2143861251406875.) (* skoY (* skoY (+ (/ 4943. 1079040698666503372800.) (* skoY (* skoY (+ (/ (- 2381.) 134880087333312921600000.) (* skoY (* skoY (+ (/ 529. 9711366287998530355200000.) (* skoY (* skoY (+ (/ (- 23.) 169948910039974281216000000.) (* skoY (* skoY (/ 1. 4078773840959382749184000000.)))))))))))))))))))))))))))))))))))))))))))))))) 0.)) (and (not (<= pi (/ 15707963. 5000000.))) (and (not (<= (/ 31415927. 10000000.) pi)) (not (<= skoY skoX))))))
(set-info :status sat)
(check-sat)

