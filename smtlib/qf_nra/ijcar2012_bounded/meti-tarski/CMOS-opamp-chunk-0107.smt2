(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(assert (and (not (<= (* skoX (* skoX (- 3600000000.))) 3600000000000000000000000.)) (and (not (<= (* skoY (* skoY (+ (+ 1800000000000000000000000. (* skoX (* skoX 1800000000.))) (* skoY (* skoY (+ (- 150000000000000000000000.) (* skoX (* skoX (- 150000000.))))))))) (+ 3600120000000000000000000. (* skoX (* skoX 3600120000.))))) (and (not (<= (/ 31415927. 10000000.) pi)) (not (<= pi (/ 15707963. 5000000.)))))))
(set-info :status unsat)
(check-sat)

