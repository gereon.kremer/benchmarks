(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS3 () Real)
(assert (and (<= skoS3 1000) (>= skoS3 (- 1000))))
(declare-fun skoSX () Real)
(assert (and (<= skoSX 1000) (>= skoSX (- 1000))))
(assert (and (<= (* skoX (+ (+ (* skoS3 (- 15.)) (* skoSX 3.)) (* skoX (* skoX (* skoS3 (- 8.)))))) 0.) (and (not (<= skoSX (* skoS3 (- 3.)))) (and (<= (* skoX (+ (+ (* skoS3 (- 1155.)) (* skoSX 231.)) (* skoX (* skoX (+ (+ (* skoS3 (- 1806.)) (* skoSX 238.)) (* skoX (* skoX (+ (+ (* skoS3 (/ (- 3507.) 5.)) (* skoSX (/ 231. 5.))) (* skoX (* skoX (* skoS3 (- 40.)))))))))))) 0.) (and (<= (* skoX (+ (+ (* skoS3 (/ 471. 100.)) (* skoSX (/ 157. 100.))) (* skoX (* skoS3 (- 8.))))) (+ (* skoS3 3.) skoSX)) (and (= (* skoX (* skoX (- 80.))) (+ 75. (* skoSX (* skoSX (- 1.))))) (and (= (* skoS3 skoS3) 3.) (and (not (<= skoX 0.)) (and (not (<= skoSX 0.)) (not (<= skoS3 0.)))))))))))
(set-info :status sat)
(check-sat)

