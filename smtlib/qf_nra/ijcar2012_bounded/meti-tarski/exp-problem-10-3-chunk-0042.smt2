(set-logic QF_NRA)

(declare-fun skoCP1 () Real)
(assert (and (<= skoCP1 1000) (>= skoCP1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoCM1 () Real)
(assert (and (<= skoCM1 1000) (>= skoCM1 (- 1000))))
(assert (and (not (= (* skoCP1 (* skoCP1 (* skoCP1 (* skoCP1 (* skoCP1 (* skoCP1 skoCP1)))))) 0.)) (and (not (= (* skoCP1 (* skoCP1 (* skoCP1 (* skoCP1 (* skoCP1 skoCP1))))) 0.)) (and (not (= (* skoCP1 (* skoCP1 (* skoCP1 (* skoCP1 skoCP1)))) 0.)) (and (not (= (* skoCP1 (* skoCP1 (* skoCP1 skoCP1))) 0.)) (and (not (= (* skoCP1 (* skoCP1 skoCP1)) 0.)) (and (not (= (* skoCP1 skoCP1) 0.)) (and (not (= skoCP1 0.)) (and (not (<= skoX 2.)) (and (not (<= skoCP1 0.)) (and (not (<= skoCM1 0.)) (and (not (<= skoC 0.)) (not (<= 10. skoX))))))))))))))
(set-info :status sat)
(check-sat)

