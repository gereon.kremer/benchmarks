(set-logic QF_NRA)

(declare-fun skoCM1 () Real)
(assert (and (<= skoCM1 1000) (>= skoCM1 (- 1000))))
(declare-fun skoCP1 () Real)
(assert (and (<= skoCP1 1000) (>= skoCP1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (* skoCP1 (* skoCM1 (- 2.))) 0.)) (not (<= skoX (/ 7. 5.)))))
(set-info :status sat)
(check-sat)

