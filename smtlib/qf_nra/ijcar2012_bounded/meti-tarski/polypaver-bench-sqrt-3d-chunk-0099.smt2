(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (* skoZ (* skoZ (* skoY (* skoY (- 1.))))) 0.)) (and (not (<= skoZ 1.)) (or (not (<= skoX 1.)) (or (not (<= skoY 1.)) (not (<= skoZ 1.)))))))
(set-info :status unsat)
(check-sat)

