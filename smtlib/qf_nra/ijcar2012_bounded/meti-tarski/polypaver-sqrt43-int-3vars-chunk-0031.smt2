(set-logic QF_NRA)

(declare-fun skoE () Real)
(assert (and (<= skoE 1000) (>= skoE (- 1000))))
(declare-fun skoR () Real)
(assert (and (<= skoR 1000) (>= skoR (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (not (<= (* skoX (+ (/ (- 1.) 2.) (* skoE (+ (/ (- 3.) 2.) (* skoE (+ (/ (- 3.) 2.) (* skoE (/ (- 1.) 2.)))))))) (* skoR (* skoR (+ (/ (- 1.) 2.) (* skoE (+ 1. (* skoE (/ 1. 2.))))))))))
(set-info :status sat)
(check-sat)

