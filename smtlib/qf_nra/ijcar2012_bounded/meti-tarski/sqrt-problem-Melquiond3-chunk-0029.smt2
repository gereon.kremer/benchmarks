(set-logic QF_NRA)

(declare-fun skoSXY () Real)
(assert (and (<= skoSXY 1000) (>= skoSXY (- 1000))))
(declare-fun skoT () Real)
(assert (and (<= skoT 1000) (>= skoT (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(assert (and (<= skoX (+ (/ (- 295.) 36.) (* skoT (/ 125. 18.)))) (and (= (+ (* skoSXY (- 1.)) (* skoT (* skoT skoSXY))) skoX) (and (= (+ (* skoSXY skoSXY) (* skoX (- 1.))) skoY) (and (<= skoY (/ 33. 32.)) (and (<= skoX 2.) (and (<= (/ 3. 2.) skoX) (and (<= 1. skoY) (and (<= 0. skoT) (<= 0. skoSXY))))))))))
(set-info :status sat)
(check-sat)

