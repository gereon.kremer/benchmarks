(set-logic QF_NRA)

(declare-fun skoT () Real)
(assert (and (<= skoT 1000) (>= skoT (- 1000))))
(declare-fun skoSXY () Real)
(assert (and (<= skoSXY 1000) (>= skoSXY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (not (= (+ (* skoSXY (- 1.)) (* skoT (* skoT skoSXY))) skoX)))
(set-info :status sat)
(check-sat)

