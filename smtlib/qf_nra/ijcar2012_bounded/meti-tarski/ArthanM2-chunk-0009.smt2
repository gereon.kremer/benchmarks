(set-logic QF_NRA)

(declare-fun skoM () Real)
(assert (and (<= skoM 1000) (>= skoM (- 1000))))
(declare-fun skoCOSS () Real)
(assert (and (<= skoCOSS 1000) (>= skoCOSS (- 1000))))
(declare-fun skoSINS () Real)
(assert (and (<= skoSINS 1000) (>= skoSINS (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(assert (and (not (= (* skoM skoM) 0.)) (and (not (= skoM 0.)) (and (= (* skoSINS skoSINS) (+ 1. (* skoCOSS (* skoCOSS (- 1.))))) (and (<= 2. skoS) (<= 2. skoM))))))
(set-info :status sat)
(check-sat)

