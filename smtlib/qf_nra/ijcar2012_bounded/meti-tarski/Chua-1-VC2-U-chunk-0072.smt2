(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(assert (and (not (<= (* skoC (/ 3. 13.)) skoS)) (not (<= (* skoX (+ (/ 693. 62500000.) (* skoX (/ (- 693.) 1562500000000.)))) (/ 231. 2500.)))))
(set-info :status unsat)
(check-sat)

