(set-logic QF_NRA)

(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoCB () Real)
(assert (and (<= skoCB 1000) (>= skoCB (- 1000))))
(declare-fun skoSB () Real)
(assert (and (<= skoSB 1000) (>= skoSB (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (<= skoX (/ 177. 366500000.)) (and (= (* skoSB skoSB) (+ 1. (* skoCB (* skoCB (- 1.))))) (and (= (* skoS skoS) (+ 1. (* skoC (* skoC (- 1.))))) (and (<= skoX (/ 1. 10000000.)) (<= 0. skoX))))))
(set-info :status sat)
(check-sat)

