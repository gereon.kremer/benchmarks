(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(assert (and (not (<= (* skoX (+ (/ (- 567.) 6250000.) (* skoX (/ 567. 156250000000.)))) (/ (- 189.) 250.))) (not (<= skoS (* skoC (/ 1770. 689.))))))
(set-info :status sat)
(check-sat)

