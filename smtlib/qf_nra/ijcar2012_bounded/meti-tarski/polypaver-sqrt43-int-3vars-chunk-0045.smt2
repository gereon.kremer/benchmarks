(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoE () Real)
(assert (and (<= skoE 1000) (>= skoE (- 1000))))
(declare-fun skoR () Real)
(assert (and (<= skoR 1000) (>= skoR (- 1000))))
(assert (and (not (<= (* skoX (+ (- 7.) (* skoX (+ (- 7.) (* skoX (- 1.)))))) 1.)) (and (not (<= (* skoX (+ (+ (/ (- 7.) 2.) (* skoR 8.)) (* skoX (+ (+ (/ (- 35.) 4.) (* skoR 8.)) (* skoX (+ (+ (/ (- 7.) 2.) (* skoR (/ 8. 7.))) (* skoX (/ (- 1.) 8.)))))))) (+ (/ 1. 8.) (* skoR (/ (- 8.) 7.))))) (and (not (<= skoX (+ (- 1.) (* skoR (/ 16. 7.))))) (and (<= (* skoX (+ (/ (- 1.) 2.) (* skoE (+ (/ (- 3.) 2.) (* skoE (+ (/ (- 3.) 2.) (* skoE (/ (- 1.) 2.)))))))) (* skoR (* skoR (+ (/ (- 1.) 2.) (* skoE (+ 1. (* skoE (/ 1. 2.)))))))) (and (<= (* skoX (+ (/ 1. 2.) (* skoE (+ (/ 3. 2.) (* skoE (+ (/ 3. 2.) (* skoE (/ 1. 2.)))))))) (* skoR (* skoR (+ (/ 1. 2.) (* skoE (+ (- 1.) (* skoE (/ (- 1.) 2.)))))))) (and (<= (* skoX (* skoX (/ (- 1.) 4.))) (+ 1. (* skoR (- 1.)))) (and (<= (* skoX (+ 1. (* skoX (/ (- 1.) 4.)))) skoR) (and (<= skoE (/ 1. 32.)) (and (<= (/ (- 1.) 32.) skoE) (and (<= skoX 2.) (and (<= skoR 3.) (and (<= (/ 1. 2.) skoX) (<= 0. skoR))))))))))))))
(set-info :status unsat)
(check-sat)

