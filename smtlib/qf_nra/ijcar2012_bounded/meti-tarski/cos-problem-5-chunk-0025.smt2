(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(assert (and (<= (* skoX (+ (- 1.) (* skoX (+ (- 2.) (* skoX (- 1.)))))) 0.) (and (<= (* skoX (+ 1. (* skoX (+ 2. skoX)))) 0.) (and (<= (* skoX (+ (+ 1. (* pi (* pi (/ (- 1.) 2.)))) (* skoX (+ 2. skoX)))) (* pi (* pi (/ 1. 2.)))) (and (= (* skoY skoY) 3.) (and (not (<= pi (/ 15707963. 5000000.))) (and (not (<= (/ 31415927. 10000000.) pi)) (and (<= 0. skoY) (<= skoY skoX)))))))))
(set-info :status unsat)
(check-sat)

