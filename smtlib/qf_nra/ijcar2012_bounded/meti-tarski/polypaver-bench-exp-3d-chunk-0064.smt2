(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(assert (not (<= (* skoZ (+ (+ (+ (/ 135. 2.) (* skoX (+ 21. (* skoX (/ 27. 8.))))) (* skoY (+ (+ 21. (* skoX (/ 27. 4.))) (* skoY (/ 27. 8.))))) (* skoZ (+ (+ (+ (/ 21. 2.) (* skoX (/ 27. 8.))) (* skoY (/ 27. 8.))) (* skoZ (/ 9. 8.)))))) (+ (+ (- 105.) (* skoX (+ (/ (- 135.) 2.) (* skoX (+ (/ (- 21.) 2.) (* skoX (/ (- 9.) 8.))))))) (* skoY (+ (+ (/ (- 135.) 2.) (* skoX (+ (- 21.) (* skoX (/ (- 27.) 8.))))) (* skoY (+ (+ (/ (- 21.) 2.) (* skoX (/ (- 27.) 8.))) (* skoY (/ (- 9.) 8.))))))))))
(set-info :status sat)
(check-sat)

