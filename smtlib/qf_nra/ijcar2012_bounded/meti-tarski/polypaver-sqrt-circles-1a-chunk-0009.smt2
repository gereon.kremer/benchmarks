(set-logic QF_NRA)

(declare-fun skoS1 () Real)
(assert (and (<= skoS1 1000) (>= skoS1 (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS2 () Real)
(assert (and (<= skoS2 1000) (>= skoS2 (- 1000))))
(assert (and (= (* skoY (+ 2. (* skoY (- 1.)))) (+ (+ 1. (* skoS1 (* skoS1 (- 1.)))) (* skoX skoX))) (and (<= skoS1 1.) (and (<= 0. skoS1) (not (<= skoS2 2.))))))
(set-info :status sat)
(check-sat)

