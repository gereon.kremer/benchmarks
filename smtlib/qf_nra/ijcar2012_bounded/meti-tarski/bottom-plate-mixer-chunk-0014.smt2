(set-logic QF_NRA)

(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (/ 177. 366500000.) skoX)) (not (<= skoS (+ (/ 760000. 7383.) (* skoC (/ (- 3400.) 7383.)))))))
(set-info :status sat)
(check-sat)

