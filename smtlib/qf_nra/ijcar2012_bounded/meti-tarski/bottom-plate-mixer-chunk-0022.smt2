(set-logic QF_NRA)

(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoCB () Real)
(assert (and (<= skoCB 1000) (>= skoCB (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoSB () Real)
(assert (and (<= skoSB 1000) (>= skoSB (- 1000))))
(assert (not (<= skoSB (+ (+ (+ (/ 12695. 52.) (* skoC (/ (- 570.) 13.))) (* skoCB (/ (- 49.) 65.))) (* skoS (- 200.))))))
(set-info :status sat)
(check-sat)

