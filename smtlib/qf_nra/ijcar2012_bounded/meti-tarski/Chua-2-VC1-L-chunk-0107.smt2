(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(assert (and (not (<= (* skoX (+ (/ (- 8352.) 625.) (* skoX (+ (/ (- 15138.) 390625.) (* skoX (+ (/ (- 73167.) 976562500.) (* skoX (+ (/ (- 14852901.) 156250000000000.) (* skoX (+ (/ (- 61533447.) 781250000000000000.) (* skoX (/ (- 594823321.) 15625000000000000000000.)))))))))))) 2304.)) (and (not (<= (* skoX (+ (/ 64032. 1375.) (* skoX (+ (/ 116058. 859375.) (* skoX (+ (/ 560947. 2148437500.) (* skoX (+ (/ 113872241. 343750000000000.) (* skoX (+ (/ 471756427. 1718750000000000000.) (* skoX (/ 13680936383. 103125000000000000000000.)))))))))))) (+ (+ (/ (- 88320.) 11.) (* skoC (/ (- 102400.) 11.))) (* skoS (/ 112896. 11.))))) (or (not (<= (* skoC (/ 400. 441.)) skoS)) (not (<= skoS (* skoC (/ 400. 441.))))))))
(set-info :status unsat)
(check-sat)

