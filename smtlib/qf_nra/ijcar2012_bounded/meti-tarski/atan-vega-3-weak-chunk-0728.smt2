(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(assert (and (<= (* skoY (* skoX (- 1.))) (- 1.)) (and (<= (* skoZ (+ (+ (- 19305.) (* skoX (* skoX (+ (- 36036.) (* skoX (* skoX (+ (- 20790.) (* skoX (* skoX (+ (- 3780.) (* skoX (* skoX (- 105.))))))))))))) (* skoY (+ (* skoX (+ 19305. (* skoX (* skoX (+ 36036. (* skoX (* skoX (+ 20790. (* skoX (* skoX (+ 3780. (* skoX (* skoX 105.))))))))))))) (* skoY (+ (+ (- 6435.) (* skoX (* skoX (+ (- 12012.) (* skoX (* skoX (+ (- 6930.) (* skoX (* skoX (+ (- 1260.) (* skoX (* skoX (- 35.))))))))))))) (* skoY (* skoX (+ 6435. (* skoX (* skoX (+ 12012. (* skoX (* skoX (+ 6930. (* skoX (* skoX (+ 1260. (* skoX (* skoX 35.)))))))))))))))))))) (+ (+ (/ 19305. 4.) (* skoX (* skoX (+ 9009. (* skoX (+ 6435. (* skoX (+ (/ 10395. 2.) (* skoX (+ 8151. (* skoX (+ 945. (* skoX (+ (/ 86823. 35.) (* skoX (+ (/ 105. 4.) (* skoX 105.))))))))))))))))) (* skoY (+ (* skoX (+ (/ (- 19305.) 4.) (* skoX (+ 19305. (* skoX (+ (- 9009.) (* skoX (+ 29601. (* skoX (+ (/ (- 10395.) 2.) (* skoX (+ 12639. (* skoX (+ (- 945.) (* skoX (+ (/ 45477. 35.) (* skoX (/ (- 105.) 4.)))))))))))))))))) (* skoY (+ (+ (/ 6435. 4.) (* skoX (+ 19305. (* skoX (+ 3003. (* skoX (+ 38181. (* skoX (+ (/ 3465. 2.) (* skoX (+ 23507. (* skoX (+ 315. (* skoX (+ (/ 161241. 35.) (* skoX (+ (/ 35. 4.) (* skoX 140.)))))))))))))))))) (* skoY (+ 6435. (* skoX (+ (/ (- 6435.) 4.) (* skoX (+ 18447. (* skoX (+ (- 3003.) (* skoX (+ 16797. (* skoX (+ (/ (- 3465.) 2.) (* skoX (+ 5473. (* skoX (+ (- 315.) (* skoX (+ (/ 16384. 35.) (* skoX (/ (- 35.) 4.)))))))))))))))))))))))))) (and (not (<= (* skoZ (+ (- 1.) (* skoY skoX))) (+ skoX skoY))) (and (not (<= 0. skoY)) (and (or (not (<= (* skoZ (+ (- 1.) (* skoY skoX))) (+ skoX skoY))) (<= 0. skoY)) (and (<= (* skoZ (+ 1. (* skoY (* skoX (- 1.))))) (+ (+ 1. (* skoX (- 1.))) (* skoY (+ (- 1.) (* skoX (- 1.)))))) (and (not (<= 0. skoX)) (and (or (not (<= 0. skoX)) (<= (* skoZ (+ 1. (* skoY (* skoX (- 1.))))) (+ (+ 1. (* skoX (- 1.))) (* skoY (+ (- 1.) (* skoX (- 1.))))))) (and (or (<= 0. skoY) (<= (* skoZ (+ 1. (* skoY (* skoX (- 1.))))) (+ (+ 1. (* skoX (- 1.))) (* skoY (+ (- 1.) (* skoX (- 1.))))))) (and (or (not (<= 0. skoY)) (<= (* skoZ (+ (- 1.) (* skoY skoX))) (+ skoX skoY))) (and (not (<= skoZ 0.)) (and (not (<= skoX (- 1.))) (and (not (<= 1. skoY)) (not (<= skoY skoX))))))))))))))))
(set-info :status unsat)
(check-sat)


