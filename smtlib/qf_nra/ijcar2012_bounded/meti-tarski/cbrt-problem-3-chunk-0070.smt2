(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (<= (* skoZ (+ (+ (+ 69. (* skoX 10.)) (* skoY (+ (+ 148. (* skoX 20.)) (* skoY 20.)))) (* skoZ (+ 10. (* skoY 20.))))) (+ (+ (- 32.) (* skoX (- 5.))) (* skoY (+ (+ (- 69.) (* skoX (- 10.))) (* skoY (- 10.)))))) (and (not (<= skoZ 0.)) (and (not (<= skoY 0.)) (not (<= skoX 0.))))))
(set-info :status unsat)
(check-sat)

