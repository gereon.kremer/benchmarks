(set-logic QF_NRA)
(set-info :source | kissing problem: pack 1 spheres in 2 dimensions |)
(declare-fun x_0_0 () Real)
(assert (and (<= x_0_0 1000) (>= x_0_0 (- 1000))))
(declare-fun x_0_1 () Real)
(assert (and (<= x_0_1 1000) (>= x_0_1 (- 1000))))
(assert (= (+ (* x_0_0 x_0_0) (* x_0_1 x_0_1)) 1.))
(check-sat)

