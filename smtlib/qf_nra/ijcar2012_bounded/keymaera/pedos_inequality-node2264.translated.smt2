(set-logic QF_NRA)
(declare-fun auscore0dollarsk!5 () Real)
(assert (and (<= auscore0dollarsk!5 1000) (>= auscore0dollarsk!5 (- 1000))))
(declare-fun asuscore0dollarsk!4 () Real)
(assert (and (<= asuscore0dollarsk!4 1000) (>= asuscore0dollarsk!4 (- 1000))))
(declare-fun xuscore0dollarsk!3 () Real)
(assert (and (<= xuscore0dollarsk!3 1000) (>= xuscore0dollarsk!3 (- 1000))))
(declare-fun xsuscore0dollarsk!2 () Real)
(assert (and (<= xsuscore0dollarsk!2 1000) (>= xsuscore0dollarsk!2 (- 1000))))
(declare-fun yuscore0dollarsk!1 () Real)
(assert (and (<= yuscore0dollarsk!1 1000) (>= yuscore0dollarsk!1 (- 1000))))
(declare-fun ysuscore0dollarsk!0 () Real)
(assert (and (<= ysuscore0dollarsk!0 1000) (>= ysuscore0dollarsk!0 (- 1000))))
(assert (>= auscore0dollarsk!5 0.0))
(assert (>= asuscore0dollarsk!4 0.0))
(assert (>= xuscore0dollarsk!3 0.0))
(assert (>= xsuscore0dollarsk!2 0.0))
(assert (>= yuscore0dollarsk!1 0.0))
(assert (>= ysuscore0dollarsk!0 0.0))
(assert (not (>= (+ (* auscore0dollarsk!5
                       auscore0dollarsk!5
                       xsuscore0dollarsk!2
                       xsuscore0dollarsk!2)
                    (* auscore0dollarsk!5
                       auscore0dollarsk!5
                       ysuscore0dollarsk!0
                       ysuscore0dollarsk!0)
                    (* (- 2.0)
                       asuscore0dollarsk!4
                       auscore0dollarsk!5
                       xsuscore0dollarsk!2
                       xuscore0dollarsk!3)
                    (* (- 2.0)
                       asuscore0dollarsk!4
                       auscore0dollarsk!5
                       ysuscore0dollarsk!0
                       yuscore0dollarsk!1)
                    (* asuscore0dollarsk!4
                       asuscore0dollarsk!4
                       xuscore0dollarsk!3
                       xuscore0dollarsk!3)
                    (* asuscore0dollarsk!4
                       asuscore0dollarsk!4
                       yuscore0dollarsk!1
                       yuscore0dollarsk!1))
                 0.0)))
(check-sat)

