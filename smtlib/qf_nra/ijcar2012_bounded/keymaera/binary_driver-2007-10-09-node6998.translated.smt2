(set-logic QF_NRA)
(declare-fun duscore3dollarsk!1 () Real)
(assert (and (<= duscore3dollarsk!1 1000) (>= duscore3dollarsk!1 (- 1000))))
(declare-fun muscore2dollarsk!3 () Real)
(assert (and (<= muscore2dollarsk!3 1000) (>= muscore2dollarsk!3 (- 1000))))
(declare-fun muscore3dollarsk!2 () Real)
(assert (and (<= muscore3dollarsk!2 1000) (>= muscore3dollarsk!2 (- 1000))))
(declare-fun b () Real)
(assert (and (<= b 1000) (>= b (- 1000))))
(declare-fun duscore2dollarsk!5 () Real)
(assert (and (<= duscore2dollarsk!5 1000) (>= duscore2dollarsk!5 (- 1000))))
(declare-fun vdesuscore3dollarsk!0 () Real)
(assert (and (<= vdesuscore3dollarsk!0 1000) (>= vdesuscore3dollarsk!0 (- 1000))))
(declare-fun zuscore2dollarsk!4 () Real)
(assert (and (<= zuscore2dollarsk!4 1000) (>= zuscore2dollarsk!4 (- 1000))))
(declare-fun vuscore2dollarsk!6 () Real)
(assert (and (<= vuscore2dollarsk!6 1000) (>= vuscore2dollarsk!6 (- 1000))))
(declare-fun z () Real)
(assert (and (<= z 1000) (>= z (- 1000))))
(declare-fun m () Real)
(assert (and (<= m 1000) (>= m (- 1000))))
(declare-fun d () Real)
(assert (and (<= d 1000) (>= d (- 1000))))
(declare-fun v () Real)
(assert (and (<= v 1000) (>= v (- 1000))))
(declare-fun ep () Real)
(assert (and (<= ep 1000) (>= ep (- 1000))))
(declare-fun amax () Real)
(assert (and (<= amax 1000) (>= amax (- 1000))))
(assert (>= duscore3dollarsk!1 0.0))
(assert (<= (+ (* duscore2dollarsk!5 duscore2dollarsk!5)
               (* (- 1.0) duscore3dollarsk!1 duscore3dollarsk!1))
            (* 2.0 b (+ muscore3dollarsk!2 (* (- 1.0) muscore2dollarsk!3)))))
(assert (>= vdesuscore3dollarsk!0 0.0))
(assert (<= (+ (* vuscore2dollarsk!6 vuscore2dollarsk!6)
               (* (- 1.0) duscore2dollarsk!5 duscore2dollarsk!5))
            (* 2.0 b (+ muscore2dollarsk!3 (* (- 1.0) zuscore2dollarsk!4)))))
(assert (>= vuscore2dollarsk!6 0.0))
(assert (>= duscore2dollarsk!5 0.0))
(assert (<= (+ (* v v) (* (- 1.0) d d)) (* 2.0 b (+ m (* (- 1.0) z)))))
(assert (>= v 0.0))
(assert (not (<= ep 0.0)))
(assert (not (<= b 0.0)))
(assert (not (<= amax 0.0)))
(assert (>= d 0.0))
(assert (not (<= (+ (* vuscore2dollarsk!6 vuscore2dollarsk!6)
                    (* (- 1.0) duscore3dollarsk!1 duscore3dollarsk!1))
                 (* 2.0 b (+ muscore3dollarsk!2 (* (- 1.0) zuscore2dollarsk!4))))))
(check-sat)

