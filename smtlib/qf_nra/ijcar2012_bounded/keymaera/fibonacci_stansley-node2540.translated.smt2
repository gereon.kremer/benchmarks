(set-logic QF_NRA)
(declare-fun xuscore2dollarsk!0 () Real)
(assert (and (<= xuscore2dollarsk!0 1000) (>= xuscore2dollarsk!0 (- 1000))))
(declare-fun ruscore2dollarsk!2 () Real)
(assert (and (<= ruscore2dollarsk!2 1000) (>= ruscore2dollarsk!2 (- 1000))))
(declare-fun quscore2dollarsk!1 () Real)
(assert (and (<= quscore2dollarsk!1 1000) (>= quscore2dollarsk!1 (- 1000))))
(assert (= (+ (* 8.0 quscore2dollarsk!1 quscore2dollarsk!1)
              (* ruscore2dollarsk!2 ruscore2dollarsk!2))
           (+ (* 2.0 quscore2dollarsk!1 ruscore2dollarsk!2)
              (* 4.0 xuscore2dollarsk!0))))
(assert (not (= (+ (* 8.0 ruscore2dollarsk!2 ruscore2dollarsk!2)
                   (* (+ (* 2.0 ruscore2dollarsk!2)
                         (* (- 8.0) quscore2dollarsk!1))
                      (+ (* 2.0 ruscore2dollarsk!2)
                         (* (- 8.0) quscore2dollarsk!1))))
                (+ (* 2.0
                      ruscore2dollarsk!2
                      (+ (* 2.0 ruscore2dollarsk!2)
                         (* (- 8.0) quscore2dollarsk!1)))
                   (* 32.0 xuscore2dollarsk!0)))))
(check-sat)

