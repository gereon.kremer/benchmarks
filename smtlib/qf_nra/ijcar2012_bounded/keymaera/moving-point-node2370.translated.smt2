(set-logic QF_NRA)
(declare-fun t2uscore0dollarsk!0 () Real)
(assert (and (<= t2uscore0dollarsk!0 1000) (>= t2uscore0dollarsk!0 (- 1000))))
(declare-fun c () Real)
(assert (and (<= c 1000) (>= c (- 1000))))
(declare-fun xuscore2dollarsk!1 () Real)
(assert (and (<= xuscore2dollarsk!1 1000) (>= xuscore2dollarsk!1 (- 1000))))
(declare-fun x () Real)
(assert (and (<= x 1000) (>= x (- 1000))))
(assert (or (not (>= t2uscore0dollarsk!0 0.0))
            (>= (+ c (* (- 1.0) t2uscore0dollarsk!0)) 0.0)))
(assert (>= t2uscore0dollarsk!0 0.0))
(assert (<= (* xuscore2dollarsk!1 xuscore2dollarsk!1) (* 16.0 c c)))
(assert (not (<= (* 16.0 c c) (* x x))))
(assert (<= xuscore2dollarsk!1 0.0))
(assert (not (<= (* (+ (* 4.0 t2uscore0dollarsk!0) xuscore2dollarsk!1)
                    (+ (* 4.0 t2uscore0dollarsk!0) xuscore2dollarsk!1))
                 (* 16.0 c c))))
(assert (or (and (<= c 0.0) (not (= c 0.0)))
            (>= (+ c (* (- 1.0) t2uscore0dollarsk!0)) 0.0)))
(assert (or (not (>= t2uscore0dollarsk!0 0.0)) (>= c 0.0)))
(check-sat)

