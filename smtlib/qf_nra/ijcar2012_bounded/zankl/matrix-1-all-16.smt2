(set-logic QF_NRA)
(set-info :source |
From termination analysis of term rewriting.

Submitted by Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status sat)
(declare-fun x6 () Real)
(assert (and (<= x6 1000) (>= x6 (- 1000))))
(declare-fun x23 () Real)
(assert (and (<= x23 1000) (>= x23 (- 1000))))
(declare-fun x40 () Real)
(assert (and (<= x40 1000) (>= x40 (- 1000))))
(declare-fun x13 () Real)
(assert (and (<= x13 1000) (>= x13 (- 1000))))
(declare-fun x30 () Real)
(assert (and (<= x30 1000) (>= x30 (- 1000))))
(declare-fun x47 () Real)
(assert (and (<= x47 1000) (>= x47 (- 1000))))
(declare-fun x3 () Real)
(assert (and (<= x3 1000) (>= x3 (- 1000))))
(declare-fun x20 () Real)
(assert (and (<= x20 1000) (>= x20 (- 1000))))
(declare-fun x37 () Real)
(assert (and (<= x37 1000) (>= x37 (- 1000))))
(declare-fun x10 () Real)
(assert (and (<= x10 1000) (>= x10 (- 1000))))
(declare-fun x27 () Real)
(assert (and (<= x27 1000) (>= x27 (- 1000))))
(declare-fun x44 () Real)
(assert (and (<= x44 1000) (>= x44 (- 1000))))
(declare-fun x0 () Real)
(assert (and (<= x0 1000) (>= x0 (- 1000))))
(declare-fun x17 () Real)
(assert (and (<= x17 1000) (>= x17 (- 1000))))
(declare-fun x34 () Real)
(assert (and (<= x34 1000) (>= x34 (- 1000))))
(declare-fun x7 () Real)
(assert (and (<= x7 1000) (>= x7 (- 1000))))
(declare-fun x24 () Real)
(assert (and (<= x24 1000) (>= x24 (- 1000))))
(declare-fun x41 () Real)
(assert (and (<= x41 1000) (>= x41 (- 1000))))
(declare-fun x14 () Real)
(assert (and (<= x14 1000) (>= x14 (- 1000))))
(declare-fun x31 () Real)
(assert (and (<= x31 1000) (>= x31 (- 1000))))
(declare-fun x48 () Real)
(assert (and (<= x48 1000) (>= x48 (- 1000))))
(declare-fun x4 () Real)
(assert (and (<= x4 1000) (>= x4 (- 1000))))
(declare-fun x21 () Real)
(assert (and (<= x21 1000) (>= x21 (- 1000))))
(declare-fun x38 () Real)
(assert (and (<= x38 1000) (>= x38 (- 1000))))
(declare-fun x11 () Real)
(assert (and (<= x11 1000) (>= x11 (- 1000))))
(declare-fun x28 () Real)
(assert (and (<= x28 1000) (>= x28 (- 1000))))
(declare-fun x45 () Real)
(assert (and (<= x45 1000) (>= x45 (- 1000))))
(declare-fun x1 () Real)
(assert (and (<= x1 1000) (>= x1 (- 1000))))
(declare-fun x18 () Real)
(assert (and (<= x18 1000) (>= x18 (- 1000))))
(declare-fun x35 () Real)
(assert (and (<= x35 1000) (>= x35 (- 1000))))
(declare-fun x8 () Real)
(assert (and (<= x8 1000) (>= x8 (- 1000))))
(declare-fun x25 () Real)
(assert (and (<= x25 1000) (>= x25 (- 1000))))
(declare-fun x42 () Real)
(assert (and (<= x42 1000) (>= x42 (- 1000))))
(declare-fun x15 () Real)
(assert (and (<= x15 1000) (>= x15 (- 1000))))
(declare-fun x32 () Real)
(assert (and (<= x32 1000) (>= x32 (- 1000))))
(declare-fun x49 () Real)
(assert (and (<= x49 1000) (>= x49 (- 1000))))
(declare-fun x5 () Real)
(assert (and (<= x5 1000) (>= x5 (- 1000))))
(declare-fun x22 () Real)
(assert (and (<= x22 1000) (>= x22 (- 1000))))
(declare-fun x39 () Real)
(assert (and (<= x39 1000) (>= x39 (- 1000))))
(declare-fun x12 () Real)
(assert (and (<= x12 1000) (>= x12 (- 1000))))
(declare-fun x29 () Real)
(assert (and (<= x29 1000) (>= x29 (- 1000))))
(declare-fun x46 () Real)
(assert (and (<= x46 1000) (>= x46 (- 1000))))
(declare-fun x2 () Real)
(assert (and (<= x2 1000) (>= x2 (- 1000))))
(declare-fun x19 () Real)
(assert (and (<= x19 1000) (>= x19 (- 1000))))
(declare-fun x36 () Real)
(assert (and (<= x36 1000) (>= x36 (- 1000))))
(declare-fun x9 () Real)
(assert (and (<= x9 1000) (>= x9 (- 1000))))
(declare-fun x26 () Real)
(assert (and (<= x26 1000) (>= x26 (- 1000))))
(declare-fun x43 () Real)
(assert (and (<= x43 1000) (>= x43 (- 1000))))
(declare-fun x16 () Real)
(assert (and (<= x16 1000) (>= x16 (- 1000))))
(declare-fun x33 () Real)
(assert (and (<= x33 1000) (>= x33 (- 1000))))
(assert (>= x6 0))
(assert (>= x23 0))
(assert (>= x40 0))
(assert (>= x13 0))
(assert (>= x30 0))
(assert (>= x47 0))
(assert (>= x3 0))
(assert (>= x20 0))
(assert (>= x37 0))
(assert (>= x10 0))
(assert (>= x27 0))
(assert (>= x44 0))
(assert (>= x0 0))
(assert (>= x17 0))
(assert (>= x34 0))
(assert (>= x7 0))
(assert (>= x24 0))
(assert (>= x41 0))
(assert (>= x14 0))
(assert (>= x31 0))
(assert (>= x48 0))
(assert (>= x4 0))
(assert (>= x21 0))
(assert (>= x38 0))
(assert (>= x11 0))
(assert (>= x28 0))
(assert (>= x45 0))
(assert (>= x1 0))
(assert (>= x18 0))
(assert (>= x35 0))
(assert (>= x8 0))
(assert (>= x25 0))
(assert (>= x42 0))
(assert (>= x15 0))
(assert (>= x32 0))
(assert (>= x49 0))
(assert (>= x5 0))
(assert (>= x22 0))
(assert (>= x39 0))
(assert (>= x12 0))
(assert (>= x29 0))
(assert (>= x46 0))
(assert (>= x2 0))
(assert (>= x19 0))
(assert (>= x36 0))
(assert (>= x9 0))
(assert (>= x26 0))
(assert (>= x43 0))
(assert (>= x16 0))
(assert (>= x33 0))
(assert (let ((?v_0 (+ (+ x0 (* x1 x2)) (* x4 x2))) (?v_1 (+ x5 (* x7 x2))) (?v_3 (+ x2 (* x3 x2)))) (let ((?v_2 (+ x8 (* x9 ?v_3))) (?v_5 (* x3 x3)) (?v_4 (+ x10 (* x11 ?v_3))) (?v_26 (+ x2 (* x3 x19)))) (let ((?v_6 (+ x15 (* x18 ?v_26))) (?v_8 (+ x10 (* x11 x2))) (?v_7 (+ x15 (* x17 x2))) (?v_10 (* x17 x3))) (let ((?v_11 (and (> ?v_7 x5) (>= ?v_7 x5))) (?v_14 (+ x23 (* x24 x2)))) (let ((?v_9 (+ (+ x15 (* x16 x20)) (* x17 ?v_14))) (?v_16 (+ x21 x22)) (?v_17 (* x24 x3)) (?v_12 (+ x15 (* x18 x20))) (?v_13 (+ x8 (* x9 x2))) (?v_35 (+ x27 (* x28 x2))) (?v_36 (+ x30 (* x33 x20))) (?v_37 (+ (+ x30 (* x31 x20)) (* x32 ?v_14)))) (let ((?v_15 (+ (+ (+ x25 (* x26 ?v_35)) (* x29 ?v_36)) (* x34 ?v_37))) (?v_40 (+ x31 (* x33 x21))) (?v_41 (* x31 ?v_16)) (?v_42 (* x28 x3)) (?v_43 (* x32 ?v_17)) (?v_44 (* x33 x22))) (let ((?v_45 (and (and (and (and (and (and (and (and (and (and (and (and (and (and (> ?v_0 x0) (>= ?v_0 x0)) (>= (* x1 x3) x1)) (>= (* x4 x3) x4)) (and (and (> ?v_1 x5) (>= ?v_1 x5)) (>= (* x7 x3) x7))) (and (and (> ?v_2 x8) (>= ?v_2 x8)) (>= (* x9 ?v_5) x9))) (and (and (> ?v_4 x10) (>= ?v_4 x10)) (>= (* x11 ?v_5) x11))) (and (and (and (> x12 ?v_6) (>= x12 ?v_6)) (>= x13 x16)) (>= x14 x17))) (and (and (> ?v_7 ?v_8) (>= ?v_7 ?v_8)) (>= ?v_10 (* x11 x3)))) (and ?v_11 (>= x16 (+ x6 x7)))) (and (and (and (> ?v_7 ?v_9) (>= ?v_7 ?v_9)) (>= x16 (* x16 ?v_16))) (>= ?v_10 (* x17 ?v_17)))) (and (and ?v_11 (>= x16 x6)) (>= x18 x7))) (and (and (and (and (> ?v_7 ?v_12) (>= ?v_7 ?v_12)) (>= x16 (+ x16 (* x18 x21)))) (>= ?v_10 x17)) (>= x18 (* x18 x22)))) (and (and (> ?v_7 ?v_13) (>= ?v_7 ?v_13)) (>= ?v_10 (* x9 x3)))) (and (and (and (and (> ?v_7 ?v_15) (>= ?v_7 ?v_15)) (>= x16 (+ (* x29 ?v_40) (* x34 ?v_41)))) (>= ?v_10 (+ (+ (* x26 ?v_42) (* x29 x32)) (* x34 ?v_43)))) (>= x18 (+ (* x29 ?v_44) (* x34 x33)))))) (?v_18 (+ x35 (* x37 x19))) (?v_19 (+ (+ x35 (* x36 x2)) (* x37 x2))) (?v_20 (+ x20 (* x22 x19))) (?v_22 (+ x38 (* x39 x20))) (?v_21 (+ x20 (* x22 x2))) (?v_23 (+ x41 (* x42 x43))) (?v_24 (+ x41 (* x42 x46))) (?v_25 (+ x27 (* x28 x19))) (?v_27 (+ x27 (* x28 ?v_26))) (?v_28 (+ x27 (* x28 ?v_3))) (?v_29 (+ x23 (* x24 x19))) (?v_30 (+ x23 (* x24 ?v_26))) (?v_32 (+ x2 (* x3 x23))) (?v_31 (+ x23 (* x24 ?v_3))) (?v_33 (+ x30 (* x33 ?v_26))) (?v_34 (+ x30 (* x32 x19))) (?v_39 (+ (+ (+ x41 (* x42 ?v_35)) (* x44 ?v_36)) (* x45 ?v_37))) (?v_38 (+ x30 (* x32 x2)))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and ?v_45 (and (and (> ?v_18 0) (>= ?v_18 0)) (>= x36 1))) (and (and (and (> ?v_19 x35) (>= ?v_19 x35)) (>= (* x36 x3) x36)) (>= (* x37 x3) x37))) (and (> ?v_20 x19) (>= ?v_20 x19))) (and (and (and (> ?v_21 ?v_22) (>= ?v_21 ?v_22)) (>= x21 (+ (* x39 x21) x40))) (>= (* x22 x3) (* x39 x22)))) (and (and (> ?v_23 0) (>= ?v_23 0)) (>= x44 1))) (and (and (> ?v_24 0) (>= ?v_24 0)) (>= x45 1))) (and (> ?v_25 x46) (>= ?v_25 x46))) (and (> ?v_27 x43) (>= ?v_27 x43))) (and (and (> ?v_28 x27) (>= ?v_28 x27)) (>= (* x28 ?v_5) x28))) (and (> ?v_29 x19) (>= ?v_29 x19))) (and (> ?v_30 x19) (>= ?v_30 x19))) (and (and (> ?v_31 ?v_32) (>= ?v_31 ?v_32)) (>= (* x24 ?v_5) (* x3 x24)))) (and (> ?v_23 x43) (>= ?v_23 x43))) (and (> ?v_24 x46) (>= ?v_24 x46))) (and (and (and (> x47 ?v_33) (>= x47 ?v_33)) (>= x48 x31)) (>= x49 x32))) (and (and (> ?v_34 0) (>= ?v_34 0)) (>= x33 1))) (and (and (and (and (> ?v_38 ?v_39) (>= ?v_38 ?v_39)) (>= x31 (+ (* x44 ?v_40) (* x45 ?v_41)))) (>= (* x32 x3) (+ (+ (* x42 ?v_42) (* x44 x32)) (* x45 ?v_43)))) (>= x33 (+ (* x44 ?v_44) (* x45 x33))))) ?v_45)))))))))
(check-sat)
(exit)

