(set-logic QF_NRA)
(set-info :source |
From termination analysis of term rewriting.

Submitted by Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status unknown)
(declare-fun x6 () Real)
(assert (and (<= x6 1000) (>= x6 (- 1000))))
(declare-fun x3 () Real)
(assert (and (<= x3 1000) (>= x3 (- 1000))))
(declare-fun x10 () Real)
(assert (and (<= x10 1000) (>= x10 (- 1000))))
(declare-fun x0 () Real)
(assert (and (<= x0 1000) (>= x0 (- 1000))))
(declare-fun x7 () Real)
(assert (and (<= x7 1000) (>= x7 (- 1000))))
(declare-fun x4 () Real)
(assert (and (<= x4 1000) (>= x4 (- 1000))))
(declare-fun x1 () Real)
(assert (and (<= x1 1000) (>= x1 (- 1000))))
(declare-fun x8 () Real)
(assert (and (<= x8 1000) (>= x8 (- 1000))))
(declare-fun x5 () Real)
(assert (and (<= x5 1000) (>= x5 (- 1000))))
(declare-fun x2 () Real)
(assert (and (<= x2 1000) (>= x2 (- 1000))))
(declare-fun x9 () Real)
(assert (and (<= x9 1000) (>= x9 (- 1000))))
(assert (>= x6 0))
(assert (>= x3 0))
(assert (>= x10 0))
(assert (>= x0 0))
(assert (>= x7 0))
(assert (>= x4 0))
(assert (>= x1 0))
(assert (>= x8 0))
(assert (>= x5 0))
(assert (>= x2 0))
(assert (>= x9 0))
(assert (let ((?v_0 (+ x0 (* x1 x2))) (?v_1 (+ x5 (* x6 x2))) (?v_3 (* x6 x4)) (?v_2 (+ x5 (* x7 x8))) (?v_18 (+ x2 (* x4 x8)))) (let ((?v_4 (+ x5 (* x7 ?v_18))) (?v_21 (* x4 x9)) (?v_6 (* x7 x3)) (?v_5 (+ x5 (* x6 x10)))) (let ((?v_11 (>= ?v_6 x1)) (?v_27 (+ x2 (* x3 x8)))) (let ((?v_26 (+ x2 (* x4 ?v_27)))) (let ((?v_7 (+ x5 (* x7 ?v_26))) (?v_29 (* x4 x4))) (let ((?v_9 (* x7 ?v_29))) (let ((?v_13 (>= ?v_9 x1)) (?v_8 (+ x5 (* x6 x8))) (?v_14 (>= ?v_6 x7)) (?v_15 (>= ?v_9 (* x6 x9)))) (let ((?v_10 (+ ?v_8 (* x7 x2))) (?v_30 (* x4 x2))) (let ((?v_33 (+ x2 ?v_30))) (let ((?v_12 (+ ?v_8 (* x7 ?v_33)))) (let ((?v_37 (and (and (and (and (and (and (and (and (and (and (and (and (and (> ?v_0 x5) (>= ?v_0 x5)) (>= (* x1 x3) x7)) (>= (* x1 x4) x6)) (and (and (> ?v_1 x0) (>= ?v_1 x0)) (>= ?v_3 x1))) (and (and (and (> ?v_1 ?v_2) (>= ?v_1 ?v_2)) (>= ?v_3 (* x7 x9))) (>= x7 x6))) (and (and (and (and (> ?v_1 ?v_4) (>= ?v_1 ?v_4)) (>= (* x6 x3) x6)) (>= ?v_3 (* x7 ?v_21))) (>= x7 ?v_6))) (and (and (> ?v_5 x0) (>= ?v_5 x0)) (>= x7 x1))) (and (and (> ?v_4 x0) (>= ?v_4 x0)) ?v_11)) (and (and (> ?v_7 x0) (>= ?v_7 x0)) ?v_13)) (and (and (and (> ?v_7 ?v_8) (>= ?v_7 ?v_8)) ?v_14) ?v_15)) (and (and (> ?v_10 x0) (>= ?v_10 x0)) ?v_11)) (and (and (> ?v_12 x0) (>= ?v_12 x0)) ?v_13)) (and (and (and (> ?v_12 ?v_8) (>= ?v_12 ?v_8)) ?v_14) ?v_15))) (?v_16 (+ x2 (* x4 x10))) (?v_17 (+ x8 (* x9 x2))) (?v_20 (+ x2 (* x4 ?v_18))) (?v_19 (+ x2 (* x3 x2))) (?v_25 (* x4 x3)) (?v_22 (+ x2 (* x3 x10))) (?v_23 (+ x8 (* x9 x8))) (?v_24 (+ x8 (* x9 x10)))) (let ((?v_32 (>= ?v_25 x9)) (?v_28 (+ x2 (* x4 ?v_26))) (?v_35 (>= ?v_25 x4)) (?v_36 (>= (* x4 ?v_29) (* x3 x9))) (?v_31 (+ ?v_27 ?v_30)) (?v_34 (+ ?v_27 (* x4 ?v_33)))) (and (and (and (and (and (and (and (and (and (and (and (and ?v_37 (and (> x2 x10) (>= x2 x10))) (and (and (> ?v_16 0) (>= ?v_16 0)) (>= x3 1))) (and (and (and (> ?v_17 x2) (>= ?v_17 x2)) (>= (* x9 x3) x4)) (>= (* x9 x4) x3))) (and (and (and (and (> ?v_19 ?v_20) (>= ?v_19 ?v_20)) (>= (* x3 x3) x3)) (>= (* x3 x4) (* x4 ?v_21))) (>= x4 ?v_25))) (and (and (> ?v_22 x8) (>= ?v_22 x8)) (>= x4 x9))) (and (and (> ?v_23 0) (>= ?v_23 0)) (>= (* x9 x9) 1))) (and (> ?v_24 x10) (>= ?v_24 x10))) (and (and (> ?v_20 x8) (>= ?v_20 x8)) ?v_32)) (and (and (and (> ?v_28 ?v_27) (>= ?v_28 ?v_27)) ?v_35) ?v_36)) (and (and (> ?v_31 x8) (>= ?v_31 x8)) ?v_32)) (and (and (and (> ?v_34 ?v_27) (>= ?v_34 ?v_27)) ?v_35) ?v_36)) ?v_37))))))))))))))
(check-sat)
(exit)

