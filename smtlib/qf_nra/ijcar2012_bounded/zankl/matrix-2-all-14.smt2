(set-logic QF_NRA)
(set-info :source |
From termination analysis of term rewriting.

Submitted by Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status unknown)
(declare-fun x6 () Real)
(assert (and (<= x6 1000) (>= x6 (- 1000))))
(declare-fun x23 () Real)
(assert (and (<= x23 1000) (>= x23 (- 1000))))
(declare-fun x40 () Real)
(assert (and (<= x40 1000) (>= x40 (- 1000))))
(declare-fun x13 () Real)
(assert (and (<= x13 1000) (>= x13 (- 1000))))
(declare-fun x30 () Real)
(assert (and (<= x30 1000) (>= x30 (- 1000))))
(declare-fun x3 () Real)
(assert (and (<= x3 1000) (>= x3 (- 1000))))
(declare-fun x20 () Real)
(assert (and (<= x20 1000) (>= x20 (- 1000))))
(declare-fun x10 () Real)
(assert (and (<= x10 1000) (>= x10 (- 1000))))
(declare-fun x27 () Real)
(assert (and (<= x27 1000) (>= x27 (- 1000))))
(declare-fun x0 () Real)
(assert (and (<= x0 1000) (>= x0 (- 1000))))
(declare-fun x17 () Real)
(assert (and (<= x17 1000) (>= x17 (- 1000))))
(declare-fun x34 () Real)
(assert (and (<= x34 1000) (>= x34 (- 1000))))
(declare-fun x7 () Real)
(assert (and (<= x7 1000) (>= x7 (- 1000))))
(declare-fun x24 () Real)
(assert (and (<= x24 1000) (>= x24 (- 1000))))
(declare-fun x41 () Real)
(assert (and (<= x41 1000) (>= x41 (- 1000))))
(declare-fun x14 () Real)
(assert (and (<= x14 1000) (>= x14 (- 1000))))
(declare-fun x31 () Real)
(assert (and (<= x31 1000) (>= x31 (- 1000))))
(declare-fun x4 () Real)
(assert (and (<= x4 1000) (>= x4 (- 1000))))
(declare-fun x21 () Real)
(assert (and (<= x21 1000) (>= x21 (- 1000))))
(declare-fun x11 () Real)
(assert (and (<= x11 1000) (>= x11 (- 1000))))
(declare-fun x28 () Real)
(assert (and (<= x28 1000) (>= x28 (- 1000))))
(declare-fun x1 () Real)
(assert (and (<= x1 1000) (>= x1 (- 1000))))
(declare-fun x18 () Real)
(assert (and (<= x18 1000) (>= x18 (- 1000))))
(declare-fun x35 () Real)
(assert (and (<= x35 1000) (>= x35 (- 1000))))
(declare-fun x8 () Real)
(assert (and (<= x8 1000) (>= x8 (- 1000))))
(declare-fun x25 () Real)
(assert (and (<= x25 1000) (>= x25 (- 1000))))
(declare-fun x42 () Real)
(assert (and (<= x42 1000) (>= x42 (- 1000))))
(declare-fun x15 () Real)
(assert (and (<= x15 1000) (>= x15 (- 1000))))
(declare-fun x32 () Real)
(assert (and (<= x32 1000) (>= x32 (- 1000))))
(declare-fun x5 () Real)
(assert (and (<= x5 1000) (>= x5 (- 1000))))
(declare-fun x22 () Real)
(assert (and (<= x22 1000) (>= x22 (- 1000))))
(declare-fun x12 () Real)
(assert (and (<= x12 1000) (>= x12 (- 1000))))
(declare-fun x29 () Real)
(assert (and (<= x29 1000) (>= x29 (- 1000))))
(declare-fun x2 () Real)
(assert (and (<= x2 1000) (>= x2 (- 1000))))
(declare-fun x19 () Real)
(assert (and (<= x19 1000) (>= x19 (- 1000))))
(declare-fun x9 () Real)
(assert (and (<= x9 1000) (>= x9 (- 1000))))
(declare-fun x26 () Real)
(assert (and (<= x26 1000) (>= x26 (- 1000))))
(declare-fun x43 () Real)
(assert (and (<= x43 1000) (>= x43 (- 1000))))
(declare-fun x16 () Real)
(assert (and (<= x16 1000) (>= x16 (- 1000))))
(declare-fun x33 () Real)
(assert (and (<= x33 1000) (>= x33 (- 1000))))
(assert (>= x6 0))
(assert (>= x23 0))
(assert (>= x40 0))
(assert (>= x13 0))
(assert (>= x30 0))
(assert (>= x3 0))
(assert (>= x20 0))
(assert (>= x10 0))
(assert (>= x27 0))
(assert (>= x0 0))
(assert (>= x17 0))
(assert (>= x34 0))
(assert (>= x7 0))
(assert (>= x24 0))
(assert (>= x41 0))
(assert (>= x14 0))
(assert (>= x31 0))
(assert (>= x4 0))
(assert (>= x21 0))
(assert (>= x11 0))
(assert (>= x28 0))
(assert (>= x1 0))
(assert (>= x18 0))
(assert (>= x35 0))
(assert (>= x8 0))
(assert (>= x25 0))
(assert (>= x42 0))
(assert (>= x15 0))
(assert (>= x32 0))
(assert (>= x5 0))
(assert (>= x22 0))
(assert (>= x12 0))
(assert (>= x29 0))
(assert (>= x2 0))
(assert (>= x19 0))
(assert (>= x9 0))
(assert (>= x26 0))
(assert (>= x43 0))
(assert (>= x16 0))
(assert (>= x33 0))
(assert (let ((?v_0 (+ (+ x0 (+ (* x1 x3) (* x2 x4))) (+ (* x5 x7) (* x6 x8)))) (?v_1 (+ (+ x0 (+ (* x1 x14) (* x2 x15))) (+ (* x5 x14) (* x6 x15))))) (let ((?v_6 (and (and (and (> ?v_0 x11) (>= ?v_0 x11)) (and (>= x9 x12) (>= x10 x13))) (and (and (> ?v_0 ?v_1) (>= ?v_0 ?v_1)) (and (>= x9 (+ (+ (+ (* x1 x16) (* x2 x18)) (+ (* x5 x16) (* x6 x18))) x9)) (>= x10 (+ (+ (+ (* x1 x17) (* x2 x19)) (+ (* x5 x17) (* x6 x19))) x10)))))) (?v_3 (+ (+ x20 (+ (* x22 x14) (* x23 x15))) (+ (* x26 x14) (* x27 x15)))) (?v_2 (+ (+ x20 (+ (* x22 x3) (* x23 x4))) (+ (* x26 x7) (* x27 x8)))) (?v_4 (+ x14 (+ (* x16 x3) (* x17 x4)))) (?v_5 (+ x14 (+ (* x16 x34) (* x17 x35))))) (and (and (and (and ?v_6 (and (and (> ?v_2 ?v_3) (and (>= ?v_2 ?v_3) (>= (+ (+ x21 (+ (* x24 x3) (* x25 x4))) (+ (* x28 x7) (* x29 x8))) (+ (+ x21 (+ (* x24 x14) (* x25 x15))) (+ (* x28 x14) (* x29 x15)))))) (and (and (and (>= x30 (+ (+ (+ (* x22 x16) (* x23 x18)) (+ (* x26 x16) (* x27 x18))) x30)) (>= x31 (+ (+ (+ (* x22 x17) (* x23 x19)) (+ (* x26 x17) (* x27 x19))) x31))) (>= x32 (+ (+ (+ (* x24 x16) (* x25 x18)) (+ (* x28 x16) (* x29 x18))) x32))) (>= x33 (+ (+ (+ (* x24 x17) (* x25 x19)) (+ (* x28 x17) (* x29 x19))) x33))))) (and (> ?v_4 x3) (and (>= ?v_4 x3) (>= (+ x15 (+ (* x18 x3) (* x19 x4))) x4)))) (and (and (> ?v_5 0) (and (>= ?v_5 0) (>= (+ x15 (+ (* x18 x34) (* x19 x35))) 0))) (and (and (and (>= (+ (* x16 x40) (* x17 x42)) 1) (>= (+ (* x16 x41) (* x17 x43)) 0)) (>= (+ (* x18 x40) (* x19 x42)) 0)) (>= (+ (* x18 x41) (* x19 x43)) 1)))) ?v_6))))
(check-sat)
(exit)

