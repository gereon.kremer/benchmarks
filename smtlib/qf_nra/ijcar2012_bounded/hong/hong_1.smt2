(set-logic QF_NRA)
(set-info :source | hong's problem \sum x_i^2 < 1 and \prod x_i > 1 |)
(declare-fun x_0 () Real)
(assert (and (<= x_0 1000) (>= x_0 (- 1000))))
(assert (< (* x_0 x_0) 1.))
(assert (> x_0 1.))
(check-sat)

