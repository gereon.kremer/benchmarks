(set-logic QF_NRA)

(declare-fun skoSP1 () Real)
(assert (and (<= skoSP1 1000) (>= skoSP1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoSM1 () Real)
(assert (and (<= skoSM1 1000) (>= skoSM1 (- 1000))))
(assert (and (not (<= (* skoSP1 (+ (- 6.) (* skoSP1 (- 12.)))) 1.)) (and (not (<= skoX 1.)) (and (not (<= skoSP1 0.)) (and (not (<= skoSM1 0.)) (and (not (<= skoS 0.)) (not (<= 5. skoX))))))))
(set-info :status unsat)
(check-sat)

