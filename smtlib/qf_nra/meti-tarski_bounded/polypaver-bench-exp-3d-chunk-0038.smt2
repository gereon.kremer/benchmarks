(set-logic QF_NRA)

(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (+ (+ 2. (* skoX (- 1.))) (* skoY (- 1.))) skoZ)) (not (<= skoZ (+ (+ (/ 3. 2.) (* skoX (- 1.))) (* skoY (- 1.)))))))
(set-info :status sat)
(check-sat)

