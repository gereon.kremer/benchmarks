(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSQ3 () Real)
(assert (and (<= skoSQ3 1000) (>= skoSQ3 (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(assert (and (not (<= (* skoSQ3 (* skoSQ3 (/ 1. 3.))) 0.)) (and (not (<= (* skoX (* skoX (+ (+ (/ 1. 2.) (* skoSQ3 (* skoSQ3 (/ (- 1.) 6.)))) (* skoX (* skoX (* skoSQ3 (* skoSQ3 (/ 1. 72.)))))))) (+ 3. (* skoSQ3 (* skoSQ3 (- 1.)))))) (and (not (<= skoSQ3 0.)) (and (not (<= skoX 0.)) (and (not (<= (/ 31415927. 10000000.) pi)) (and (not (<= pi (/ 15707963. 5000000.))) (not (<= (+ (/ (- 1.) 10000000.) (* pi (/ 1. 2.))) skoX)))))))))
(set-info :status sat)
(check-sat)

