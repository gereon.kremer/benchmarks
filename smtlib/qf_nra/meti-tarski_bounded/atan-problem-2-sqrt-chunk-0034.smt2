(set-logic QF_NRA)

(declare-fun skoT () Real)
(assert (and (<= skoT 1000) (>= skoT (- 1000))))
(declare-fun skoB () Real)
(assert (and (<= skoB 1000) (>= skoB (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(assert (and (not (<= (* skoT (* skoT (/ 1. 2.))) (+ skoA (* skoB (- 1.))))) (and (not (<= (* skoT (* skoT (+ skoA (* skoB (- 1.))))) 0.)) (and (not (<= skoB skoA)) (and (not (<= 2. skoB)) (and (not (<= skoA 0.)) (not (= skoT 0.))))))))
(set-info :status unsat)
(check-sat)

