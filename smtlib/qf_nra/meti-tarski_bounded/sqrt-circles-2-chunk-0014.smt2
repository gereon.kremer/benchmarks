(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoD () Real)
(assert (and (<= skoD 1000) (>= skoD (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (- 1.) skoD)) (not (<= (* skoY (+ (+ (- 1.) (* skoD (- 1.))) (* skoY (/ 1. 2.)))) (+ (* skoD (* skoD (/ (- 1.) 2.))) (* skoX (* skoX (/ (- 1.) 2.))))))))
(set-info :status sat)
(check-sat)

