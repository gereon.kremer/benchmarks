(set-logic QF_NRA)

(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (+ (* skoX (/ 127. 860.)) (* skoY (/ 493. 17200.))) skoZ)) (and (not (<= (* skoZ (+ (+ (* skoX (/ 213. 1000.)) (* skoY (/ 413. 10000.))) (* skoZ (/ (- 18.) 25.)))) (+ (+ (/ (- 1.) 10.) (* skoX (* skoX (/ 261. 100.)))) (* skoY (+ (* skoX (/ 21. 20.)) (* skoY (/ 141. 100.))))))) (or (not (= skoX 0.)) (or (not (= skoY 0.)) (not (= skoZ 0.)))))))
(set-info :status sat)
(check-sat)

