(set-logic QF_NRA)

(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoCM1 () Real)
(assert (and (<= skoCM1 1000) (>= skoCM1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (= (* skoC (* skoC (* skoC (* skoC (* skoC (* skoC (* skoC (* skoC skoC)))))))) 0.)) (and (not (= (* skoC (* skoC (* skoC (* skoC (* skoC (* skoC (* skoC skoC))))))) 0.)) (and (not (= (* skoC (* skoC (* skoC (* skoC (* skoC (* skoC skoC)))))) 0.)) (and (not (= (* skoC (* skoC (* skoC (* skoC (* skoC skoC))))) 0.)) (and (not (= (* skoC (* skoC (* skoC (* skoC skoC)))) 0.)) (and (not (= (* skoC (* skoC (* skoC skoC))) 0.)) (and (not (= (* skoC (* skoC skoC)) 0.)) (and (not (= (* skoC skoC) 0.)) (and (not (= skoC 0.)) (and (not (<= skoX 1.)) (and (not (<= skoCM1 0.)) (not (<= skoC 0.))))))))))))))
(set-info :status sat)
(check-sat)

