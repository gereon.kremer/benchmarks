(set-logic QF_NRA)

(declare-fun skoSX () Real)
(assert (and (<= skoSX 1000) (>= skoSX (- 1000))))
(declare-fun skoSMX () Real)
(assert (and (<= skoSMX 1000) (>= skoSMX (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (<= (+ (- 4.) (* skoSMX (- 1.))) skoSX) (and (= skoX (+ 1. (* skoSMX (* skoSMX (- 1.))))) (and (= (+ (- 1.) (* skoSX skoSX)) skoX) (and (<= skoX 1.) (and (<= 0. skoSX) (and (<= 0. skoSMX) (not (<= skoX 0.)))))))))
(set-info :status sat)
(check-sat)

