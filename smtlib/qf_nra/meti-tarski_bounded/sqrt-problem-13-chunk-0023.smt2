(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSS () Real)
(assert (and (<= skoSS 1000) (>= skoSS (- 1000))))
(declare-fun skoSP () Real)
(assert (and (<= skoSP 1000) (>= skoSP (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(assert (and (not (<= (* skoX (+ (+ (- 4.) (* skoSM (- 1.))) (* skoSP (- 1.)))) (+ (+ (* skoSM (/ 957. 250.)) (* skoSP (/ (- 957.) 250.))) (* skoSS (+ (* skoSM (/ 957. 500.)) (* skoSP (/ (- 957.) 500.))))))) (and (not (<= skoSS (- 2.))) (not (<= 1. skoX)))))
(set-info :status sat)
(check-sat)

