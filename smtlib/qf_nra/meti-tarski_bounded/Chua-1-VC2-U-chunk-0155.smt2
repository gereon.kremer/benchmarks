(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(assert (and (not (<= (/ (- 3000.) 19.) skoX)) (and (not (<= skoX 0.)) (not (<= skoS (* skoC (/ 3. 13.)))))))
(set-info :status unsat)
(check-sat)

