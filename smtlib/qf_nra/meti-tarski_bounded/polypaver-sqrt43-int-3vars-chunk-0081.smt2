(set-logic QF_NRA)

(declare-fun skoEC1 () Real)
(assert (and (<= skoEC1 1000) (>= skoEC1 (- 1000))))
(declare-fun skoRC1 () Real)
(assert (and (<= skoRC1 1000) (>= skoRC1 (- 1000))))
(declare-fun skoXC1 () Real)
(assert (and (<= skoXC1 1000) (>= skoXC1 (- 1000))))
(assert (not (<= (* skoXC1 (+ (/ (- 1.) 2.) (* skoEC1 (+ (/ (- 3.) 2.) (* skoEC1 (+ (/ (- 3.) 2.) (* skoEC1 (/ (- 1.) 2.)))))))) (* skoRC1 (* skoRC1 (+ (/ (- 1.) 2.) (* skoEC1 (+ 1. (* skoEC1 (/ 1. 2.))))))))))
(set-info :status sat)
(check-sat)

