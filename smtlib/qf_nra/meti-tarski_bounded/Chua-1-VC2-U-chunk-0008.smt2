(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(assert (and (<= skoS (* skoC (/ 3. 13.))) (and (not (<= (* skoC (/ 3. 13.)) skoS)) (and (<= skoX 289.) (<= 0. skoX)))))
(set-info :status sat)
(check-sat)

