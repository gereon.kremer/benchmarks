(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(assert (and (<= 0. skoY) (and (<= (* skoZ (+ (+ (* skoX 6.) (* skoY (+ (+ 6. (* skoX (* skoX (- 6.)))) (* skoY (* skoX (- 6.)))))) (* skoZ (+ 3. (* skoY (+ (* skoX (- 6.)) (* skoY (* skoX (* skoX 3.))))))))) (+ (+ (- 1.) (* skoX (* skoX (- 3.)))) (* skoY (+ (* skoX (- 4.)) (* skoY (+ (- 3.) (* skoX (* skoX (- 1.))))))))) (and (<= (* skoZ (+ (+ (+ 9. (* skoX (+ (/ 1197. 50.) (* skoX (+ 21. (* skoX (/ 399. 50.))))))) (* skoY (+ (+ (/ 1197. 50.) (* skoX (+ 18. (* skoX (+ (/ (- 399.) 25.) (* skoX (+ (- 18.) (* skoX (/ (- 399.) 50.))))))))) (* skoY (+ (+ 18. (* skoX (+ (/ (- 1197.) 50.) (* skoX (+ (- 21.) (* skoX (+ (/ (- 399.) 50.) (* skoX (- 3.))))))))) (* skoY (* skoX (+ (- 18.) (* skoX (* skoX (- 6.))))))))))) (* skoZ (+ (+ (/ 1197. 100.) (* skoX (+ 9. (* skoX (/ 399. 100.))))) (* skoY (+ (+ 9. (* skoX (+ (/ (- 1197.) 50.) (* skoX (+ (- 15.) (* skoX (/ (- 399.) 50.))))))) (* skoY (+ (* skoX (+ (- 18.) (* skoX (+ (/ 1197. 100.) (* skoX (+ 3. (* skoX (/ 399. 100.)))))))) (* skoY (* skoX (* skoX (+ 9. (* skoX (* skoX 3.)))))))))))))) (+ (+ (/ (- 399.) 100.) (* skoX (+ (- 12.) (* skoX (+ (/ (- 133.) 10.) (* skoX (+ (- 12.) (* skoX (/ (- 399.) 100.))))))))) (* skoY (+ (+ (- 12.) (* skoX (+ (/ (- 399.) 25.) (* skoX (+ (- 16.) (* skoX (/ (- 133.) 25.))))))) (* skoY (+ (+ (/ (- 1197.) 100.) (* skoX (+ (- 12.) (* skoX (+ (/ (- 399.) 50.) (* skoX (+ (- 4.) (* skoX (/ (- 133.) 100.))))))))) (* skoY (+ (- 9.) (* skoX (* skoX (+ (- 6.) (* skoX (* skoX (- 1.)))))))))))))) (and (not (<= 0. skoX)) (and (or (<= 0. skoY) (<= (* skoZ (+ 1. (* skoY (* skoX (- 1.))))) (+ (+ 1. (* skoX (- 1.))) (* skoY (+ (- 1.) (* skoX (- 1.))))))) (and (or (not (<= 0. skoY)) (<= (* skoZ (+ (- 1.) (* skoY skoX))) (+ skoX skoY))) (and (not (<= skoZ 0.)) (and (not (<= skoX (- 1.))) (and (not (<= 1. skoY)) (not (<= skoY skoX))))))))))))
(set-info :status unsat)
(check-sat)

