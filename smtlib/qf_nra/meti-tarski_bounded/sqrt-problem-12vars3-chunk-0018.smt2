(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSX () Real)
(assert (and (<= skoSX 1000) (>= skoSX (- 1000))))
(declare-fun skoSMX () Real)
(assert (and (<= skoSMX 1000) (>= skoSMX (- 1000))))
(assert (and (not (<= (+ (- 4.) (* skoSMX (- 1.))) skoSX)) (not (<= skoX 0.))))
(set-info :status sat)
(check-sat)

