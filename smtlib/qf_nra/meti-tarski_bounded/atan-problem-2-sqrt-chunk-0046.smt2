(set-logic QF_NRA)

(declare-fun skoT () Real)
(assert (and (<= skoT 1000) (>= skoT (- 1000))))
(declare-fun skoB () Real)
(assert (and (<= skoB 1000) (>= skoB (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(assert (and (not (<= (* skoT (* skoT (/ 1. 2.))) (+ skoA (* skoB (- 1.))))) (and (not (<= (* skoT (* skoT (+ (+ (* skoA (+ 1. (* skoA (* skoA (- 1.))))) (* skoB (+ (+ (- 1.) (* skoA skoA)) (* skoB (+ (* skoA (+ (- 1.) (* skoA (/ 1. 2.)))) skoB))))) (* skoT (* skoT (+ (+ (* skoA (+ (- 1.) (* skoA (/ 1. 2.)))) (* skoB (+ 1. (* skoB (/ 1. 2.))))) (* skoT (* skoT (/ 1. 2.))))))))) (* skoB (* skoB (+ (* skoA (* skoA skoA)) (* skoB (* skoA (* skoA (- 1.))))))))) (and (not (<= (* skoT (* skoT (+ (+ (* skoA skoA) (* skoB skoB)) (* skoT skoT)))) (+ 1. (* skoB (* skoB (* skoA (* skoA (- 1.)))))))) (and (not (<= skoB skoA)) (and (not (<= 2. skoB)) (and (not (<= skoA 0.)) (and (not (= skoT 0.)) (<= 0. skoT)))))))))
(set-info :status sat)
(check-sat)

