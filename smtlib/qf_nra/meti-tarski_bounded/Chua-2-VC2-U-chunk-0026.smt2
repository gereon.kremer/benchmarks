(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(assert (and (<= (* skoX (+ (/ (- 16128.) 5.) (* skoX (+ (/ 28224. 625.) (* skoX (+ (/ (- 32928.) 78125.) (* skoX (+ (/ 50421. 19531250.) (* skoX (+ (/ (- 50421.) 4882812500.) (* skoX (/ 117649. 4882812500000.)))))))))))) (- 112896.)) (and (<= (* skoX (+ (+ (* skoC (/ (- 306432.) 125.)) (* skoS (/ 12096. 25.))) (* skoX (+ (+ (* skoC (/ 536256. 15625.)) (* skoS (/ (- 21168.) 3125.))) (* skoX (+ (+ (* skoC (/ (- 625632.) 1953125.)) (* skoS (/ 24696. 390625.))) (* skoX (+ (+ (* skoC (/ 957999. 488281250.)) (* skoS (/ (- 151263.) 390625000.))) (* skoX (+ (+ (* skoC (/ (- 957999.) 122070312500.)) (* skoS (/ 151263. 97656250000.))) (* skoX (+ (* skoC (/ 2235331. 122070312500000.)) (* skoS (/ (- 352947.) 97656250000000.)))))))))))))) (+ (* skoC (- 87552.)) (* skoS 17280.))) (and (<= (* skoX (+ (+ (* skoC (/ 306432. 125.)) (* skoS (/ (- 12096.) 25.))) (* skoX (+ (+ (* skoC (/ (- 536256.) 15625.)) (* skoS (/ 21168. 3125.))) (* skoX (+ (+ (* skoC (/ 625632. 1953125.)) (* skoS (/ (- 24696.) 390625.))) (* skoX (+ (+ (* skoC (/ (- 957999.) 488281250.)) (* skoS (/ 151263. 390625000.))) (* skoX (+ (+ (* skoC (/ 957999. 122070312500.)) (* skoS (/ (- 151263.) 97656250000.))) (* skoX (+ (* skoC (/ (- 2235331.) 122070312500000.)) (* skoS (/ 352947. 97656250000000.)))))))))))))) (+ (* skoC 87552.) (* skoS (- 17280.)))) (and (<= skoX 0.) (and (= (* skoS skoS) (+ 1. (* skoC (* skoC (- 1.))))) (and (<= skoX 75.) (<= 0. skoX))))))))
(set-info :status unsat)
(check-sat)

