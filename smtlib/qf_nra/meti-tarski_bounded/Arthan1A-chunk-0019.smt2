(set-logic QF_NRA)

(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoSINS () Real)
(assert (and (<= skoSINS 1000) (>= skoSINS (- 1000))))
(declare-fun skoCOSS () Real)
(assert (and (<= skoCOSS 1000) (>= skoCOSS (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(assert (and (not (<= (* skoSINS (+ (+ (+ (- 3.) (* skoCOSS (- 2.))) (* skoS (+ (- 4.) (* skoS (+ 2. skoS))))) (* skoSINS (+ 1. skoS)))) (+ (+ 2. (* skoCOSS (+ (- 2.) (* skoCOSS (- 2.))))) (* skoS (+ (* skoCOSS (+ (- 10.) (* skoCOSS (- 2.)))) (* skoS (+ (+ (- 6.) (* skoCOSS (- 6.))) (* skoS (- 2.))))))))) (and (not (<= (* pi (/ 1. 2.)) skoS)) (and (not (<= pi (/ 15707963. 5000000.))) (and (not (<= (/ 31415927. 10000000.) pi)) (and (<= 0. skoS) (and (<= 0. skoCOSS) (<= skoSINS skoS))))))))
(set-info :status sat)
(check-sat)

