(set-logic QF_NRA)

(declare-fun skoSXY () Real)
(assert (and (<= skoSXY 1000) (>= skoSXY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(assert (and (<= (* skoX (+ (- 1.) (* skoSXY (/ 36. 125.)))) (* skoSXY (/ (- 9.) 25.))) (and (not (<= (/ 33. 32.) skoY)) (and (not (<= 2. skoX)) (and (not (<= skoSXY 0.)) (and (not (<= skoX (/ 3. 2.))) (and (not (<= skoY 1.)) (= (+ (* skoSXY skoSXY) (* skoX (- 1.))) skoY))))))))
(set-info :status sat)
(check-sat)

