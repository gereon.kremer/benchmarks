(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(assert (and (<= (* skoX (+ (/ (- 39888.) 625.) (* skoX (+ (/ 690561. 781250.) (* skoX (+ (/ (- 63761799.) 7812500000.) (* skoX (+ (/ 123634128261. 2500000000000000.) (* skoX (+ (/ (- 4892379075471.) 25000000000000000000.) (* skoX (/ 451729667968489. 1000000000000000000000000.)))))))))))) (- 2304.)) (and (<= skoX 0.) (and (= (* skoS skoS) (+ 1. (* skoC (* skoC (- 1.))))) (and (<= skoX 75.) (<= 0. skoX))))))
(set-info :status unsat)
(check-sat)

