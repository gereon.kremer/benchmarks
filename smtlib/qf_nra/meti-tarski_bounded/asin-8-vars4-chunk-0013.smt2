(set-logic QF_NRA)

(declare-fun skoSP () Real)
(assert (and (<= skoSP 1000) (>= skoSP (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS2 () Real)
(assert (and (<= skoS2 1000) (>= skoS2 (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(assert (and (not (= (+ (- 1.) (* skoSP skoSP)) skoX)) (and (not (<= skoX 0.)) (and (not (<= skoSP 0.)) (and (not (<= skoSM 0.)) (and (not (<= skoS2 0.)) (not (<= 1. skoX))))))))
(set-info :status sat)
(check-sat)

