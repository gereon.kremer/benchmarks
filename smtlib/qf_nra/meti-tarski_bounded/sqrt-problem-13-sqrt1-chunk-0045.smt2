(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSS () Real)
(assert (and (<= skoSS 1000) (>= skoSS (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(assert (and (<= (* skoX (+ 4. skoSM)) (+ (* skoSM (/ (- 957.) 250.)) (* skoSS (* skoSM (/ (- 957.) 500.))))) (and (<= (+ (/ 957. 250.) (* skoSS (/ 957. 500.))) skoX) (and (<= skoX (+ (/ 957. 250.) (* skoSS (/ 957. 500.)))) (and (not (<= 1. skoX)) (and (not (<= skoX 0.)) (and (<= 0. skoSM) (and (<= 0. skoSS) (and (= skoX (+ 1. (* skoSM (* skoSM (- 1.))))) (= (* skoX skoX) (+ 1. (* skoSS (* skoSS (- 1.))))))))))))))
(set-info :status unsat)
(check-sat)

