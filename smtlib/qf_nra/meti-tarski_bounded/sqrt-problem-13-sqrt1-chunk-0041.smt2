(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(declare-fun skoSS () Real)
(assert (and (<= skoSS 1000) (>= skoSS (- 1000))))
(assert (and (not (<= skoX (+ (- 10.) (* skoSM (- 2.))))) (and (not (<= 1. skoX)) (and (not (<= skoX 0.)) (and (<= 0. skoSM) (and (<= 0. skoSS) (and (= skoX (+ 1. (* skoSM (* skoSM (- 1.))))) (= (* skoX skoX) (+ 1. (* skoSS (* skoSS (- 1.))))))))))))
(set-info :status sat)
(check-sat)

