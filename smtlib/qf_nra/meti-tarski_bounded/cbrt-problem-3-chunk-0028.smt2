(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(assert (and (<= skoX (/ (- 1.) 2.)) (and (not (<= (* skoZ (+ (+ (+ (/ 4. 3.) (* skoX (+ 8. (* skoX (/ (- 4.) 3.))))) (* skoY (+ (+ 8. (* skoX (+ (/ 80. 3.) (* skoX (/ (- 8.) 3.))))) (* skoY (+ (/ (- 4.) 3.) (* skoX (/ (- 8.) 3.))))))) (* skoZ (+ (+ (/ (- 2.) 3.) (* skoX (/ (- 4.) 3.))) (* skoY (+ (/ (- 4.) 3.) (* skoX (/ (- 8.) 3.)))))))) (+ (+ (/ 2. 3.) (* skoX (+ (/ (- 4.) 3.) (* skoX (/ 2. 3.))))) (* skoY (+ (+ (/ (- 4.) 3.) (* skoX (+ (- 8.) (* skoX (/ 4. 3.))))) (* skoY (+ (/ 2. 3.) (* skoX (/ 4. 3.))))))))) (and (not (<= skoZ 0.)) (and (not (<= skoY 0.)) (not (<= skoX 0.)))))))
(set-info :status unsat)
(check-sat)

