(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(assert (not (<= (* skoY (+ (* skoA (- 2.)) skoY)) (* skoX (* skoX (- 1.))))))
(set-info :status sat)
(check-sat)

