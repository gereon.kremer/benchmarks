(set-logic QF_NRA)

(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (/ (- 250.) 7.) skoX)) (or (not (<= (* skoC (/ 400. 441.)) skoS)) (not (<= skoS (* skoC (/ 400. 441.)))))))
(set-info :status sat)
(check-sat)

