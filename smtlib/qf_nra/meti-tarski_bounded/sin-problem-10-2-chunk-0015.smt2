(set-logic QF_NRA)

(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoSM1 () Real)
(assert (and (<= skoSM1 1000) (>= skoSM1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSP1 () Real)
(assert (and (<= skoSP1 1000) (>= skoSP1 (- 1000))))
(assert (and (not (= (+ 1. (* skoSM1 skoSM1)) skoX)) (and (= (* skoS skoS) skoX) (and (<= 0. skoSP1) (and (<= 0. skoSM1) (and (<= 0. skoS) (not (<= skoX (/ 7. 5.)))))))))
(set-info :status sat)
(check-sat)

