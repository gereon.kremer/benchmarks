(set-logic QF_NRA)

(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (<= (* skoZ (+ (+ (+ 60. (* skoX (+ (- 36.) (* skoX (+ 9. (* skoX (- 1.))))))) (* skoY (+ (+ 24. (* skoX (+ (- 18.) (* skoX (+ 6. (* skoX (- 1.))))))) (* skoY (+ (+ (- 27.) (* skoX (+ 15. (* skoX (- 3.))))) (* skoY (+ (+ 8. (* skoX (- 3.))) (* skoY (- 1.))))))))) (* skoZ (+ (+ (+ (- 48.) (* skoX (+ 21. (* skoX (- 3.))))) (* skoY (+ (+ (- 27.) (* skoX (+ 15. (* skoX (- 3.))))) (* skoY (+ (+ 18. (* skoX (- 6.))) (* skoY (- 3.))))))) (* skoZ (+ (+ (+ 11. (* skoX (- 3.))) (* skoY (+ (+ 8. (* skoX (- 3.))) (* skoY (- 3.))))) (* skoZ (+ (- 1.) (* skoY (- 1.)))))))))) (+ (+ (- 120.) (* skoX (+ 60. (* skoX (+ (- 12.) skoX))))) (* skoY (+ (+ (- 60.) (* skoX (+ 36. (* skoX (+ (- 9.) skoX))))) (* skoY (+ (+ 48. (* skoX (+ (- 21.) (* skoX 3.)))) (* skoY (+ (+ (- 11.) (* skoX 3.)) skoY)))))))) (and (<= (* skoZ (+ (+ (+ (/ 135. 2.) (* skoX (+ 21. (* skoX (/ 27. 8.))))) (* skoY (+ (+ 21. (* skoX (/ 27. 4.))) (* skoY (/ 27. 8.))))) (* skoZ (+ (+ (+ (/ 21. 2.) (* skoX (/ 27. 8.))) (* skoY (/ 27. 8.))) (* skoZ (/ 9. 8.)))))) (+ (+ (- 105.) (* skoX (+ (/ (- 135.) 2.) (* skoX (+ (/ (- 21.) 2.) (* skoX (/ (- 9.) 8.))))))) (* skoY (+ (+ (/ (- 135.) 2.) (* skoX (+ (- 21.) (* skoX (/ (- 27.) 8.))))) (* skoY (+ (+ (/ (- 21.) 2.) (* skoX (/ (- 27.) 8.))) (* skoY (/ (- 9.) 8.)))))))) (and (<= 0. skoX) (and (<= 0. skoY) (and (<= 0. skoZ) (and (<= skoX 1.) (and (<= skoY 1.) (and (<= skoZ 1.) (and (<= skoZ (+ (+ 2. (* skoX (- 1.))) (* skoY (- 1.)))) (<= (+ (+ 2. (* skoX (- 1.))) (* skoY (- 1.))) skoZ)))))))))))
(set-info :status unsat)
(check-sat)

