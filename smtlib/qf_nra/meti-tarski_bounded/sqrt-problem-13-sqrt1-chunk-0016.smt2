(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSS () Real)
(assert (and (<= skoSS 1000) (>= skoSS (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(assert (and (<= (- 2.) skoSS) (and (= (* skoX skoX) (+ 1. (* skoSS (* skoSS (- 1.))))) (and (= skoX (+ 1. (* skoSM (* skoSM (- 1.))))) (and (<= 0. skoSS) (and (<= 0. skoSM) (and (not (<= skoX 0.)) (not (<= 1. skoX)))))))))
(set-info :status sat)
(check-sat)

