(set-logic QF_NRA)

(declare-fun skoT () Real)
(assert (and (<= skoT 1000) (>= skoT (- 1000))))
(declare-fun skoB () Real)
(assert (and (<= skoB 1000) (>= skoB (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(assert (and (<= (* skoT (* skoT (+ (+ (* skoA skoA) (* skoB skoB)) (* skoT skoT)))) (+ 1. (* skoB (* skoB (* skoA (* skoA (- 1.))))))) (and (not (<= skoB skoA)) (and (not (<= 2. skoB)) (and (not (<= skoA 0.)) (and (not (= skoT 0.)) (<= 0. skoT)))))))
(set-info :status sat)
(check-sat)

