(set-logic QF_NRA)

(declare-fun skoB () Real)
(assert (and (<= skoB 1000) (>= skoB (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(assert (and (not (<= (* skoS (* skoB (- 1.))) 0.)) (and (not (<= (* skoS (+ (- 1.) (* skoB (+ (/ 207. 100.) (* skoA (- 1.)))))) (* skoB (+ (* skoA (- 1.)) skoB)))) (and (not (<= skoB 1.)) (and (not (<= skoS 0.)) (and (not (<= skoA 0.)) (and (not (<= 2. skoB)) (not (<= skoB skoA)))))))))
(set-info :status unsat)
(check-sat)

