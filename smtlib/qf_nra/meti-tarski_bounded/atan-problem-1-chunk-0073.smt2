(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS3 () Real)
(assert (and (<= skoS3 1000) (>= skoS3 (- 1000))))
(declare-fun skoSX () Real)
(assert (and (<= skoSX 1000) (>= skoSX (- 1000))))
(assert (and (not (<= (* skoX (+ (+ (* skoS3 (/ 1413711. 20000.)) (* skoSX (/ 471237. 20000.))) (* skoX (+ (+ (* skoS3 (- 267.)) (* skoSX (- 49.))) (* skoX (+ (+ (* skoS3 (/ 3298659. 10000.)) (* skoSX (/ 1099553. 10000.))) (* skoX (+ (+ (* skoS3 (- 749.)) (* skoSX (- 63.))) (* skoX (+ (+ (* skoS3 (/ 29687931. 100000.)) (* skoSX (/ 9895977. 100000.))) (* skoX (* skoS3 (- 504.))))))))))))) (+ (* skoS3 (/ 64. 5.)) (* skoSX (/ 64. 15.))))) (and (not (<= skoX 1.)) (and (not (<= skoX 0.)) (and (not (<= skoSX 0.)) (not (<= skoS3 0.)))))))
(set-info :status sat)
(check-sat)

