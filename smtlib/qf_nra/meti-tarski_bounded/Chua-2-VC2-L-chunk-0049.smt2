(set-logic QF_NRA)

(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= 0. skoX)) (and (<= skoX 0.) (and (<= skoS (* skoC (/ 76. 15.))) (and (or (not (<= (* skoC (/ 76. 15.)) skoS)) (not (<= skoS (* skoC (/ 76. 15.))))) (and (= (* skoS skoS) (+ 1. (* skoC (* skoC (- 1.))))) (and (<= skoX 75.) (<= 0. skoX))))))))
(set-info :status unsat)
(check-sat)

