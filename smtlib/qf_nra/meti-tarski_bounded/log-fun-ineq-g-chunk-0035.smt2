(set-logic QF_NRA)

(declare-fun skoB () Real)
(assert (and (<= skoB 1000) (>= skoB (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(assert (and (<= (* skoX (+ (* skoB 2.) (* skoX 3.))) 0.) (and (not (<= (* skoB (* skoB (/ 3. 2.))) 0.)) (and (not (= skoX 0.)) (and (not (<= skoA 0.)) (and (not (<= skoB 0.)) (not (<= skoX 0.))))))))
(set-info :status unsat)
(check-sat)

