(set-logic QF_NRA)

(declare-fun skoSP () Real)
(assert (and (<= skoSP 1000) (>= skoSP (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(assert (and (not (= (+ (- 1.) (* skoSP skoSP)) skoX)) (and (not (<= pi (/ 15707963. 5000000.))) (and (not (<= (/ 31415927. 10000000.) pi)) (and (not (<= skoX 0.)) (not (<= 1. skoX)))))))
(set-info :status sat)
(check-sat)

