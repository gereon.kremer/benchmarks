(set-logic QF_NRA)

(declare-fun skoSS () Real)
(assert (and (<= skoSS 1000) (>= skoSS (- 1000))))
(declare-fun skoSP () Real)
(assert (and (<= skoSP 1000) (>= skoSP (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(declare-fun skoS2 () Real)
(assert (and (<= skoS2 1000) (>= skoS2 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (* skoX (+ (+ (- 4.) (* skoSM (- 1.))) (* skoSP (- 1.)))) (+ (+ (* skoSM (+ 1. (* skoS2 2.))) (* skoSP (+ (- 1.) (* skoS2 (- 2.))))) (* skoSS (+ (* skoSM (+ (/ 1. 2.) skoS2)) (* skoSP (+ (/ (- 1.) 2.) (* skoS2 (- 1.))))))))) (and (not (<= skoSS (- 2.))) (and (not (<= skoX 0.)) (not (<= (/ 4. 5.) skoX))))))
(set-info :status sat)
(check-sat)


