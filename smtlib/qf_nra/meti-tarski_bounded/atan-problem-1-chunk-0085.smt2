(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS3 () Real)
(assert (and (<= skoS3 1000) (>= skoS3 (- 1000))))
(declare-fun skoSX () Real)
(assert (and (<= skoSX 1000) (>= skoSX (- 1000))))
(assert (and (not (<= (* skoX (+ (+ (* skoS3 (- 1155.)) (* skoSX 231.)) (* skoX (* skoX (+ (+ (* skoS3 (- 1806.)) (* skoSX 238.)) (* skoX (* skoX (+ (+ (* skoS3 (/ (- 3507.) 5.)) (* skoSX (/ 231. 5.))) (* skoX (* skoX (* skoS3 (- 40.)))))))))))) 0.)) (and (not (<= skoX 0.)) (and (not (<= skoSX 0.)) (not (<= skoS3 0.))))))
(set-info :status sat)
(check-sat)

