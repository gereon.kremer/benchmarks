(set-logic QF_NRA)

(declare-fun skoM () Real)
(assert (and (<= skoM 1000) (>= skoM (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoSINS () Real)
(assert (and (<= skoSINS 1000) (>= skoSINS (- 1000))))
(declare-fun skoCOSS () Real)
(assert (and (<= skoCOSS 1000) (>= skoCOSS (- 1000))))
(assert (and (<= (* skoSINS (+ (+ (* skoM (* skoM (* skoM (+ (* skoCOSS (- 2.)) (* skoM (- 5.)))))) (* skoS (+ (* skoM (* skoM (* skoM (* skoM (- 3.))))) (* skoS (+ (* skoM (* skoM (* skoM (* skoM 3.)))) (* skoS (* skoM (* skoM (* skoM skoM))))))))) (* skoSINS (+ (* skoM (* skoM skoM)) (* skoS (* skoM (* skoM skoM))))))) (+ (* skoM (* skoM (* skoM (+ (* skoCOSS (* skoCOSS (- 2.))) (* skoM (+ (* skoCOSS (- 6.)) (* skoM (- 2.)))))))) (* skoS (+ (* skoM (* skoM (* skoM (+ (* skoCOSS (* skoCOSS (- 2.))) (* skoM (+ (* skoCOSS (- 12.)) (* skoM (- 6.)))))))) (* skoS (+ (* skoM (* skoM (* skoM (* skoM (+ (* skoCOSS (- 6.)) (* skoM (- 6.))))))) (* skoS (* skoM (* skoM (* skoM (* skoM (* skoM (- 2.))))))))))))) (and (not (<= (* skoM skoM) 0.)) (and (= (* skoSINS skoSINS) (+ 1. (* skoCOSS (* skoCOSS (- 1.))))) (and (<= 2. skoS) (<= 2. skoM))))))
(set-info :status unsat)
(check-sat)

