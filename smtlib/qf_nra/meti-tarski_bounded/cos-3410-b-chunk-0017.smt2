(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun skoZ () Real)
(assert (and (<= skoZ 1000) (>= skoZ (- 1000))))
(assert (and (not (<= (* skoZ (+ (* skoX (- 1.)) (* skoZ (/ 1. 2.)))) (+ (/ 51. 100.) (* skoX (* skoX (/ (- 1.) 2.)))))) (and (not (<= (* skoY (+ (* skoX (- 1.)) (* skoY (/ 1. 2.)))) (+ (/ 51. 100.) (* skoX (* skoX (/ (- 1.) 2.)))))) (and (not (<= skoZ (/ (- 3.) 2.))) (and (not (<= skoY (/ (- 3.) 2.))) (and (not (<= skoX (/ (- 3.) 2.))) (and (not (<= (/ 3. 2.) skoZ)) (and (not (<= (/ 3. 2.) skoY)) (not (<= (/ 3. 2.) skoX))))))))))
(set-info :status sat)
(check-sat)

