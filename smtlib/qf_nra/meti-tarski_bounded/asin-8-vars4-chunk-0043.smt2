(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(declare-fun skoSP () Real)
(assert (and (<= skoSP 1000) (>= skoSP (- 1000))))
(declare-fun skoS2 () Real)
(assert (and (<= skoS2 1000) (>= skoS2 (- 1000))))
(assert (and (not (<= (* skoSP (+ (/ (- 13.) 8.) (* skoS2 (/ (- 63.) 20.)))) (+ (/ 1. 5.) (* skoSM (+ (/ (- 61.) 40.) (* skoS2 (/ (- 63.) 20.))))))) (and (not (<= (* skoX (+ (+ (+ (- 4.) (* skoSM (- 1.))) (* skoSP (- 1.))) (* skoX (+ (+ (/ (- 1.) 5.) (* skoSM (+ (/ 61. 40.) (* skoS2 (/ 63. 20.))))) (* skoSP (+ (/ (- 13.) 8.) (* skoS2 (/ (- 63.) 20.)))))))) (+ (+ (/ (- 1.) 5.) (* skoSM (+ (/ 61. 40.) (* skoS2 (/ 63. 20.))))) (* skoSP (+ (/ (- 13.) 8.) (* skoS2 (/ (- 63.) 20.))))))) (and (not (<= skoX 0.)) (and (not (<= skoSP 0.)) (and (not (<= skoSM 0.)) (and (not (<= skoS2 0.)) (not (<= 1. skoX)))))))))
(set-info :status unsat)
(check-sat)

