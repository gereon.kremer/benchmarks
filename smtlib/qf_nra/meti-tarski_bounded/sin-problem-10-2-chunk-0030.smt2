(set-logic QF_NRA)

(declare-fun skoSM1 () Real)
(assert (and (<= skoSM1 1000) (>= skoSM1 (- 1000))))
(declare-fun skoSP1 () Real)
(assert (and (<= skoSP1 1000) (>= skoSP1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (* skoSP1 (* skoSM1 2.)) 0.)) (not (<= skoX (/ 7. 5.)))))
(set-info :status sat)
(check-sat)

