(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoS () Real)
(assert (and (<= skoS 1000) (>= skoS (- 1000))))
(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(assert (and (not (<= (+ (/ 760000. 7383.) (* skoC (/ (- 3400.) 7383.))) skoS)) (and (<= skoX (/ 1. 10000000.)) (<= 0. skoX))))
(set-info :status sat)
(check-sat)

