(set-logic QF_NRA)

(declare-fun skoB () Real)
(assert (and (<= skoB 1000) (>= skoB (- 1000))))
(declare-fun skoA () Real)
(assert (and (<= skoA 1000) (>= skoA (- 1000))))
(declare-fun skoT () Real)
(assert (and (<= skoT 1000) (>= skoT (- 1000))))
(assert (and (not (<= (* skoT (+ (* skoA (- 1.)) (* skoB (+ 1. skoA)))) (* skoB (* skoA (/ (- 157.) 50.))))) (and (not (<= skoB skoA)) (and (not (<= 2. skoB)) (and (not (<= skoA 0.)) (not (<= skoT (/ 3. 2.))))))))
(set-info :status sat)
(check-sat)

