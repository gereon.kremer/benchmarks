(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoY () Real)
(assert (and (<= skoY 1000) (>= skoY (- 1000))))
(declare-fun pi () Real)
(assert (and (<= pi 1000) (>= pi (- 1000))))
(assert (and (not (<= skoY 0.)) (and (not (<= (* skoY (+ (/ (- 1.) 2000.) (* skoY (+ (* skoX (/ (- 1.) 6.)) (* skoY (+ (/ 1. 12000.) (* skoY (+ (* skoX (/ 1. 120.)) (* skoY (/ (- 1.) 240000.)))))))))) (* skoX (* skoX (* skoX (+ (/ (- 1.) 6.) (* skoX (* skoX (+ (/ 1. 120.) (* skoX (* skoX (/ (- 1.) 5040.)))))))))))) (and (not (<= (* skoY (+ (* skoX (* skoX (* skoX (/ 1. 6.)))) (* skoY (+ (/ (- 1.) 2000.) (* skoY (+ (* skoX (/ (- 1.) 6.)) (* skoY (+ (/ 1. 12000.) (* skoY (+ (* skoX (/ 1. 120.)) (* skoY (/ (- 1.) 240000.)))))))))))) 0.)) (and (not (<= pi (/ 15707963. 5000000.))) (and (not (<= (/ 31415927. 10000000.) pi)) (and (not (<= (* pi (/ 1. 2.)) skoY)) (and (not (<= skoX 0.)) (and (not (<= skoY skoX)) (or (not (<= (* skoX 2000.) skoY)) (not (<= skoY (* skoX 2000.)))))))))))))
(set-info :status sat)
(check-sat)

