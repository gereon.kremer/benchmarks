(set-logic QF_NRA)

(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(declare-fun skoSP () Real)
(assert (and (<= skoSP 1000) (>= skoSP (- 1000))))
(declare-fun skoSM () Real)
(assert (and (<= skoSM 1000) (>= skoSM (- 1000))))
(declare-fun skoSS () Real)
(assert (and (<= skoSS 1000) (>= skoSS (- 1000))))
(assert (and (not (= (* skoX skoX) (+ 1. (* skoSS (* skoSS (- 1.)))))) (and (= skoX (+ 1. (* skoSM (* skoSM (- 1.))))) (and (= (+ (- 1.) (* skoSP skoSP)) skoX) (and (<= 0. skoX) (and (<= 0. skoSS) (and (<= 0. skoSP) (and (<= 0. skoSM) (not (<= 1. skoX))))))))))
(set-info :status sat)
(check-sat)

