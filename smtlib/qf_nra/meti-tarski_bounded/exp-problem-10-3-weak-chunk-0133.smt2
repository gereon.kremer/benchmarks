(set-logic QF_NRA)

(declare-fun skoC () Real)
(assert (and (<= skoC 1000) (>= skoC (- 1000))))
(declare-fun skoCM1 () Real)
(assert (and (<= skoCM1 1000) (>= skoCM1 (- 1000))))
(declare-fun skoX () Real)
(assert (and (<= skoX 1000) (>= skoX (- 1000))))
(assert (and (not (<= (* skoCM1 (+ (- 6.) (* skoCM1 12.))) (- 1.))) (and (not (<= (* skoCM1 (+ (+ (- 6.) (* skoC (+ 72. (* skoC (+ (- 504.) (* skoC (+ 2304. (* skoC (+ (- 6912.) (* skoC (+ 13824. (* skoC (- 27648.))))))))))))) (* skoCM1 (+ (- 12.) (* skoC (+ 144. (* skoC (+ (- 1008.) (* skoC (+ 4608. (* skoC (+ (- 13824.) (* skoC 27648.))))))))))))) (+ 1. (* skoC (+ (- 12.) (* skoC (+ 84. (* skoC (+ (- 384.) (* skoC (+ 1152. (* skoC (- 2304.))))))))))))) (and (not (<= skoX 1.)) (and (not (<= skoCM1 0.)) (not (<= skoC 0.)))))))
(set-info :status sat)
(check-sat)

