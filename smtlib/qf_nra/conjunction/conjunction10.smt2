(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "drafted")
(declare-fun a () Real)
(declare-fun b () Real)
(assert (<= a 3))
(assert (>= a 3))
(assert (> (+ a (* 3 b)) 1))
(assert (= a 3))
(check-sat)
(exit)
