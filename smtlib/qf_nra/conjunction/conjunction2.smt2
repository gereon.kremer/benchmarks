(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(declare-fun b () Real)
(assert (and (> b 5) (< b 4) (<= b 4)))
(check-sat)
(exit)
