(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(declare-fun a () Real)
(declare-fun b () Real)
(assert (and (or (= a 1) (= b 2)) (or (= a 2) (= b 3))))
(check-sat)
(exit)
