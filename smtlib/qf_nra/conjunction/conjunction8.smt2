(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "drafted")
(declare-fun x_0 () Real)
(declare-fun y_0 () Real)
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun delta_x () Real)
(declare-fun delta_y () Real)
(assert (and 
       (<= (+ (* delta_x (* delta_y 1)) (- 15)) 0)
       (not (<= (+ (* y_1 (- 1)) (+ (* y_0 1) (- 2))) 0))
       (= 0 (+ (* delta_y 1) (+ (* y_1 1) (+ (* y_0 (- 1)) (- 3)))))
       (not (<= (+ (* y_1 (- 1)) (* y_0 1)) 0))
       (<= (+ (* x_1 (- 1)) (+ (* x_0 1) 2)) 0)
       (not (<= (+ (* x_1 1) (* x_0 (- 1))) 0))
       (= 0 (+ (* delta_x 1) (+ (* x_1 (- 1)) (+ (* x_0 1) (- 2)))))
))
(check-sat)
(exit)
