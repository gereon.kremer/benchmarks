(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "drafted")
(declare-fun x_0 () Real)
(declare-fun y_0 () Real)
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun delta_x () Real)
(declare-fun delta_y () Real)
(assert (and (> (- delta_x) 0) (> (+ 2 (- x_0) x_1) 0) (> (+ 2 x_0 (- x_1)) 0) (<= (+ (- 2) (- x_0) x_1 delta_x) 0) (<= (+ 10 (* (- 3) delta_x)) 0))) 
(check-sat)
(exit)
