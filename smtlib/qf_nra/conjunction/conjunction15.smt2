(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(declare-fun x_0 () Real)
(declare-fun y_0 () Real)
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun x_2 () Real)
(declare-fun y_2 () Real)
(declare-fun delta_x () Real)
(declare-fun delta_y () Real)
(assert (and (<= (+ (* delta_x (* delta_y 1)) (- 50)) 0) (<= (+ (* delta_x (- 1)) 1) 0) (<= (+ (* y_1 (- 1)) (+ (* y_0 1) 3)) 0) (not (<= (+ (* y_1 1) (+ (* y_0 (- 1)) 5)) 0)) (not (<= (+ (* y_1 1) (* y_0 (- 1))) 0)) (<= (+ (* y_2 1) (* y_1 (- 1))) 0) (not (<= (+ (* y_2 (- 1)) (+ (* y_1 1) 5)) 0)) (not (<= (+ (* y_2 (- 1)) (+ (* y_1 1) 4)) 0)) (<= (+ (* delta_y 1) (+ (* y_2 (- 1)) (+ (* y_0 1) (- 1)))) 0) (<= (+ (* delta_y 1) (- 3)) 0) (not (<= (+ (* delta_y (- 1)) (+ (* y_2 (- 1)) (+ (* y_1 1) 5))) 0)) (not (<= (+ (* delta_y (- 1)) 5) 0)) (<= (+ (* y_2 (- 1)) (+ (* y_0 1) 3)) 0) (not (<= (+ (* y_2 1) (+ (* y_0 (- 1)) 1)) 0)) (<= (+ (* delta_y 1) (+ (* y_1 (- 1)) (+ (* y_0 1) (- 5)))) 0) (not (<= (+ (* delta_y (- 1)) (+ (* y_1 1) (+ (* y_0 (- 1)) 5))) 0)) (<= (+ (* x_1 1) (+ (* x_0 (- 1)) 2)) 0) (not (<= (+ (* delta_x 1) (+ (* x_1 (- 1)) (+ (* x_0 1) (- 2)))) 0)) (not (<= (+ (* x_1 (- 1)) (* x_0 1)) 0)) (<= (+ (* delta_y (- 1)) 3) 0) (<= (+ (* x_2 1) (+ (* x_0 (- 1)) 1)) 0) (not (<= (+ (* x_2 (- 1)) (* x_0 1)) 0)) (<= (+ (* x_2 (- 1)) (+ (* x_1 1) 2)) 0) (not (<= (+ (* x_2 1) (* x_1 (- 1))) 0))))
(check-sat)
(exit)
