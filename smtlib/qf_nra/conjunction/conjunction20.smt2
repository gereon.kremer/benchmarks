(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(declare-fun x () Real)
(declare-fun y () Real)
(assert (and 
(<= (+ (* y (- 1)) (* x x x) (* (* x x) (- 2))) 0)
(<= (+ (* y (- 1)) (* x 4)) 0)
(<= (* x (- 1)) 0)
(< (+ (* x 16384) (- 125)) 0)
(<= (+ (* y (- 1)) (- 1000)) 0)
(< (+ (* y 4278977) 4278944353) 0)
))
(check-sat)
(exit)
