(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(declare-fun x_0 () Real)
(declare-fun y_0 () Real)
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun x_2 () Real)
(declare-fun y_2 () Real)
(declare-fun delta_x () Real)
(declare-fun delta_y () Real)
(assert (and (<= (+ (- 50) (* 3 delta_x)) 0) (= (+ (- y_1) y_0) 0) (<= (+ 2 (- y_2) y_0) 0) (>= (+ (- 1) (- x_2) x_1) 0) (< (+ 4 (- y_2) y_1) 0) (<= (+ (- 1) (- x_2) delta_x x_1) 0) (< (+ (- 1) delta_x) 0) (<= (+ (- 2) x_2 delta_x (- x_1)) 0) (= (+ (- 2) delta_x x_0 (- x_1)) 0) (<= (+ (- x_2) x_0) 0) ))
(check-sat)
(exit)
