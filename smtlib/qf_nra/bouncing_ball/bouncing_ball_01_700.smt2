(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun bounce_1 () Bool)
(declare-fun bounce_2 () Bool)
(declare-fun p_0 () Real)
(declare-fun height_0 () Real)
(declare-fun delta_0 () Real)
(declare-fun p_1 () Real)
(declare-fun height_1 () Real)
(declare-fun delta_1 () Real)
(assert (and (> height_0 80) (<= height_0 200) (>= delta_0 (/ 1 10)) (<= delta_0 (/ 2 10)) (= p_0 0) bounce_1 (or (not bounce_1) (= height_1 (* (/ 8 10) height_0))) (or (not bounce_1) (= delta_1 (* (+ 1 (/ 1 10)) delta_0))) (or (not bounce_1) (= 0 (+ height_1 (* (- 1) delta_1 p_1 p_1)))) (or (>= (+ p_0 p_1) 683) (not bounce_1) bounce_2) (or (<= (+ p_0 p_1) 717) (not bounce_1) bounce_2) (not bounce_2)))
(check-sat)
(exit)

