#!/bin/bash
for bounces in {01..20}; do
    for holedistance in {100..900..100}; do
	echo "Generating bouncing_ball_"$bounces"_"$holedistance".smt2..."
        python ../generate_bouncing_ball_example_smtlib.py $bounces $holedistance > "bouncing_ball_"$bounces"_"$holedistance".smt2";
    done;
done;
