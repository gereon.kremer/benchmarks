(set-logic QF_NRA)
(set-info :source | hong's problem \sum x_i^2 < 1 and \prod x_i > 1 |)
(declare-fun x_0 () Real)
(assert (and (<= x_0 1000) (>= x_0 (- 1000))))
(declare-fun x_1 () Real)
(assert (and (<= x_1 1000) (>= x_1 (- 1000))))
(declare-fun x_2 () Real)
(assert (and (<= x_2 1000) (>= x_2 (- 1000))))
(declare-fun x_3 () Real)
(assert (and (<= x_3 1000) (>= x_3 (- 1000))))
(declare-fun x_4 () Real)
(assert (and (<= x_4 1000) (>= x_4 (- 1000))))
(declare-fun x_5 () Real)
(assert (and (<= x_5 1000) (>= x_5 (- 1000))))
(declare-fun x_6 () Real)
(assert (and (<= x_6 1000) (>= x_6 (- 1000))))
(declare-fun x_7 () Real)
(assert (and (<= x_7 1000) (>= x_7 (- 1000))))
(declare-fun x_8 () Real)
(assert (and (<= x_8 1000) (>= x_8 (- 1000))))
(declare-fun x_9 () Real)
(assert (and (<= x_9 1000) (>= x_9 (- 1000))))
(declare-fun x_10 () Real)
(assert (and (<= x_10 1000) (>= x_10 (- 1000))))
(declare-fun x_11 () Real)
(assert (and (<= x_11 1000) (>= x_11 (- 1000))))
(declare-fun x_12 () Real)
(assert (and (<= x_12 1000) (>= x_12 (- 1000))))
(declare-fun x_13 () Real)
(assert (and (<= x_13 1000) (>= x_13 (- 1000))))
(declare-fun x_14 () Real)
(assert (and (<= x_14 1000) (>= x_14 (- 1000))))
(declare-fun x_15 () Real)
(assert (and (<= x_15 1000) (>= x_15 (- 1000))))
(assert (< (+ (* x_0 x_0) (+ (* x_1 x_1) (+ (* x_2 x_2) (+ (* x_3 x_3) (+ (* x_4 x_4) (+ (* x_5 x_5) (+ (* x_6 x_6) (+ (* x_7 x_7) (+ (* x_8 x_8) (+ (* x_9 x_9) (+ (* x_10 x_10) (+ (* x_11 x_11) (+ (* x_12 x_12) (+ (* x_13 x_13) (+ (* x_14 x_14) (* x_15 x_15)))))))))))))))) 1.))
(assert (> (* x_0 (* x_1 (* x_2 (* x_3 (* x_4 (* x_5 (* x_6 (* x_7 (* x_8 (* x_9 (* x_10 (* x_11 (* x_12 (* x_13 (* x_14 x_15))))))))))))))) 1.))
(check-sat)

