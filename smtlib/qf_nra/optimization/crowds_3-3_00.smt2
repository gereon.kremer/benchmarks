(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun PF_0 () Real)
(declare-fun badC_0 () Real)
(assert
  (and
    (<= (+ PF_0 (- 1)) 0) (>= (+ PF_0 1) 0) (<= (+ badC_0 (- 1)) 0) (>= (+ badC_0 1) 0) (<> (+ (* 27 PF_0 PF_0 PF_0 badC_0 badC_0 badC_0) (* (- 81) PF_0 PF_0 PF_0 badC_0 badC_0) (* 81 PF_0 PF_0 PF_0 badC_0) (* (- 27) PF_0 PF_0 PF_0) (* 81 PF_0 PF_0 badC_0 badC_0) (* (- 162) PF_0 PF_0 badC_0) (* 81 PF_0 PF_0) (* 81 PF_0 badC_0) (* (- 81) PF_0) 27) 0)
  )
)
(check-sat)
(exit)
