(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun PF_0 () Real)
(declare-fun badC_0 () Real)
(declare-fun PF_1 () Real)
(declare-fun badC_1 () Real)
(assert
  (and
    (<= (+ PF_0 (- 1)) 0) (>= (+ PF_0 1) 0) (<= (+ badC_0 (- 1)) 0) (>= (+ badC_0 1) 0) (<> (+ (* 27 PF_0 PF_0 PF_0 badC_0 badC_0 badC_0) (* (- 81) PF_0 PF_0 PF_0 badC_0 badC_0) (* 81 PF_0 PF_0 PF_0 badC_0) (* (- 27) PF_0 PF_0 PF_0) (* 81 PF_0 PF_0 badC_0 badC_0) (* (- 162) PF_0 PF_0 badC_0) (* 81 PF_0 PF_0) (* 81 PF_0 badC_0) (* (- 81) PF_0) 27) 0) (<= (+ PF_1 (- 1)) 0) (>= (+ PF_1 1) 0) (<= (+ badC_1 (- 1)) 0) (>= (+ badC_1 1) 0) (<> (+ (* 27 PF_1 PF_1 PF_1 badC_1 badC_1 badC_1) (* (- 81) PF_1 PF_1 PF_1 badC_1 badC_1) (* 81 PF_1 PF_1 PF_1 badC_1) (* (- 27) PF_1 PF_1 PF_1) (* 81 PF_1 PF_1 badC_1 badC_1) (* (- 162) PF_1 PF_1 badC_1) (* 81 PF_1 PF_1) (* 81 PF_1 badC_1) (* (- 81) PF_1) 27) 0) (> (+ (* (- 16) PF_1 PF_1 PF_1 badC_1 badC_1 badC_1 badC_1 badC_1 badC_1) (* 84 PF_1 PF_1 PF_1 badC_1 badC_1 badC_1 badC_1 badC_1) (* (- 156) PF_1 PF_1 PF_1 badC_1 badC_1 badC_1 badC_1) (* 124 PF_1 PF_1 PF_1 badC_1 badC_1 badC_1) (* (- 36) PF_1 PF_1 PF_1 badC_1 badC_1) (* (- 72) PF_1 PF_1 badC_1 badC_1 badC_1 badC_1 badC_1) (* 288 PF_1 PF_1 badC_1 badC_1 badC_1 badC_1) (* (- 360) PF_1 PF_1 badC_1 badC_1 badC_1) (* 144 PF_1 PF_1 badC_1 badC_1) (* (- 108) PF_1 badC_1 badC_1 badC_1 badC_1) (* 297 PF_1 badC_1 badC_1 badC_1) (* (- 189) PF_1 badC_1 badC_1) (* (- 54) badC_1 badC_1 badC_1) (* 81 badC_1 badC_1)) (+ (+ (* (- 16) PF_0 PF_0 PF_0 badC_0 badC_0 badC_0 badC_0 badC_0 badC_0) (* 84 PF_0 PF_0 PF_0 badC_0 badC_0 badC_0 badC_0 badC_0) (* (- 156) PF_0 PF_0 PF_0 badC_0 badC_0 badC_0 badC_0) (* 124 PF_0 PF_0 PF_0 badC_0 badC_0 badC_0) (* (- 36) PF_0 PF_0 PF_0 badC_0 badC_0) (* (- 72) PF_0 PF_0 badC_0 badC_0 badC_0 badC_0 badC_0) (* 288 PF_0 PF_0 badC_0 badC_0 badC_0 badC_0) (* (- 360) PF_0 PF_0 badC_0 badC_0 badC_0) (* 144 PF_0 PF_0 badC_0 badC_0) (* (- 108) PF_0 badC_0 badC_0 badC_0 badC_0) (* 297 PF_0 badC_0 badC_0 badC_0) (* (- 189) PF_0 badC_0 badC_0) (* (- 54) badC_0 badC_0 badC_0) (* 81 badC_0 badC_0)) 32768))
  )
)
(check-sat)
(exit)
