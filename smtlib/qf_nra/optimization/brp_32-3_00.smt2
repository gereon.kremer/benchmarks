(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun pK_0 () Real)
(declare-fun pL_0 () Real)
(assert
  (and
    (<= (+ pK_0 (- 1)) 0) (>= (+ pK_0 1) 0) (<= (+ pL_0 (- 1)) 0) (>= (+ pL_0 1) 0)
  )
)
(check-sat)
(exit)
