(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun perr_0 () Real)
(declare-fun prob1_0 () Real)
(assert
  (and
    (<= (+ perr_0 (- 1)) 0) (>= (+ perr_0 1) 0) (<= (+ prob1_0 (- 1)) 0) (>= (+ prob1_0 1) 0)
  )
)
(check-sat)
(exit)
