(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun x_0 () Real)
(declare-fun y_0 () Real)
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun x_2 () Real)
(declare-fun y_2 () Real)
(declare-fun x_3 () Real)
(declare-fun y_3 () Real)
(declare-fun x_4 () Real)
(declare-fun y_4 () Real)
(declare-fun x_5 () Real)
(declare-fun y_5 () Real)
(assert
  (and
    (<= (+ x_0 (- 2)) 0) (>= (+ x_0 2) 0) (<= (+ y_0 (- 2)) 0) (>= (+ y_0 2) 0) (<= (+ x_1 (- 2)) 0) (>= (+ x_1 2) 0) (<= (+ y_1 (- 2)) 0) (>= (+ y_1 2) 0) (< (+ (* x_1 x_1) (* y_1 y_1) (- 1)) (+ (* x_0 x_0) (* y_0 y_0) (- 1))) (<= (+ x_2 (- 2)) 0) (>= (+ x_2 2) 0) (<= (+ y_2 (- 2)) 0) (>= (+ y_2 2) 0) (< (+ (* x_2 x_2) (* y_2 y_2) (- 1)) (+ (* x_1 x_1) (* y_1 y_1) (- 1))) (<= (+ x_3 (- 2)) 0) (>= (+ x_3 2) 0) (<= (+ y_3 (- 2)) 0) (>= (+ y_3 2) 0) (< (+ (* x_3 x_3) (* y_3 y_3) (- 1)) (+ (* x_2 x_2) (* y_2 y_2) (- 1))) (<= (+ x_4 (- 2)) 0) (>= (+ x_4 2) 0) (<= (+ y_4 (- 2)) 0) (>= (+ y_4 2) 0) (< (+ (* x_4 x_4) (* y_4 y_4) (- 1)) (+ (* x_3 x_3) (* y_3 y_3) (- 1))) (<= (+ x_5 (- 2)) 0) (>= (+ x_5 2) 0) (<= (+ y_5 (- 2)) 0) (>= (+ y_5 2) 0) (< (+ (* x_5 x_5) (* y_5 y_5) (- 1)) (+ (* x_4 x_4) (* y_4 y_4) (- 1)))
  )
)
(check-sat)
(exit)
