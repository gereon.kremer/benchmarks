(set-logic QF_NRA)
(set-info :source |
Non-linear optimization task
Ulrich Loup <loup@cs.rwth-aachen.de>
|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun x_0 () Real)
(declare-fun x_1 () Real)
(declare-fun x_2 () Real)
(declare-fun x_3 () Real)
(declare-fun x_4 () Real)
(declare-fun x_5 () Real)
(declare-fun x_6 () Real)
(assert
  (and
    (<= (+ x_0 (- 2)) 0) (>= (+ x_0 2) 0) (<= (+ x_1 (- 2)) 0) (>= (+ x_1 2) 0) (< (+ (* x_1 x_1) (- 1)) (+ (* x_0 x_0) (- 1))) (<= (+ x_2 (- 2)) 0) (>= (+ x_2 2) 0) (< (+ (* x_2 x_2) (- 1)) (+ (* x_1 x_1) (- 1))) (<= (+ x_3 (- 2)) 0) (>= (+ x_3 2) 0) (< (+ (* x_3 x_3) (- 1)) (+ (* x_2 x_2) (- 1))) (<= (+ x_4 (- 2)) 0) (>= (+ x_4 2) 0) (< (+ (* x_4 x_4) (- 1)) (+ (* x_3 x_3) (- 1))) (<= (+ x_5 (- 2)) 0) (>= (+ x_5 2) 0) (< (+ (* x_5 x_5) (- 1)) (+ (* x_4 x_4) (- 1))) (<= (+ x_6 (- 2)) 0) (>= (+ x_6 2) 0) (< (+ (* x_6 x_6) (- 1)) (+ (* x_5 x_5) (- 1)))
  )
)
(check-sat)
(exit)
