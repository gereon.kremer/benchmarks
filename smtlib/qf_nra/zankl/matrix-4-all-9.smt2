(set-logic QF_NRA)
(set-info :source |
From termination analysis of term rewriting.

Submitted by Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status unknown)
(declare-fun x145 () Real)
(declare-fun x6 () Real)
(declare-fun x84 () Real)
(declare-fun x162 () Real)
(declare-fun x23 () Real)
(declare-fun x101 () Real)
(declare-fun x179 () Real)
(declare-fun x40 () Real)
(declare-fun x118 () Real)
(declare-fun x57 () Real)
(declare-fun x135 () Real)
(declare-fun x74 () Real)
(declare-fun x152 () Real)
(declare-fun x13 () Real)
(declare-fun x91 () Real)
(declare-fun x169 () Real)
(declare-fun x30 () Real)
(declare-fun x108 () Real)
(declare-fun x186 () Real)
(declare-fun x47 () Real)
(declare-fun x125 () Real)
(declare-fun x64 () Real)
(declare-fun x142 () Real)
(declare-fun x3 () Real)
(declare-fun x81 () Real)
(declare-fun x159 () Real)
(declare-fun x20 () Real)
(declare-fun x98 () Real)
(declare-fun x176 () Real)
(declare-fun x37 () Real)
(declare-fun x115 () Real)
(declare-fun x54 () Real)
(declare-fun x132 () Real)
(declare-fun x71 () Real)
(declare-fun x149 () Real)
(declare-fun x10 () Real)
(declare-fun x88 () Real)
(declare-fun x166 () Real)
(declare-fun x27 () Real)
(declare-fun x105 () Real)
(declare-fun x183 () Real)
(declare-fun x44 () Real)
(declare-fun x122 () Real)
(declare-fun x61 () Real)
(declare-fun x139 () Real)
(declare-fun x0 () Real)
(declare-fun x78 () Real)
(declare-fun x156 () Real)
(declare-fun x17 () Real)
(declare-fun x95 () Real)
(declare-fun x173 () Real)
(declare-fun x34 () Real)
(declare-fun x112 () Real)
(declare-fun x190 () Real)
(declare-fun x51 () Real)
(declare-fun x129 () Real)
(declare-fun x68 () Real)
(declare-fun x146 () Real)
(declare-fun x7 () Real)
(declare-fun x85 () Real)
(declare-fun x163 () Real)
(declare-fun x24 () Real)
(declare-fun x102 () Real)
(declare-fun x180 () Real)
(declare-fun x41 () Real)
(declare-fun x119 () Real)
(declare-fun x58 () Real)
(declare-fun x136 () Real)
(declare-fun x75 () Real)
(declare-fun x153 () Real)
(declare-fun x14 () Real)
(declare-fun x92 () Real)
(declare-fun x170 () Real)
(declare-fun x31 () Real)
(declare-fun x109 () Real)
(declare-fun x187 () Real)
(declare-fun x48 () Real)
(declare-fun x126 () Real)
(declare-fun x65 () Real)
(declare-fun x143 () Real)
(declare-fun x4 () Real)
(declare-fun x82 () Real)
(declare-fun x160 () Real)
(declare-fun x21 () Real)
(declare-fun x99 () Real)
(declare-fun x177 () Real)
(declare-fun x38 () Real)
(declare-fun x116 () Real)
(declare-fun x55 () Real)
(declare-fun x133 () Real)
(declare-fun x72 () Real)
(declare-fun x150 () Real)
(declare-fun x11 () Real)
(declare-fun x89 () Real)
(declare-fun x167 () Real)
(declare-fun x28 () Real)
(declare-fun x106 () Real)
(declare-fun x184 () Real)
(declare-fun x45 () Real)
(declare-fun x123 () Real)
(declare-fun x62 () Real)
(declare-fun x140 () Real)
(declare-fun x1 () Real)
(declare-fun x79 () Real)
(declare-fun x157 () Real)
(declare-fun x18 () Real)
(declare-fun x96 () Real)
(declare-fun x174 () Real)
(declare-fun x35 () Real)
(declare-fun x113 () Real)
(declare-fun x191 () Real)
(declare-fun x52 () Real)
(declare-fun x130 () Real)
(declare-fun x69 () Real)
(declare-fun x147 () Real)
(declare-fun x8 () Real)
(declare-fun x86 () Real)
(declare-fun x164 () Real)
(declare-fun x25 () Real)
(declare-fun x103 () Real)
(declare-fun x181 () Real)
(declare-fun x42 () Real)
(declare-fun x120 () Real)
(declare-fun x59 () Real)
(declare-fun x137 () Real)
(declare-fun x76 () Real)
(declare-fun x154 () Real)
(declare-fun x15 () Real)
(declare-fun x93 () Real)
(declare-fun x171 () Real)
(declare-fun x32 () Real)
(declare-fun x110 () Real)
(declare-fun x188 () Real)
(declare-fun x49 () Real)
(declare-fun x127 () Real)
(declare-fun x66 () Real)
(declare-fun x144 () Real)
(declare-fun x5 () Real)
(declare-fun x83 () Real)
(declare-fun x161 () Real)
(declare-fun x22 () Real)
(declare-fun x100 () Real)
(declare-fun x178 () Real)
(declare-fun x39 () Real)
(declare-fun x117 () Real)
(declare-fun x56 () Real)
(declare-fun x134 () Real)
(declare-fun x73 () Real)
(declare-fun x151 () Real)
(declare-fun x12 () Real)
(declare-fun x90 () Real)
(declare-fun x168 () Real)
(declare-fun x29 () Real)
(declare-fun x107 () Real)
(declare-fun x185 () Real)
(declare-fun x46 () Real)
(declare-fun x124 () Real)
(declare-fun x63 () Real)
(declare-fun x141 () Real)
(declare-fun x2 () Real)
(declare-fun x80 () Real)
(declare-fun x158 () Real)
(declare-fun x19 () Real)
(declare-fun x97 () Real)
(declare-fun x175 () Real)
(declare-fun x36 () Real)
(declare-fun x114 () Real)
(declare-fun x192 () Real)
(declare-fun x53 () Real)
(declare-fun x131 () Real)
(declare-fun x70 () Real)
(declare-fun x148 () Real)
(declare-fun x9 () Real)
(declare-fun x87 () Real)
(declare-fun x165 () Real)
(declare-fun x26 () Real)
(declare-fun x104 () Real)
(declare-fun x182 () Real)
(declare-fun x43 () Real)
(declare-fun x121 () Real)
(declare-fun x60 () Real)
(declare-fun x138 () Real)
(declare-fun x77 () Real)
(declare-fun x155 () Real)
(declare-fun x16 () Real)
(declare-fun x94 () Real)
(declare-fun x172 () Real)
(declare-fun x33 () Real)
(declare-fun x111 () Real)
(declare-fun x189 () Real)
(declare-fun x50 () Real)
(declare-fun x128 () Real)
(declare-fun x67 () Real)
(assert (>= x145 0))
(assert (>= x6 0))
(assert (>= x84 0))
(assert (>= x162 0))
(assert (>= x23 0))
(assert (>= x101 0))
(assert (>= x179 0))
(assert (>= x40 0))
(assert (>= x118 0))
(assert (>= x57 0))
(assert (>= x135 0))
(assert (>= x74 0))
(assert (>= x152 0))
(assert (>= x13 0))
(assert (>= x91 0))
(assert (>= x169 0))
(assert (>= x30 0))
(assert (>= x108 0))
(assert (>= x186 0))
(assert (>= x47 0))
(assert (>= x125 0))
(assert (>= x64 0))
(assert (>= x142 0))
(assert (>= x3 0))
(assert (>= x81 0))
(assert (>= x159 0))
(assert (>= x20 0))
(assert (>= x98 0))
(assert (>= x176 0))
(assert (>= x37 0))
(assert (>= x115 0))
(assert (>= x54 0))
(assert (>= x132 0))
(assert (>= x71 0))
(assert (>= x149 0))
(assert (>= x10 0))
(assert (>= x88 0))
(assert (>= x166 0))
(assert (>= x27 0))
(assert (>= x105 0))
(assert (>= x183 0))
(assert (>= x44 0))
(assert (>= x122 0))
(assert (>= x61 0))
(assert (>= x139 0))
(assert (>= x0 0))
(assert (>= x78 0))
(assert (>= x156 0))
(assert (>= x17 0))
(assert (>= x95 0))
(assert (>= x173 0))
(assert (>= x34 0))
(assert (>= x112 0))
(assert (>= x190 0))
(assert (>= x51 0))
(assert (>= x129 0))
(assert (>= x68 0))
(assert (>= x146 0))
(assert (>= x7 0))
(assert (>= x85 0))
(assert (>= x163 0))
(assert (>= x24 0))
(assert (>= x102 0))
(assert (>= x180 0))
(assert (>= x41 0))
(assert (>= x119 0))
(assert (>= x58 0))
(assert (>= x136 0))
(assert (>= x75 0))
(assert (>= x153 0))
(assert (>= x14 0))
(assert (>= x92 0))
(assert (>= x170 0))
(assert (>= x31 0))
(assert (>= x109 0))
(assert (>= x187 0))
(assert (>= x48 0))
(assert (>= x126 0))
(assert (>= x65 0))
(assert (>= x143 0))
(assert (>= x4 0))
(assert (>= x82 0))
(assert (>= x160 0))
(assert (>= x21 0))
(assert (>= x99 0))
(assert (>= x177 0))
(assert (>= x38 0))
(assert (>= x116 0))
(assert (>= x55 0))
(assert (>= x133 0))
(assert (>= x72 0))
(assert (>= x150 0))
(assert (>= x11 0))
(assert (>= x89 0))
(assert (>= x167 0))
(assert (>= x28 0))
(assert (>= x106 0))
(assert (>= x184 0))
(assert (>= x45 0))
(assert (>= x123 0))
(assert (>= x62 0))
(assert (>= x140 0))
(assert (>= x1 0))
(assert (>= x79 0))
(assert (>= x157 0))
(assert (>= x18 0))
(assert (>= x96 0))
(assert (>= x174 0))
(assert (>= x35 0))
(assert (>= x113 0))
(assert (>= x191 0))
(assert (>= x52 0))
(assert (>= x130 0))
(assert (>= x69 0))
(assert (>= x147 0))
(assert (>= x8 0))
(assert (>= x86 0))
(assert (>= x164 0))
(assert (>= x25 0))
(assert (>= x103 0))
(assert (>= x181 0))
(assert (>= x42 0))
(assert (>= x120 0))
(assert (>= x59 0))
(assert (>= x137 0))
(assert (>= x76 0))
(assert (>= x154 0))
(assert (>= x15 0))
(assert (>= x93 0))
(assert (>= x171 0))
(assert (>= x32 0))
(assert (>= x110 0))
(assert (>= x188 0))
(assert (>= x49 0))
(assert (>= x127 0))
(assert (>= x66 0))
(assert (>= x144 0))
(assert (>= x5 0))
(assert (>= x83 0))
(assert (>= x161 0))
(assert (>= x22 0))
(assert (>= x100 0))
(assert (>= x178 0))
(assert (>= x39 0))
(assert (>= x117 0))
(assert (>= x56 0))
(assert (>= x134 0))
(assert (>= x73 0))
(assert (>= x151 0))
(assert (>= x12 0))
(assert (>= x90 0))
(assert (>= x168 0))
(assert (>= x29 0))
(assert (>= x107 0))
(assert (>= x185 0))
(assert (>= x46 0))
(assert (>= x124 0))
(assert (>= x63 0))
(assert (>= x141 0))
(assert (>= x2 0))
(assert (>= x80 0))
(assert (>= x158 0))
(assert (>= x19 0))
(assert (>= x97 0))
(assert (>= x175 0))
(assert (>= x36 0))
(assert (>= x114 0))
(assert (>= x192 0))
(assert (>= x53 0))
(assert (>= x131 0))
(assert (>= x70 0))
(assert (>= x148 0))
(assert (>= x9 0))
(assert (>= x87 0))
(assert (>= x165 0))
(assert (>= x26 0))
(assert (>= x104 0))
(assert (>= x182 0))
(assert (>= x43 0))
(assert (>= x121 0))
(assert (>= x60 0))
(assert (>= x138 0))
(assert (>= x77 0))
(assert (>= x155 0))
(assert (>= x16 0))
(assert (>= x94 0))
(assert (>= x172 0))
(assert (>= x33 0))
(assert (>= x111 0))
(assert (>= x189 0))
(assert (>= x50 0))
(assert (>= x128 0))
(assert (>= x67 0))
(assert (let ((?v_0 (+ (+ (+ (* x25 x5) (* x26 x6)) (* x27 x7)) (* x28 x8)))) (let ((?v_2 (+ (+ x0 (+ (+ (+ (* x1 x29) (* x2 x30)) (* x3 x31)) (* x4 x32))) ?v_0)) (?v_7 (+ x0 (+ (+ (+ (* x1 x5) (* x2 x6)) (* x3 x7)) (* x4 x8))))) (let ((?v_1 (+ ?v_7 ?v_0)) (?v_9 (+ (+ (+ (* x1 x9) (* x2 x13)) (* x3 x17)) (* x4 x21))) (?v_10 (+ (+ (+ (* x1 x10) (* x2 x14)) (* x3 x18)) (* x4 x22))) (?v_11 (+ (+ (+ (* x1 x11) (* x2 x15)) (* x3 x19)) (* x4 x23))) (?v_12 (+ (+ (+ (* x1 x12) (* x2 x16)) (* x3 x20)) (* x4 x24))) (?v_3 (+ (+ (+ (* x25 x9) (* x26 x13)) (* x27 x17)) (* x28 x21))) (?v_4 (+ (+ (+ (* x25 x10) (* x26 x14)) (* x27 x18)) (* x28 x22))) (?v_5 (+ (+ (+ (* x25 x11) (* x26 x15)) (* x27 x19)) (* x28 x23))) (?v_6 (+ (+ (+ (* x25 x12) (* x26 x16)) (* x27 x20)) (* x28 x24))) (?v_8 (+ ?v_7 (+ (+ (+ (* x25 x29) (* x26 x30)) (* x27 x31)) (* x28 x32))))) (let ((?v_129 (and (and (and (and (> ?v_1 ?v_2) (>= ?v_1 ?v_2)) (and (and (and (>= ?v_9 (+ (+ (+ (* x1 x33) (* x2 x37)) (* x3 x41)) (* x4 x45))) (>= ?v_10 (+ (+ (+ (* x1 x34) (* x2 x38)) (* x3 x42)) (* x4 x46)))) (>= ?v_11 (+ (+ (+ (* x1 x35) (* x2 x39)) (* x3 x43)) (* x4 x47)))) (>= ?v_12 (+ (+ (+ (* x1 x36) (* x2 x40)) (* x3 x44)) (* x4 x48))))) (and (and (and (>= ?v_3 (+ (+ (+ (+ (* x1 x49) (* x2 x53)) (* x3 x57)) (* x4 x61)) ?v_3)) (>= ?v_4 (+ (+ (+ (+ (* x1 x50) (* x2 x54)) (* x3 x58)) (* x4 x62)) ?v_4))) (>= ?v_5 (+ (+ (+ (+ (* x1 x51) (* x2 x55)) (* x3 x59)) (* x4 x63)) ?v_5))) (>= ?v_6 (+ (+ (+ (+ (* x1 x52) (* x2 x56)) (* x3 x60)) (* x4 x64)) ?v_6)))) (and (and (and (> ?v_1 ?v_8) (>= ?v_1 ?v_8)) (and (and (and (>= ?v_9 (+ ?v_9 (+ (+ (+ (* x25 x49) (* x26 x53)) (* x27 x57)) (* x28 x61)))) (>= ?v_10 (+ ?v_10 (+ (+ (+ (* x25 x50) (* x26 x54)) (* x27 x58)) (* x28 x62))))) (>= ?v_11 (+ ?v_11 (+ (+ (+ (* x25 x51) (* x26 x55)) (* x27 x59)) (* x28 x63))))) (>= ?v_12 (+ ?v_12 (+ (+ (+ (* x25 x52) (* x26 x56)) (* x27 x60)) (* x28 x64)))))) (and (and (and (>= ?v_3 (+ (+ (+ (* x25 x33) (* x26 x37)) (* x27 x41)) (* x28 x45))) (>= ?v_4 (+ (+ (+ (* x25 x34) (* x26 x38)) (* x27 x42)) (* x28 x46)))) (>= ?v_5 (+ (+ (+ (* x25 x35) (* x26 x39)) (* x27 x43)) (* x28 x47)))) (>= ?v_6 (+ (+ (+ (* x25 x36) (* x26 x40)) (* x27 x44)) (* x28 x48))))))) (?v_13 (+ x65 (+ (+ (+ (* x85 x101) (* x86 x102)) (* x87 x103)) (* x88 x104)))) (?v_14 (+ x65 (+ (+ (+ (* x69 x101) (* x70 x102)) (* x71 x103)) (* x72 x104)))) (?v_15 (+ x65 (+ (+ (+ (* x69 x5) (* x70 x6)) (* x71 x7)) (* x72 x8))))) (let ((?v_21 (+ ?v_15 (+ (+ (+ (* x85 x29) (* x86 x30)) (* x87 x31)) (* x88 x32)))) (?v_19 (+ x66 (+ (+ (+ (* x73 x5) (* x74 x6)) (* x75 x7)) (* x76 x8))))) (let ((?v_22 (+ ?v_19 (+ (+ (+ (* x89 x29) (* x90 x30)) (* x91 x31)) (* x92 x32)))) (?v_29 (+ x67 (+ (+ (+ (* x77 x5) (* x78 x6)) (* x79 x7)) (* x80 x8))))) (let ((?v_23 (+ ?v_29 (+ (+ (+ (* x93 x29) (* x94 x30)) (* x95 x31)) (* x96 x32)))) (?v_31 (+ x68 (+ (+ (+ (* x81 x5) (* x82 x6)) (* x83 x7)) (* x84 x8))))) (let ((?v_24 (+ ?v_31 (+ (+ (+ (* x97 x29) (* x98 x30)) (* x99 x31)) (* x100 x32)))) (?v_16 (+ (+ (+ (* x85 x5) (* x86 x6)) (* x87 x7)) (* x88 x8)))) (let ((?v_25 (+ (+ x65 (+ (+ (+ (* x69 x29) (* x70 x30)) (* x71 x31)) (* x72 x32))) ?v_16)) (?v_20 (+ (+ (+ (* x89 x5) (* x90 x6)) (* x91 x7)) (* x92 x8)))) (let ((?v_26 (+ (+ x66 (+ (+ (+ (* x73 x29) (* x74 x30)) (* x75 x31)) (* x76 x32))) ?v_20)) (?v_30 (+ (+ (+ (* x93 x5) (* x94 x6)) (* x95 x7)) (* x96 x8)))) (let ((?v_27 (+ (+ x67 (+ (+ (+ (* x77 x29) (* x78 x30)) (* x79 x31)) (* x80 x32))) ?v_30)) (?v_32 (+ (+ (+ (* x97 x5) (* x98 x6)) (* x99 x7)) (* x100 x8)))) (let ((?v_28 (+ (+ x68 (+ (+ (+ (* x81 x29) (* x82 x30)) (* x83 x31)) (* x84 x32))) ?v_32))) (let ((?v_18 (+ (+ (+ x105 (+ (+ (+ (* x109 x125) (* x110 x126)) (* x111 x127)) (* x112 x128))) (+ (+ (+ (* x161 ?v_21) (* x162 ?v_22)) (* x163 ?v_23)) (* x164 ?v_24))) (+ (+ (+ (* x177 ?v_25) (* x178 ?v_26)) (* x179 ?v_27)) (* x180 ?v_28)))) (?v_17 (+ ?v_15 ?v_16)) (?v_33 (+ (+ (+ (* x69 x9) (* x70 x13)) (* x71 x17)) (* x72 x21)))) (let ((?v_38 (+ ?v_33 (+ (+ (+ (* x85 x49) (* x86 x53)) (* x87 x57)) (* x88 x61)))) (?v_37 (+ (+ (+ (* x73 x9) (* x74 x13)) (* x75 x17)) (* x76 x21)))) (let ((?v_39 (+ ?v_37 (+ (+ (+ (* x89 x49) (* x90 x53)) (* x91 x57)) (* x92 x61)))) (?v_73 (+ (+ (+ (* x77 x9) (* x78 x13)) (* x79 x17)) (* x80 x21)))) (let ((?v_40 (+ ?v_73 (+ (+ (+ (* x93 x49) (* x94 x53)) (* x95 x57)) (* x96 x61)))) (?v_77 (+ (+ (+ (* x81 x9) (* x82 x13)) (* x83 x17)) (* x84 x21)))) (let ((?v_41 (+ ?v_77 (+ (+ (+ (* x97 x49) (* x98 x53)) (* x99 x57)) (* x100 x61)))) (?v_42 (+ (+ (+ (* x69 x33) (* x70 x37)) (* x71 x41)) (* x72 x45))) (?v_43 (+ (+ (+ (* x73 x33) (* x74 x37)) (* x75 x41)) (* x76 x45))) (?v_44 (+ (+ (+ (* x77 x33) (* x78 x37)) (* x79 x41)) (* x80 x45))) (?v_45 (+ (+ (+ (* x81 x33) (* x82 x37)) (* x83 x41)) (* x84 x45))) (?v_34 (+ (+ (+ (* x69 x10) (* x70 x14)) (* x71 x18)) (* x72 x22)))) (let ((?v_47 (+ ?v_34 (+ (+ (+ (* x85 x50) (* x86 x54)) (* x87 x58)) (* x88 x62)))) (?v_46 (+ (+ (+ (* x73 x10) (* x74 x14)) (* x75 x18)) (* x76 x22)))) (let ((?v_48 (+ ?v_46 (+ (+ (+ (* x89 x50) (* x90 x54)) (* x91 x58)) (* x92 x62)))) (?v_74 (+ (+ (+ (* x77 x10) (* x78 x14)) (* x79 x18)) (* x80 x22)))) (let ((?v_49 (+ ?v_74 (+ (+ (+ (* x93 x50) (* x94 x54)) (* x95 x58)) (* x96 x62)))) (?v_78 (+ (+ (+ (* x81 x10) (* x82 x14)) (* x83 x18)) (* x84 x22)))) (let ((?v_50 (+ ?v_78 (+ (+ (+ (* x97 x50) (* x98 x54)) (* x99 x58)) (* x100 x62)))) (?v_51 (+ (+ (+ (* x69 x34) (* x70 x38)) (* x71 x42)) (* x72 x46))) (?v_52 (+ (+ (+ (* x73 x34) (* x74 x38)) (* x75 x42)) (* x76 x46))) (?v_53 (+ (+ (+ (* x77 x34) (* x78 x38)) (* x79 x42)) (* x80 x46))) (?v_54 (+ (+ (+ (* x81 x34) (* x82 x38)) (* x83 x42)) (* x84 x46))) (?v_35 (+ (+ (+ (* x69 x11) (* x70 x15)) (* x71 x19)) (* x72 x23)))) (let ((?v_56 (+ ?v_35 (+ (+ (+ (* x85 x51) (* x86 x55)) (* x87 x59)) (* x88 x63)))) (?v_55 (+ (+ (+ (* x73 x11) (* x74 x15)) (* x75 x19)) (* x76 x23)))) (let ((?v_57 (+ ?v_55 (+ (+ (+ (* x89 x51) (* x90 x55)) (* x91 x59)) (* x92 x63)))) (?v_75 (+ (+ (+ (* x77 x11) (* x78 x15)) (* x79 x19)) (* x80 x23)))) (let ((?v_58 (+ ?v_75 (+ (+ (+ (* x93 x51) (* x94 x55)) (* x95 x59)) (* x96 x63)))) (?v_79 (+ (+ (+ (* x81 x11) (* x82 x15)) (* x83 x19)) (* x84 x23)))) (let ((?v_59 (+ ?v_79 (+ (+ (+ (* x97 x51) (* x98 x55)) (* x99 x59)) (* x100 x63)))) (?v_60 (+ (+ (+ (* x69 x35) (* x70 x39)) (* x71 x43)) (* x72 x47))) (?v_61 (+ (+ (+ (* x73 x35) (* x74 x39)) (* x75 x43)) (* x76 x47))) (?v_62 (+ (+ (+ (* x77 x35) (* x78 x39)) (* x79 x43)) (* x80 x47))) (?v_63 (+ (+ (+ (* x81 x35) (* x82 x39)) (* x83 x43)) (* x84 x47))) (?v_36 (+ (+ (+ (* x69 x12) (* x70 x16)) (* x71 x20)) (* x72 x24)))) (let ((?v_65 (+ ?v_36 (+ (+ (+ (* x85 x52) (* x86 x56)) (* x87 x60)) (* x88 x64)))) (?v_64 (+ (+ (+ (* x73 x12) (* x74 x16)) (* x75 x20)) (* x76 x24)))) (let ((?v_66 (+ ?v_64 (+ (+ (+ (* x89 x52) (* x90 x56)) (* x91 x60)) (* x92 x64)))) (?v_76 (+ (+ (+ (* x77 x12) (* x78 x16)) (* x79 x20)) (* x80 x24)))) (let ((?v_67 (+ ?v_76 (+ (+ (+ (* x93 x52) (* x94 x56)) (* x95 x60)) (* x96 x64)))) (?v_80 (+ (+ (+ (* x81 x12) (* x82 x16)) (* x83 x20)) (* x84 x24)))) (let ((?v_68 (+ ?v_80 (+ (+ (+ (* x97 x52) (* x98 x56)) (* x99 x60)) (* x100 x64)))) (?v_69 (+ (+ (+ (* x69 x36) (* x70 x40)) (* x71 x44)) (* x72 x48))) (?v_70 (+ (+ (+ (* x73 x36) (* x74 x40)) (* x75 x44)) (* x76 x48))) (?v_71 (+ (+ (+ (* x77 x36) (* x78 x40)) (* x79 x44)) (* x80 x48))) (?v_72 (+ (+ (+ (* x81 x36) (* x82 x40)) (* x83 x44)) (* x84 x48))) (?v_86 (+ (+ (+ (* x85 x33) (* x86 x37)) (* x87 x41)) (* x88 x45))) (?v_87 (+ (+ (+ (* x89 x33) (* x90 x37)) (* x91 x41)) (* x92 x45))) (?v_88 (+ (+ (+ (* x93 x33) (* x94 x37)) (* x95 x41)) (* x96 x45))) (?v_89 (+ (+ (+ (* x97 x33) (* x98 x37)) (* x99 x41)) (* x100 x45))) (?v_81 (+ (+ (+ (* x85 x9) (* x86 x13)) (* x87 x17)) (* x88 x21)))) (let ((?v_90 (+ (+ (+ (+ (* x69 x49) (* x70 x53)) (* x71 x57)) (* x72 x61)) ?v_81)) (?v_85 (+ (+ (+ (* x89 x9) (* x90 x13)) (* x91 x17)) (* x92 x21)))) (let ((?v_91 (+ (+ (+ (+ (* x73 x49) (* x74 x53)) (* x75 x57)) (* x76 x61)) ?v_85)) (?v_121 (+ (+ (+ (* x93 x9) (* x94 x13)) (* x95 x17)) (* x96 x21)))) (let ((?v_92 (+ (+ (+ (+ (* x77 x49) (* x78 x53)) (* x79 x57)) (* x80 x61)) ?v_121)) (?v_125 (+ (+ (+ (* x97 x9) (* x98 x13)) (* x99 x17)) (* x100 x21)))) (let ((?v_93 (+ (+ (+ (+ (* x81 x49) (* x82 x53)) (* x83 x57)) (* x84 x61)) ?v_125)) (?v_95 (+ (+ (+ (* x85 x34) (* x86 x38)) (* x87 x42)) (* x88 x46))) (?v_96 (+ (+ (+ (* x89 x34) (* x90 x38)) (* x91 x42)) (* x92 x46))) (?v_97 (+ (+ (+ (* x93 x34) (* x94 x38)) (* x95 x42)) (* x96 x46))) (?v_98 (+ (+ (+ (* x97 x34) (* x98 x38)) (* x99 x42)) (* x100 x46))) (?v_82 (+ (+ (+ (* x85 x10) (* x86 x14)) (* x87 x18)) (* x88 x22)))) (let ((?v_99 (+ (+ (+ (+ (* x69 x50) (* x70 x54)) (* x71 x58)) (* x72 x62)) ?v_82)) (?v_94 (+ (+ (+ (* x89 x10) (* x90 x14)) (* x91 x18)) (* x92 x22)))) (let ((?v_100 (+ (+ (+ (+ (* x73 x50) (* x74 x54)) (* x75 x58)) (* x76 x62)) ?v_94)) (?v_122 (+ (+ (+ (* x93 x10) (* x94 x14)) (* x95 x18)) (* x96 x22)))) (let ((?v_101 (+ (+ (+ (+ (* x77 x50) (* x78 x54)) (* x79 x58)) (* x80 x62)) ?v_122)) (?v_126 (+ (+ (+ (* x97 x10) (* x98 x14)) (* x99 x18)) (* x100 x22)))) (let ((?v_102 (+ (+ (+ (+ (* x81 x50) (* x82 x54)) (* x83 x58)) (* x84 x62)) ?v_126)) (?v_104 (+ (+ (+ (* x85 x35) (* x86 x39)) (* x87 x43)) (* x88 x47))) (?v_105 (+ (+ (+ (* x89 x35) (* x90 x39)) (* x91 x43)) (* x92 x47))) (?v_106 (+ (+ (+ (* x93 x35) (* x94 x39)) (* x95 x43)) (* x96 x47))) (?v_107 (+ (+ (+ (* x97 x35) (* x98 x39)) (* x99 x43)) (* x100 x47))) (?v_83 (+ (+ (+ (* x85 x11) (* x86 x15)) (* x87 x19)) (* x88 x23)))) (let ((?v_108 (+ (+ (+ (+ (* x69 x51) (* x70 x55)) (* x71 x59)) (* x72 x63)) ?v_83)) (?v_103 (+ (+ (+ (* x89 x11) (* x90 x15)) (* x91 x19)) (* x92 x23)))) (let ((?v_109 (+ (+ (+ (+ (* x73 x51) (* x74 x55)) (* x75 x59)) (* x76 x63)) ?v_103)) (?v_123 (+ (+ (+ (* x93 x11) (* x94 x15)) (* x95 x19)) (* x96 x23)))) (let ((?v_110 (+ (+ (+ (+ (* x77 x51) (* x78 x55)) (* x79 x59)) (* x80 x63)) ?v_123)) (?v_127 (+ (+ (+ (* x97 x11) (* x98 x15)) (* x99 x19)) (* x100 x23)))) (let ((?v_111 (+ (+ (+ (+ (* x81 x51) (* x82 x55)) (* x83 x59)) (* x84 x63)) ?v_127)) (?v_113 (+ (+ (+ (* x85 x36) (* x86 x40)) (* x87 x44)) (* x88 x48))) (?v_114 (+ (+ (+ (* x89 x36) (* x90 x40)) (* x91 x44)) (* x92 x48))) (?v_115 (+ (+ (+ (* x93 x36) (* x94 x40)) (* x95 x44)) (* x96 x48))) (?v_116 (+ (+ (+ (* x97 x36) (* x98 x40)) (* x99 x44)) (* x100 x48))) (?v_84 (+ (+ (+ (* x85 x12) (* x86 x16)) (* x87 x20)) (* x88 x24)))) (let ((?v_117 (+ (+ (+ (+ (* x69 x52) (* x70 x56)) (* x71 x60)) (* x72 x64)) ?v_84)) (?v_112 (+ (+ (+ (* x89 x12) (* x90 x16)) (* x91 x20)) (* x92 x24)))) (let ((?v_118 (+ (+ (+ (+ (* x73 x52) (* x74 x56)) (* x75 x60)) (* x76 x64)) ?v_112)) (?v_124 (+ (+ (+ (* x93 x12) (* x94 x16)) (* x95 x20)) (* x96 x24)))) (let ((?v_119 (+ (+ (+ (+ (* x77 x52) (* x78 x56)) (* x79 x60)) (* x80 x64)) ?v_124)) (?v_128 (+ (+ (+ (* x97 x12) (* x98 x16)) (* x99 x20)) (* x100 x24)))) (let ((?v_120 (+ (+ (+ (+ (* x81 x52) (* x82 x56)) (* x83 x60)) (* x84 x64)) ?v_128))) (and (and (and (and ?v_129 (and (and (> ?v_13 0) (and (and (and (>= ?v_13 0) (>= (+ x66 (+ (+ (+ (* x89 x101) (* x90 x102)) (* x91 x103)) (* x92 x104))) 0)) (>= (+ x67 (+ (+ (+ (* x93 x101) (* x94 x102)) (* x95 x103)) (* x96 x104))) 0)) (>= (+ x68 (+ (+ (+ (* x97 x101) (* x98 x102)) (* x99 x103)) (* x100 x104))) 0))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x69 1) (>= x70 0)) (>= x71 0)) (>= x72 0)) (>= x73 0)) (>= x74 1)) (>= x75 0)) (>= x76 0)) (>= x77 0)) (>= x78 0)) (>= x79 1)) (>= x80 0)) (>= x81 0)) (>= x82 0)) (>= x83 0)) (>= x84 1)))) (and (and (> ?v_14 0) (and (and (and (>= ?v_14 0) (>= (+ x66 (+ (+ (+ (* x73 x101) (* x74 x102)) (* x75 x103)) (* x76 x104))) 0)) (>= (+ x67 (+ (+ (+ (* x77 x101) (* x78 x102)) (* x79 x103)) (* x80 x104))) 0)) (>= (+ x68 (+ (+ (+ (* x81 x101) (* x82 x102)) (* x83 x103)) (* x84 x104))) 0))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x85 1) (>= x86 0)) (>= x87 0)) (>= x88 0)) (>= x89 0)) (>= x90 1)) (>= x91 0)) (>= x92 0)) (>= x93 0)) (>= x94 0)) (>= x95 1)) (>= x96 0)) (>= x97 0)) (>= x98 0)) (>= x99 0)) (>= x100 1)))) (and (and (and (> ?v_17 ?v_18) (and (and (and (>= ?v_17 ?v_18) (>= (+ ?v_19 ?v_20) (+ (+ (+ x106 (+ (+ (+ (* x113 x125) (* x114 x126)) (* x115 x127)) (* x116 x128))) (+ (+ (+ (* x165 ?v_21) (* x166 ?v_22)) (* x167 ?v_23)) (* x168 ?v_24))) (+ (+ (+ (* x181 ?v_25) (* x182 ?v_26)) (* x183 ?v_27)) (* x184 ?v_28))))) (>= (+ ?v_29 ?v_30) (+ (+ (+ x107 (+ (+ (+ (* x117 x125) (* x118 x126)) (* x119 x127)) (* x120 x128))) (+ (+ (+ (* x169 ?v_21) (* x170 ?v_22)) (* x171 ?v_23)) (* x172 ?v_24))) (+ (+ (+ (* x185 ?v_25) (* x186 ?v_26)) (* x187 ?v_27)) (* x188 ?v_28))))) (>= (+ ?v_31 ?v_32) (+ (+ (+ x108 (+ (+ (+ (* x121 x125) (* x122 x126)) (* x123 x127)) (* x124 x128))) (+ (+ (+ (* x173 ?v_21) (* x174 ?v_22)) (* x175 ?v_23)) (* x176 ?v_24))) (+ (+ (+ (* x189 ?v_25) (* x190 ?v_26)) (* x191 ?v_27)) (* x192 ?v_28)))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= ?v_33 (+ (+ (+ (+ (+ (* x109 x129) (* x110 x133)) (* x111 x137)) (* x112 x141)) (+ (+ (+ (* x161 ?v_38) (* x162 ?v_39)) (* x163 ?v_40)) (* x164 ?v_41))) (+ (+ (+ (* x177 ?v_42) (* x178 ?v_43)) (* x179 ?v_44)) (* x180 ?v_45)))) (>= ?v_34 (+ (+ (+ (+ (+ (* x109 x130) (* x110 x134)) (* x111 x138)) (* x112 x142)) (+ (+ (+ (* x161 ?v_47) (* x162 ?v_48)) (* x163 ?v_49)) (* x164 ?v_50))) (+ (+ (+ (* x177 ?v_51) (* x178 ?v_52)) (* x179 ?v_53)) (* x180 ?v_54))))) (>= ?v_35 (+ (+ (+ (+ (+ (* x109 x131) (* x110 x135)) (* x111 x139)) (* x112 x143)) (+ (+ (+ (* x161 ?v_56) (* x162 ?v_57)) (* x163 ?v_58)) (* x164 ?v_59))) (+ (+ (+ (* x177 ?v_60) (* x178 ?v_61)) (* x179 ?v_62)) (* x180 ?v_63))))) (>= ?v_36 (+ (+ (+ (+ (+ (* x109 x132) (* x110 x136)) (* x111 x140)) (* x112 x144)) (+ (+ (+ (* x161 ?v_65) (* x162 ?v_66)) (* x163 ?v_67)) (* x164 ?v_68))) (+ (+ (+ (* x177 ?v_69) (* x178 ?v_70)) (* x179 ?v_71)) (* x180 ?v_72))))) (>= ?v_37 (+ (+ (+ (+ (+ (* x113 x129) (* x114 x133)) (* x115 x137)) (* x116 x141)) (+ (+ (+ (* x165 ?v_38) (* x166 ?v_39)) (* x167 ?v_40)) (* x168 ?v_41))) (+ (+ (+ (* x181 ?v_42) (* x182 ?v_43)) (* x183 ?v_44)) (* x184 ?v_45))))) (>= ?v_46 (+ (+ (+ (+ (+ (* x113 x130) (* x114 x134)) (* x115 x138)) (* x116 x142)) (+ (+ (+ (* x165 ?v_47) (* x166 ?v_48)) (* x167 ?v_49)) (* x168 ?v_50))) (+ (+ (+ (* x181 ?v_51) (* x182 ?v_52)) (* x183 ?v_53)) (* x184 ?v_54))))) (>= ?v_55 (+ (+ (+ (+ (+ (* x113 x131) (* x114 x135)) (* x115 x139)) (* x116 x143)) (+ (+ (+ (* x165 ?v_56) (* x166 ?v_57)) (* x167 ?v_58)) (* x168 ?v_59))) (+ (+ (+ (* x181 ?v_60) (* x182 ?v_61)) (* x183 ?v_62)) (* x184 ?v_63))))) (>= ?v_64 (+ (+ (+ (+ (+ (* x113 x132) (* x114 x136)) (* x115 x140)) (* x116 x144)) (+ (+ (+ (* x165 ?v_65) (* x166 ?v_66)) (* x167 ?v_67)) (* x168 ?v_68))) (+ (+ (+ (* x181 ?v_69) (* x182 ?v_70)) (* x183 ?v_71)) (* x184 ?v_72))))) (>= ?v_73 (+ (+ (+ (+ (+ (* x117 x129) (* x118 x133)) (* x119 x137)) (* x120 x141)) (+ (+ (+ (* x169 ?v_38) (* x170 ?v_39)) (* x171 ?v_40)) (* x172 ?v_41))) (+ (+ (+ (* x185 ?v_42) (* x186 ?v_43)) (* x187 ?v_44)) (* x188 ?v_45))))) (>= ?v_74 (+ (+ (+ (+ (+ (* x117 x130) (* x118 x134)) (* x119 x138)) (* x120 x142)) (+ (+ (+ (* x169 ?v_47) (* x170 ?v_48)) (* x171 ?v_49)) (* x172 ?v_50))) (+ (+ (+ (* x185 ?v_51) (* x186 ?v_52)) (* x187 ?v_53)) (* x188 ?v_54))))) (>= ?v_75 (+ (+ (+ (+ (+ (* x117 x131) (* x118 x135)) (* x119 x139)) (* x120 x143)) (+ (+ (+ (* x169 ?v_56) (* x170 ?v_57)) (* x171 ?v_58)) (* x172 ?v_59))) (+ (+ (+ (* x185 ?v_60) (* x186 ?v_61)) (* x187 ?v_62)) (* x188 ?v_63))))) (>= ?v_76 (+ (+ (+ (+ (+ (* x117 x132) (* x118 x136)) (* x119 x140)) (* x120 x144)) (+ (+ (+ (* x169 ?v_65) (* x170 ?v_66)) (* x171 ?v_67)) (* x172 ?v_68))) (+ (+ (+ (* x185 ?v_69) (* x186 ?v_70)) (* x187 ?v_71)) (* x188 ?v_72))))) (>= ?v_77 (+ (+ (+ (+ (+ (* x121 x129) (* x122 x133)) (* x123 x137)) (* x124 x141)) (+ (+ (+ (* x173 ?v_38) (* x174 ?v_39)) (* x175 ?v_40)) (* x176 ?v_41))) (+ (+ (+ (* x189 ?v_42) (* x190 ?v_43)) (* x191 ?v_44)) (* x192 ?v_45))))) (>= ?v_78 (+ (+ (+ (+ (+ (* x121 x130) (* x122 x134)) (* x123 x138)) (* x124 x142)) (+ (+ (+ (* x173 ?v_47) (* x174 ?v_48)) (* x175 ?v_49)) (* x176 ?v_50))) (+ (+ (+ (* x189 ?v_51) (* x190 ?v_52)) (* x191 ?v_53)) (* x192 ?v_54))))) (>= ?v_79 (+ (+ (+ (+ (+ (* x121 x131) (* x122 x135)) (* x123 x139)) (* x124 x143)) (+ (+ (+ (* x173 ?v_56) (* x174 ?v_57)) (* x175 ?v_58)) (* x176 ?v_59))) (+ (+ (+ (* x189 ?v_60) (* x190 ?v_61)) (* x191 ?v_62)) (* x192 ?v_63))))) (>= ?v_80 (+ (+ (+ (+ (+ (* x121 x132) (* x122 x136)) (* x123 x140)) (* x124 x144)) (+ (+ (+ (* x173 ?v_65) (* x174 ?v_66)) (* x175 ?v_67)) (* x176 ?v_68))) (+ (+ (+ (* x189 ?v_69) (* x190 ?v_70)) (* x191 ?v_71)) (* x192 ?v_72)))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= ?v_81 (+ (+ (+ (+ (+ (* x109 x145) (* x110 x149)) (* x111 x153)) (* x112 x157)) (+ (+ (+ (* x161 ?v_86) (* x162 ?v_87)) (* x163 ?v_88)) (* x164 ?v_89))) (+ (+ (+ (* x177 ?v_90) (* x178 ?v_91)) (* x179 ?v_92)) (* x180 ?v_93)))) (>= ?v_82 (+ (+ (+ (+ (+ (* x109 x146) (* x110 x150)) (* x111 x154)) (* x112 x158)) (+ (+ (+ (* x161 ?v_95) (* x162 ?v_96)) (* x163 ?v_97)) (* x164 ?v_98))) (+ (+ (+ (* x177 ?v_99) (* x178 ?v_100)) (* x179 ?v_101)) (* x180 ?v_102))))) (>= ?v_83 (+ (+ (+ (+ (+ (* x109 x147) (* x110 x151)) (* x111 x155)) (* x112 x159)) (+ (+ (+ (* x161 ?v_104) (* x162 ?v_105)) (* x163 ?v_106)) (* x164 ?v_107))) (+ (+ (+ (* x177 ?v_108) (* x178 ?v_109)) (* x179 ?v_110)) (* x180 ?v_111))))) (>= ?v_84 (+ (+ (+ (+ (+ (* x109 x148) (* x110 x152)) (* x111 x156)) (* x112 x160)) (+ (+ (+ (* x161 ?v_113) (* x162 ?v_114)) (* x163 ?v_115)) (* x164 ?v_116))) (+ (+ (+ (* x177 ?v_117) (* x178 ?v_118)) (* x179 ?v_119)) (* x180 ?v_120))))) (>= ?v_85 (+ (+ (+ (+ (+ (* x113 x145) (* x114 x149)) (* x115 x153)) (* x116 x157)) (+ (+ (+ (* x165 ?v_86) (* x166 ?v_87)) (* x167 ?v_88)) (* x168 ?v_89))) (+ (+ (+ (* x181 ?v_90) (* x182 ?v_91)) (* x183 ?v_92)) (* x184 ?v_93))))) (>= ?v_94 (+ (+ (+ (+ (+ (* x113 x146) (* x114 x150)) (* x115 x154)) (* x116 x158)) (+ (+ (+ (* x165 ?v_95) (* x166 ?v_96)) (* x167 ?v_97)) (* x168 ?v_98))) (+ (+ (+ (* x181 ?v_99) (* x182 ?v_100)) (* x183 ?v_101)) (* x184 ?v_102))))) (>= ?v_103 (+ (+ (+ (+ (+ (* x113 x147) (* x114 x151)) (* x115 x155)) (* x116 x159)) (+ (+ (+ (* x165 ?v_104) (* x166 ?v_105)) (* x167 ?v_106)) (* x168 ?v_107))) (+ (+ (+ (* x181 ?v_108) (* x182 ?v_109)) (* x183 ?v_110)) (* x184 ?v_111))))) (>= ?v_112 (+ (+ (+ (+ (+ (* x113 x148) (* x114 x152)) (* x115 x156)) (* x116 x160)) (+ (+ (+ (* x165 ?v_113) (* x166 ?v_114)) (* x167 ?v_115)) (* x168 ?v_116))) (+ (+ (+ (* x181 ?v_117) (* x182 ?v_118)) (* x183 ?v_119)) (* x184 ?v_120))))) (>= ?v_121 (+ (+ (+ (+ (+ (* x117 x145) (* x118 x149)) (* x119 x153)) (* x120 x157)) (+ (+ (+ (* x169 ?v_86) (* x170 ?v_87)) (* x171 ?v_88)) (* x172 ?v_89))) (+ (+ (+ (* x185 ?v_90) (* x186 ?v_91)) (* x187 ?v_92)) (* x188 ?v_93))))) (>= ?v_122 (+ (+ (+ (+ (+ (* x117 x146) (* x118 x150)) (* x119 x154)) (* x120 x158)) (+ (+ (+ (* x169 ?v_95) (* x170 ?v_96)) (* x171 ?v_97)) (* x172 ?v_98))) (+ (+ (+ (* x185 ?v_99) (* x186 ?v_100)) (* x187 ?v_101)) (* x188 ?v_102))))) (>= ?v_123 (+ (+ (+ (+ (+ (* x117 x147) (* x118 x151)) (* x119 x155)) (* x120 x159)) (+ (+ (+ (* x169 ?v_104) (* x170 ?v_105)) (* x171 ?v_106)) (* x172 ?v_107))) (+ (+ (+ (* x185 ?v_108) (* x186 ?v_109)) (* x187 ?v_110)) (* x188 ?v_111))))) (>= ?v_124 (+ (+ (+ (+ (+ (* x117 x148) (* x118 x152)) (* x119 x156)) (* x120 x160)) (+ (+ (+ (* x169 ?v_113) (* x170 ?v_114)) (* x171 ?v_115)) (* x172 ?v_116))) (+ (+ (+ (* x185 ?v_117) (* x186 ?v_118)) (* x187 ?v_119)) (* x188 ?v_120))))) (>= ?v_125 (+ (+ (+ (+ (+ (* x121 x145) (* x122 x149)) (* x123 x153)) (* x124 x157)) (+ (+ (+ (* x173 ?v_86) (* x174 ?v_87)) (* x175 ?v_88)) (* x176 ?v_89))) (+ (+ (+ (* x189 ?v_90) (* x190 ?v_91)) (* x191 ?v_92)) (* x192 ?v_93))))) (>= ?v_126 (+ (+ (+ (+ (+ (* x121 x146) (* x122 x150)) (* x123 x154)) (* x124 x158)) (+ (+ (+ (* x173 ?v_95) (* x174 ?v_96)) (* x175 ?v_97)) (* x176 ?v_98))) (+ (+ (+ (* x189 ?v_99) (* x190 ?v_100)) (* x191 ?v_101)) (* x192 ?v_102))))) (>= ?v_127 (+ (+ (+ (+ (+ (* x121 x147) (* x122 x151)) (* x123 x155)) (* x124 x159)) (+ (+ (+ (* x173 ?v_104) (* x174 ?v_105)) (* x175 ?v_106)) (* x176 ?v_107))) (+ (+ (+ (* x189 ?v_108) (* x190 ?v_109)) (* x191 ?v_110)) (* x192 ?v_111))))) (>= ?v_128 (+ (+ (+ (+ (+ (* x121 x148) (* x122 x152)) (* x123 x156)) (* x124 x160)) (+ (+ (+ (* x173 ?v_113) (* x174 ?v_114)) (* x175 ?v_115)) (* x176 ?v_116))) (+ (+ (+ (* x189 ?v_117) (* x190 ?v_118)) (* x191 ?v_119)) (* x192 ?v_120))))))) ?v_129)))))))))))))))))))))))))))))))))))))))))))))))
(check-sat)
(exit)
