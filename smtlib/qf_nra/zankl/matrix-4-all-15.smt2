(set-logic QF_NRA)
(set-info :source |
From termination analysis of term rewriting.

Submitted by Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status unknown)
(declare-fun x145 () Real)
(declare-fun x6 () Real)
(declare-fun x84 () Real)
(declare-fun x162 () Real)
(declare-fun x23 () Real)
(declare-fun x101 () Real)
(declare-fun x179 () Real)
(declare-fun x40 () Real)
(declare-fun x118 () Real)
(declare-fun x196 () Real)
(declare-fun x57 () Real)
(declare-fun x135 () Real)
(declare-fun x213 () Real)
(declare-fun x74 () Real)
(declare-fun x152 () Real)
(declare-fun x13 () Real)
(declare-fun x91 () Real)
(declare-fun x169 () Real)
(declare-fun x30 () Real)
(declare-fun x108 () Real)
(declare-fun x186 () Real)
(declare-fun x47 () Real)
(declare-fun x125 () Real)
(declare-fun x203 () Real)
(declare-fun x64 () Real)
(declare-fun x142 () Real)
(declare-fun x3 () Real)
(declare-fun x81 () Real)
(declare-fun x159 () Real)
(declare-fun x20 () Real)
(declare-fun x98 () Real)
(declare-fun x176 () Real)
(declare-fun x37 () Real)
(declare-fun x115 () Real)
(declare-fun x193 () Real)
(declare-fun x54 () Real)
(declare-fun x132 () Real)
(declare-fun x210 () Real)
(declare-fun x71 () Real)
(declare-fun x149 () Real)
(declare-fun x10 () Real)
(declare-fun x88 () Real)
(declare-fun x166 () Real)
(declare-fun x27 () Real)
(declare-fun x105 () Real)
(declare-fun x183 () Real)
(declare-fun x44 () Real)
(declare-fun x122 () Real)
(declare-fun x200 () Real)
(declare-fun x61 () Real)
(declare-fun x139 () Real)
(declare-fun x0 () Real)
(declare-fun x78 () Real)
(declare-fun x156 () Real)
(declare-fun x17 () Real)
(declare-fun x95 () Real)
(declare-fun x173 () Real)
(declare-fun x34 () Real)
(declare-fun x112 () Real)
(declare-fun x190 () Real)
(declare-fun x51 () Real)
(declare-fun x129 () Real)
(declare-fun x207 () Real)
(declare-fun x68 () Real)
(declare-fun x146 () Real)
(declare-fun x7 () Real)
(declare-fun x85 () Real)
(declare-fun x163 () Real)
(declare-fun x24 () Real)
(declare-fun x102 () Real)
(declare-fun x180 () Real)
(declare-fun x41 () Real)
(declare-fun x119 () Real)
(declare-fun x197 () Real)
(declare-fun x58 () Real)
(declare-fun x136 () Real)
(declare-fun x214 () Real)
(declare-fun x75 () Real)
(declare-fun x153 () Real)
(declare-fun x14 () Real)
(declare-fun x92 () Real)
(declare-fun x170 () Real)
(declare-fun x31 () Real)
(declare-fun x109 () Real)
(declare-fun x187 () Real)
(declare-fun x48 () Real)
(declare-fun x126 () Real)
(declare-fun x204 () Real)
(declare-fun x65 () Real)
(declare-fun x143 () Real)
(declare-fun x4 () Real)
(declare-fun x82 () Real)
(declare-fun x160 () Real)
(declare-fun x21 () Real)
(declare-fun x99 () Real)
(declare-fun x177 () Real)
(declare-fun x38 () Real)
(declare-fun x116 () Real)
(declare-fun x194 () Real)
(declare-fun x55 () Real)
(declare-fun x133 () Real)
(declare-fun x211 () Real)
(declare-fun x72 () Real)
(declare-fun x150 () Real)
(declare-fun x11 () Real)
(declare-fun x89 () Real)
(declare-fun x167 () Real)
(declare-fun x28 () Real)
(declare-fun x106 () Real)
(declare-fun x184 () Real)
(declare-fun x45 () Real)
(declare-fun x123 () Real)
(declare-fun x201 () Real)
(declare-fun x62 () Real)
(declare-fun x140 () Real)
(declare-fun x1 () Real)
(declare-fun x79 () Real)
(declare-fun x157 () Real)
(declare-fun x18 () Real)
(declare-fun x96 () Real)
(declare-fun x174 () Real)
(declare-fun x35 () Real)
(declare-fun x113 () Real)
(declare-fun x191 () Real)
(declare-fun x52 () Real)
(declare-fun x130 () Real)
(declare-fun x208 () Real)
(declare-fun x69 () Real)
(declare-fun x147 () Real)
(declare-fun x8 () Real)
(declare-fun x86 () Real)
(declare-fun x164 () Real)
(declare-fun x25 () Real)
(declare-fun x103 () Real)
(declare-fun x181 () Real)
(declare-fun x42 () Real)
(declare-fun x120 () Real)
(declare-fun x198 () Real)
(declare-fun x59 () Real)
(declare-fun x137 () Real)
(declare-fun x76 () Real)
(declare-fun x154 () Real)
(declare-fun x15 () Real)
(declare-fun x93 () Real)
(declare-fun x171 () Real)
(declare-fun x32 () Real)
(declare-fun x110 () Real)
(declare-fun x188 () Real)
(declare-fun x49 () Real)
(declare-fun x127 () Real)
(declare-fun x205 () Real)
(declare-fun x66 () Real)
(declare-fun x144 () Real)
(declare-fun x5 () Real)
(declare-fun x83 () Real)
(declare-fun x161 () Real)
(declare-fun x22 () Real)
(declare-fun x100 () Real)
(declare-fun x178 () Real)
(declare-fun x39 () Real)
(declare-fun x117 () Real)
(declare-fun x195 () Real)
(declare-fun x56 () Real)
(declare-fun x134 () Real)
(declare-fun x212 () Real)
(declare-fun x73 () Real)
(declare-fun x151 () Real)
(declare-fun x12 () Real)
(declare-fun x90 () Real)
(declare-fun x168 () Real)
(declare-fun x29 () Real)
(declare-fun x107 () Real)
(declare-fun x185 () Real)
(declare-fun x46 () Real)
(declare-fun x124 () Real)
(declare-fun x202 () Real)
(declare-fun x63 () Real)
(declare-fun x141 () Real)
(declare-fun x2 () Real)
(declare-fun x80 () Real)
(declare-fun x158 () Real)
(declare-fun x19 () Real)
(declare-fun x97 () Real)
(declare-fun x175 () Real)
(declare-fun x36 () Real)
(declare-fun x114 () Real)
(declare-fun x192 () Real)
(declare-fun x53 () Real)
(declare-fun x131 () Real)
(declare-fun x209 () Real)
(declare-fun x70 () Real)
(declare-fun x148 () Real)
(declare-fun x9 () Real)
(declare-fun x87 () Real)
(declare-fun x165 () Real)
(declare-fun x26 () Real)
(declare-fun x104 () Real)
(declare-fun x182 () Real)
(declare-fun x43 () Real)
(declare-fun x121 () Real)
(declare-fun x199 () Real)
(declare-fun x60 () Real)
(declare-fun x138 () Real)
(declare-fun x77 () Real)
(declare-fun x155 () Real)
(declare-fun x16 () Real)
(declare-fun x94 () Real)
(declare-fun x172 () Real)
(declare-fun x33 () Real)
(declare-fun x111 () Real)
(declare-fun x189 () Real)
(declare-fun x50 () Real)
(declare-fun x128 () Real)
(declare-fun x206 () Real)
(declare-fun x67 () Real)
(assert (>= x145 0))
(assert (>= x6 0))
(assert (>= x84 0))
(assert (>= x162 0))
(assert (>= x23 0))
(assert (>= x101 0))
(assert (>= x179 0))
(assert (>= x40 0))
(assert (>= x118 0))
(assert (>= x196 0))
(assert (>= x57 0))
(assert (>= x135 0))
(assert (>= x213 0))
(assert (>= x74 0))
(assert (>= x152 0))
(assert (>= x13 0))
(assert (>= x91 0))
(assert (>= x169 0))
(assert (>= x30 0))
(assert (>= x108 0))
(assert (>= x186 0))
(assert (>= x47 0))
(assert (>= x125 0))
(assert (>= x203 0))
(assert (>= x64 0))
(assert (>= x142 0))
(assert (>= x3 0))
(assert (>= x81 0))
(assert (>= x159 0))
(assert (>= x20 0))
(assert (>= x98 0))
(assert (>= x176 0))
(assert (>= x37 0))
(assert (>= x115 0))
(assert (>= x193 0))
(assert (>= x54 0))
(assert (>= x132 0))
(assert (>= x210 0))
(assert (>= x71 0))
(assert (>= x149 0))
(assert (>= x10 0))
(assert (>= x88 0))
(assert (>= x166 0))
(assert (>= x27 0))
(assert (>= x105 0))
(assert (>= x183 0))
(assert (>= x44 0))
(assert (>= x122 0))
(assert (>= x200 0))
(assert (>= x61 0))
(assert (>= x139 0))
(assert (>= x0 0))
(assert (>= x78 0))
(assert (>= x156 0))
(assert (>= x17 0))
(assert (>= x95 0))
(assert (>= x173 0))
(assert (>= x34 0))
(assert (>= x112 0))
(assert (>= x190 0))
(assert (>= x51 0))
(assert (>= x129 0))
(assert (>= x207 0))
(assert (>= x68 0))
(assert (>= x146 0))
(assert (>= x7 0))
(assert (>= x85 0))
(assert (>= x163 0))
(assert (>= x24 0))
(assert (>= x102 0))
(assert (>= x180 0))
(assert (>= x41 0))
(assert (>= x119 0))
(assert (>= x197 0))
(assert (>= x58 0))
(assert (>= x136 0))
(assert (>= x214 0))
(assert (>= x75 0))
(assert (>= x153 0))
(assert (>= x14 0))
(assert (>= x92 0))
(assert (>= x170 0))
(assert (>= x31 0))
(assert (>= x109 0))
(assert (>= x187 0))
(assert (>= x48 0))
(assert (>= x126 0))
(assert (>= x204 0))
(assert (>= x65 0))
(assert (>= x143 0))
(assert (>= x4 0))
(assert (>= x82 0))
(assert (>= x160 0))
(assert (>= x21 0))
(assert (>= x99 0))
(assert (>= x177 0))
(assert (>= x38 0))
(assert (>= x116 0))
(assert (>= x194 0))
(assert (>= x55 0))
(assert (>= x133 0))
(assert (>= x211 0))
(assert (>= x72 0))
(assert (>= x150 0))
(assert (>= x11 0))
(assert (>= x89 0))
(assert (>= x167 0))
(assert (>= x28 0))
(assert (>= x106 0))
(assert (>= x184 0))
(assert (>= x45 0))
(assert (>= x123 0))
(assert (>= x201 0))
(assert (>= x62 0))
(assert (>= x140 0))
(assert (>= x1 0))
(assert (>= x79 0))
(assert (>= x157 0))
(assert (>= x18 0))
(assert (>= x96 0))
(assert (>= x174 0))
(assert (>= x35 0))
(assert (>= x113 0))
(assert (>= x191 0))
(assert (>= x52 0))
(assert (>= x130 0))
(assert (>= x208 0))
(assert (>= x69 0))
(assert (>= x147 0))
(assert (>= x8 0))
(assert (>= x86 0))
(assert (>= x164 0))
(assert (>= x25 0))
(assert (>= x103 0))
(assert (>= x181 0))
(assert (>= x42 0))
(assert (>= x120 0))
(assert (>= x198 0))
(assert (>= x59 0))
(assert (>= x137 0))
(assert (>= x76 0))
(assert (>= x154 0))
(assert (>= x15 0))
(assert (>= x93 0))
(assert (>= x171 0))
(assert (>= x32 0))
(assert (>= x110 0))
(assert (>= x188 0))
(assert (>= x49 0))
(assert (>= x127 0))
(assert (>= x205 0))
(assert (>= x66 0))
(assert (>= x144 0))
(assert (>= x5 0))
(assert (>= x83 0))
(assert (>= x161 0))
(assert (>= x22 0))
(assert (>= x100 0))
(assert (>= x178 0))
(assert (>= x39 0))
(assert (>= x117 0))
(assert (>= x195 0))
(assert (>= x56 0))
(assert (>= x134 0))
(assert (>= x212 0))
(assert (>= x73 0))
(assert (>= x151 0))
(assert (>= x12 0))
(assert (>= x90 0))
(assert (>= x168 0))
(assert (>= x29 0))
(assert (>= x107 0))
(assert (>= x185 0))
(assert (>= x46 0))
(assert (>= x124 0))
(assert (>= x202 0))
(assert (>= x63 0))
(assert (>= x141 0))
(assert (>= x2 0))
(assert (>= x80 0))
(assert (>= x158 0))
(assert (>= x19 0))
(assert (>= x97 0))
(assert (>= x175 0))
(assert (>= x36 0))
(assert (>= x114 0))
(assert (>= x192 0))
(assert (>= x53 0))
(assert (>= x131 0))
(assert (>= x209 0))
(assert (>= x70 0))
(assert (>= x148 0))
(assert (>= x9 0))
(assert (>= x87 0))
(assert (>= x165 0))
(assert (>= x26 0))
(assert (>= x104 0))
(assert (>= x182 0))
(assert (>= x43 0))
(assert (>= x121 0))
(assert (>= x199 0))
(assert (>= x60 0))
(assert (>= x138 0))
(assert (>= x77 0))
(assert (>= x155 0))
(assert (>= x16 0))
(assert (>= x94 0))
(assert (>= x172 0))
(assert (>= x33 0))
(assert (>= x111 0))
(assert (>= x189 0))
(assert (>= x50 0))
(assert (>= x128 0))
(assert (>= x206 0))
(assert (>= x67 0))
(assert (let ((?v_0 (+ x0 (+ (+ (+ (* x1 x5) (* x2 x6)) (* x3 x7)) (* x4 x8)))) (?v_1 (+ x18 (+ (+ (+ (* x23 x27) (* x24 x28)) (* x25 x29)) (* x26 x30)))) (?v_4 (and (and (and (>= x19 x14) (>= x20 x15)) (>= x21 x16)) (>= x22 x17))) (?v_2 (+ x18 (+ (+ (+ (* x23 x31) (* x24 x32)) (* x25 x33)) (* x26 x34))))) (let ((?v_3 (and (> ?v_2 x13) (>= ?v_2 x13))) (?v_6 (+ (+ (+ (* x23 x35) (* x24 x39)) (* x25 x43)) (* x26 x47))) (?v_7 (+ (+ (+ (* x23 x36) (* x24 x40)) (* x25 x44)) (* x26 x48))) (?v_8 (+ (+ (+ (* x23 x37) (* x24 x41)) (* x25 x45)) (* x26 x49))) (?v_9 (+ (+ (+ (* x23 x38) (* x24 x42)) (* x25 x46)) (* x26 x50))) (?v_5 (+ (+ x18 (+ (+ (+ (* x19 x51) (* x20 x52)) (* x21 x53)) (* x22 x54))) (+ (+ (+ (* x23 x51) (* x24 x52)) (* x25 x53)) (* x26 x54)))) (?v_19 (+ (+ (+ (* x19 x55) (* x20 x59)) (* x21 x63)) (* x22 x67))) (?v_21 (+ (+ (+ (* x19 x56) (* x20 x60)) (* x21 x64)) (* x22 x68))) (?v_23 (+ (+ (+ (* x19 x57) (* x20 x61)) (* x21 x65)) (* x22 x69))) (?v_25 (+ (+ (+ (* x19 x58) (* x20 x62)) (* x21 x66)) (* x22 x70))) (?v_27 (+ (+ (+ (* x23 x55) (* x24 x59)) (* x25 x63)) (* x26 x67))) (?v_29 (+ (+ (+ (* x23 x56) (* x24 x60)) (* x25 x64)) (* x26 x68))) (?v_31 (+ (+ (+ (* x23 x57) (* x24 x61)) (* x25 x65)) (* x26 x69))) (?v_33 (+ (+ (+ (* x23 x58) (* x24 x62)) (* x25 x66)) (* x26 x70))) (?v_10 (+ x13 (+ (+ (+ (* x14 x71) (* x15 x72)) (* x16 x73)) (* x17 x74)))) (?v_12 (+ (+ (+ (* x14 x75) (* x15 x79)) (* x16 x83)) (* x17 x87))) (?v_13 (+ (+ (+ (* x14 x76) (* x15 x80)) (* x16 x84)) (* x17 x88))) (?v_14 (+ (+ (+ (* x14 x77) (* x15 x81)) (* x16 x85)) (* x17 x89))) (?v_15 (+ (+ (+ (* x14 x78) (* x15 x82)) (* x16 x86)) (* x17 x90))) (?v_11 (+ x0 (+ (+ (+ (* x1 x51) (* x2 x52)) (* x3 x53)) (* x4 x54)))) (?v_16 (+ x13 (+ (+ (+ (* x14 x107) (* x15 x108)) (* x16 x109)) (* x17 x110))))) (let ((?v_17 (and (> ?v_16 x13) (>= ?v_16 x13))) (?v_26 (+ (+ (+ (* x14 x127) (* x15 x131)) (* x16 x135)) (* x17 x139))) (?v_28 (+ (+ (+ (* x14 x128) (* x15 x132)) (* x16 x136)) (* x17 x140))) (?v_30 (+ (+ (+ (* x14 x129) (* x15 x133)) (* x16 x137)) (* x17 x141))) (?v_32 (+ (+ (+ (* x14 x130) (* x15 x134)) (* x16 x138)) (* x17 x142))) (?v_18 (+ (+ (+ (* x14 x111) (* x15 x115)) (* x16 x119)) (* x17 x123))) (?v_20 (+ (+ (+ (* x14 x112) (* x15 x116)) (* x16 x120)) (* x17 x124))) (?v_22 (+ (+ (+ (* x14 x113) (* x15 x117)) (* x16 x121)) (* x17 x125))) (?v_24 (+ (+ (+ (* x14 x114) (* x15 x118)) (* x16 x122)) (* x17 x126))) (?v_34 (+ x13 (+ (+ (+ (* x14 x31) (* x15 x32)) (* x16 x33)) (* x17 x34))))) (let ((?v_82 (and (and (and (and (and (and (and (and (and (and (and (and (> ?v_0 x13) (>= ?v_0 x13)) (and (and (and (>= x9 x14) (>= x10 x15)) (>= x11 x16)) (>= x12 x17))) (and (and (> ?v_1 x13) (>= ?v_1 x13)) ?v_4)) (and ?v_3 (and (and (and (>= ?v_6 x14) (>= ?v_7 x15)) (>= ?v_8 x16)) (>= ?v_9 x17)))) (and ?v_3 ?v_4)) (and (and (and (> ?v_2 ?v_5) (>= ?v_2 ?v_5)) (and (and (and (>= x19 ?v_19) (>= x20 ?v_21)) (>= x21 ?v_23)) (>= x22 ?v_25))) (and (and (and (>= ?v_6 ?v_27) (>= ?v_7 ?v_29)) (>= ?v_8 ?v_31)) (>= ?v_9 ?v_33)))) (and (and (> ?v_10 x13) (>= ?v_10 x13)) (and (and (and (>= ?v_12 x14) (>= ?v_13 x15)) (>= ?v_14 x16)) (>= ?v_15 x17)))) (and (and (and (> ?v_10 ?v_11) (>= ?v_10 ?v_11)) (and (and (and (>= ?v_12 (+ (+ (+ (* x1 x55) (* x2 x59)) (* x3 x63)) (* x4 x67))) (>= ?v_13 (+ (+ (+ (* x1 x56) (* x2 x60)) (* x3 x64)) (* x4 x68)))) (>= ?v_14 (+ (+ (+ (* x1 x57) (* x2 x61)) (* x3 x65)) (* x4 x69)))) (>= ?v_15 (+ (+ (+ (* x1 x58) (* x2 x62)) (* x3 x66)) (* x4 x70))))) (and (and (and (>= (+ (+ (+ (* x14 x91) (* x15 x95)) (* x16 x99)) (* x17 x103)) x9) (>= (+ (+ (+ (* x14 x92) (* x15 x96)) (* x16 x100)) (* x17 x104)) x10)) (>= (+ (+ (+ (* x14 x93) (* x15 x97)) (* x16 x101)) (* x17 x105)) x11)) (>= (+ (+ (+ (* x14 x94) (* x15 x98)) (* x16 x102)) (* x17 x106)) x12)))) (and ?v_17 (and (and (and (>= ?v_26 x14) (>= ?v_28 x15)) (>= ?v_30 x16)) (>= ?v_32 x17)))) (and ?v_17 (and (and (and (>= ?v_18 x14) (>= ?v_20 x15)) (>= ?v_22 x16)) (>= ?v_24 x17)))) (and (and (and (> ?v_16 ?v_5) (>= ?v_16 ?v_5)) (and (and (and (>= ?v_18 ?v_19) (>= ?v_20 ?v_21)) (>= ?v_22 ?v_23)) (>= ?v_24 ?v_25))) (and (and (and (>= ?v_26 ?v_27) (>= ?v_28 ?v_29)) (>= ?v_30 ?v_31)) (>= ?v_32 ?v_33)))) (and (and (> ?v_34 x13) (>= ?v_34 x13)) (and (and (and (>= (+ (+ (+ (* x14 x35) (* x15 x39)) (* x16 x43)) (* x17 x47)) x14) (>= (+ (+ (+ (* x14 x36) (* x15 x40)) (* x16 x44)) (* x17 x48)) x15)) (>= (+ (+ (+ (* x14 x37) (* x15 x41)) (* x16 x45)) (* x17 x49)) x16)) (>= (+ (+ (+ (* x14 x38) (* x15 x42)) (* x16 x46)) (* x17 x50)) x17))))) (?v_35 (+ x143 (+ (+ (+ (* x147 x5) (* x148 x6)) (* x149 x7)) (* x150 x8)))) (?v_36 (+ x179 (+ (+ (+ (* x199 x27) (* x200 x28)) (* x201 x29)) (* x202 x30)))) (?v_39 (+ (+ x179 (+ (+ (+ (* x183 x51) (* x184 x52)) (* x185 x53)) (* x186 x54))) (+ (+ (+ (* x199 x51) (* x200 x52)) (* x201 x53)) (* x202 x54)))) (?v_40 (+ (+ x180 (+ (+ (+ (* x187 x51) (* x188 x52)) (* x189 x53)) (* x190 x54))) (+ (+ (+ (* x203 x51) (* x204 x52)) (* x205 x53)) (* x206 x54)))) (?v_41 (+ (+ x181 (+ (+ (+ (* x191 x51) (* x192 x52)) (* x193 x53)) (* x194 x54))) (+ (+ (+ (* x207 x51) (* x208 x52)) (* x209 x53)) (* x210 x54)))) (?v_42 (+ (+ x182 (+ (+ (+ (* x195 x51) (* x196 x52)) (* x197 x53)) (* x198 x54))) (+ (+ (+ (* x211 x51) (* x212 x52)) (* x213 x53)) (* x214 x54))))) (let ((?v_38 (+ x31 (+ (+ (+ (* x35 ?v_39) (* x36 ?v_40)) (* x37 ?v_41)) (* x38 ?v_42)))) (?v_37 (+ x179 (+ (+ (+ (* x199 x31) (* x200 x32)) (* x201 x33)) (* x202 x34)))) (?v_43 (+ (+ (+ (* x183 x55) (* x184 x59)) (* x185 x63)) (* x186 x67))) (?v_44 (+ (+ (+ (* x187 x55) (* x188 x59)) (* x189 x63)) (* x190 x67))) (?v_45 (+ (+ (+ (* x191 x55) (* x192 x59)) (* x193 x63)) (* x194 x67))) (?v_46 (+ (+ (+ (* x195 x55) (* x196 x59)) (* x197 x63)) (* x198 x67))) (?v_47 (+ (+ (+ (* x183 x56) (* x184 x60)) (* x185 x64)) (* x186 x68))) (?v_48 (+ (+ (+ (* x187 x56) (* x188 x60)) (* x189 x64)) (* x190 x68))) (?v_49 (+ (+ (+ (* x191 x56) (* x192 x60)) (* x193 x64)) (* x194 x68))) (?v_50 (+ (+ (+ (* x195 x56) (* x196 x60)) (* x197 x64)) (* x198 x68))) (?v_51 (+ (+ (+ (* x183 x57) (* x184 x61)) (* x185 x65)) (* x186 x69))) (?v_52 (+ (+ (+ (* x187 x57) (* x188 x61)) (* x189 x65)) (* x190 x69))) (?v_53 (+ (+ (+ (* x191 x57) (* x192 x61)) (* x193 x65)) (* x194 x69))) (?v_54 (+ (+ (+ (* x195 x57) (* x196 x61)) (* x197 x65)) (* x198 x69))) (?v_55 (+ (+ (+ (* x183 x58) (* x184 x62)) (* x185 x66)) (* x186 x70))) (?v_56 (+ (+ (+ (* x187 x58) (* x188 x62)) (* x189 x66)) (* x190 x70))) (?v_57 (+ (+ (+ (* x191 x58) (* x192 x62)) (* x193 x66)) (* x194 x70))) (?v_58 (+ (+ (+ (* x195 x58) (* x196 x62)) (* x197 x66)) (* x198 x70))) (?v_59 (+ (+ (+ (* x199 x55) (* x200 x59)) (* x201 x63)) (* x202 x67))) (?v_60 (+ (+ (+ (* x203 x55) (* x204 x59)) (* x205 x63)) (* x206 x67))) (?v_61 (+ (+ (+ (* x207 x55) (* x208 x59)) (* x209 x63)) (* x210 x67))) (?v_62 (+ (+ (+ (* x211 x55) (* x212 x59)) (* x213 x63)) (* x214 x67))) (?v_63 (+ (+ (+ (* x199 x56) (* x200 x60)) (* x201 x64)) (* x202 x68))) (?v_64 (+ (+ (+ (* x203 x56) (* x204 x60)) (* x205 x64)) (* x206 x68))) (?v_65 (+ (+ (+ (* x207 x56) (* x208 x60)) (* x209 x64)) (* x210 x68))) (?v_66 (+ (+ (+ (* x211 x56) (* x212 x60)) (* x213 x64)) (* x214 x68))) (?v_67 (+ (+ (+ (* x199 x57) (* x200 x61)) (* x201 x65)) (* x202 x69))) (?v_68 (+ (+ (+ (* x203 x57) (* x204 x61)) (* x205 x65)) (* x206 x69))) (?v_69 (+ (+ (+ (* x207 x57) (* x208 x61)) (* x209 x65)) (* x210 x69))) (?v_70 (+ (+ (+ (* x211 x57) (* x212 x61)) (* x213 x65)) (* x214 x69))) (?v_71 (+ (+ (+ (* x199 x58) (* x200 x62)) (* x201 x66)) (* x202 x70))) (?v_72 (+ (+ (+ (* x203 x58) (* x204 x62)) (* x205 x66)) (* x206 x70))) (?v_73 (+ (+ (+ (* x207 x58) (* x208 x62)) (* x209 x66)) (* x210 x70))) (?v_74 (+ (+ (+ (* x211 x58) (* x212 x62)) (* x213 x66)) (* x214 x70))) (?v_76 (+ x143 (+ (+ (+ (* x147 x51) (* x148 x52)) (* x149 x53)) (* x150 x54)))) (?v_75 (+ x51 (+ (+ (+ (* x55 x71) (* x56 x72)) (* x57 x73)) (* x58 x74)))) (?v_77 (+ x51 (+ (+ (+ (* x55 x107) (* x56 x108)) (* x57 x109)) (* x58 x110)))) (?v_78 (+ x51 (+ (+ (+ (* x55 x5) (* x56 x6)) (* x57 x7)) (* x58 x8)))) (?v_79 (+ x51 (+ (+ (+ (* x55 x27) (* x56 x28)) (* x57 x29)) (* x58 x30)))) (?v_81 (+ x31 (+ (+ (+ (* x35 x51) (* x36 x52)) (* x37 x53)) (* x38 x54)))) (?v_80 (+ x51 (+ (+ (+ (* x55 x31) (* x56 x32)) (* x57 x33)) (* x58 x34))))) (and (and (and (and (and (and (and (and (and (and (and ?v_82 (and (and (> ?v_35 x51) (and (and (and (>= ?v_35 x51) (>= (+ x144 (+ (+ (+ (* x151 x5) (* x152 x6)) (* x153 x7)) (* x154 x8))) x52)) (>= (+ x145 (+ (+ (+ (* x155 x5) (* x156 x6)) (* x157 x7)) (* x158 x8))) x53)) (>= (+ x146 (+ (+ (+ (* x159 x5) (* x160 x6)) (* x161 x7)) (* x162 x8))) x54))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x163 x55) (>= x164 x56)) (>= x165 x57)) (>= x166 x58)) (>= x167 x59)) (>= x168 x60)) (>= x169 x61)) (>= x170 x62)) (>= x171 x63)) (>= x172 x64)) (>= x173 x65)) (>= x174 x66)) (>= x175 x67)) (>= x176 x68)) (>= x177 x69)) (>= x178 x70)))) (and (and (> ?v_36 x51) (and (and (and (>= ?v_36 x51) (>= (+ x180 (+ (+ (+ (* x203 x27) (* x204 x28)) (* x205 x29)) (* x206 x30))) x52)) (>= (+ x181 (+ (+ (+ (* x207 x27) (* x208 x28)) (* x209 x29)) (* x210 x30))) x53)) (>= (+ x182 (+ (+ (+ (* x211 x27) (* x212 x28)) (* x213 x29)) (* x214 x30))) x54))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x183 x55) (>= x184 x56)) (>= x185 x57)) (>= x186 x58)) (>= x187 x59)) (>= x188 x60)) (>= x189 x61)) (>= x190 x62)) (>= x191 x63)) (>= x192 x64)) (>= x193 x65)) (>= x194 x66)) (>= x195 x67)) (>= x196 x68)) (>= x197 x69)) (>= x198 x70)))) (and (and (and (> ?v_37 ?v_38) (and (and (and (>= ?v_37 ?v_38) (>= (+ x180 (+ (+ (+ (* x203 x31) (* x204 x32)) (* x205 x33)) (* x206 x34))) (+ x32 (+ (+ (+ (* x39 ?v_39) (* x40 ?v_40)) (* x41 ?v_41)) (* x42 ?v_42))))) (>= (+ x181 (+ (+ (+ (* x207 x31) (* x208 x32)) (* x209 x33)) (* x210 x34))) (+ x33 (+ (+ (+ (* x43 ?v_39) (* x44 ?v_40)) (* x45 ?v_41)) (* x46 ?v_42))))) (>= (+ x182 (+ (+ (+ (* x211 x31) (* x212 x32)) (* x213 x33)) (* x214 x34))) (+ x34 (+ (+ (+ (* x47 ?v_39) (* x48 ?v_40)) (* x49 ?v_41)) (* x50 ?v_42)))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x183 (+ (+ (+ (* x35 ?v_43) (* x36 ?v_44)) (* x37 ?v_45)) (* x38 ?v_46))) (>= x184 (+ (+ (+ (* x35 ?v_47) (* x36 ?v_48)) (* x37 ?v_49)) (* x38 ?v_50)))) (>= x185 (+ (+ (+ (* x35 ?v_51) (* x36 ?v_52)) (* x37 ?v_53)) (* x38 ?v_54)))) (>= x186 (+ (+ (+ (* x35 ?v_55) (* x36 ?v_56)) (* x37 ?v_57)) (* x38 ?v_58)))) (>= x187 (+ (+ (+ (* x39 ?v_43) (* x40 ?v_44)) (* x41 ?v_45)) (* x42 ?v_46)))) (>= x188 (+ (+ (+ (* x39 ?v_47) (* x40 ?v_48)) (* x41 ?v_49)) (* x42 ?v_50)))) (>= x189 (+ (+ (+ (* x39 ?v_51) (* x40 ?v_52)) (* x41 ?v_53)) (* x42 ?v_54)))) (>= x190 (+ (+ (+ (* x39 ?v_55) (* x40 ?v_56)) (* x41 ?v_57)) (* x42 ?v_58)))) (>= x191 (+ (+ (+ (* x43 ?v_43) (* x44 ?v_44)) (* x45 ?v_45)) (* x46 ?v_46)))) (>= x192 (+ (+ (+ (* x43 ?v_47) (* x44 ?v_48)) (* x45 ?v_49)) (* x46 ?v_50)))) (>= x193 (+ (+ (+ (* x43 ?v_51) (* x44 ?v_52)) (* x45 ?v_53)) (* x46 ?v_54)))) (>= x194 (+ (+ (+ (* x43 ?v_55) (* x44 ?v_56)) (* x45 ?v_57)) (* x46 ?v_58)))) (>= x195 (+ (+ (+ (* x47 ?v_43) (* x48 ?v_44)) (* x49 ?v_45)) (* x50 ?v_46)))) (>= x196 (+ (+ (+ (* x47 ?v_47) (* x48 ?v_48)) (* x49 ?v_49)) (* x50 ?v_50)))) (>= x197 (+ (+ (+ (* x47 ?v_51) (* x48 ?v_52)) (* x49 ?v_53)) (* x50 ?v_54)))) (>= x198 (+ (+ (+ (* x47 ?v_55) (* x48 ?v_56)) (* x49 ?v_57)) (* x50 ?v_58))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= (+ (+ (+ (* x199 x35) (* x200 x39)) (* x201 x43)) (* x202 x47)) (+ (+ (+ (* x35 ?v_59) (* x36 ?v_60)) (* x37 ?v_61)) (* x38 ?v_62))) (>= (+ (+ (+ (* x199 x36) (* x200 x40)) (* x201 x44)) (* x202 x48)) (+ (+ (+ (* x35 ?v_63) (* x36 ?v_64)) (* x37 ?v_65)) (* x38 ?v_66)))) (>= (+ (+ (+ (* x199 x37) (* x200 x41)) (* x201 x45)) (* x202 x49)) (+ (+ (+ (* x35 ?v_67) (* x36 ?v_68)) (* x37 ?v_69)) (* x38 ?v_70)))) (>= (+ (+ (+ (* x199 x38) (* x200 x42)) (* x201 x46)) (* x202 x50)) (+ (+ (+ (* x35 ?v_71) (* x36 ?v_72)) (* x37 ?v_73)) (* x38 ?v_74)))) (>= (+ (+ (+ (* x203 x35) (* x204 x39)) (* x205 x43)) (* x206 x47)) (+ (+ (+ (* x39 ?v_59) (* x40 ?v_60)) (* x41 ?v_61)) (* x42 ?v_62)))) (>= (+ (+ (+ (* x203 x36) (* x204 x40)) (* x205 x44)) (* x206 x48)) (+ (+ (+ (* x39 ?v_63) (* x40 ?v_64)) (* x41 ?v_65)) (* x42 ?v_66)))) (>= (+ (+ (+ (* x203 x37) (* x204 x41)) (* x205 x45)) (* x206 x49)) (+ (+ (+ (* x39 ?v_67) (* x40 ?v_68)) (* x41 ?v_69)) (* x42 ?v_70)))) (>= (+ (+ (+ (* x203 x38) (* x204 x42)) (* x205 x46)) (* x206 x50)) (+ (+ (+ (* x39 ?v_71) (* x40 ?v_72)) (* x41 ?v_73)) (* x42 ?v_74)))) (>= (+ (+ (+ (* x207 x35) (* x208 x39)) (* x209 x43)) (* x210 x47)) (+ (+ (+ (* x43 ?v_59) (* x44 ?v_60)) (* x45 ?v_61)) (* x46 ?v_62)))) (>= (+ (+ (+ (* x207 x36) (* x208 x40)) (* x209 x44)) (* x210 x48)) (+ (+ (+ (* x43 ?v_63) (* x44 ?v_64)) (* x45 ?v_65)) (* x46 ?v_66)))) (>= (+ (+ (+ (* x207 x37) (* x208 x41)) (* x209 x45)) (* x210 x49)) (+ (+ (+ (* x43 ?v_67) (* x44 ?v_68)) (* x45 ?v_69)) (* x46 ?v_70)))) (>= (+ (+ (+ (* x207 x38) (* x208 x42)) (* x209 x46)) (* x210 x50)) (+ (+ (+ (* x43 ?v_71) (* x44 ?v_72)) (* x45 ?v_73)) (* x46 ?v_74)))) (>= (+ (+ (+ (* x211 x35) (* x212 x39)) (* x213 x43)) (* x214 x47)) (+ (+ (+ (* x47 ?v_59) (* x48 ?v_60)) (* x49 ?v_61)) (* x50 ?v_62)))) (>= (+ (+ (+ (* x211 x36) (* x212 x40)) (* x213 x44)) (* x214 x48)) (+ (+ (+ (* x47 ?v_63) (* x48 ?v_64)) (* x49 ?v_65)) (* x50 ?v_66)))) (>= (+ (+ (+ (* x211 x37) (* x212 x41)) (* x213 x45)) (* x214 x49)) (+ (+ (+ (* x47 ?v_67) (* x48 ?v_68)) (* x49 ?v_69)) (* x50 ?v_70)))) (>= (+ (+ (+ (* x211 x38) (* x212 x42)) (* x213 x46)) (* x214 x50)) (+ (+ (+ (* x47 ?v_71) (* x48 ?v_72)) (* x49 ?v_73)) (* x50 ?v_74)))))) (and (and (and (> ?v_75 ?v_76) (and (and (and (>= ?v_75 ?v_76) (>= (+ x52 (+ (+ (+ (* x59 x71) (* x60 x72)) (* x61 x73)) (* x62 x74))) (+ x144 (+ (+ (+ (* x151 x51) (* x152 x52)) (* x153 x53)) (* x154 x54))))) (>= (+ x53 (+ (+ (+ (* x63 x71) (* x64 x72)) (* x65 x73)) (* x66 x74))) (+ x145 (+ (+ (+ (* x155 x51) (* x156 x52)) (* x157 x53)) (* x158 x54))))) (>= (+ x54 (+ (+ (+ (* x67 x71) (* x68 x72)) (* x69 x73)) (* x70 x74))) (+ x146 (+ (+ (+ (* x159 x51) (* x160 x52)) (* x161 x53)) (* x162 x54)))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= (+ (+ (+ (* x55 x75) (* x56 x79)) (* x57 x83)) (* x58 x87)) (+ (+ (+ (* x147 x55) (* x148 x59)) (* x149 x63)) (* x150 x67))) (>= (+ (+ (+ (* x55 x76) (* x56 x80)) (* x57 x84)) (* x58 x88)) (+ (+ (+ (* x147 x56) (* x148 x60)) (* x149 x64)) (* x150 x68)))) (>= (+ (+ (+ (* x55 x77) (* x56 x81)) (* x57 x85)) (* x58 x89)) (+ (+ (+ (* x147 x57) (* x148 x61)) (* x149 x65)) (* x150 x69)))) (>= (+ (+ (+ (* x55 x78) (* x56 x82)) (* x57 x86)) (* x58 x90)) (+ (+ (+ (* x147 x58) (* x148 x62)) (* x149 x66)) (* x150 x70)))) (>= (+ (+ (+ (* x59 x75) (* x60 x79)) (* x61 x83)) (* x62 x87)) (+ (+ (+ (* x151 x55) (* x152 x59)) (* x153 x63)) (* x154 x67)))) (>= (+ (+ (+ (* x59 x76) (* x60 x80)) (* x61 x84)) (* x62 x88)) (+ (+ (+ (* x151 x56) (* x152 x60)) (* x153 x64)) (* x154 x68)))) (>= (+ (+ (+ (* x59 x77) (* x60 x81)) (* x61 x85)) (* x62 x89)) (+ (+ (+ (* x151 x57) (* x152 x61)) (* x153 x65)) (* x154 x69)))) (>= (+ (+ (+ (* x59 x78) (* x60 x82)) (* x61 x86)) (* x62 x90)) (+ (+ (+ (* x151 x58) (* x152 x62)) (* x153 x66)) (* x154 x70)))) (>= (+ (+ (+ (* x63 x75) (* x64 x79)) (* x65 x83)) (* x66 x87)) (+ (+ (+ (* x155 x55) (* x156 x59)) (* x157 x63)) (* x158 x67)))) (>= (+ (+ (+ (* x63 x76) (* x64 x80)) (* x65 x84)) (* x66 x88)) (+ (+ (+ (* x155 x56) (* x156 x60)) (* x157 x64)) (* x158 x68)))) (>= (+ (+ (+ (* x63 x77) (* x64 x81)) (* x65 x85)) (* x66 x89)) (+ (+ (+ (* x155 x57) (* x156 x61)) (* x157 x65)) (* x158 x69)))) (>= (+ (+ (+ (* x63 x78) (* x64 x82)) (* x65 x86)) (* x66 x90)) (+ (+ (+ (* x155 x58) (* x156 x62)) (* x157 x66)) (* x158 x70)))) (>= (+ (+ (+ (* x67 x75) (* x68 x79)) (* x69 x83)) (* x70 x87)) (+ (+ (+ (* x159 x55) (* x160 x59)) (* x161 x63)) (* x162 x67)))) (>= (+ (+ (+ (* x67 x76) (* x68 x80)) (* x69 x84)) (* x70 x88)) (+ (+ (+ (* x159 x56) (* x160 x60)) (* x161 x64)) (* x162 x68)))) (>= (+ (+ (+ (* x67 x77) (* x68 x81)) (* x69 x85)) (* x70 x89)) (+ (+ (+ (* x159 x57) (* x160 x61)) (* x161 x65)) (* x162 x69)))) (>= (+ (+ (+ (* x67 x78) (* x68 x82)) (* x69 x86)) (* x70 x90)) (+ (+ (+ (* x159 x58) (* x160 x62)) (* x161 x66)) (* x162 x70))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= (+ (+ (+ (* x55 x91) (* x56 x95)) (* x57 x99)) (* x58 x103)) x163) (>= (+ (+ (+ (* x55 x92) (* x56 x96)) (* x57 x100)) (* x58 x104)) x164)) (>= (+ (+ (+ (* x55 x93) (* x56 x97)) (* x57 x101)) (* x58 x105)) x165)) (>= (+ (+ (+ (* x55 x94) (* x56 x98)) (* x57 x102)) (* x58 x106)) x166)) (>= (+ (+ (+ (* x59 x91) (* x60 x95)) (* x61 x99)) (* x62 x103)) x167)) (>= (+ (+ (+ (* x59 x92) (* x60 x96)) (* x61 x100)) (* x62 x104)) x168)) (>= (+ (+ (+ (* x59 x93) (* x60 x97)) (* x61 x101)) (* x62 x105)) x169)) (>= (+ (+ (+ (* x59 x94) (* x60 x98)) (* x61 x102)) (* x62 x106)) x170)) (>= (+ (+ (+ (* x63 x91) (* x64 x95)) (* x65 x99)) (* x66 x103)) x171)) (>= (+ (+ (+ (* x63 x92) (* x64 x96)) (* x65 x100)) (* x66 x104)) x172)) (>= (+ (+ (+ (* x63 x93) (* x64 x97)) (* x65 x101)) (* x66 x105)) x173)) (>= (+ (+ (+ (* x63 x94) (* x64 x98)) (* x65 x102)) (* x66 x106)) x174)) (>= (+ (+ (+ (* x67 x91) (* x68 x95)) (* x69 x99)) (* x70 x103)) x175)) (>= (+ (+ (+ (* x67 x92) (* x68 x96)) (* x69 x100)) (* x70 x104)) x176)) (>= (+ (+ (+ (* x67 x93) (* x68 x97)) (* x69 x101)) (* x70 x105)) x177)) (>= (+ (+ (+ (* x67 x94) (* x68 x98)) (* x69 x102)) (* x70 x106)) x178)))) (and (and (and (> ?v_77 ?v_39) (and (and (and (>= ?v_77 ?v_39) (>= (+ x52 (+ (+ (+ (* x59 x107) (* x60 x108)) (* x61 x109)) (* x62 x110))) ?v_40)) (>= (+ x53 (+ (+ (+ (* x63 x107) (* x64 x108)) (* x65 x109)) (* x66 x110))) ?v_41)) (>= (+ x54 (+ (+ (+ (* x67 x107) (* x68 x108)) (* x69 x109)) (* x70 x110))) ?v_42))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= (+ (+ (+ (* x55 x111) (* x56 x115)) (* x57 x119)) (* x58 x123)) ?v_43) (>= (+ (+ (+ (* x55 x112) (* x56 x116)) (* x57 x120)) (* x58 x124)) ?v_47)) (>= (+ (+ (+ (* x55 x113) (* x56 x117)) (* x57 x121)) (* x58 x125)) ?v_51)) (>= (+ (+ (+ (* x55 x114) (* x56 x118)) (* x57 x122)) (* x58 x126)) ?v_55)) (>= (+ (+ (+ (* x59 x111) (* x60 x115)) (* x61 x119)) (* x62 x123)) ?v_44)) (>= (+ (+ (+ (* x59 x112) (* x60 x116)) (* x61 x120)) (* x62 x124)) ?v_48)) (>= (+ (+ (+ (* x59 x113) (* x60 x117)) (* x61 x121)) (* x62 x125)) ?v_52)) (>= (+ (+ (+ (* x59 x114) (* x60 x118)) (* x61 x122)) (* x62 x126)) ?v_56)) (>= (+ (+ (+ (* x63 x111) (* x64 x115)) (* x65 x119)) (* x66 x123)) ?v_45)) (>= (+ (+ (+ (* x63 x112) (* x64 x116)) (* x65 x120)) (* x66 x124)) ?v_49)) (>= (+ (+ (+ (* x63 x113) (* x64 x117)) (* x65 x121)) (* x66 x125)) ?v_53)) (>= (+ (+ (+ (* x63 x114) (* x64 x118)) (* x65 x122)) (* x66 x126)) ?v_57)) (>= (+ (+ (+ (* x67 x111) (* x68 x115)) (* x69 x119)) (* x70 x123)) ?v_46)) (>= (+ (+ (+ (* x67 x112) (* x68 x116)) (* x69 x120)) (* x70 x124)) ?v_50)) (>= (+ (+ (+ (* x67 x113) (* x68 x117)) (* x69 x121)) (* x70 x125)) ?v_54)) (>= (+ (+ (+ (* x67 x114) (* x68 x118)) (* x69 x122)) (* x70 x126)) ?v_58))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= (+ (+ (+ (* x55 x127) (* x56 x131)) (* x57 x135)) (* x58 x139)) ?v_59) (>= (+ (+ (+ (* x55 x128) (* x56 x132)) (* x57 x136)) (* x58 x140)) ?v_63)) (>= (+ (+ (+ (* x55 x129) (* x56 x133)) (* x57 x137)) (* x58 x141)) ?v_67)) (>= (+ (+ (+ (* x55 x130) (* x56 x134)) (* x57 x138)) (* x58 x142)) ?v_71)) (>= (+ (+ (+ (* x59 x127) (* x60 x131)) (* x61 x135)) (* x62 x139)) ?v_60)) (>= (+ (+ (+ (* x59 x128) (* x60 x132)) (* x61 x136)) (* x62 x140)) ?v_64)) (>= (+ (+ (+ (* x59 x129) (* x60 x133)) (* x61 x137)) (* x62 x141)) ?v_68)) (>= (+ (+ (+ (* x59 x130) (* x60 x134)) (* x61 x138)) (* x62 x142)) ?v_72)) (>= (+ (+ (+ (* x63 x127) (* x64 x131)) (* x65 x135)) (* x66 x139)) ?v_61)) (>= (+ (+ (+ (* x63 x128) (* x64 x132)) (* x65 x136)) (* x66 x140)) ?v_65)) (>= (+ (+ (+ (* x63 x129) (* x64 x133)) (* x65 x137)) (* x66 x141)) ?v_69)) (>= (+ (+ (+ (* x63 x130) (* x64 x134)) (* x65 x138)) (* x66 x142)) ?v_73)) (>= (+ (+ (+ (* x67 x127) (* x68 x131)) (* x69 x135)) (* x70 x139)) ?v_62)) (>= (+ (+ (+ (* x67 x128) (* x68 x132)) (* x69 x136)) (* x70 x140)) ?v_66)) (>= (+ (+ (+ (* x67 x129) (* x68 x133)) (* x69 x137)) (* x70 x141)) ?v_70)) (>= (+ (+ (+ (* x67 x130) (* x68 x134)) (* x69 x138)) (* x70 x142)) ?v_74)))) (and (> ?v_78 x5) (and (and (and (>= ?v_78 x5) (>= (+ x52 (+ (+ (+ (* x59 x5) (* x60 x6)) (* x61 x7)) (* x62 x8))) x6)) (>= (+ x53 (+ (+ (+ (* x63 x5) (* x64 x6)) (* x65 x7)) (* x66 x8))) x7)) (>= (+ x54 (+ (+ (+ (* x67 x5) (* x68 x6)) (* x69 x7)) (* x70 x8))) x8)))) (and (> ?v_79 x27) (and (and (and (>= ?v_79 x27) (>= (+ x52 (+ (+ (+ (* x59 x27) (* x60 x28)) (* x61 x29)) (* x62 x30))) x28)) (>= (+ x53 (+ (+ (+ (* x63 x27) (* x64 x28)) (* x65 x29)) (* x66 x30))) x29)) (>= (+ x54 (+ (+ (+ (* x67 x27) (* x68 x28)) (* x69 x29)) (* x70 x30))) x30)))) (and (and (> ?v_80 ?v_81) (and (and (and (>= ?v_80 ?v_81) (>= (+ x52 (+ (+ (+ (* x59 x31) (* x60 x32)) (* x61 x33)) (* x62 x34))) (+ x32 (+ (+ (+ (* x39 x51) (* x40 x52)) (* x41 x53)) (* x42 x54))))) (>= (+ x53 (+ (+ (+ (* x63 x31) (* x64 x32)) (* x65 x33)) (* x66 x34))) (+ x33 (+ (+ (+ (* x43 x51) (* x44 x52)) (* x45 x53)) (* x46 x54))))) (>= (+ x54 (+ (+ (+ (* x67 x31) (* x68 x32)) (* x69 x33)) (* x70 x34))) (+ x34 (+ (+ (+ (* x47 x51) (* x48 x52)) (* x49 x53)) (* x50 x54)))))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= (+ (+ (+ (* x55 x35) (* x56 x39)) (* x57 x43)) (* x58 x47)) (+ (+ (+ (* x35 x55) (* x36 x59)) (* x37 x63)) (* x38 x67))) (>= (+ (+ (+ (* x55 x36) (* x56 x40)) (* x57 x44)) (* x58 x48)) (+ (+ (+ (* x35 x56) (* x36 x60)) (* x37 x64)) (* x38 x68)))) (>= (+ (+ (+ (* x55 x37) (* x56 x41)) (* x57 x45)) (* x58 x49)) (+ (+ (+ (* x35 x57) (* x36 x61)) (* x37 x65)) (* x38 x69)))) (>= (+ (+ (+ (* x55 x38) (* x56 x42)) (* x57 x46)) (* x58 x50)) (+ (+ (+ (* x35 x58) (* x36 x62)) (* x37 x66)) (* x38 x70)))) (>= (+ (+ (+ (* x59 x35) (* x60 x39)) (* x61 x43)) (* x62 x47)) (+ (+ (+ (* x39 x55) (* x40 x59)) (* x41 x63)) (* x42 x67)))) (>= (+ (+ (+ (* x59 x36) (* x60 x40)) (* x61 x44)) (* x62 x48)) (+ (+ (+ (* x39 x56) (* x40 x60)) (* x41 x64)) (* x42 x68)))) (>= (+ (+ (+ (* x59 x37) (* x60 x41)) (* x61 x45)) (* x62 x49)) (+ (+ (+ (* x39 x57) (* x40 x61)) (* x41 x65)) (* x42 x69)))) (>= (+ (+ (+ (* x59 x38) (* x60 x42)) (* x61 x46)) (* x62 x50)) (+ (+ (+ (* x39 x58) (* x40 x62)) (* x41 x66)) (* x42 x70)))) (>= (+ (+ (+ (* x63 x35) (* x64 x39)) (* x65 x43)) (* x66 x47)) (+ (+ (+ (* x43 x55) (* x44 x59)) (* x45 x63)) (* x46 x67)))) (>= (+ (+ (+ (* x63 x36) (* x64 x40)) (* x65 x44)) (* x66 x48)) (+ (+ (+ (* x43 x56) (* x44 x60)) (* x45 x64)) (* x46 x68)))) (>= (+ (+ (+ (* x63 x37) (* x64 x41)) (* x65 x45)) (* x66 x49)) (+ (+ (+ (* x43 x57) (* x44 x61)) (* x45 x65)) (* x46 x69)))) (>= (+ (+ (+ (* x63 x38) (* x64 x42)) (* x65 x46)) (* x66 x50)) (+ (+ (+ (* x43 x58) (* x44 x62)) (* x45 x66)) (* x46 x70)))) (>= (+ (+ (+ (* x67 x35) (* x68 x39)) (* x69 x43)) (* x70 x47)) (+ (+ (+ (* x47 x55) (* x48 x59)) (* x49 x63)) (* x50 x67)))) (>= (+ (+ (+ (* x67 x36) (* x68 x40)) (* x69 x44)) (* x70 x48)) (+ (+ (+ (* x47 x56) (* x48 x60)) (* x49 x64)) (* x50 x68)))) (>= (+ (+ (+ (* x67 x37) (* x68 x41)) (* x69 x45)) (* x70 x49)) (+ (+ (+ (* x47 x57) (* x48 x61)) (* x49 x65)) (* x50 x69)))) (>= (+ (+ (+ (* x67 x38) (* x68 x42)) (* x69 x46)) (* x70 x50)) (+ (+ (+ (* x47 x58) (* x48 x62)) (* x49 x66)) (* x50 x70)))))) (and (and (and (> x143 x71) (and (and (and (>= x143 x71) (>= x144 x72)) (>= x145 x73)) (>= x146 x74))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x147 x75) (>= x148 x76)) (>= x149 x77)) (>= x150 x78)) (>= x151 x79)) (>= x152 x80)) (>= x153 x81)) (>= x154 x82)) (>= x155 x83)) (>= x156 x84)) (>= x157 x85)) (>= x158 x86)) (>= x159 x87)) (>= x160 x88)) (>= x161 x89)) (>= x162 x90))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x163 x91) (>= x164 x92)) (>= x165 x93)) (>= x166 x94)) (>= x167 x95)) (>= x168 x96)) (>= x169 x97)) (>= x170 x98)) (>= x171 x99)) (>= x172 x100)) (>= x173 x101)) (>= x174 x102)) (>= x175 x103)) (>= x176 x104)) (>= x177 x105)) (>= x178 x106)))) (and (and (and (> x179 x107) (and (and (and (>= x179 x107) (>= x180 x108)) (>= x181 x109)) (>= x182 x110))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x183 x111) (>= x184 x112)) (>= x185 x113)) (>= x186 x114)) (>= x187 x115)) (>= x188 x116)) (>= x189 x117)) (>= x190 x118)) (>= x191 x119)) (>= x192 x120)) (>= x193 x121)) (>= x194 x122)) (>= x195 x123)) (>= x196 x124)) (>= x197 x125)) (>= x198 x126))) (and (and (and (and (and (and (and (and (and (and (and (and (and (and (and (>= x199 x127) (>= x200 x128)) (>= x201 x129)) (>= x202 x130)) (>= x203 x131)) (>= x204 x132)) (>= x205 x133)) (>= x206 x134)) (>= x207 x135)) (>= x208 x136)) (>= x209 x137)) (>= x210 x138)) (>= x211 x139)) (>= x212 x140)) (>= x213 x141)) (>= x214 x142)))) ?v_82)))))))
(check-sat)
(exit)
