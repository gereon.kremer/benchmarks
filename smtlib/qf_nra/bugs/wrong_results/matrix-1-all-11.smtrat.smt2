(set-info :status  sat)
(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)


(declare-fun x9 () Real)
(declare-fun x13 () Real)
(declare-fun x11 () Real)
(declare-fun x12 () Real)
(declare-fun x3 () Real)
(declare-fun x6 () Real)
(declare-fun x2 () Real)
(declare-fun x5 () Real)
(declare-fun x4 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x17 () Real)
(declare-fun x15 () Real)
(declare-fun x7 () Real)
(declare-fun x18 () Real)
(declare-fun x8 () Real)
(declare-fun x14 () Real)
(declare-fun x16 () Real)
(declare-fun x10 () Real)
(assert (let ((.def_157 (* x1 x2)))
(let ((.def_179 (*  (- 1.0)  x4)))
(let ((.def_182 (+ .def_179 .def_157)))
(let ((.def_183 (+ x0 .def_182)))
(let ((.def_185 (<= .def_183  0.0 )))
(let ((.def_186 (not .def_185)))
(let ((.def_184 (<=  0.0  .def_183)))
(let ((.def_187 (and .def_184 .def_186)))
(let ((.def_178 (<= x5 x3)))
(let ((.def_188 (and .def_178 .def_187)))
(let ((.def_159 (*  (- 1.0)  .def_157)))
(let ((.def_168 (*  (- 1.0)  x0)))
(let ((.def_171 (+ .def_168 .def_159)))
(let ((.def_172 (+ x6 .def_171)))
(let ((.def_174 (<=  0.0  .def_172)))
(let ((.def_175 (not .def_174)))
(let ((.def_173 (<= .def_172  0.0 )))
(let ((.def_176 (and .def_173 .def_175)))
(let ((.def_167 (<= x7 x3)))
(let ((.def_177 (and .def_167 .def_176)))
(let ((.def_189 (and .def_177 .def_188)))
(let ((.def_154 (* x1 x8)))
(let ((.def_160 (+ .def_154 .def_159)))
(let ((.def_152 (* x3 x10)))
(let ((.def_161 (+ .def_152 .def_160)))
(let ((.def_163 (<=  0.0  .def_161)))
(let ((.def_164 (not .def_163)))
(let ((.def_162 (<= .def_161  0.0 )))
(let ((.def_165 (and .def_162 .def_164)))
(let ((.def_147 (* x1 x9)))
(let ((.def_148 (*  (- 1.0)  .def_147)))
(let ((.def_145 (* x3 x11)))
(let ((.def_146 (*  (- 1.0)  .def_145)))
(let ((.def_149 (+ .def_146 .def_148)))
(let ((.def_150 (+ x3 .def_149)))
(let ((.def_151 (<=  0.0  .def_150)))
(let ((.def_166 (and .def_151 .def_165)))
(let ((.def_190 (and .def_166 .def_189)))
(let ((.def_84 (* x13 x12)))
(let ((.def_85 (+ x12 .def_84)))
(let ((.def_139 (* x7 .def_85)))
(let ((.def_141 (<= .def_139  0.0 )))
(let ((.def_142 (not .def_141)))
(let ((.def_140 (<=  0.0  .def_139)))
(let ((.def_143 (and .def_140 .def_142)))
(let ((.def_81 (* x13 x13)))
(let ((.def_137 (* x7 .def_81)))
(let ((.def_138 (<= x7 .def_137)))
(let ((.def_144 (and .def_138 .def_143)))
(let ((.def_191 (and .def_144 .def_190)))
(let ((.def_126 (* x8 x15)))
(let ((.def_128 (*  (- 1.0)  .def_126)))
(let ((.def_124 (* x10 x16)))
(let ((.def_129 (*  (- 1.0)  .def_124)))
(let ((.def_130 (+ .def_129 .def_128)))
(let ((.def_122 (* x15 x2)))
(let ((.def_131 (+ .def_122 .def_130)))
(let ((.def_133 (<= .def_131  0.0 )))
(let ((.def_134 (not .def_133)))
(let ((.def_132 (<=  0.0  .def_131)))
(let ((.def_135 (and .def_132 .def_134)))
(let ((.def_117 (* x15 x9)))
(let ((.def_118 (*  (- 1.0)  .def_117)))
(let ((.def_115 (* x11 x16)))
(let ((.def_116 (*  (- 1.0)  .def_115)))
(let ((.def_119 (+ .def_116 .def_118)))
(let ((.def_120 (+ x16 .def_119)))
(let ((.def_121 (<=  0.0  .def_120)))
(let ((.def_136 (and .def_121 .def_135)))
(let ((.def_192 (and .def_136 .def_191)))
(let ((.def_104 (* x17 x9)))
(let ((.def_105 (*  (- 1.0)  .def_104)))
(let ((.def_106 (*  (- 1.0)  x8)))
(let ((.def_109 (+ .def_106 .def_105)))
(let ((.def_110 (+ x18 .def_109)))
(let ((.def_112 (<=  0.0  .def_110)))
(let ((.def_113 (not .def_112)))
(let ((.def_111 (<= .def_110  0.0 )))
(let ((.def_114 (and .def_111 .def_113)))
(let ((.def_193 (and .def_114 .def_192)))
(let ((.def_92 (* x13 x17)))
(let ((.def_93 (+ x12 .def_92)))
(let ((.def_94 (* x9 .def_93)))
(let ((.def_95 (*  (- 1.0)  x2)))
(let ((.def_98 (+ .def_95 .def_94)))
(let ((.def_99 (+ x8 .def_98)))
(let ((.def_101 (<= .def_99  0.0 )))
(let ((.def_102 (not .def_101)))
(let ((.def_100 (<=  0.0  .def_99)))
(let ((.def_103 (and .def_100 .def_102)))
(let ((.def_194 (and .def_103 .def_193)))
(let ((.def_86 (* x9 .def_85)))
(let ((.def_88 (<= .def_86  0.0 )))
(let ((.def_89 (not .def_88)))
(let ((.def_87 (<=  0.0  .def_86)))
(let ((.def_90 (and .def_87 .def_89)))
(let ((.def_82 (* x9 .def_81)))
(let ((.def_83 (<= x9 .def_82)))
(let ((.def_91 (and .def_83 .def_90)))
(let ((.def_195 (and .def_91 .def_194)))
(let ((.def_70 (* x17 x11)))
(let ((.def_72 (*  (- 1.0)  x17)))
(let ((.def_75 (+ .def_72 .def_70)))
(let ((.def_76 (+ x10 .def_75)))
(let ((.def_78 (<= .def_76  0.0 )))
(let ((.def_79 (not .def_78)))
(let ((.def_77 (<=  0.0  .def_76)))
(let ((.def_80 (and .def_77 .def_79)))
(let ((.def_196 (and .def_80 .def_195)))
(let ((.def_63 (* x11 x12)))
(let ((.def_64 (+ x10 .def_63)))
(let ((.def_66 (<= .def_64  0.0 )))
(let ((.def_67 (not .def_66)))
(let ((.def_65 (<=  0.0  .def_64)))
(let ((.def_68 (and .def_65 .def_67)))
(let ((.def_60 (* x13 x11)))
(let ((.def_62 (<=  1.0  .def_60)))
(let ((.def_69 (and .def_62 .def_68)))
(let ((.def_197 (and .def_69 .def_196)))
(let ((.def_198 (and .def_191 .def_197)))
(let ((.def_58 (<=  0.0  x16)))
(let ((.def_55 (<=  0.0  x9)))
(let ((.def_52 (<=  0.0  x2)))
(let ((.def_49 (<=  0.0  x12)))
(let ((.def_46 (<=  0.0  x5)))
(let ((.def_43 (<=  0.0  x15)))
(let ((.def_40 (<=  0.0  x8)))
(let ((.def_37 (<=  0.0  x18)))
(let ((.def_34 (<=  0.0  x1)))
(let ((.def_31 (<=  0.0  x11)))
(let ((.def_28 (<=  0.0  x4)))
(let ((.def_25 (<=  0.0  x14)))
(let ((.def_22 (<=  0.0  x7)))
(let ((.def_19 (<=  0.0  x17)))
(let ((.def_16 (<=  0.0  x0)))
(let ((.def_13 (<=  0.0  x10)))
(let ((.def_10 (<=  0.0  x3)))
(let ((.def_7 (<=  0.0  x6)))
(let ((.def_5 (<=  0.0  x13)))
(let ((.def_8 (and .def_5 .def_7)))
(let ((.def_11 (and .def_8 .def_10)))
(let ((.def_14 (and .def_11 .def_13)))
(let ((.def_17 (and .def_14 .def_16)))
(let ((.def_20 (and .def_17 .def_19)))
(let ((.def_23 (and .def_20 .def_22)))
(let ((.def_26 (and .def_23 .def_25)))
(let ((.def_29 (and .def_26 .def_28)))
(let ((.def_32 (and .def_29 .def_31)))
(let ((.def_35 (and .def_32 .def_34)))
(let ((.def_38 (and .def_35 .def_37)))
(let ((.def_41 (and .def_38 .def_40)))
(let ((.def_44 (and .def_41 .def_43)))
(let ((.def_47 (and .def_44 .def_46)))
(let ((.def_50 (and .def_47 .def_49)))
(let ((.def_53 (and .def_50 .def_52)))
(let ((.def_56 (and .def_53 .def_55)))
(let ((.def_59 (and .def_56 .def_58)))
(let ((.def_199 (and .def_59 .def_198)))
.def_199)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
(check-sat)
(exit)
