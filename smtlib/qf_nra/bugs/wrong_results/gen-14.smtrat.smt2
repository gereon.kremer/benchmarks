(set-info :status  unsat)
(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)


(declare-fun a () Real)
(assert (let ((.def_4 (* a a)))
(let ((.def_7 (= .def_4  (- 3.0) )))
.def_7)))
(check-sat)
(exit)
