(set-info :status  unsat)
(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)


(declare-fun x1 () Real)
(declare-fun x3 () Real)
(declare-fun x2 () Real)
(assert (let ((.def_15 (= x1  1.0 )))
(let ((.def_13 (<=  2.0  x2)))
(let ((.def_16 (and .def_13 .def_15)))
(let ((.def_11 (<=  4.0  x3)))
(let ((.def_17 (and .def_11 .def_16)))
(let ((.def_5 (* x1 x2)))
(let ((.def_7 (+ .def_5 x3)))
(let ((.def_9 (<= .def_7  5.0 )))
(let ((.def_18 (and .def_9 .def_17)))
.def_18))))))))))
(check-sat)
(exit)
