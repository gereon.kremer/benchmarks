(set-info :status  unsat)
(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)


(declare-fun x () Real)
(declare-fun b () Real)
(declare-fun a () Real)
(assert (let ((.def_22 (* x x)))
(let ((.def_16 (*  (- 1.0)  a)))
(let ((.def_26 (+ .def_16 .def_22)))
(let ((.def_27 (+ b .def_26)))
(let ((.def_28 (<= .def_27  0.0 )))
(let ((.def_29 (not .def_28)))
(let ((.def_17 (+ b .def_16)))
(let ((.def_18 (+ x .def_17)))
(let ((.def_19 (<=  0.0  .def_18)))
(let ((.def_5 (<= x  2.0 )))
(let ((.def_6 (not .def_5)))
(let ((.def_20 (and .def_6 .def_19)))
(let ((.def_21 (not .def_20)))
(let ((.def_30 (or .def_21 .def_29)))
(let ((.def_31 (not .def_30)))
.def_31))))))))))))))))
(check-sat)
(exit)
