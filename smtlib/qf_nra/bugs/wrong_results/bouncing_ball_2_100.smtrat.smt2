(set-info :status  sat)
(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)


(declare-fun bounce_2 () Real)
(declare-fun p_0 () Real)
(declare-fun bounce_1 () Real)
(declare-fun height_1 () Real)
(declare-fun delta_1 () Real)
(declare-fun bounce_3 () Real)
(declare-fun height_2 () Real)
(declare-fun height_0 () Real)
(declare-fun p_2 () Real)
(declare-fun delta_0 () Real)
(declare-fun p_1 () Real)
(declare-fun delta_2 () Real)
(assert (let ((.def_114 (= bounce_3  0.0 )))
(let ((.def_102 (*  3.0  p_1)))
(let ((.def_103 (+ p_0 .def_102)))
(let ((.def_110 (<= .def_103  117.0 )))
(let ((.def_9 (= bounce_2  0.0 )))
(let ((.def_111 (or .def_9 .def_110)))
(let ((.def_107 (= bounce_3  1.0 )))
(let ((.def_112 (or .def_107 .def_111)))
(let ((.def_104 (<=  83.0  .def_103)))
(let ((.def_105 (or .def_9 .def_104)))
(let ((.def_108 (or .def_105 .def_107)))
(let ((.def_95 (*  (- 1.0)  delta_2)))
(let ((.def_96 (* p_2 .def_95)))
(let ((.def_97 (* p_2 .def_96)))
(let ((.def_98 (+ height_2 .def_97)))
(let ((.def_99 (= .def_98  0.0 )))
(let ((.def_100 (or .def_9 .def_99)))
(let ((.def_88 (*  (- 10.0)  delta_2)))
(let ((.def_89 (*  11.0  delta_1)))
(let ((.def_91 (+ .def_89 .def_88)))
(let ((.def_92 (= .def_91  0.0 )))
(let ((.def_93 (or .def_9 .def_92)))
(let ((.def_80 (*  (- 5.0)  height_2)))
(let ((.def_81 (*  4.0  height_1)))
(let ((.def_83 (+ .def_81 .def_80)))
(let ((.def_84 (= .def_83  0.0 )))
(let ((.def_85 (or .def_9 .def_84)))
(let ((.def_66 (*  3.0  p_0)))
(let ((.def_69 (+ .def_66 p_1)))
(let ((.def_75 (<= .def_69  117.0 )))
(let ((.def_43 (= bounce_1  0.0 )))
(let ((.def_76 (or .def_43 .def_75)))
(let ((.def_8 (= bounce_2  1.0 )))
(let ((.def_77 (or .def_8 .def_76)))
(let ((.def_70 (<=  83.0  .def_69)))
(let ((.def_71 (or .def_43 .def_70)))
(let ((.def_72 (or .def_8 .def_71)))
(let ((.def_57 (*  (- 1.0)  delta_1)))
(let ((.def_59 (* .def_57 p_1)))
(let ((.def_60 (* p_1 .def_59)))
(let ((.def_61 (+ height_1 .def_60)))
(let ((.def_62 (= .def_61  0.0 )))
(let ((.def_63 (or .def_43 .def_62)))
(let ((.def_48 (*  (- 10.0)  delta_1)))
(let ((.def_50 (*  11.0  delta_0)))
(let ((.def_52 (+ .def_50 .def_48)))
(let ((.def_53 (= .def_52  0.0 )))
(let ((.def_54 (or .def_43 .def_53)))
(let ((.def_37 (*  (- 5.0)  height_1)))
(let ((.def_39 (*  4.0  height_0)))
(let ((.def_41 (+ .def_39 .def_37)))
(let ((.def_42 (= .def_41  0.0 )))
(let ((.def_44 (or .def_42 .def_43)))
(let ((.def_33 (= bounce_1  1.0 )))
(let ((.def_30 (= p_0  0.0 )))
(let ((.def_27  (<= (* delta_0 5) 1)))
(let ((.def_23  (<= 1 (* 10 delta_0))))
(let ((.def_18 (<= height_0  200.0 )))
(let ((.def_14 (<= height_0  80.0 )))
(let ((.def_15 (not .def_14)))
(let ((.def_10 (or .def_8 .def_9)))
(let ((.def_5 (<=  0.0  p_2)))
(let ((.def_11 (and .def_5 .def_10)))
(let ((.def_16 (and .def_11 .def_15)))
(let ((.def_19 (and .def_16 .def_18)))
(let ((.def_24 (and .def_19 .def_23)))
(let ((.def_28 (and .def_24 .def_27)))
(let ((.def_31 (and .def_28 .def_30)))
(let ((.def_34 (and .def_31 .def_33)))
(let ((.def_45 (and .def_34 .def_44)))
(let ((.def_55 (and .def_45 .def_54)))
(let ((.def_64 (and .def_55 .def_63)))
(let ((.def_73 (and .def_64 .def_72)))
(let ((.def_78 (and .def_73 .def_77)))
(let ((.def_86 (and .def_78 .def_85)))
(let ((.def_94 (and .def_86 .def_93)))
(let ((.def_101 (and .def_94 .def_100)))
(let ((.def_109 (and .def_101 .def_108)))
(let ((.def_113 (and .def_109 .def_112)))
(let ((.def_115 (and .def_113 .def_114)))
.def_115)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))
(check-sat)
(exit)
