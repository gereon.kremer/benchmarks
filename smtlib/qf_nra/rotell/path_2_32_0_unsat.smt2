(set-logic QF_NRA)
(declare-fun x0at0 () Real)
(declare-fun x0at1 () Real)
(declare-fun x0at2 () Real)
(declare-fun x0at3 () Real)
(declare-fun x0at4 () Real)
(declare-fun x0at5 () Real)
(declare-fun x0at6 () Real)
(declare-fun x0at7 () Real)
(declare-fun x0at8 () Real)
(declare-fun x0at9 () Real)
(declare-fun x0at10 () Real)
(declare-fun x0at11 () Real)
(declare-fun x0at12 () Real)
(declare-fun x0at13 () Real)
(declare-fun x0at14 () Real)
(declare-fun x0at15 () Real)
(declare-fun x0at16 () Real)
(declare-fun x0at17 () Real)
(declare-fun x0at18 () Real)
(declare-fun x0at19 () Real)
(declare-fun x0at20 () Real)
(declare-fun x0at21 () Real)
(declare-fun x0at22 () Real)
(declare-fun x0at23 () Real)
(declare-fun x0at24 () Real)
(declare-fun x0at25 () Real)
(declare-fun x0at26 () Real)
(declare-fun x0at27 () Real)
(declare-fun x0at28 () Real)
(declare-fun x0at29 () Real)
(declare-fun x0at30 () Real)
(declare-fun x0at31 () Real)
(declare-fun x0at32 () Real)
(declare-fun x0at33 () Real)
(declare-fun x1at0 () Real)
(declare-fun x1at1 () Real)
(declare-fun x1at2 () Real)
(declare-fun x1at3 () Real)
(declare-fun x1at4 () Real)
(declare-fun x1at5 () Real)
(declare-fun x1at6 () Real)
(declare-fun x1at7 () Real)
(declare-fun x1at8 () Real)
(declare-fun x1at9 () Real)
(declare-fun x1at10 () Real)
(declare-fun x1at11 () Real)
(declare-fun x1at12 () Real)
(declare-fun x1at13 () Real)
(declare-fun x1at14 () Real)
(declare-fun x1at15 () Real)
(declare-fun x1at16 () Real)
(declare-fun x1at17 () Real)
(declare-fun x1at18 () Real)
(declare-fun x1at19 () Real)
(declare-fun x1at20 () Real)
(declare-fun x1at21 () Real)
(declare-fun x1at22 () Real)
(declare-fun x1at23 () Real)
(declare-fun x1at24 () Real)
(declare-fun x1at25 () Real)
(declare-fun x1at26 () Real)
(declare-fun x1at27 () Real)
(declare-fun x1at28 () Real)
(declare-fun x1at29 () Real)
(declare-fun x1at30 () Real)
(declare-fun x1at31 () Real)
(declare-fun x1at32 () Real)
(declare-fun x1at33 () Real)


































(assert (<= (+ (* (- x0at0 0) (+ (* 1 (- x0at0 0)) (* 0 (- x0at0 0)))) (* (- x1at0 0) (+ (* 0 (- x1at0 0)) (* 1 (- x1at0 0))))) 1))
(assert (<= (+ (* 1 x0at0) (* 1 x1at0)) 1.66678))
(assert (= x0at1 (+ (* 0.5 x0at0) (* -1 x1at0) )))
(assert (= x1at1 (+ (* 0.97 x0at0) (* 0 x1at0) )))
(assert (<= (+ (* -1 x0at1) (* 1.5464 x1at1)) 1.66722))
(assert (= x0at2 (+ (* 0.5 x0at1) (* -1 x1at1) )))
(assert (= x1at2 (+ (* 0.97 x0at1) (* 0 x1at1) )))
(assert (<= (+ (* -1.54639 x0at2) (* -0.233818 x1at2)) 1.66728))
(assert (= x0at3 (+ (* 0.5 x0at2) (* -1 x1at2) )))
(assert (= x1at3 (+ (* 0.97 x0at2) (* 0 x1at2) )))
(assert (<= (+ (* 0.233819 x0at3) (* -1.71474 x1at3)) 1.66698))
(assert (= x0at4 (+ (* 0.5 x0at3) (* -1 x1at3) )))
(assert (= x1at4 (+ (* 0.97 x0at3) (* 0 x1at3) )))
(assert (<= (+ (* 1.71475 x0at4) (* -0.642838 x1at4)) 1.66707))
(assert (= x0at5 (+ (* 0.5 x0at4) (* -1 x1at4) )))
(assert (= x1at5 (+ (* 0.97 x0at4) (* 0 x1at4) )))
(assert (<= (+ (* 0.642839 x0at5) (* 1.43642 x1at5)) 1.66735))
(assert (= x0at6 (+ (* 0.5 x0at5) (* -1 x1at5) )))
(assert (= x1at6 (+ (* 0.97 x0at5) (* 0 x1at5) )))
(assert (<= (+ (* -1.43641 x0at6) (* 1.40315 x1at6)) 1.66745))
(assert (= x0at7 (+ (* 0.5 x0at6) (* -1 x1at6) )))
(assert (= x1at7 (+ (* 0.97 x0at6) (* 0 x1at6) )))
(assert (<= (+ (* -1.40314 x0at7) (* -0.757573 x1at7)) 1.66742))
(assert (= x0at8 (+ (* 0.5 x0at7) (* -1 x1at7) )))
(assert (= x1at8 (+ (* 0.97 x0at7) (* 0 x1at7) )))
(assert (<= (+ (* 0.757574 x0at8) (* -1.83703 x1at8)) 1.66736))
(assert (= x0at9 (+ (* 0.5 x0at8) (* -1 x1at8) )))
(assert (= x1at9 (+ (* 0.97 x0at8) (* 0 x1at8) )))
(assert (<= (+ (* 1.83704 x0at9) (* -0.165923 x1at9)) 1.66729))
(assert (= x0at10 (+ (* 0.5 x0at9) (* -1 x1at9) )))
(assert (= x1at10 (+ (* 0.97 x0at9) (* 0 x1at9) )))
(assert (<= (+ (* 0.165924 x0at10) (* 1.80833 x1at10)) 1.66721))
(assert (= x0at11 (+ (* 0.5 x0at10) (* -1 x1at10) )))
(assert (= x1at11 (+ (* 0.97 x0at10) (* 0 x1at10) )))
(assert (<= (+ (* -1.80832 x0at11) (* 1.10319 x1at11)) 1.66779))
(assert (= x0at12 (+ (* 0.5 x0at11) (* -1 x1at11) )))
(assert (= x1at12 (+ (* 0.97 x0at11) (* 0 x1at11) )))
(assert (<= (+ (* -1.10318 x0at12) (* -1.2956 x1at12)) 1.66754))
(assert (= x0at13 (+ (* 0.5 x0at12) (* -1 x1at12) )))
(assert (= x1at13 (+ (* 0.97 x0at12) (* 0 x1at12) )))
(assert (<= (+ (* 1.29561 x0at13) (* -1.80513 x1at13)) 1.66736))
(assert (= x0at14 (+ (* 0.5 x0at13) (* -1 x1at13) )))
(assert (= x1at14 (+ (* 0.97 x0at13) (* 0 x1at13) )))
(assert (<= (+ (* 1.80514 x0at14) (* 0.405191 x1at14)) 1.66795))
(assert (= x0at15 (+ (* 0.5 x0at14) (* -1 x1at14) )))
(assert (= x1at15 (+ (* 0.97 x0at14) (* 0 x1at14) )))
(assert (<= (+ (* -0.40519 x0at15) (* 2.06983 x1at15)) 1.6676))
(assert (= x0at16 (+ (* 0.5 x0at15) (* -1 x1at15) )))
(assert (= x1at16 (+ (* 0.97 x0at15) (* 0 x1at15) )))
(assert (<= (+ (* -2.06982 x0at16) (* 0.6492 x1at16)) 1.66736))
(assert (= x0at17 (+ (* 0.5 x0at16) (* -1 x1at16) )))
(assert (= x1at17 (+ (* 0.97 x0at16) (* 0 x1at16) )))
(assert (<= (+ (* -0.649199 x0at17) (* -1.7992 x1at17)) 1.66789))
(assert (= x0at18 (+ (* 0.5 x0at17) (* -1 x1at17) )))
(assert (= x1at18 (+ (* 0.97 x0at17) (* 0 x1at17) )))
(assert (<= (+ (* 1.79921 x0at18) (* -1.5967 x1at18)) 1.66751))
(assert (= x0at19 (+ (* 0.5 x0at18) (* -1 x1at18) )))
(assert (= x1at19 (+ (* 0.97 x0at18) (* 0 x1at18) )))
(assert (<= (+ (* 1.59671 x0at19) (* 1.03181 x1at19)) 1.66732))
(assert (= x0at20 (+ (* 0.5 x0at19) (* -1 x1at19) )))
(assert (= x1at20 (+ (* 0.97 x0at19) (* 0 x1at19) )))
(assert (<= (+ (* -1.0318 x0at20) (* 2.17795 x1at20)) 1.66767))
(assert (= x0at21 (+ (* 0.5 x0at20) (* -1 x1at20) )))
(assert (= x1at21 (+ (* 0.97 x0at20) (* 0 x1at20) )))
(assert (<= (+ (* -2.17794 x0at21) (* 0.0589335 x1at21)) 1.66739))
(assert (= x0at22 (+ (* 0.5 x0at21) (* -1 x1at21) )))
(assert (= x1at22 (+ (* 0.97 x0at21) (* 0 x1at21) )))
(assert (<= (+ (* -0.0589334 x0at22) (* -2.21492 x1at22)) 1.66784))
(assert (= x0at23 (+ (* 0.5 x0at22) (* -1 x1at22) )))
(assert (= x1at23 (+ (* 0.97 x0at22) (* 0 x1at22) )))
(assert (<= (+ (* 2.21493 x0at23) (* -1.20247 x1at23)) 1.66747))
(assert (= x0at24 (+ (* 0.5 x0at23) (* -1 x1at23) )))
(assert (= x1at24 (+ (* 0.97 x0at23) (* 0 x1at23) )))
(assert (<= (+ (* 1.20248 x0at24) (* 1.6636 x1at24)) 1.66794))
(assert (= x0at25 (+ (* 0.5 x0at24) (* -1 x1at24) )))
(assert (= x1at25 (+ (* 0.97 x0at24) (* 0 x1at24) )))
(assert (<= (+ (* -1.66359 x0at25) (* 2.09719 x1at25)) 1.66753))
(assert (= x0at26 (+ (* 0.5 x0at25) (* -1 x1at25) )))
(assert (= x1at26 (+ (* 0.97 x0at25) (* 0 x1at25) )))
(assert (<= (+ (* -2.09718 x0at26) (* -0.634026 x1at26)) 1.66735))
(assert (= x0at27 (+ (* 0.5 x0at26) (* -1 x1at26) )))
(assert (= x1at27 (+ (* 0.97 x0at26) (* 0 x1at26) )))
(assert (<= (+ (* 0.634027 x0at27) (* -2.48886 x1at27)) 1.66759))
(assert (= x0at28 (+ (* 0.5 x0at27) (* -1 x1at27) )))
(assert (= x1at28 (+ (* 0.97 x0at27) (* 0 x1at27) )))
(assert (<= (+ (* 2.48887 x0at28) (* -0.629284 x1at28)) 1.66797))
(assert (= x0at29 (+ (* 0.5 x0at28) (* -1 x1at28) )))
(assert (= x1at29 (+ (* 0.97 x0at28) (* 0 x1at28) )))
(assert (<= (+ (* 0.629285 x0at29) (* 2.24147 x1at29)) 1.66764))
(assert (= x0at30 (+ (* 0.5 x0at29) (* -1 x1at29) )))
(assert (= x1at30 (+ (* 0.97 x0at29) (* 0 x1at29) )))
(assert (<= (+ (* -2.24146 x0at30) (* 1.80415 x1at30)) 1.66799))
(assert (= x0at31 (+ (* 0.5 x0at30) (* -1 x1at30) )))
(assert (= x1at31 (+ (* 0.97 x0at30) (* 0 x1at30) )))
(assert (<= (+ (* -1.80414 x0at31) (* -1.38082 x1at31)) 1.66756))
(assert (= x0at32 (+ (* 0.5 x0at31) (* -1 x1at31) )))
(assert (= x1at32 (+ (* 0.97 x0at31) (* 0 x1at31) )))
(assert (<= (+ (* -1.38082 x0at32) (* 2.57171 x1at32)) -4.61527))
(assert (= x0at33 (+ (* 1 x0at32) (* 0 x1at32) )))
(assert (= x1at33 (+ (* 0 x0at32) (* 1 x1at32) )))
(check-sat)
(exit)
