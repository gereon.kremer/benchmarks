(set-logic QF_NRA)
(declare-fun x0at0 () Real)
(declare-fun x0at1 () Real)
(declare-fun x0at2 () Real)
(declare-fun x0at3 () Real)
(declare-fun x0at4 () Real)
(declare-fun x1at0 () Real)
(declare-fun x1at1 () Real)
(declare-fun x1at2 () Real)
(declare-fun x1at3 () Real)
(declare-fun x1at4 () Real)





(assert (<= (+ (* (- x0at0 0) (+ (* 1 (- x0at0 0)) (* 0 (- x0at0 0)))) (* (- x1at0 0) (+ (* 0 (- x1at0 0)) (* 1 (- x1at0 0))))) 1))
(assert (<= (+ (* 1 x0at0) (* 1 x1at0)) 0.888947))
(assert (= x0at1 (+ (* 0.5 x0at0) (* -1 x1at0) )))
(assert (= x1at1 (+ (* 0.97 x0at0) (* 0 x1at0) )))
(assert (<= (+ (* -1 x0at1) (* 1.5464 x1at1)) 0.889101))
(assert (= x0at2 (+ (* 0.5 x0at1) (* -1 x1at1) )))
(assert (= x1at2 (+ (* 0.97 x0at1) (* 0 x1at1) )))
(assert (<= (+ (* -1.54639 x0at2) (* -0.233818 x1at2)) 0.889386))
(assert (= x0at3 (+ (* 0.5 x0at2) (* -1 x1at2) )))
(assert (= x1at3 (+ (* 0.97 x0at2) (* 0 x1at2) )))
(assert (<= (+ (* 0.233819 x0at3) (* -1.71474 x1at3)) 0.889076))
(assert (= x0at4 (+ (* 0.5 x0at3) (* -1 x1at3) )))
(assert (= x1at4 (+ (* 0.97 x0at3) (* 0 x1at3) )))
(check-sat)
(exit)
