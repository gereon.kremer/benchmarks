(set-logic QF_NRA)
(declare-fun x0at0 () Real)
(declare-fun x0at1 () Real)
(declare-fun x0at2 () Real)
(declare-fun x1at0 () Real)
(declare-fun x1at1 () Real)
(declare-fun x1at2 () Real)
(declare-fun x2at0 () Real)
(declare-fun x2at1 () Real)
(declare-fun x2at2 () Real)
(declare-fun x3at0 () Real)
(declare-fun x3at1 () Real)
(declare-fun x3at2 () Real)
(declare-fun x4at0 () Real)
(declare-fun x4at1 () Real)
(declare-fun x4at2 () Real)
(declare-fun x5at0 () Real)
(declare-fun x5at1 () Real)
(declare-fun x5at2 () Real)
(declare-fun x6at0 () Real)
(declare-fun x6at1 () Real)
(declare-fun x6at2 () Real)
(declare-fun x7at0 () Real)
(declare-fun x7at1 () Real)
(declare-fun x7at2 () Real)
(declare-fun x8at0 () Real)
(declare-fun x8at1 () Real)
(declare-fun x8at2 () Real)
(declare-fun x9at0 () Real)
(declare-fun x9at1 () Real)
(declare-fun x9at2 () Real)
(declare-fun x10at0 () Real)
(declare-fun x10at1 () Real)
(declare-fun x10at2 () Real)
(declare-fun x11at0 () Real)
(declare-fun x11at1 () Real)
(declare-fun x11at2 () Real)
(declare-fun x12at0 () Real)
(declare-fun x12at1 () Real)
(declare-fun x12at2 () Real)
(declare-fun x13at0 () Real)
(declare-fun x13at1 () Real)
(declare-fun x13at2 () Real)
(declare-fun x14at0 () Real)
(declare-fun x14at1 () Real)
(declare-fun x14at2 () Real)
(declare-fun x15at0 () Real)
(declare-fun x15at1 () Real)
(declare-fun x15at2 () Real)

(declare-fun i0at0 () Real)
(declare-fun i1at0 () Real)
(declare-fun i2at0 () Real)
(declare-fun i3at0 () Real)
(declare-fun i4at0 () Real)
(declare-fun i5at0 () Real)
(declare-fun i6at0 () Real)
(declare-fun i7at0 () Real)
(declare-fun i8at0 () Real)
(declare-fun i9at0 () Real)
(declare-fun i10at0 () Real)
(declare-fun i11at0 () Real)
(declare-fun i12at0 () Real)
(declare-fun i13at0 () Real)
(declare-fun i14at0 () Real)
(declare-fun i15at0 () Real)


(assert (<= (+ (* (- x0at0 0) (+ (* 1 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)) (* 0 (- x0at0 0)))) (* (- x1at0 0) (+ (* 0 (- x1at0 0)) (* 1 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)) (* 0 (- x1at0 0)))) (* (- x2at0 0) (+ (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 1 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)) (* 0 (- x2at0 0)))) (* (- x3at0 0) (+ (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 1 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)) (* 0 (- x3at0 0)))) (* (- x4at0 0) (+ (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 1 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)) (* 0 (- x4at0 0)))) (* (- x5at0 0) (+ (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 1 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)) (* 0 (- x5at0 0)))) (* (- x6at0 0) (+ (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 1 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)) (* 0 (- x6at0 0)))) (* (- x7at0 0) (+ (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 1 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)) (* 0 (- x7at0 0)))) (* (- x8at0 0) (+ (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 1 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)) (* 0 (- x8at0 0)))) (* (- x9at0 0) (+ (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 1 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)) (* 0 (- x9at0 0)))) (* (- x10at0 0) (+ (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 1 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)) (* 0 (- x10at0 0)))) (* (- x11at0 0) (+ (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 1 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)) (* 0 (- x11at0 0)))) (* (- x12at0 0) (+ (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 1 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)) (* 0 (- x12at0 0)))) (* (- x13at0 0) (+ (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)) (* 1 (- x13at0 0)) (* 0 (- x13at0 0)) (* 0 (- x13at0 0)))) (* (- x14at0 0) (+ (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 0 (- x14at0 0)) (* 1 (- x14at0 0)) (* 0 (- x14at0 0)))) (* (- x15at0 0) (+ (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 0 (- x15at0 0)) (* 1 (- x15at0 0))))) 1))
(assert (<= (+ (* i0at0 i0at0) (* i1at0 i1at0) (* i2at0 i2at0) (* i3at0 i3at0) (* i4at0 i4at0) (* i5at0 i5at0) (* i6at0 i6at0) (* i7at0 i7at0) (* i8at0 i8at0) (* i9at0 i9at0) (* i10at0 i10at0) (* i11at0 i11at0) (* i12at0 i12at0) (* i13at0 i13at0) (* i14at0 i14at0) (* i15at0 i15at0)) 1))
(assert (<= (+ (* 1 x0at0) (* 1 x1at0) (* 1 x2at0) (* 1 x3at0) (* 1 x4at0) (* 1 x5at0) (* 1 x6at0) (* 1 x7at0) (* 1 x8at0) (* 1 x9at0) (* 1 x10at0) (* 1 x11at0) (* 1 x12at0) (* 1 x13at0) (* 1 x14at0) (* 1 x15at0)) 0))
(assert (= x0at1 (+ (* 0.5 x0at0) (* -1 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 1 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x1at1 (+ (* 0.97 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 1 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x2at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0.5 x2at0) (* -1 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 1 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x3at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0.97 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 1 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x4at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0.5 x4at0) (* -1 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 1 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x5at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0.97 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 1 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x6at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0.5 x6at0) (* -1 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 1 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x7at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0.97 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 1 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x8at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0.5 x8at0) (* -1 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 1 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x9at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0.97 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 1 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x10at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0.5 x10at0) (* -1 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 1 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x11at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0.97 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 1 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x12at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0.5 x12at0) (* -1 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 1 i12at0) (* 0 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x13at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0.97 x12at0) (* 0 x13at0) (* 0 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 1 i13at0) (* 0 i14at0) (* 0 i15at0) 0)))
(assert (= x14at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0.5 x14at0) (* -1 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 1 i14at0) (* 0 i15at0) 0)))
(assert (= x15at1 (+ (* 0 x0at0) (* 0 x1at0) (* 0 x2at0) (* 0 x3at0) (* 0 x4at0) (* 0 x5at0) (* 0 x6at0) (* 0 x7at0) (* 0 x8at0) (* 0 x9at0) (* 0 x10at0) (* 0 x11at0) (* 0 x12at0) (* 0 x13at0) (* 0.97 x14at0) (* 0 x15at0) (* 0 i0at0) (* 0 i1at0) (* 0 i2at0) (* 0 i3at0) (* 0 i4at0) (* 0 i5at0) (* 0 i6at0) (* 0 i7at0) (* 0 i8at0) (* 0 i9at0) (* 0 i10at0) (* 0 i11at0) (* 0 i12at0) (* 0 i13at0) (* 0 i14at0) (* 1 i15at0) 0)))
(assert (<= (+ (* -1 x0at1) (* 1.5464 x1at1) (* -1 x2at1) (* 1.5464 x3at1) (* -1 x4at1) (* 1.5464 x5at1) (* -1 x6at1) (* 1.5464 x7at1) (* -1 x8at1) (* 1.5464 x9at1) (* -1 x10at1) (* 1.5464 x11at1) (* -1 x12at1) (* 1.5464 x13at1) (* -1 x14at1) (* 1.5464 x15at1)) -0.0460021))
(assert (= x0at2 (+ (* 0.5 x0at1) (* -1 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x1at2 (+ (* 0.97 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x2at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0.5 x2at1) (* -1 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x3at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0.97 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x4at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0.5 x4at1) (* -1 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x5at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0.97 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x6at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0.5 x6at1) (* -1 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x7at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0.97 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x8at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0.5 x8at1) (* -1 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x9at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0.97 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x10at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0.5 x10at1) (* -1 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x11at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0.97 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x12at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0.5 x12at1) (* -1 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x13at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0.97 x12at1) (* 0 x13at1) (* 0 x14at1) (* 0 x15at1) )))
(assert (= x14at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0.5 x14at1) (* -1 x15at1) )))
(assert (= x15at2 (+ (* 0 x0at1) (* 0 x1at1) (* 0 x2at1) (* 0 x3at1) (* 0 x4at1) (* 0 x5at1) (* 0 x6at1) (* 0 x7at1) (* 0 x8at1) (* 0 x9at1) (* 0 x10at1) (* 0 x11at1) (* 0 x12at1) (* 0 x13at1) (* 0.97 x14at1) (* 0 x15at1) )))
(check-sat)
(exit)
