(set-logic QF_NRA)
(declare-fun x0 () Real)
(declare-fun y0 () Real)

(declare-fun i0 () Real)
(declare-fun j0 () Real)

(declare-fun i1 () Real)
(declare-fun j1 () Real)

(assert (<= i0 1))
(assert (<= j0 1))
(assert (<= i1 1))
(assert (<= j1 1))
(assert (<= x0 1))
(assert (<= y0 1))
(assert (>= i0 (- 1)))
(assert (>= j0 (- 1)))
(assert (>= i1 (- 1)))
(assert (>= j1 (- 1)))
(assert (>= x0 (- 1)))
(assert (>= y0 (- 1)))

(assert (<= (+ i0 j0) 1.5))
(assert (>= (+ i0 j0) (- 1.5)))
(assert (<= (- i0 j0) 1.5))
(assert (>= (- i0 j0) (- 1.5)))
(assert (<= (+ (* i0 i0) (* j0 j0)) 1))

(assert (<= (+ i1 j1) 1.5))
(assert (>= (+ i1 j1) (- 1.5)))
(assert (<= (- i1 j1) 1.5))
(assert (>= (- i1 j1) (- 1.5)))
(assert (<= (+ (* i1 i1) (* j1 j1)) 1))

(assert (<= (+ x0 y0) 1.5))
(assert (>= (+ x0 y0) (- 1.5)))
(assert (<= (- x0 y0) 1.5))
(assert (>= (- x0 y0) (- 1.5)))
(assert (<= (+ (* x0 x0) (* y0 y0)) 1))

(assert
	(>=
		(+
			(* 147 (+ i0 (- y0)))
			(* 100 (+ i1 j1 (- j0)))
		)
		(+
			488.077
			(* 23.5 x0)
		)
	)
)

(check-sat)
(exit)


;j0^2+i0^2+(-1)<=0
;j1^2+i1^2+(-1)<=0
;10000*j1^2 +
;(-9700)*i0*j0 +
;10000*y2*j0 + 
;(-20000)*y2*j1 + 
;19400*i0*j1 + 
;(-10000)*y1*y2+9409*i0^2+(-10000)*j0*j1+9700*y1*i0+10000*y1*j1+12500*j0^2+(-25000)*y1*j0+10000*y2^2+(-19400)*y2*i0+12500*y1^2+(-9409)<=0
;5000000*j1+(-9700000)*i1+(-14700000)*y2+9700000*y1+47343469<=0
