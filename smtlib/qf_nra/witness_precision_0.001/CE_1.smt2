(set-logic QF_NRA)
(declare-fun x () Real)
(declare-fun w () Real)
(declare-fun z () Real)
(declare-fun y () Real)

(assert (> (+ (* x x) (* w w) (* z z) (* y y)) 0.2))

(assert (<= x 1.0))
(assert (>= x (- 1.0)))
(assert (<= w 1.0))
(assert (>= w (- 1.0)))
(assert (<= z 1.0))
(assert (>= z (- 1.0)))
(assert (<= y 1.0))
(assert (>= y (- 1.0)))
(assert (<= (+
	(* 2.2617 (* y  y ) )
	(* 0.23608 (* y  z ))
	(* 10.0 (* z  z ) )
	(* 0.0 (* w  y ))
	(* 0.0 (* w  z ))
	(* 2.2617 (* w  w ) )
	(* (- 10.0) (* x  y ))
	(* (- 0.23608) (* x  z ))
	(* 0.02359 (* w  x ))
	(* 3.7617 (* x  x ) )
) (* (- 0 0) (+ (* y  y )  (* z  z )  (* w  w )  (* x  x ) ))))
(check-sat)
(exit)
