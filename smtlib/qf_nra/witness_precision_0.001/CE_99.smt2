(set-logic QF_NRA)
(declare-fun x () Real)
(declare-fun w () Real)
(declare-fun z () Real)
(declare-fun y () Real)

(assert (> (+ (* x x) (* w w) (* z z) (* y y)) 0.2))

(assert (<= x 1.0))
(assert (>= x (- 1.0)))
(assert (<= w 1.0))
(assert (>= w (- 1.0)))
(assert (<= z 1.0))
(assert (>= z (- 1.0)))
(assert (<= y 1.0))
(assert (>= y (- 1.0)))
(assert (<= (+
	(* 3.3867 (* y  y ) )
	(* 5.5312 (* y  z ))
	(* 10.0 (* z  z ) )
	(* 6.2266 (* w  y ))
	(* 6.1172 (* w  z ))
	(* 3.0156 (* w  w ) )
	(* (- 8.0312) (* x  y ))
	(* (- 10.0) (* x  z ))
	(* (- 7.9766) (* w  x ))
	(* 10.0 (* x  x ) )
) (* (- 0 0) (+ (* y  y )  (* z  z )  (* w  w )  (* x  x ) ))))
(check-sat)
(exit)
