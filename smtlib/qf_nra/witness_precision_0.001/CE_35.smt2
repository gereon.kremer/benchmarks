(set-logic QF_NRA)
(declare-fun x () Real)
(declare-fun w () Real)
(declare-fun z () Real)
(declare-fun y () Real)

(assert (> (+ (* x x) (* w w) (* z z) (* y y)) 0.2))

(assert (<= x 1.0))
(assert (>= x -1.0))
(assert (<= w 1.0))
(assert (>= w -1.0))
(assert (<= z 1.0))
(assert (>= z -1.0))
(assert (<= y 1.0))
(assert (>= y -1.0))
(assert (<= (+
	(* 3.6133 (* y  y ) )
	(* 3.9805 (* y  z ))
	(* 10.0 (* z  z ) )
	(* 0.062988 (* w  y ))
	(* -0.37207 (* w  z ))
	(* 2.1602 (* w  w ) )
	(* -7.8438 (* x  y ))
	(* -10.0 (* x  z ))
	(* -0.7793 (* w  x ))
	(* 9.0938 (* x  x ) )
) (* (- 0 0) (+ (* y  y )  (* z  z )  (* w  w )  (* x  x ) ))))
(check-sat)
(exit)
