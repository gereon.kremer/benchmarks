(set-logic QF_NRA)
(set-info :status unsat)
(declare-fun a () Real)
(declare-fun y () Real)
(declare-fun d () Real)
(declare-fun x () Real)
(assert 
    (and 
        (<= 
            (+ 
                (* a a) 
                (* (- 2) (* a y)) 
                (* y y) 
                (- 1)
            ) 
            x
        )
        (<= 
            (+ 
                (* (- 2) (* a y)) 
                (* y y) 
            ) 
            x
        )
        (> 
            (+ 
                (* a a) 
                (* (- 2) (* a y)) 
                (* y y) 
                (* (- 1) a)
            ) 
            x
        )
        (> 
            (+ 
                (* a a) 
                (* 2 (* a d)) 
                (* d d) 
                (* (- 2) d) 
                (* (- 2) a) 
                (* (- 2) (* a y)) 
                (* (- 2) (* y d)) 
                (* y y) 
                1
            ) 
            x
        )
        (>= 0 x)
        (>= d 0)
        (>= a 0)
    )
)
(check-sat)
(exit)

