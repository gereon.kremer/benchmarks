(set-logic QF_NIA)

(declare-fun x () Int)
(declare-fun y () Int)
(assert 
	(and 
		(<= (+ (* x y) (* 2 x) (- 3)) 0) 
		(>= (+ (* 4 x) (* (- 1) y) (- 3)) 0) 
		(>= (+ x (* (- 2) y y) (- (/ 1 2))) 0)
	))
(check-sat)
