#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

# Example input file generator for the SMT-Solver using virtual substitution.
# Example originally from (2004-04-14): http://hysat.informatik.uni-oldenburg.de/benchmarks/etcs_train_system.hys
# @author: Florian Corzilius
# @author: Ulrich Loup
# @version: 2010-12-13

#
# Constants
#

#
# Default values
#

dt = "dt" # time advances in steps of dt seconds
n = 3 # messages are sent every n + 1 steps of the model
s = "400" # safety distance added to braking distance
length = "200" # length of a train (m)
a_min = "(- (/ 14 10))" # maximal deceleration
a_nom = "(- (/ 7 10))" # nominal deceleration
a_max = "(/ 7 10)" # maximal acceleration
v_max = "(/ 834 10)" # maximum speed of a train (m/s)
x_max = "5000" # length of track
param_on = "(/ 7 10)" # switch on point of relay block
param_off = "(/ 3 10)" # switch off point of relay block

#
# Functions
#

def train1Ranges(m):
  formula = ""
  for k in range(0,m+1):
    formula += " (>= a_free1_"+str(k)+" "+a_nom+") (<= a_free1_"+str(k)+" "+a_max+")" # acceleration in 'free running' mode
    formula += " (>= a1_"+str(k)+" "+a_min+") (<= a1_"+str(k)+" "+a_max+")" # acceleration (m/s^2)
    formula += " (>= xr1_"+str(k)+" 0) (<= xr1_"+str(k)+" "+x_max+")" # position of the rear of the train (m)
    formula += " (>= v1_"+str(k)+" 0) (<= v1_"+str(k)+" "+v_max+")" # speed of train 1 (m/s)
  return formula

def train2Ranges(m):
  formula = ""
  for k in range(0,m+1):
    formula += " (or"
    for i in range(0,n+1):
      formula += " (= clk_"+str(k)+" "+str(i)+")"
    formula += ")"
    formula += " (>= i1_"+str(k)+" 0) (<= i1_"+str(k)+" "+v_max+")"
    formula += " (>= o1_"+str(k)+" 0) (<= o1_"+str(k)+" "+v_max+")"
    formula += " (>= o1_mem_"+str(k)+" 0) (<= o1_mem_"+str(k)+" "+v_max+")"
    formula += " (>= i2_"+str(k)+" 0) (<= i2_"+str(k)+" "+x_max+")"
    formula += " (>= o2_"+str(k)+" 0) (<= o2_"+str(k)+" "+x_max+")"
    formula += " (>= o2_mem_"+str(k)+" 0) (<= o2_mem_"+str(k)+" "+x_max+")"
    formula += " (>= i3_"+str(k)+" 0) (<= i3_"+str(k)+" "+x_max+")"
    formula += " (>= o3_"+str(k)+" 0) (<= o3_"+str(k)+" "+x_max+")"
    formula += " (>= o3_mem_"+str(k)+" 0) (<= o3_mem_"+str(k)+" "+x_max+")"
    formula += " (>= h1_"+str(k)+" 0) (<= h1_"+str(k)+" 7000)"
    formula += " (>= h2_"+str(k)+" (- 1000)) (<= h2_"+str(k)+" 10000)"
    formula += " (>= h3_"+str(k)+" (- 10)) (<= h3_"+str(k)+" 10)"
    formula += " (>= h4_"+str(k)+" 0) (<= h4_"+str(k)+" (- "+a_min+"))"
    formula += " (>= a_brake_"+str(k)+" "+a_min+") (<= a_brake_"+str(k)+" 0)" # deceleration when in 'braking' mode
    formula += " (>= a_free2_"+str(k)+" "+a_nom+") (<= a_free2_"+str(k)+" "+a_max+")" # acceleration in 'free running' mode
    formula += " (>= xr2_"+str(k)+" 0) (<= xr2_"+str(k)+" "+x_max+")" # position of the rear of the train (m)
    formula += " (>= v2_"+str(k)+" 0) (<= v2_"+str(k)+" "+v_max+")" # speed of train 1 (m/s)
    formula += " (>= a2_"+str(k)+" "+a_min+") (<= a2_"+str(k)+" "+a_max+")" # acceleration (m/s^2)
  return formula

def init():
  formula = " (= v1_0 0) (= v2_0 0) (= xr1_0 (+ xr2_0 "+length+" 100)) (= xr2_0 0)" # Initially trains are stopped and have a distance of 1000 meters.
  formula += " (= clk_0 0) (= o1_mem_0 i1_0) (= o2_mem_0 i2_0) (= o3_mem_0 i3_0) (not p_0)" # Initialize state-holding blocks.
  return formula

def transitionSteps(m):
  formula = ""
  for k in range(0,m):
    # Dynamics of train 1.
    # Euler approximation of integrators. Integrator of a1 has
    # 0 as lower and v_max as upper saturation limit.
    formula += " (= a1_"+str(k)+" a_free1_"+str(k)+")"
    formula += " (= xr1_"+str(k+1)+" (+ xr1_"+str(k)+" (* "+dt+" v1_"+str(k)+")))"
    formula += " (or (= v1_"+str(k+1)+" (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+"))) (= v1_"+str(k+1)+" 0) (= v1_"+str(k+1)+" "+v_max+"))"  #pro1
    formula += " (or (= v1_"+str(k+1)+" (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+"))) (<= (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+")) 0) (= v1_"+str(k+1)+" "+v_max+"))"
    formula += " (or (= v1_"+str(k+1)+" (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+"))) (= v1_"+str(k+1)+" 0) (= v1_"+str(k+1)+" "+v_max+"))"
    formula += " (or (= v1_"+str(k+1)+" (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+"))) (<= (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+")) 0) (>= (+ v1_"+str(k)+" (* "+dt+" a1_"+str(k)+")) "+v_max+"))"
    # Dynamics of train 2.
    # Wiring.
    formula += " (= i1_"+str(k)+" v2_"+str(k)+")"
    formula += " (= i2_"+str(k)+" (+ xr2_"+str(k)+" "+length+"))"
    formula += " (= i3_"+str(k)+" xr1_"+str(k)+")"
    # Timer subsystem
    formula += " (or (not (= clk_"+str(k)+" "+str(n)+")) (= clk_"+str(k+1)+" 0))"
    formula += " (or (not (< clk_"+str(k)+" "+str(n)+")) (= clk_"+str(k+1)+" (+ clk_"+str(k)+" 1)))"
    formula += " (or (not (= clk_"+str(k)+" 0)) (= o1_"+str(k)+" i1_"+str(k)+"))"
    formula += " (or (not (= clk_"+str(k)+" 0)) (= o2_"+str(k)+" i2_"+str(k)+"))"
    formula += " (or (not (= clk_"+str(k)+" 0)) (= o3_"+str(k)+" i3_"+str(k)+"))"
    formula += " (or (not (= clk_"+str(k)+" 0)) (= o1_mem_"+str(k+1)+" i1_"+str(k)+"))"
    formula += " (or (not (= clk_"+str(k)+" 0)) (= o2_mem_"+str(k+1)+" i2_"+str(k)+"))"
    formula += " (or (not (= clk_"+str(k)+" 0)) (= o3_mem_"+str(k+1)+" o3_"+str(k)+"))"         #pro2
    formula += " (or (not (> clk_"+str(k)+" 0)) (= o1_"+str(k)+" o1_mem_"+str(k)+"))"
    formula += " (or (not (> clk_"+str(k)+" 0)) (= o2_"+str(k)+" o2_mem_"+str(k)+"))"
    formula += " (or (not (> clk_"+str(k)+" 0)) (= o3_"+str(k)+" o3_mem_"+str(k)+"))"
    formula += " (or (not (> clk_"+str(k)+" 0)) (= o1_mem_"+str(k+1)+" o1_mem_"+str(k)+"))"
    formula += " (or (not (> clk_"+str(k)+" 0)) (= o2_mem_"+str(k+1)+" o2_mem_"+str(k)+"))"
    formula += " (or (not (> clk_"+str(k)+" 0)) (= o3_mem_"+str(k+1)+" o3_mem_"+str(k)+"))"
    # Compute deceleration
    formula += " (= h1_"+str(k)+" (* o1_"+str(k)+" "+"o1_"+str(k)+"))"
    formula += " (= h2_"+str(k)+" (+ o3_"+str(k)+" (* (- 1) o2_"+str(k)+") (* (- 1) "+s+")))"
    formula += " (= (* 2 h3_"+str(k)+" h2_"+str(k)+") h1_"+str(k)+")"
    # Saturation block: When the input signal is within the range
    # specified by the Lower limit and Upper limit parameters, the input
    # signal passes through unchanged. When the input signal is outside
    # these bounds, the signal is clipped to the upper or lower bound.
    formula += " (or (not (<= h3_"+str(k)+" 0)) (= h4_"+str(k)+" 0))"
    formula += " (or (not (>= h3_"+str(k)+" (* (- 1) "+a_min+"))) (= h4_"+str(k)+" (* (- 1) "+a_min+")))"
    formula += " (or (not (> h3_"+str(k)+" 0)) (not (< h3_"+str(k)+" (* (- 1) "+a_min+"))) (= h4_"+str(k)+" h3_"+str(k)+"))"
    # Gain block
    formula += " (= a_brake_"+str(k)+" (* (- 1) h4_"+str(k)+"))"
    # Relay block: When the relay is on, it remains on until the input
    # drops below the value of the switch off point parameter. When the
    # relay is off, it remains off until the input exceeds the value of
    # the switch on point parameter.
    formula += " (or p_"+str(k)+" (not (>= h4_"+str(k)+" "+param_on+")) p_"+str(k+1)+")"
    formula += " (or p_"+str(k)+" (not (>= h4_"+str(k)+" "+param_on+")) brake_"+str(k)+")"
    formula += " (or p_"+str(k)+" (not (< h4_"+str(k)+" "+param_on+")) (not p_"+str(k+1)+"))"
    formula += " (or p_"+str(k)+" (not (< h4_"+str(k)+" "+param_on+")) (not brake_"+str(k)+"))"
    formula += " (or (not p_"+str(k)+") (not (<= h4_"+str(k)+" "+param_off+")) (not p_"+str(k+1)+"))"
    formula += " (or (not p_"+str(k)+") (not (<= h4_"+str(k)+" "+param_off+")) (not brake_"+str(k)+"))"
    formula += " (or (not p_"+str(k)+") (not (> h4_"+str(k)+" "+param_off+")) p_"+str(k+1)+")"
    formula += " (or (not p_"+str(k)+") (not (> h4_"+str(k)+" "+param_off+")) brake_"+str(k)+")"
    # Switch
    formula += " (or (not brake_"+str(k)+") (= a2_"+str(k)+" a_brake_"+str(k)+"))"
    formula += " (or brake_"+str(k)+" (= a2_"+str(k)+" a_free2_"+str(k)+"))"
    # Euler approximation of integrators. Integrator of a2 has
    # 0 as lower and v_max as upper saturation limit.
    formula += " (= xr2_"+str(k+1)+" (+ xr2_"+str(k)+" (* "+dt+" v2_"+str(k)+")))"
    formula += " (or (= v2_"+str(k+1)+" (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+"))) (= v2_"+str(k+1)+" 0) (= v2_"+str(k+1)+" "+v_max+"))"
    formula += " (or (= v2_"+str(k+1)+" (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+"))) (<= (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+")) 0) (= v2_"+str(k+1)+" "+v_max+"))"
    formula += " (or (= v2_"+str(k+1)+" (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+"))) (= v2_"+str(k+1)+" 0) (>= (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+")) "+v_max+"))"
    formula += " (or (= v2_"+str(k+1)+" (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+"))) (<= (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+")) 0) (>= (+ v2_"+str(k)+" (* "+dt+" a2_"+str(k)+")) "+v_max+"))"
  return formula

#
# Generate BMC formula up to length n.
#
def formula(m,l,u):
  assert( m>0 )
  assert( l<u )
  lower = ""
  if l < 0:
    lower = "(- " + str(abs(l)) + ")"
  else:
    lower = str(l)
  upper = ""
  if u < 0:
    upper = "(- " + str(abs(u)) + ")"
  else:
    upper = str(u)
  formula = "(set-logic QF_NRA)\n"
  formula += "(set-info :source |\n"
  formula += "Florian Corzilius <corzilius@cs.rwth-aachen.de>\n"
  formula += "\n"
  formula += "|)\n"
  formula += "(set-info :smt-lib-version 2.0)\n"
  formula += "(set-info :category \"industrial\")\n"
  formula += "(declare-fun dt () Real)\n"
  # all Pseudo-Boolean variables
  for k in range(0,m+1):
    formula += "(declare-fun brake_"+str(k)+" () Bool)\n"
  for k in range(0,m+1):
    formula += "(declare-fun p_"+str(k)+" () Bool)\n"
  # all real variables
  for k in range(0,m+1):
    formula += "(declare-fun a_free1_"+str(k)+" () Real)\n"
    formula += "(declare-fun a1_"+str(k)+" () Real)\n"
    formula += "(declare-fun xr1_"+str(k)+" () Real)\n"
    formula += "(declare-fun v1_"+str(k)+" () Real)\n"
    formula += "(declare-fun clk_"+str(k)+" () Real)\n"
    formula += "(declare-fun i1_"+str(k)+" () Real)\n"
    formula += "(declare-fun o1_"+str(k)+" () Real)\n"
    formula += "(declare-fun o1_mem_"+str(k)+" () Real)\n"
    formula += "(declare-fun i2_"+str(k)+" () Real)\n"
    formula += "(declare-fun o2_"+str(k)+" () Real)\n"
    formula += "(declare-fun o2_mem_"+str(k)+" () Real)\n"
    formula += "(declare-fun i3_"+str(k)+" () Real)\n"
    formula += "(declare-fun o3_"+str(k)+" () Real)\n"
    formula += "(declare-fun o3_mem_"+str(k)+" () Real)\n"
    formula += "(declare-fun h1_"+str(k)+" () Real)\n"
    formula += "(declare-fun h2_"+str(k)+" () Real)\n"
    formula += "(declare-fun h3_"+str(k)+" () Real)\n"
    formula += "(declare-fun h4_"+str(k)+" () Real)\n"
    formula += "(declare-fun a_brake_"+str(k)+" () Real)\n"
    formula += "(declare-fun a_free2_"+str(k)+" () Real)\n"
    formula += "(declare-fun xr2_"+str(k)+" () Real)\n"
    formula += "(declare-fun v2_"+str(k)+" () Real)\n"
    formula += "(declare-fun a2_"+str(k)+" () Real)\n"
  formula += "(assert (>= dt " + lower + "))\n"
  formula += "(assert (<= dt " + upper + "))\n"
  formula += "(assert "
  formula += "(and"
  formula += train1Ranges(m)
  formula += train2Ranges(m)
  formula += init()
  formula += transitionSteps(m)
  formula += " (<= (+ xr1_"+str(m)+" (* (- 1) xr2_"+str(m)+") (* (- 1) "+length+")) 0)" #Can trains collide?
  formula += "))\n"
  formula += "(check-sat)\n"
  formula += "(exit)\n"
  return formula

# Print usage information
def printUsage():
  print( "Usage: python "+sys.argv[0]+" m s\n" )
  print( "            m: number (>0), number of steps in the bmc formula" )
  print( "            s: number (>0), safety distance" )
  print( "            l: number being the lower bound of dt" )
  print( "            u: number (>l) being the upper bound of dt" )
  print( "\n" )
  print( "Example: python generate_etcs_example_smtlib.py 3 -1000 1000" )

#
# Parse input.
#
i = 0
for entry in sys.argv:
  try:
    if i == 1:
      m = int(entry)
      if (not(isinstance(m,int)) or m<1): raise ValueError()
    elif i == 2:
      s = int(entry)
      x_max = str(10 * s)
      length = str(s/2)
      if (not(isinstance(s,int)) or s<1): raise ValueError()
      s = str(s)
    if i == 3:
      l = int(entry)
      if (not(isinstance(l,int))): raise ValueError()
    if i == 4:
      u = int(entry)
      if (not(isinstance(u,int)) or l>=u): raise ValueError()
  except ValueError:
    print( "Error:",entry, "should be an appropriate value at position %i." % i )
    printUsage()
    sys.exit(1)
  i += 1
if i != 5:
  print( "Error: Insufficient number of arguments." )
  printUsage()
  sys.exit(1)

#
# Run formula run!
#
print( formula(m,l,u) )
