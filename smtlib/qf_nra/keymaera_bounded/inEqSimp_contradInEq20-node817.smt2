(set-logic QF_NRA)
(set-info :source | KeYmaera example: inEqSimp_contradInEq20, node 81
Andre Platzer, Jan-David Quesel, and Philipp Rümmer. Real world verification. In Renate A. Schmidt, editor, International Conference on Automated Deduction, CADE'09, Montreal, Canada, Proceedings, volume 5663 of LNCS, pages 485(- 501.) Springer, 2009.
 |)
(set-info :smt-lib-version 2.0)
(declare-const contradCoeffBiggeruscore74 Real)
(assert (and (<= contradCoeffBiggeruscore74 1000) (>= contradCoeffBiggeruscore74 (- 1000))))
(declare-const contradCoeffSmalleruscore76 Real)
(assert (and (<= contradCoeffSmalleruscore76 1000) (>= contradCoeffSmalleruscore76 (- 1000))))
(declare-const contradLeftuscore75 Real)
(assert (and (<= contradLeftuscore75 1000) (>= contradLeftuscore75 (- 1000))))
(declare-const contradRightBiggeruscore73 Real)
(assert (and (<= contradRightBiggeruscore73 1000) (>= contradRightBiggeruscore73 (- 1000))))
(declare-const contradRightSmalleruscore77 Real)
(assert (and (<= contradRightSmalleruscore77 1000) (>= contradRightSmalleruscore77 (- 1000))))
(assert (not (=> (and (and (and (> contradCoeffBiggeruscore74 0. ) (> contradCoeffSmalleruscore76 0. )) (>= (* contradLeftuscore75 contradCoeffBiggeruscore74) contradRightBiggeruscore73 )) (< (* contradLeftuscore75 contradCoeffSmalleruscore76) contradRightSmalleruscore77 )) (> (* contradCoeffBiggeruscore74 contradRightSmalleruscore77) (* contradCoeffSmalleruscore76 contradRightBiggeruscore73) ))))
(check-sat)

