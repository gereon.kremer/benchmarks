(set-logic QF_NRA)
(set-info :source | KeYmaera example: bouncing-ball-inv, node 193
Andre Platzer, Jan-David Quesel, and Philipp Rümmer. Real world verification. In Renate A. Schmidt, editor, International Conference on Automated Deduction, CADE'09, Montreal, Canada, Proceedings, volume 5663 of LNCS, pages 485(- 501.) Springer, 2009.
 |)
(set-info :smt-lib-version 2.0)
(declare-const h Real)
(assert (and (<= h 1000) (>= h (- 1000))))
(declare-const v Real)
(assert (and (<= v 1000) (>= v (- 1000))))
(declare-const V Real)
(assert (and (<= V 1000) (>= V (- 1000))))
(declare-const g Real)
(assert (and (<= g 1000) (>= g (- 1000))))
(declare-const c Real)
(assert (and (<= c 1000) (>= c (- 1000))))
(assert (not (=> (and (and (and (and (and (= h 0. ) (= v V )) (> V 0. )) (> g 0. )) (<= 0. c )) (< c 1. )) (<= v V ))))
(check-sat)

