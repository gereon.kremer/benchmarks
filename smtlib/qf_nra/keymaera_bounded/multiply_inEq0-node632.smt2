(set-logic QF_NRA)
(set-info :source | KeYmaera example: multiply_inEq0, node 63
Andre Platzer, Jan-David Quesel, and Philipp Rümmer. Real world verification. In Renate A. Schmidt, editor, International Conference on Automated Deduction, CADE'09, Montreal, Canada, Proceedings, volume 5663 of LNCS, pages 485(- 501.) Springer, 2009.
 |)
(set-info :smt-lib-version 2.0)
(declare-const multLeftuscore105 Real)
(assert (and (<= multLeftuscore105 1000) (>= multLeftuscore105 (- 1000))))
(declare-const multRightuscore104 Real)
(assert (and (<= multRightuscore104 1000) (>= multRightuscore104 (- 1000))))
(declare-const multFacuscore103 Real)
(assert (and (<= multFacuscore103 1000) (>= multFacuscore103 (- 1000))))
(assert (not (=> (<= multLeftuscore105 multRightuscore104 ) (or (>= multFacuscore103 0. ) (>= (* multLeftuscore105 multFacuscore103) (* multRightuscore104 multFacuscore103) )))))
(check-sat)

