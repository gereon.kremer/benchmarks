(set-logic QF_NRA)
(set-info :source | KeYmaera example: exit-simultaneous-different-directions, node 303
Andre Platzer and Edmund M. Clarke. Formal verification of curved flight collision avoidance maneuvers: A case study. In Ana Cavalcanti and Dennis Dams, editors, 16th International Symposium on Formal Methods, FM, Eindhoven, Netherlands, Proceedings, volume 5850 of LNCS, pages 547(- 562.) Springer, 2009.
 |)
(set-info :smt-lib-version 2.0)
(declare-const d1 Real)
(assert (and (<= d1 1000) (>= d1 (- 1000))))
(declare-const om Real)
(assert (and (<= om 1000) (>= om (- 1000))))
(declare-const x2 Real)
(assert (and (<= x2 1000) (>= x2 (- 1000))))
(declare-const c2 Real)
(assert (and (<= c2 1000) (>= c2 (- 1000))))
(declare-const d2 Real)
(assert (and (<= d2 1000) (>= d2 (- 1000))))
(declare-const x1 Real)
(assert (and (<= x1 1000) (>= x1 (- 1000))))
(declare-const c1 Real)
(assert (and (<= c1 1000) (>= c1 (- 1000))))
(declare-const e1 Real)
(assert (and (<= e1 1000) (>= e1 (- 1000))))
(declare-const y2 Real)
(assert (and (<= y2 1000) (>= y2 (- 1000))))
(declare-const e2 Real)
(assert (and (<= e2 1000) (>= e2 (- 1000))))
(declare-const y1 Real)
(assert (and (<= y1 1000) (>= y1 (- 1000))))
(assert (not (=> (and (and (and (= d1 (* (- om) (- x2 c2)) ) (= d2 (* om (- x1 c1)) )) (= e1 (* (- om) (- y2 c2)) )) (= e2 (* om (- y1 c1)) )) (or (= d1 e1 ) (> (+ (* (- d1 e1) (- d1 e1)) (* (- d2 e2) (- d2 e2))) 0. )))))
(check-sat)

