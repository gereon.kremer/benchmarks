(set-logic QF_NRA)
(declare-fun yuscore2dollarsk!1 () Real)
(assert (and (<= yuscore2dollarsk!1 1000) (>= yuscore2dollarsk!1 (- 1000))))
(declare-fun stuscore2dollarsk!0 () Real)
(assert (and (<= stuscore2dollarsk!0 1000) (>= stuscore2dollarsk!0 (- 1000))))
(assert (= yuscore2dollarsk!1 10.0))
(assert (= stuscore2dollarsk!0 0.0))
(assert (>= yuscore2dollarsk!1 1.0))
(assert (<= yuscore2dollarsk!1 12.0))
(assert (not (= stuscore2dollarsk!0 1.0)))
(assert (not (= stuscore2dollarsk!0 3.0)))
(assert (not (<= yuscore2dollarsk!1 10.0)))
(check-sat)

