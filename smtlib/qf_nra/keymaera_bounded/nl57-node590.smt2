(set-logic QF_NRA)
(set-info :source | KeYmaera example: nl57, node 59
Andre Platzer, Jan-David Quesel, and Philipp Rümmer. Real world verification. In Renate A. Schmidt, editor, International Conference on Automated Deduction, CADE'09, Montreal, Canada, Proceedings, volume 5663 of LNCS, pages 485(- 501.) Springer, 2009.
 |)
(set-info :smt-lib-version 2.0)
(declare-const a Real)
(assert (and (<= a 1000) (>= a (- 1000))))
(declare-const b Real)
(assert (and (<= b 1000) (>= b (- 1000))))
(declare-const x Real)
(assert (and (<= x 1000) (>= x (- 1000))))
(assert (not (=> (and (<= a (+ b x) ) (< 2. x )) (< a (+ b (* (* x x) x)) ))))
(check-sat)

