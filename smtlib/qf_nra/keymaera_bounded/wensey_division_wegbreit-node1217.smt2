(set-logic QF_NRA)
(set-info :source | KeYmaera example: wensey_division_wegbreit, node 121
Andre Platzer, Jan-David Quesel, and Philipp Rümmer. Real world verification. In Renate A. Schmidt, editor, International Conference on Automated Deduction, CADE'09, Montreal, Canada, Proceedings, volume 5663 of LNCS, pages 485(- 501.) Springer, 2009.
 |)
(set-info :smt-lib-version 2.0)
(declare-const Q Real)
(assert (and (<= Q 1000) (>= Q (- 1000))))
(assert (not (= (* 2. (/ Q 2.)) (* 1. Q) )))
(check-sat)

