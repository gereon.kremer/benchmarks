(set-logic QF_NRA)
(declare-fun stuscore2dollarsk!0 () Real)
(assert (and (<= stuscore2dollarsk!0 1000) (>= stuscore2dollarsk!0 (- 1000))))
(declare-fun vuscore2dollarsk!4 () Real)
(assert (and (<= vuscore2dollarsk!4 1000) (>= vuscore2dollarsk!4 (- 1000))))
(declare-fun tuscore2dollarsk!1 () Real)
(assert (and (<= tuscore2dollarsk!1 1000) (>= tuscore2dollarsk!1 (- 1000))))
(declare-fun suscore2dollarsk!2 () Real)
(assert (and (<= suscore2dollarsk!2 1000) (>= suscore2dollarsk!2 (- 1000))))
(declare-fun zuscore2dollarsk!3 () Real)
(assert (and (<= zuscore2dollarsk!3 1000) (>= zuscore2dollarsk!3 (- 1000))))
(assert (= stuscore2dollarsk!0 1.0))
(assert (= vuscore2dollarsk!4 0.0))
(assert (= zuscore2dollarsk!3
           (+ (* 4000.0 suscore2dollarsk!2)
              (* tuscore2dollarsk!1
                 tuscore2dollarsk!1
                 (+ 5.0 (* (- 5.0) stuscore2dollarsk!0)))
              (* stuscore2dollarsk!0
                 (+ 2000.0
                    (* 200.0 tuscore2dollarsk!1)
                    (* (- 5.0) tuscore2dollarsk!1 tuscore2dollarsk!1))))))
(assert (>= tuscore2dollarsk!1 0.0))
(assert (>= suscore2dollarsk!2 0.0))
(assert (>= vuscore2dollarsk!4 0.0))
(assert (>= zuscore2dollarsk!3 0.0))
(assert (= vuscore2dollarsk!4
           (+ (* tuscore2dollarsk!1 (+ 10.0 (* (- 10.0) stuscore2dollarsk!0)))
              (* stuscore2dollarsk!0 (+ 200.0 (* (- 10.0) tuscore2dollarsk!1))))))
(assert (not (>= suscore2dollarsk!2 (- 1.0))))
(check-sat)

