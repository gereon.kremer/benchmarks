(set-logic QF_NRA)
(declare-fun a () Real)
(assert (and (<= a 1000) (>= a (- 1000))))
(declare-fun x2 () Real)
(assert (and (<= x2 1000) (>= x2 (- 1000))))
(declare-fun x1 () Real)
(assert (and (<= x1 1000) (>= x1 (- 1000))))
(declare-fun x2uscore1dollarsk!1 () Real)
(assert (and (<= x2uscore1dollarsk!1 1000) (>= x2uscore1dollarsk!1 (- 1000))))
(declare-fun x1uscore1dollarsk!0 () Real)
(assert (and (<= x1uscore1dollarsk!0 1000) (>= x1uscore1dollarsk!0 (- 1000))))
(assert (>= (* (- 1.0) x1 x2) a))
(assert (not (>= (+ (* (- 1.0)
                       x2uscore1dollarsk!1
                       (+ x1uscore1dollarsk!0
                          (* (- 1.0) x2uscore1dollarsk!1)
                          (* x1uscore1dollarsk!0 x2uscore1dollarsk!1)))
                    (* (- 1.0)
                       x1uscore1dollarsk!0
                       (+ (* (- 1.0) x2uscore1dollarsk!1)
                          (* (- 1.0) x2uscore1dollarsk!1 x2uscore1dollarsk!1))))
                 0.0)))
(check-sat)

