#!/bin/bash
rectangles=$1
for area in {100..500..50}; do
    echo "Generating rectangle_positioning_"$rectangles"in"$area".smt2...";
    python ../generate_rectangle_placement_example_smtlib.py $rectangles $area > "rectangle_positioning_"$rectangles"in"$area".smt2";
done;
