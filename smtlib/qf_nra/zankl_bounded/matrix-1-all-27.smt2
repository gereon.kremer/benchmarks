(set-logic QF_NRA)
(set-info :source |
From termination analysis of term rewriting.

Submitted by Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(set-info :status sat)
(declare-fun x6 () Real)
(assert (and (<= x6 1000) (>= x6 (- 1000))))
(declare-fun x23 () Real)
(assert (and (<= x23 1000) (>= x23 (- 1000))))
(declare-fun x13 () Real)
(assert (and (<= x13 1000) (>= x13 (- 1000))))
(declare-fun x3 () Real)
(assert (and (<= x3 1000) (>= x3 (- 1000))))
(declare-fun x20 () Real)
(assert (and (<= x20 1000) (>= x20 (- 1000))))
(declare-fun x10 () Real)
(assert (and (<= x10 1000) (>= x10 (- 1000))))
(declare-fun x27 () Real)
(assert (and (<= x27 1000) (>= x27 (- 1000))))
(declare-fun x0 () Real)
(assert (and (<= x0 1000) (>= x0 (- 1000))))
(declare-fun x17 () Real)
(assert (and (<= x17 1000) (>= x17 (- 1000))))
(declare-fun x7 () Real)
(assert (and (<= x7 1000) (>= x7 (- 1000))))
(declare-fun x24 () Real)
(assert (and (<= x24 1000) (>= x24 (- 1000))))
(declare-fun x14 () Real)
(assert (and (<= x14 1000) (>= x14 (- 1000))))
(declare-fun x4 () Real)
(assert (and (<= x4 1000) (>= x4 (- 1000))))
(declare-fun x21 () Real)
(assert (and (<= x21 1000) (>= x21 (- 1000))))
(declare-fun x11 () Real)
(assert (and (<= x11 1000) (>= x11 (- 1000))))
(declare-fun x28 () Real)
(assert (and (<= x28 1000) (>= x28 (- 1000))))
(declare-fun x1 () Real)
(assert (and (<= x1 1000) (>= x1 (- 1000))))
(declare-fun x18 () Real)
(assert (and (<= x18 1000) (>= x18 (- 1000))))
(declare-fun x8 () Real)
(assert (and (<= x8 1000) (>= x8 (- 1000))))
(declare-fun x25 () Real)
(assert (and (<= x25 1000) (>= x25 (- 1000))))
(declare-fun x15 () Real)
(assert (and (<= x15 1000) (>= x15 (- 1000))))
(declare-fun x5 () Real)
(assert (and (<= x5 1000) (>= x5 (- 1000))))
(declare-fun x22 () Real)
(assert (and (<= x22 1000) (>= x22 (- 1000))))
(declare-fun x12 () Real)
(assert (and (<= x12 1000) (>= x12 (- 1000))))
(declare-fun x2 () Real)
(assert (and (<= x2 1000) (>= x2 (- 1000))))
(declare-fun x19 () Real)
(assert (and (<= x19 1000) (>= x19 (- 1000))))
(declare-fun x9 () Real)
(assert (and (<= x9 1000) (>= x9 (- 1000))))
(declare-fun x26 () Real)
(assert (and (<= x26 1000) (>= x26 (- 1000))))
(declare-fun x16 () Real)
(assert (and (<= x16 1000) (>= x16 (- 1000))))
(assert (>= x6 0))
(assert (>= x23 0))
(assert (>= x13 0))
(assert (>= x3 0))
(assert (>= x20 0))
(assert (>= x10 0))
(assert (>= x27 0))
(assert (>= x0 0))
(assert (>= x17 0))
(assert (>= x7 0))
(assert (>= x24 0))
(assert (>= x14 0))
(assert (>= x4 0))
(assert (>= x21 0))
(assert (>= x11 0))
(assert (>= x28 0))
(assert (>= x1 0))
(assert (>= x18 0))
(assert (>= x8 0))
(assert (>= x25 0))
(assert (>= x15 0))
(assert (>= x5 0))
(assert (>= x22 0))
(assert (>= x12 0))
(assert (>= x2 0))
(assert (>= x19 0))
(assert (>= x9 0))
(assert (>= x26 0))
(assert (>= x16 0))
(assert (let ((?v_11 (+ x2 (* x4 x5)))) (let ((?v_0 (+ x0 (* x1 ?v_11))) (?v_13 (* x4 x6)) (?v_14 (+ x15 (* x16 x17)))) (let ((?v_1 (+ x12 (* x14 ?v_14))) (?v_16 (* x16 x18)) (?v_2 (+ x8 (* x9 x5))) (?v_4 (* x9 x6)) (?v_3 (+ x12 (* x13 x19))) (?v_5 (+ x8 (* x9 x15))) (?v_7 (* x9 x16)) (?v_6 (+ x10 (* x11 x19))) (?v_8 (+ x8 (* x9 x17))) (?v_10 (* x9 x18)) (?v_9 (+ x21 (* x22 x19)))) (let ((?v_23 (and (and (and (and (and (and (and (and (and (> ?v_0 x8) (>= ?v_0 x8)) (>= (* x1 ?v_13) x9)) (and (and (> x10 ?v_1) (>= x10 ?v_1)) (>= x11 (+ x13 (* x14 ?v_16))))) (and (and (> ?v_2 x8) (>= ?v_2 x8)) (>= ?v_4 x9))) (and (and (and (> ?v_2 ?v_3) (>= ?v_2 ?v_3)) (>= ?v_4 (* x13 x20))) (>= (* x9 x7) x14))) (and (and (> ?v_5 x8) (>= ?v_5 x8)) (>= ?v_7 x9))) (and (and (> ?v_5 ?v_6) (>= ?v_5 ?v_6)) (>= ?v_7 (* x11 x20)))) (and (and (> ?v_8 x8) (>= ?v_8 x8)) (>= ?v_10 x9))) (and (and (> ?v_8 ?v_9) (>= ?v_8 ?v_9)) (>= ?v_10 (* x22 x20))))) (?v_12 (+ x23 (* x24 ?v_11))) (?v_15 (+ x2 (* x4 ?v_14))) (?v_18 (+ x2 (* x3 x19))) (?v_17 (+ x19 (* x20 x5))) (?v_20 (+ x25 (* x26 x19))) (?v_19 (+ x19 (* x20 x15))) (?v_22 (+ x27 (* x28 x19))) (?v_21 (+ x19 (* x20 x17)))) (and (and (and (and (and (and (and (and (and (and ?v_23 (and (and (> ?v_12 x19) (>= ?v_12 x19)) (>= (* x24 ?v_13) x20))) (and (and (> x25 ?v_15) (>= x25 ?v_15)) (>= x26 (+ x3 (* x4 ?v_16))))) (and (and (and (> x2 x5) (>= x2 x5)) (>= x3 x6)) (>= x4 x7))) (and (and (> x25 x15) (>= x25 x15)) (>= x26 x16))) (and (and (> x27 x17) (>= x27 x17)) (>= x28 x18))) (and (and (and (> ?v_17 ?v_18) (>= ?v_17 ?v_18)) (>= (* x20 x6) (* x3 x20))) (>= (* x20 x7) x4))) (and (and (> ?v_19 ?v_20) (>= ?v_19 ?v_20)) (>= (* x20 x16) (* x26 x20)))) (and (and (> ?v_21 ?v_22) (>= ?v_21 ?v_22)) (>= (* x20 x18) (* x28 x20)))) (and (and (> x19 0) (>= x19 0)) (>= x20 1))) ?v_23))))))
(check-sat)
(exit)

