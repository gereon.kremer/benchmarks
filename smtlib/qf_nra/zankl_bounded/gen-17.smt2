(set-logic QF_NRA)
(set-info :source |
Harald Roman Zankl <Harald.Zankl@uibk.ac.at>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")
(set-info :status unsat)
(declare-fun a () Real)
(assert (and (<= a 1000) (>= a (- 1000))))
(declare-fun b () Real)
(assert (and (<= b 1000) (>= b (- 1000))))
(assert (= (* a a) 2))
(assert (= (* b (* b b)) 3))
(assert (= a b))
(check-sat)
(exit)

