#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

# Example input file generator smtlib 2.0.
# Example originally from (2004-04-14): http://hysat.informatik.uni-oldenburg.de/benchmarks/etcs_train_system.hys
# @author: Florian Corzilius
# @author: Ulrich Loup
# @version: 2010-12-13

#
# Constants
#

#
# Default values
#

#
# Functions
#



#
# Generate BMC formula up to length n.
#
def formula(n):
  assert( n>0 )
  formula = "(set-logic QF_NIA)\n"
  formula += "(set-info :source |\n"
  formula += "Florian Corzilius <corzilius@cs.rwth-aachen.de>\n"
  formula += "\n"
  formula += "|)\n"
  formula += "(set-info :smt-lib-version 2.0)\n"
  formula += "(set-info :category \"industrial\")\n"
  # all real variables
  for k in range(1,n+1):
    formula += "(declare-fun a_"+str(i)+" () Int)\n"
  for k in range(1,n+1):
    formula += "(declare-fun b_"+str(i)+" () Int)\n"
  formula += "(declare-fun alpha () Int)\n"
  formula += "(declare-fun beta () Int)\n"
  for k in range(1,n+1):
    formula += "(assert (or (= a_"+str(i)+" 1) (= a_"+str(i)+" (- 1))))\n"
  for k in range(1,n+1):
    formula += "(assert (or (= b_"+str(i)+" 1) (= b_"+str(i)+" (- 1))))\n"
  formula += "(assert (= alpha (+"
  for k in range(1,n+1):
    formula += " a_" + +str(i)
  formula += ")))\n"
  formula += "(assert (= beta (+"
  for k in range(1,n+1):
    formula += " b_" + +str(i)
  formula += ")))\n"
  formula += "(check-sat)\n"
  formula += "(exit)\n"
  return formula

# Print usage information
def printUsage():
  print( "Usage: python "+sys.argv[0]+" m d \n" )
  print( "            m: number (>0) being the number of allowed bounces" )
  print( "            d: number (>0) being the distance of the hole in cm" )
  print("\n")
  print( "Example: python generate_bouncing_ball_example_2_smtlib.py 3 600" )

#
# Parse input.
#
i = 0
for entry in sys.argv:
  try:
    if i == 1:
      m = int(entry)
      if (not(isinstance(m,int)) or m<1): raise ValueError()
    if i == 2:
      d = int(entry)
      if (not(isinstance(d,int)) or d<1): raise ValueError()
  except ValueError:
    print( "Error:",entry, "should be an appropriate value at position %i." % i )
    printUsage()
    sys.exit(1)
  i += 1
if i != 3:
  print( "Error: Insufficient number of arguments." )
  printUsage()
  sys.exit(1)

#
# Run formula run!
#
print( formula(m,d) )
