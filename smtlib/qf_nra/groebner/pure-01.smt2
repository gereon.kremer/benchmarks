(set-logic QF_NRA)
(set-info :source |
Sebastian Junges
|)
(set-info :smt-lib-version 2.0)
(set-info :category "crafted")

(declare-fun x () Real)
(declare-fun y () Real)

(assert (and (= (+ (* x x x) (- (* 2 x y)) ) 0) (= (+ (* x x y ) (- (* 2 y y )) x) 0)))
(check-sat)
(exit)
