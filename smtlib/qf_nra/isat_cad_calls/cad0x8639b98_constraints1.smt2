(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x1 x0) (* (* x0 x0) (- (/ 15 11))) (* x1 (* x0 x0 x0) (/ 15 11)) (* x1 (* x0 x0 x0 x0 x0 x0 x0) (/ 5 231)) (* x1 (* x0 x0 x0 x0 x0) (/ 5 11)) (* (* x0 x0 x0 x0) (- (/ 5 11))) (* (* x0 x0 x0 x0 x0 x0) (- (/ 5 231))) (- 1)) 0) (> (+ (* x1 (- 1)) x0) 0) (> x2 0) (<= (+ x2 (- 1000)) 0) (> (+ x0 (/ 1289116335 1290732533)) 0) (<= (+ x0 (/ 114090800 114313413)) 0) (> (+ x1 1) 0) (< (+ x1 (/ 71415977 71461835)) 0)))
(check-sat)
(exit)