(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (> (* x0 x2) 0) (> (+ (* x0 x1 (- 1)) (* x0 x0) (* x0 x1 x2) (* x0 x2 (- (/ 207 100))) x2) 0) (<= (+ (* x2 x2) (* (* x0 x0) (- 1)) (* (* x1 x1) (- 1)) (* (* x0 x0) (* x1 x1) (- 1)) (- 1)) 0) (>= (+ (* x2 x2) (* (* x0 x0) (- 1)) (* (* x1 x1) (- 1)) (* (* x0 x0) (* x1 x1) (- 1)) (- 1)) 0) (> (+ x0 (* x1 (- 1))) 0) (> (+ x0 (- (/ 45849835 35387496))) 0) (< (+ x0 (- (/ 128193999 98874359))) 0) (> (+ x2 (- (/ 69283930 25859291))) 0) (< (+ x2 (- (/ 317765667 118548761))) 0) (> (+ x1 (- (/ 88550376 68359517))) 0) (< (+ x1 (- (/ 119356745 92083748))) 0)))
(check-sat)
(exit)