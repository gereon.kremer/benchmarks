(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x4 () Real)
(declare-fun x3 () Real)
(assert (and  (= (+ (* (* x3 x3) 1000) (* (* x4 x4) 1000) (* (* x3 x3 x3 x3) (/ 1 4000)) (* (+ (* x3 x3) 2000000) x4 (- 1)) 1000000000) 0) (>= (+ x4 1000) 0) (<= (+ x4 (- 1000)) 0) (>= (+ x3 1000) 0) (<= (+ x3 (- 1000)) 0)))
(check-sat)
(exit)