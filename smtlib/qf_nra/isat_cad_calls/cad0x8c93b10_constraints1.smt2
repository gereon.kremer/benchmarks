(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* x1 x0 (- (/ 50 91))) (* (* x0 x0) (- (/ 200 273))) (* x2 (* x1 x1) (* x0 x0) (/ 1 9)) (* (* x0 x0 x0) (/ 1 3)) (* x2 x1 (- (/ 50 91))) (* x2 (* x1 x1) (* x0 x0 x0) (/ 50 273)) (* x2 (* x1 x1 x1) x0 (- (/ 1 3))) (* x2 (* x1 x1 x1) (* x0 x0) (/ 50 273)) x2 (* x2 (* x1 x1) (/ 1 3)) (* x2 (* x1 x1 x1) (* x0 x0 x0) (- (/ 1 9))) (* x2 (* x1 x1) x0 (/ 100 273)) (* x1 (* x0 x0) (/ 1 3)) (* (* x1 x1 x1) (/ 1 3)) (* x2 x1 x0 (- 1)) (* x2 x0 (- (/ 50 91))) (* (* x1 x1) (* x0 x0) (- (/ 50 117))) (* (* x1 x1) (- (/ 200 273))) x1 (* (* x1 x1) (* x0 x0 x0) (/ 1 9)) (* (* x1 x1 x1) (* x0 x0) (/ 1 9)) (* x2 x1 (* x0 x0 x0) (- (/ 1 3))) (* (* x1 x1 x1) (* x0 x0 x0) (/ 50 819)) (* (* x1 x1) x0 (/ 1 3)) (* x2 (* x0 x0) (/ 1 3)) (* x2 x1 (* x0 x0) (/ 100 273)) x0 (- (/ 50 91))) 0) (> (+ (* x1 (- 1)) x0) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (< (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ x2 (- (/ 3489338777 12911081))) 0) (< (+ x2 (- (/ 661439193 2447342))) 0) (>= (+ x0 (/ 45998333 46192257)) 0) (< (+ x0 (/ 137660313 138240674)) 0) (> (+ x1 (/ 157496187 157999258)) 0) (<= (+ x1 (/ 167392627 167927309)) 0)))
(check-sat)
(exit)