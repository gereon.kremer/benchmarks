(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x4 () Real)
(declare-fun x3 () Real)
(assert (and  (= (+ (* (* x3 x3) (/ 1 2000)) (* x4 (- 1)) 500) 0) (>= (+ x4 1000) 0) (<= (+ x4 (- 1000)) 0) (>= (+ x3 1000) 0) (<= (+ x3 (- 1000)) 0)))
(check-sat)
(exit)