(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ x0 x2 x1) 0) (< (+ x1 (- (/ 11 10))) 0) (< (+ x0 (- (/ 11 10))) 0) (< (+ x2 (- (/ 11 10))) 0) (> (+ x1 (- (/ 1 10))) 0) (> (+ x0 (- (/ 1 10))) 0) (> (+ x2 (- (/ 1 10))) 0) (> (+ x0 (- (/ 655069036708436 6550690367084361))) 0) (< (+ x0 (- (/ 6388741247725 59257889833971))) 0) (> (+ x1 (- (/ 655069036708436 6550690367084361))) 0) (< (+ x1 (- (/ 6388741247725 59257889833971))) 0) (> (+ x2 (- (/ 655069036708436 6550690367084361))) 0) (<= (+ x2 (- (/ 6388741247725 59257889833971))) 0)))
(check-sat)
(exit)