(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x0 x0 x0) (- (/ 1 6))) x0) 0) (> (+ (* x2 (- 1)) x0) 0) (< (+ (* x1 (- (/ 1 2))) x0) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (> x2 0) (< (+ x2 (- (/ 5998271 977565825))) 0) (> x0 0) (< (+ x0 (- (/ 5998271 977565825))) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)