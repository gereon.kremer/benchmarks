(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (< (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (> (+ (* x2 (* x0 x0) (* x1 x1) (/ 1 9)) (* (* x1 x1 x1) (/ 1 3)) (* x2 (* x0 x0 x0) x1 (- (/ 1 3))) x2 (* x2 (* x0 x0) (/ 1 3)) (* x2 (* x0 x0 x0) (* x1 x1 x1) (- (/ 1 9))) (* x0 (* x1 x1)) (* (* x0 x0 x0) (/ 1 3)) (* x2 x0 x1 (- 1)) (* (* x0 x0) (* x1 x1 x1) (/ 4 9)) (* (* x0 x0 x0) (* x1 x1) (/ 4 9)) (* x2 x0 (* x1 x1 x1) (- (/ 1 3))) (* (* x0 x0) x1) (* x2 (* x1 x1) (/ 1 3))) 0) (< (+ x0 (* x1 (- 1))) 0) (<= (+ (* x2 (* x0 x0 x0) x1 (- 1)) (* x2 3) (* x2 (* x0 x0)) (* x0 (* x1 x1) 3) (* x0 x0 x0) (* x2 x0 x1 (- 3)) (* (* x0 x0 x0) (* x1 x1)) (* (* x0 x0) x1 3)) 0) (>= (+ x2 (- (/ 4426253 65941783))) 0) (< (+ x2 (- (/ 17504681 252149032))) 0) (> (+ x1 (- (/ 96186611 290782665))) 0) (< (+ x1 (- (/ 50025064 150270849))) 0) (> (+ x0 (/ 87270849 87443833)) 0) (< (+ x0 (/ 147842834 148608505)) 0)))
(check-sat)
(exit)