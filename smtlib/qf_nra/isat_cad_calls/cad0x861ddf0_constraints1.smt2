(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* x2 x2) (* (* x0 x0) (- 1)) (* (* x1 x1) (- 1)) (* (* x0 x0) (* x1 x1) (- 1)) (- 1)) 0) (>= (+ (* x2 x2) (* (* x0 x0) (- 1)) (* (* x1 x1) (- 1)) (* (* x0 x0) (* x1 x1) (- 1)) (- 1)) 0) (> (+ x0 (* x1 (- 1))) 0) (> x0 0) (<= (+ x0 (- (/ 5454236 627615523))) 0) (>= (+ x2 (- 1)) 0) (<= (+ x2 (- (/ 32671763 32644976))) 0) (> x1 0) (<= (+ x1 (- (/ 9732227 1017130249))) 0)))
(check-sat)
(exit)