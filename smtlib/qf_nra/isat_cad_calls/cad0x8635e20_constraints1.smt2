(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(assert (and  (<= (+ (* (* x2 x2) (* x0 x0) (- 1)) (* x3 x3) (* (* x1 x1) (* x0 x0) (- 1)) (* (* x2 x2) (* x1 x1) (- 1)) (* (* x0 x0 x0 x0) (- 1))) 0) (>= (+ (* (* x2 x2) (* x0 x0) (- 1)) (* x3 x3) (* (* x1 x1) (* x0 x0) (- 1)) (* (* x2 x2) (* x1 x1) (- 1)) (* (* x0 x0 x0 x0) (- 1))) 0) (> (+ (* x2 (- 1)) x1) 0) (>= (+ (* x1 (- 1)) x0) 0) (> (+ x1 x0) 0) (> x2 0) (< (+ x2 (- (/ 13950943 1751630869))) 0) (> (+ x0 (- 1)) 0) (<= (+ x0 (- (/ 57924008 57808331))) 0) (> x1 0) (< (+ x1 (- (/ 13950943 1751630869))) 0) (> (+ x3 (- 1)) 0) (< (+ x3 (- (/ 129852535 129521948))) 0)))
(check-sat)
(exit)