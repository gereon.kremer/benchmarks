(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x0 x0) 3) 0) (< (+ (* x0 x1 (- (/ 1 4))) (* (* x1 x1) (/ 1 12)) (* x2 (* x0 x0) (* x1 x1) (/ 1 9)) (* (* x1 x1 x1) (/ 1 3)) (* x2 (* x0 x0 x0) x1 (- (/ 1 3))) x2 (* x2 (* x0 x0) (/ 1 3)) (* x0 (* x1 x1 x1) (- (/ 1 12))) (* x2 (* x0 x0 x0) (* x1 x1 x1) (- (/ 1 9))) (* x0 (* x1 x1)) (* (* x0 x0 x0) (/ 1 3)) (* x2 x0 x1 (- 1)) (* (* x0 x0) (* x1 x1) (/ 1 36)) (* (* x0 x0) (/ 1 12)) (* (* x0 x0) (* x1 x1 x1) (/ 4 9)) (* (* x0 x0 x0) x1 (- (/ 1 12))) (* (* x0 x0 x0) (* x1 x1) (/ 4 9)) (* x2 x0 (* x1 x1 x1) (- (/ 1 3))) (* (* x0 x0 x0) (* x1 x1 x1) (- (/ 1 36))) (* (* x0 x0) x1) (* x2 (* x1 x1) (/ 1 3)) (/ 1 4)) 0) (< (+ x0 (* x1 (- 1))) 0) (> x2 0) (< (+ x2 (- (/ 21447737 2728738791))) 0) (>= (+ x1 (/ 46611179 175781764)) 0) (<= (+ x1 (/ 30015701 115891271)) 0) (>= (+ x0 (/ 71916072 116233925)) 0) (< (+ x0 (/ 52737800 85897067)) 0)))
(check-sat)
(exit)