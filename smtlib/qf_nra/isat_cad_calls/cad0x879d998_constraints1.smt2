(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0) (/ 1 362880)) (* (* x0 x0 x0) (- (/ 1 6))) (* (* x0 x0 x0 x0 x0) (/ 1 120)) (* (* x0 x0 x0 x0 x0 x0 x0) (- (/ 1 5040))) x0) 0) (> (+ (* x1 (* x0 x0 x0 x0) 42) (* (* x1 x1 x1) 840) (* x1 x1 x1 x1 x1 x1 x1) (* (* x1 x1 x1 x1 x1) (- 42)) (* x1 (* x0 x0) (- 840))) 0) (> (+ (* (* x0 x0 x0) (- 20)) (* (* x1 x1) x0 20) (* x0 x0 x0 x0 x0)) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ (* x1 (- 1)) x0) 0) (> x1 0) (<= (+ x1 (- (/ 17961333 2319635207))) 0) (> x0 0) (< (+ x0 (- (/ 6402831 790473883))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)