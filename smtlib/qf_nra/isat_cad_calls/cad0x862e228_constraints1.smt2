(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (<= (+ (* x0 x1 2) (* (* x0 x0) (- 2)) x0 (- (/ 1 6))) 0) (<= (+ (* x1 x1 x1) (* x2 (- 1)) 1) 0) (>= (+ (* x1 x1 x1) (* x2 (- 1)) 1) 0) (<= (+ (* x2 (- 1)) (* x0 x0 x0)) 0) (>= (+ (* x2 (- 1)) (* x0 x0 x0)) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 75465755 75443934))) 0) (> x1 0) (< (+ x1 (- (/ 8253900 3238748657))) 0) (> (+ x0 (- 1)) 0) (< (+ x0 (- (/ 144563449 144452120))) 0)))
(check-sat)
(exit)