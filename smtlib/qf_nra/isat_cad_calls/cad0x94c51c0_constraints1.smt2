(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (< (+ (* (* x0 x0) (- (/ 950 819))) (* x2 x1 (* x0 x0 x0 x0 x0 x0) (/ 640 17199)) (* (* x0 x0 x0) (/ 10 9)) (* x2 x1 (* x0 x0 x0 x0) (/ 50 117)) (* x2 (* x0 x0 x0 x0) (/ 5 21)) (* x2 (* x0 x0 x0 x0 x0) (- (/ 640 17199))) (* x2 x1 (* x0 x0 x0 x0 x0) (- (/ 5 21))) x2 (* x1 (* x0 x0 x0) (/ 50 273)) (* x1 (* x0 x0) (/ 10 9)) (* x2 x1 x0 (- 1)) (* x2 x0 (- (/ 50 91))) (* x1 (* x0 x0 x0 x0) (/ 5 21)) x1 (* x1 (* x0 x0 x0 x0 x0) (/ 230 2457)) (* (* x0 x0 x0 x0 x0) (/ 5 21)) (* x2 x1 (* x0 x0 x0) (- (/ 10 9))) (* x2 (* x0 x0 x0) (- (/ 50 117))) (* (* x0 x0 x0 x0) (- (/ 3200 5733))) (* x2 (* x0 x0) (/ 10 9)) (* x2 x1 (* x0 x0) (/ 50 91)) x0 (* (* x0 x0 x0 x0 x0 x0) (- (/ 640 17199))) (- (/ 50 91))) 0) (> (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ (* x1 (- 1)) x0) 0) (> (+ x2 (- (/ 1641538311 1903141))) 0) (< (+ x2 (- (/ 5069858168 5877803))) 0) (> (+ x0 (/ 223976285 224497703)) 0) (<= (+ x0 (/ 88828963 89061875)) 0) (> (+ x1 1) 0) (< (+ x1 (/ 615335685 615453322)) 0)))
(check-sat)
(exit)