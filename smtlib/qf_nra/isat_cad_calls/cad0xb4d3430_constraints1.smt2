(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x3 () Real)
(assert (and  (> (+ (* x2 x0 (/ 159 1000)) (* x1 x3 (* x0 x0) (/ 2637651 13400000)) (* x1 x2 (/ 1 5)) (* x1 x3 x0 (/ 49767 13400)) (* x3 (/ 7825 67)) (* x2 (* x0 x0) (/ 8427 1000000)) (* x1 x2 x0 (/ 159 5000)) x2 (* x3 (* x0 x0) (/ 2637651 2680000)) (* x1 x3 (/ 1565 67)) (* x3 x0 (/ 49767 2680)) (* x1 x2 (* x0 x0) (/ 8427 5000000))) 0) (< (+ x1 (- (/ 151 50))) 0) (> (+ x1 (- (/ 26312532 57719869))) 0) (<= (+ x1 (- (/ 85164163 186669094))) 0) (>= (+ x0 (- (/ 871537813 28898593))) 0) (<= (+ x0 (- (/ 555467085 18417719))) 0) (> (+ x2 (/ 826348333 1022097)) 0) (< (+ x2 (/ 4442499512 5494859)) 0) (> (+ x3 (- (/ 393227835 54552352))) 0) (<= (+ x3 (- (/ 432561216 60006097))) 0)))
(check-sat)
(exit)