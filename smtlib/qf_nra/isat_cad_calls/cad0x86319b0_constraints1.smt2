(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* (* x0 x0) (- 5)) (* (* x0 x0 x0) 10) x0 (- (/ 1 12))) 0) (<= (+ (* x0 x0 x0) (* x2 (- 1)) 1) 0) (>= (+ (* x0 x0 x0) (* x2 (- 1)) 1) 0) (<= (+ (* x2 (- 1)) (* x1 x1 x1)) 0) (>= (+ (* x2 (- 1)) (* x1 x1 x1)) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 25101683 25099862))) 0) (> x0 0) (< (+ x0 (- (/ 7975643 1554153233))) 0) (> (+ x1 (- 1)) 0) (< (+ x1 (- (/ 138242005 138238662))) 0)))
(check-sat)
(exit)