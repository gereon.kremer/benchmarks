(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x4 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (= (+ (* x1 (/ 2928529677 1840337)) (* x0 (- (/ 2928529677 1840337))) (* x4 x4) (- (/ 6364638679571543809 101840309827216))) 0) (> (+ x4 (- (/ 1605927948 6423839))) 0) (<= (+ x4 (- (/ 1122199526 4488879))) 0) (> (+ x0 (/ 1132804697 3020901)) 0) (<= (+ x0 (/ 4672996056 12461729)) 0) (> (+ x1 (/ 2024295779 5398268)) 0) (<= (+ x1 (/ 2419593157 6452451)) 0)))
(check-sat)
(exit)