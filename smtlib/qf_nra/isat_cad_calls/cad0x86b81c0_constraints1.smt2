(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* x2 x1 (* x0 x0)) x0) 0) (>= (+ (* x1 x0) (* x2 (* x1 x1) (* x0 x0) (/ 15 11)) (* x2 (* x1 x1 x1) (* x0 x0) (- (/ 1 11))) (* x2 (* x1 x1) x0 (- (/ 5 11))) (* (* x2 x2) (* x1 x1 x1) (* x0 x0) (- (/ 1 11))) (* x2 x1 x0 (- (/ 5 11))) (* (* x2 x2) (* x1 x1) (* x0 x0) (- (/ 1 11))) (* (* x1 x1) x0 (- (/ 5 11)))) 0) (>= (+ (* x1 x0) (* x2 (* x1 x1) (* x0 x0) (/ 15 11)) (* x2 (* x1 x1) (* x0 x0 x0) (- (/ 1 11))) (* x2 (* x1 x1 x1) (* x0 x0) (- (/ 1 11))) (* x2 (* x1 x1 x1) (* x0 x0 x0) (- (/ 1 11))) (* x1 (* x0 x0) (- (/ 5 11))) (* (* x1 x1) (* x0 x0) (- (/ 5 11))) (* (* x1 x1) x0 (- (/ 5 11)))) 0) (<= (+ (* x1 x0) (* x2 (* x1 x1) (* x0 x0) 2) (* x2 (* x1 x1) x0 2) (* x2 x1 x0 (- 14)) x1 (* x2 x1 (* x0 x0) 2) x0 1) 0) (>= (+ x2 (- (/ 427397405 215069349))) 0) (<= (+ x2 (- (/ 76161955 38306529))) 0) (> (+ x0 (- (/ 191077988 111224603))) 0) (<= (+ x0 (- (/ 59445867 34602850))) 0) (>= (+ x1 (- (/ 463676515 321699717))) 0) (< (+ x1 (- (/ 139697645 96888976))) 0)))
(check-sat)
(exit)