(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ x1 x2) 0) (< (+ (* (* x1 x1 x1) (/ 10 3)) (* x0 x0 x0 x0) (* (* x1 x1) (* x0 x0)) (* (* x0 x0) x2 (/ 10 3))) 0) (> (+ x1 (* x2 (- 1))) 0) (> (+ x1 (- (/ 45732493 103618173))) 0) (<= (+ x1 (- (/ 62080237 140199760))) 0) (> (+ x2 (/ 62080237 140199760)) 0) (< (+ x2 (/ 45732493 103618173)) 0) (> (+ x0 (- 1)) 0) (< (+ x0 (- (/ 222967828 222699395))) 0)))
(check-sat)
(exit)