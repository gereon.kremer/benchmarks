(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x6 () Real)
(assert (and  (= (+ x6 (- (/ 3690607577223215625 3690624855375872))) 0) (>= (+ x6 (/ 1959497027 5878434)) 0) (<= (+ x6 (/ 566315537 1698969)) 0)))
(check-sat)
(exit)