(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* (* x0 x0 x0) (/ 361 6000000)) x0 (* (* x0 x0) (/ 19 2000)) (* (* x0 x0 x0 x0 x0 x0) (/ 2476099 2304000000000000000)) (* (* x0 x0 x0 x0 x0) (/ 130321 192000000000000)) (* (* x0 x0 x0 x0) (/ 48013 192000000000)) (/ 69425 1349)) 0) (> (+ (* (* x0 x0 x0) (/ 361 6000000)) x0 (* (* x0 x0) (/ 19 2000)) (* (* x0 x0 x0 x0 x0 x0) (/ 2476099 2304000000000000000)) (* (* x0 x0 x0 x0 x0) (/ 130321 192000000000000)) (* (* x0 x0 x0 x0) (/ 48013 192000000000)) (/ 1000 19)) 0) (< (+ (* x2 (- (/ 689 1770))) x1) 0) (>= (+ x0 (/ 3035029574 18387971)) 0) (< (+ x0 (/ 1619661329 9812908)) 0) (>= (+ x1 1000) 0) (<= (+ x1 (/ 1265559206 1265571)) 0) (>= (+ x2 1000) 0) (<= (+ x2 (/ 16383875 16384)) 0)))
(check-sat)
(exit)