(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* (* x2 x2) (* x1 x1) (* x0 x0 x0 x0) 4) (* x0 x0) (* (* x2 x2) x1 (* x0 x0) (- 64)) (* x2 x1 x0 4) (* (* x2 x2) (* x1 x1) (* x0 x0) 4) (* x2 x1 (* x0 x0 x0) 4) (* x2 x1 (* x0 x0) 8) (* x0 2) (* (* x2 x2) (* x1 x1) (* x0 x0 x0) 8) 1) 0) (<= (+ (* x1 x0) (* x2 (* x1 x1) (* x0 x0) 2) (* x2 (* x1 x1) x0 2) (* x2 x1 x0 (- 14)) x1 (* x2 x1 (* x0 x0) 2) x0 1) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- 1000)) 0) (>= (+ x0 1000) 0) (<= (+ x0 (- 1)) 0) (>= (+ x1 1000) 0) (<= (+ x1 (- 1)) 0)))
(check-sat)
(exit)