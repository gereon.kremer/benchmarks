(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x6 () Real)
(declare-fun x0 () Real)
(assert (and  (= (+ x6 (* x0 (/ 3229585348619975 2960463503023932))) 0) (>= (+ x6 (/ 1959497027 5878434)) 0) (<= (+ x6 (/ 566315537 1698969)) 0) (>= (+ x0 1000) 0) (<= (+ x0 (/ 4287422822 4287431)) 0)))
(check-sat)
(exit)