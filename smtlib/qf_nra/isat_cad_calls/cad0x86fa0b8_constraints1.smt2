(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ x0 (* (* x0 x0) (/ 3 20)) (/ 20 9)) 0) (<= (+ x0 (* x2 (- (/ 127 860))) (* x1 (- (/ 493 17200)))) 0) (> (+ (* x2 x1 (- (/ 1500 59))) (* (* x1 x1) (- (/ 14100 413))) (* x0 x2 (/ 2130 413)) (* (* x0 x0) (- (/ 7200 413))) (* x0 x1) (* (* x2 x2) (- (/ 26100 413))) (/ 1000 413)) 0) (< (+ x0 (* x2 (- (/ 127 860))) (* x1 (- (/ 493 17200)))) 0) (<= (+ (* (* x1 x1) (- (/ 125 43))) (* x0 x2 (/ 229 1720)) x0 (* (* x0 x0) (- (/ 655 172))) (* x0 x1 (/ 443 17200)) (* (* x2 x2) (- (/ 125 43))) (* x2 (- (/ 127 860))) (* x1 (- (/ 493 17200)))) 0) (> x0 0) (< (+ x0 (- (/ 2510275 3726182611))) 0) (> (+ x1 (/ 47651536 172103839)) 0) (< (+ x1 (/ 68851363 250952160)) 0) (> (+ x2 (- (/ 28233045 601223344))) 0) (< (+ x2 (- (/ 48601698 1004829475))) 0)))
(check-sat)
(exit)