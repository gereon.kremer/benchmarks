(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x0 x0 x0) (* x2 3) (* x1 (* x0 x0)) (* x2 x1 x0 (- 3)) (* x1 3) (* x2 x1 (* x0 x0 x0) (- 1)) (* x2 (* x0 x0)) (* x0 3)) 0) (> (+ (* x1 (- 1)) x0) 0) (> (+ x2 (- (/ 4663515087 6977161))) 0) (<= (+ x2 (- (/ 573481987 857991))) 0) (> (+ x0 (/ 46031663 46100426)) 0) (<= (+ x0 (/ 167665529 168104446)) 0) (> (+ x1 1) 0) (< (+ x1 (/ 92160438 92299967)) 0)))
(check-sat)
(exit)