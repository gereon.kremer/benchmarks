(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x0 x0) x2 x1) x0) 0) (>= (+ (* (* x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* (* x0 x0) (* x1 x1) (- (/ 5 11))) (* (* x0 x0) x2 (* x1 x1) (/ 15 11)) (* (* x0 x0) x1 (- (/ 5 11))) (* (* x0 x0 x0) x2 (* x1 x1) (- (/ 1 11))) (* x0 x1) (* (* x0 x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* x0 (* x1 x1) (- (/ 5 11)))) 0) (<= (+ (* (* x0 x0) x2 (* x1 x1) 2) (* (* x0 x0) x2 x1 2) x0 (* x0 x2 x1 (- 14)) (* x0 x1) (* x0 x2 (* x1 x1) 2) x1 1) 0) (>= (+ x0 (- 1)) 0) (<= (+ x0 (- 1)) 0) (> (+ x1 (- (/ 32340782 32286255))) 0) (< (+ x1 (- (/ 142148469 141689678))) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- (/ 68802962 68730045))) 0)))
(check-sat)
(exit)