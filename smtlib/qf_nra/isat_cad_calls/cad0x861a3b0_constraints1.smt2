(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* x1 (- (/ 400 441))) x0) 0) (<= (+ (* x0 x0) (* x1 x1) (- 1)) 0) (>= (+ (* x0 x0) (* x1 x1) (- 1)) 0) (> (+ (* x1 (- (/ 400 441))) x0) 0) (<= (+ x2 (- (/ 1570 21))) 0) (>= x2 0) (<= x2 0) (>= (+ x0 (- (/ 91535825 136149004))) 0) (<= (+ x0 (- (/ 58661329 86612813))) 0) (>= (+ x1 (- (/ 126102863 171399873))) 0) (<= (+ x1 (- (/ 64454593 87070233))) 0)))
(check-sat)
(exit)