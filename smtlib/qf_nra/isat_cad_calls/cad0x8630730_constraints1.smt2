(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* (* x0 x0) 360) (* x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0) (- 30)) (- 720)) 0) (> (+ (* x0 x0) (- 2)) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (<= (+ x0 (* x1 (- (/ 1 2)))) 0) (>= (+ x2 (- (/ 1 20))) 0) (> (+ x0 (* x2 (- 1))) 0) (> (+ x0 (- (/ 186444716 131836323))) 0) (<= (+ x0 (- (/ 141628415 100117828))) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0) (> (+ x2 (- (/ 132706244 94215511))) 0) (< (+ x2 (- (/ 141628415 100117828))) 0)))
(check-sat)
(exit)