(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x1 x0 (- 4)) (* x0 x0) (* x2 x1 2) (* (* x2 x2) x1 x0 (- 2)) (* x2 (* x1 x1) x0 (- 2)) (* x2 x2) (* x2 x0 2) (* (* x1 x1) (* x0 x0) 3) (* x1 x1) (* (* x2 x2) (* x1 x1) (* x0 x0)) (* x2 x1 (* x0 x0) (- 2)) 3) 0) (> (+ (* x1 (- 1)) x0) 0) (>= (+ x2 (- (/ 41049265 253808958))) 0) (<= (+ x2 (- (/ 50208019 300653262))) 0) (>= (+ x0 (/ 21306286 241053109)) 0) (<= (+ x0 (/ 12898132 151045627)) 0) (>= (+ x1 (/ 38363439 403584655)) 0) (<= (+ x1 (/ 33914177 368389398)) 0)))
(check-sat)
(exit)