(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x2 (- (/ 86400000 2025130727))) x1) 0) (> (+ x0 (* (* x0 x0) (/ 23 24000)) (/ 8000 23)) 0) (< (+ (* x0 x2 (- (/ 27 625))) x0 (* (* x0 x0) x1 (- (/ 46578006721 48000000000000))) (* (* x0 x0) (/ 23 24000)) (* x0 x1 (/ 2025130727 2000000000)) (* x2 (/ 1728 115)) (* (* x0 x0) x2 (/ 207 5000000)) (* x1 (- (/ 2025130727 5750000))) (/ 8000 23)) 0) (<= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (>= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (> (+ (* x2 (- (/ 86400000 2025130727))) x1) 0) (> x0 0) (< (+ x0 (- (/ 11122295 1413212049))) 0) (> (+ x1 (- (/ 92333281 92661062))) 0) (<= (+ x1 (- (/ 129725431 130138885))) 0) (>= (+ x2 (- (/ 31910171 400635372))) 0) (< (+ x2 (- (/ 34724676 430312301))) 0)))
(check-sat)
(exit)