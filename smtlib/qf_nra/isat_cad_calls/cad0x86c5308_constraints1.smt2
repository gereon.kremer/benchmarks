(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (< (+ (* (* x1 x1 x1) (* x0 x0) x2 (- (/ 1 11))) (* x1 x0) (* (* x1 x1 x1) (* x0 x0 x0) x2 (- (/ 1 11))) (* x1 (* x0 x0) (- (/ 5 11))) (* (* x1 x1) (* x0 x0) x2 (/ 15 11)) (* (* x1 x1) (* x0 x0) (- (/ 5 11))) (* (* x1 x1) x0 (- (/ 5 11))) (* (* x1 x1) (* x0 x0 x0) x2 (- (/ 1 11)))) 0) (<= (+ (* x1 x0) (* (* x1 x1) x0 x2 2) x1 (* x1 (* x0 x0) x2 2) (* x1 x0 x2 (- 14)) (* (* x1 x1) (* x0 x0) x2 2) x0 1) 0) (>= (+ x1 (/ 3350430823 3556626)) 0) (< (+ x1 (/ 2287994273 2428805)) 0) (> (+ x2 (- (/ 2315809921 2315811))) 0) (<= (+ x2 (- 1000)) 0) (>= x0 0) (< (+ x0 (- (/ 296354 135304516999))) 0)))
(check-sat)
(exit)