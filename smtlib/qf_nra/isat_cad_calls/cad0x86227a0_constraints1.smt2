(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x4 () Real)
(assert (and  (>= (+ x1 x0 4) 0) (<= (+ x3 (* x1 x1) (- 1)) 0) (>= (+ x3 (* x1 x1) (- 1)) 0) (<= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x2 x2) (- 2)) 0) (>= (+ (* x2 x2) (- 2)) 0) (> (+ x4 (- (/ 15707963 5000000))) 0) (< (+ x4 (- (/ 31415927 10000000))) 0) (> (+ x3 (- (/ 151895983 152039333))) 0) (< (+ x3 (- 1)) 0) (> (+ x0 (- (/ 121245554 85753769))) 0) (< (+ x0 (- (/ 131836323 93222358))) 0) (>= x1 0) (< (+ x1 (- (/ 1437261 195023342))) 0) (>= (+ x2 (- (/ 186444716 131836323))) 0) (<= (+ x2 (- (/ 131836323 93222358))) 0) (> (+ x4 (- (/ 1064670109 338895027))) 0) (< (+ x4 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)