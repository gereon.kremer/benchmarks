(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (= (+ (* x1 x2) (/ 2008837 321928634)) 0) (>= (+ x1 (- (/ 26277413226283 200480752764))) 0) (<= (+ x1 (- (/ 1646438947 12560697))) 0) (>= x2 0) (<= (+ x2 (- (/ 3603927 4020957808))) 0)))
(check-sat)
(exit)