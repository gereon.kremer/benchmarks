(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* (* x0 x0 x0) (/ 10 9)) (* x2 (* x0 x0 x0 x0) (/ 5 21)) (* x2 x1 (* x0 x0 x0 x0 x0) (- (/ 5 21))) x2 (* x1 (* x0 x0) (/ 10 9)) (* x2 x1 x0 (- 1)) (* x1 (* x0 x0 x0 x0) (/ 5 21)) x1 (* (* x0 x0 x0 x0 x0) (/ 5 21)) (* x2 x1 (* x0 x0 x0) (- (/ 10 9))) (* x2 (* x0 x0) (/ 10 9)) x0) 0) (>= (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ (* x1 (- 1)) x0) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (> (+ x2 (- (/ 69870691 141366385))) 0) (<= (+ x2 (- (/ 48260341 96642967))) 0) (>= x0 0) (<= (+ x0 (- (/ 3 512))) 0) (>= (+ x1 (/ 257 512)) 0) (<= (+ x1 (/ 127 256)) 0)))
(check-sat)
(exit)