(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (>= (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (> (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (< (+ (* x0 x1 (- (/ 50 91))) (* (* x1 x1) (- (/ 50 91))) (* x2 x0 (- (/ 50 91))) (* x2 (* x0 x0 x0) x1 (- (/ 1 3))) (* x2 (* x0 x0 x0) (* x1 x1) (/ 50 273)) x2 (* x2 (* x0 x0) (/ 1 3)) (* x2 (* x0 x0) x1 (/ 100 273)) (* (* x0 x0 x0) (/ 1 3)) (* x2 x0 x1 (- 1)) (* x2 x1 (- (/ 50 91))) (* (* x0 x0) (* x1 x1) (- (/ 50 273))) (* (* x0 x0) (- (/ 200 273))) x0 (* (* x0 x0) x1 (/ 1 3)) (* x2 x0 (* x1 x1) (/ 50 91)) x1 (- (/ 50 91))) 0) (< (+ x0 (* x1 (- 1))) 0) (> (+ x2 (- (/ 219559255 441133997))) 0) (< (+ x2 (- (/ 18331160 36421797))) 0) (> (+ x1 (- (/ 83001358 250835301))) 0) (< (+ x1 (- (/ 32066127 96127063))) 0) (> (+ x0 (/ 164463575 164921203)) 0) (< (+ x0 (/ 401070367 403314889)) 0)))
(check-sat)
(exit)