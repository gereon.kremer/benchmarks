(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) 1004293914624000) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 45444) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 466089120) (* (* x0 x0) (- 316352583106560000)) (* (* x0 x0 x0 x0 x0 x0) (- 14060114804736000)) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 272)) (* (* x0 x0 x0 x0) 105450861035520000) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 5392800)) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 29393280000)) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 1351923955200) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 44635285094400))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x0 (* x2 (- 1))) 0) (>= (+ x0 (/ 2818114507 5765895)) 0) (<= (+ x0 (/ 1355841481 2774070)) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0) (>= (+ x2 (/ 4179023476 4179053)) 0) (<= (+ x2 (/ 3202162917 3202187)) 0)))
(check-sat)
(exit)