(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* x0 x2 (/ 266 1875)) (* (* x0 x0 x0 x0 x0 x0) x2 (- (/ 2235331 2109375000000000000))) (* (* x0 x0 x0) x1 (- (/ 343 93750000))) (* (* x0 x0 x0 x0 x0) x2 (/ 319333 703125000000000)) (* (* x0 x0) x1 (/ 49 125000)) (* (* x0 x0 x0 x0) x2 (- (/ 319333 2812500000000))) (* (* x0 x0 x0 x0 x0 x0) x1 (/ 117649 562500000000000000)) (* x0 x1 (- (/ 7 250))) (* (* x0 x0 x0) x2 (/ 6517 351562500)) (* x2 (- (/ 76 15))) (* (* x0 x0) x2 (- (/ 931 468750))) (* (* x0 x0 x0 x0) x1 (/ 16807 750000000000)) (* (* x0 x0 x0 x0 x0) x1 (- (/ 16807 187500000000000))) x1) 0) (<= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (>= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (>= x0 0) (<= x0 0) (>= (+ x1 (- (/ 298276539 299739628))) 0) (<= (+ x1 (- (/ 271178487 272439419))) 0) (>= (+ x2 (- (/ 24965675 256407763))) 0) (<= (+ x2 (- (/ 17956936313288 181963621307985))) 0)))
(check-sat)
(exit)