(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x7 () Real)
(declare-fun x10 () Real)
(assert (and  (= (+ (* (+ x10 (- 1)) x7) (* x10 (- (/ 195888061 33842373))) (/ 195888061 67684746)) 0) (>= (+ x7 (- (/ 125468795 37676922))) 0) (< (+ x7 (- (/ 63809581 19160301))) 0) (> (+ x10 (- (/ 3945311 1626711965))) 0) (<= (+ x10 (- (/ 4598523 1342500403))) 0)))
(check-sat)
(exit)