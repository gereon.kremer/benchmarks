(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* (* x2 x2) (* x1 x1) (* x0 x0 x0 x0) (- (/ 1 12))) (* x1 x0) (* (* x0 x0) (- (/ 1 2))) (* (* x2 x2) x1 (* x0 x0 x0) (/ 1 6)) (* (* x2 x2) (* x0 x0) (- (/ 1 12))) (* x2 x1 (* x0 x0 x0 x0) (/ 1 6)) (* x2 x1 (- (/ 1 2))) (* x2 (* x1 x1) (* x0 x0 x0) (/ 1 6)) (* (* x2 x2) x1 x0 (/ 1 2)) (* (* x1 x1) (* x0 x0 x0 x0) (- (/ 1 4))) (* x1 (* x0 x0 x0) (/ 1 3)) (* x2 (* x1 x1) x0 (/ 1 2)) (* (* x2 x2) (- (/ 1 4))) (* x2 x0 (- (/ 1 2))) (* (* x1 x1) (* x0 x0) (- (/ 5 6))) (* (* x1 x1) (- (/ 1 4))) (* (* x2 x2) (* x1 x1) (* x0 x0) (- (/ 1 4))) (* x2 (* x0 x0 x0) (- (/ 1 6))) (* (* x0 x0 x0 x0) (- (/ 1 12))) (* x2 x1 (* x0 x0) (/ 1 3)) (- (/ 3 4))) 0) (> (+ (* x1 (- 1)) x0) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (<= (+ (* x2 (* x1 x1 x1) x0 (- 1)) (* x2 3) (* x2 (* x1 x1)) (* x1 (* x0 x0) 3) (* x1 x1 x1) (* x2 x1 x0 (- 3)) (* (* x1 x1 x1) (* x0 x0)) (* (* x1 x1) x0 3)) 0) (< (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> x2 0) (< (+ x2 (- (/ 4948781 2125519489))) 0) (> (+ x0 (/ 31616631 486506189)) 0) (< (+ x0 (/ 36568681 633595312)) 0) (> (+ x1 (/ 36652147 287496886)) 0) (< (+ x1 (/ 19042097 158398828)) 0)))
(check-sat)
(exit)