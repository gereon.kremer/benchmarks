(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (> (+ (* x1 x0 (- (/ 100 157))) (* (* x0 x0) (- (/ 100 157))) (* x2 x1 (- (/ 100 157))) x2 (* x2 (* x1 x1) x0 (/ 100 157)) (* x2 x1 x0 (- 1)) (* x2 x0 (- (/ 100 157))) (* (* x1 x1) (- (/ 100 157))) x1 (* x2 x1 (* x0 x0) (/ 100 157)) x0 (- (/ 100 157))) 0) (< (+ (* x1 (- 1)) x0) 0) (>= (+ x2 (- (/ 113931359 75856707))) 0) (< (+ x2 (- (/ 121337888 80773145))) 0) (>= (+ x0 (/ 142239256 142795959)) 0) (<= (+ x0 (/ 110628944 111063637)) 0) (>= (+ x1 (- (/ 44302679 133432323))) 0) (<= (+ x1 (- (/ 48277451 145396969))) 0)))
(check-sat)
(exit)