(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x1 (- 1)) x0) 0) (< (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (< (+ (* x1 x0 (- (/ 1 4))) (* (* x0 x0) (/ 1 12)) (* x2 (* x1 x1) (* x0 x0) (/ 1 9)) (* (* x0 x0 x0) (/ 1 3)) (* x2 (* x1 x1 x1) x0 (- (/ 1 3))) x2 (* x2 (* x1 x1) (/ 1 3)) (* x1 (* x0 x0 x0) (- (/ 1 12))) (* x2 (* x1 x1 x1) (* x0 x0 x0) (- (/ 1 9))) (* x1 (* x0 x0)) (* (* x1 x1 x1) (/ 1 3)) (* x2 x1 x0 (- 1)) (* (* x1 x1) (* x0 x0) (/ 1 36)) (* (* x1 x1) (/ 1 12)) (* (* x1 x1) (* x0 x0 x0) (/ 4 9)) (* (* x1 x1 x1) x0 (- (/ 1 12))) (* (* x1 x1 x1) (* x0 x0) (/ 4 9)) (* x2 x1 (* x0 x0 x0) (- (/ 1 3))) (* (* x1 x1 x1) (* x0 x0 x0) (- (/ 1 36))) (* (* x1 x1) x0) (* x2 (* x0 x0) (/ 1 3)) (/ 1 4)) 0) (> x2 0) (< (+ x2 (- (/ 18923201 4644154901))) 0) (> (+ x0 (/ 76523851 227611136)) 0) (< (+ x0 (/ 79602212 237769179)) 0) (> (+ x1 (/ 67794295 101699308)) 0) (< (+ x1 (/ 49504043 74419952)) 0)))
(check-sat)
(exit)