(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (<= (+ (* (* x1 x1) (- (/ 1 4))) x2 (- 1)) 0) (<= (+ x1 (* (* x1 x1) (- (/ 1 4))) (* x2 (- 1))) 0) (<= (+ (* (* x2 x2) (- 1)) (* x1 x0 3) x1 (* x1 (* x0 x0) 3) (* x0 (* x2 x2) 2) (* x1 (* x0 x0 x0)) (* (* x0 x0) (* x2 x2))) 0) (>= (+ (* (* x2 x2) (- 1)) (* x1 x0 3) x1 (* x1 (* x0 x0) 3) (* x0 (* x2 x2) 2) (* x1 (* x0 x0 x0)) (* (* x0 x0) (* x2 x2))) 0) (>= (+ x1 (- (/ 1 2))) 0) (<= (+ x1 (- (/ 266883424 526115709))) 0) (>= (+ x2 (- (/ 51846485 79564163))) 0) (<= (+ x2 (- (/ 50180887 76224940))) 0) (>= (+ x0 (/ 1 32)) 0) (<= (+ x0 (/ 16156887 580565962)) 0)))
(check-sat)
(exit)