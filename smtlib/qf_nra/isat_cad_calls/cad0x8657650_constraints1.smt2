(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) 8229600) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0) (- 2612736000)) (* (* x0 x0 x0 x0 x0 x0) (- 116121600)) (* (* x0 x0 x0 x0) 870912000) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 120)) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 8100) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 335520))) 0) (>= (+ (* x2 (- 2)) x1 (- (/ 2 5))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (>= (+ x0 (- (/ 1 10))) 0) (< (+ x0 (* x2 (- 1))) 0) (> (+ x0 (- (/ 36583892 341552267))) 0) (<= (+ x0 (- (/ 60411892 549558247))) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0) (> (+ x2 (- (/ 196723603 143806019))) 0) (< (+ x2 (- (/ 215947220 157534137))) 0)))
(check-sat)
(exit)