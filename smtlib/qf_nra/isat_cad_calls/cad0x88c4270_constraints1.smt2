(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (< (+ (* x0 x1 (- (/ 1413711 256000))) (* (* x0 x0 x0 x0 x0 x0) x1 (/ 315 8)) (* (* x0 x0 x0) x2 (- (/ 1099553 128000))) (* (* x0 x0 x0 x0 x0) x1 (- (/ 29687931 1280000))) (* (* x0 x0) x2 (/ 245 64)) (* (* x0 x0 x0 x0) x1 (/ 3745 64)) (* x0 x2 (- (/ 471237 256000))) (* (* x0 x0 x0) x1 (- (/ 3298659 128000))) x1 (* (* x0 x0) x1 (/ 1335 64)) (* (* x0 x0 x0 x0) x2 (/ 315 64)) (* (* x0 x0 x0 x0 x0) x2 (- (/ 9895977 1280000))) (* x2 (/ 1 3))) 0) (> (+ x0 (- (/ 495660870 455424239))) 0) (<= (+ x0 (- (/ 733151482 673635899))) 0) (> (+ x2 (- (/ 152520147472 399822415389))) 0) (<= (+ x2 (- (/ 559240534972 1466015507997))) 0) (> x1 0) (< (+ x1 (- (/ 2452781 2045583282))) 0)))
(check-sat)
(exit)