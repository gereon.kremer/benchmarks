(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x5 () Real)
(declare-fun x4 () Real)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* (* x0 x0 x0) (* x4 x4 x4) (- 1)) (* (* x0 x0) (* x4 x4))) 0) (<= (+ (* x0 x3 (* x4 x4) (- 1)) (* (* x0 x0) x3)) 0) (< (+ (* x5 x0 (* x4 x4 x4)) (* x5 (- 1)) (* (* x0 x0) x2 x4 (- 1)) x2 (* x2 x4) (* (* x0 x0) x2 (- 1)) (* x5 x0 (- 1)) (* x2 (* x4 x4)) (* x5 (* x4 x4 x4)) (* x5 (* x0 x0) (* x4 x4 x4))) 0) (<= (+ (* x0 x3 (* x4 x4) (- 1)) x3) 0) (<= (+ (* x0 x3 (* x4 x4) (- 1)) (* x0 x3)) 0) (> (+ (* x0 x2 x3) (* x0 x2 x3 x4) (* x5 x0 x3 (- 1))) 0) (> (+ (* x5 x3) (* x0 x2 x3) (* x0 x2 x3 x4)) 0) (> (+ (* x0 x2 x3) (* x0 x2 x3 x4)) 0) (>= x5 0) (< (+ x5 (- (/ 571237 8907333205))) 0) (> (+ x4 (- (/ 4884181671 4884184))) 0) (<= (+ x4 (- 1000)) 0) (> (+ x3 (- (/ 17940882780 17940917))) 0) (<= (+ x3 (- 1000)) 0) (> (+ x0 (- (/ 41592025 483743101))) 0) (<= (+ x0 (- (/ 50918039 589897575))) 0) (>= x2 0) (< (+ x2 (- (/ 34137 470349566644))) 0) (>= x1 0) (<= (+ x1 (- 1000)) 0)))
(check-sat)
(exit)