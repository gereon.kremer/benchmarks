(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x4 () Real)
(declare-fun x6 () Real)
(declare-fun x0 () Real)
(assert (and  (= (+ (* (+ (* x6 3690624855375872) 3690607577223215625) x4 (- (/ 1 3690624855375872))) (* x0 (- (/ 3690607577223215625 3690624855375872)))) 0) (>= (+ x4 (- (/ 3596289427 7846500))) 0) (<= (+ x4 (- (/ 7801840647 17022245))) 0) (>= (+ x6 (/ 1959497027 5878434)) 0) (<= (+ x6 (/ 566315537 1698969)) 0) (>= (+ x0 1000) 0) (<= (+ x0 (/ 4287422822 4287431)) 0)))
(check-sat)
(exit)