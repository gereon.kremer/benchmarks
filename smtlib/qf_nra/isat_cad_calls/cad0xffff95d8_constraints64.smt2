(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x5 () Real)
(declare-fun x4 () Real)
(declare-fun x3 () Real)
(assert (and  (= (+ (* x4 (* x3 x3)) (* (* x5 x5) (/ 4864384663 4939672))) 0) (>= (+ x5 (/ 887312441 9373018)) 0) (<= (+ x5 (/ 4338300277 45827190)) 0) (> (+ x4 (/ 4848374536 4848389)) 0) (<= (+ x4 (/ 4877430901 4877448)) 0) (>= (+ x3 (/ 30269165 344887361)) 0) (<= (+ x3 (/ 23086942 280589861)) 0)))
(check-sat)
(exit)