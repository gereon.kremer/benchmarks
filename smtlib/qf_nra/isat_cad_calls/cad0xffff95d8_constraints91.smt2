(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (= (+ (* x1 (- 1)) x0) 0) (>= (+ x0 1000) 0) (<= (+ x0 (- 1000)) 0) (>= (+ x1 1000) 0) (<= (+ x1 (- 1000)) 0)))
(check-sat)
(exit)