(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* x2 x0 (/ 50 157)) (* x1 x2 (- (/ 50 157))) (* x1 x2 x0 (/ 50 157)) (* x1 x0)) 0) (> (+ (* x1 (- 1)) x0) 0) (>= (+ x1 (- (/ 18347860 479125983))) 0) (<= (+ x1 (- (/ 3485949 88712560))) 0) (>= (+ x0 (- (/ 18347860 479125983))) 0) (<= (+ x0 (- (/ 3485949 88712560))) 0) (> (+ x2 (- (/ 1468064104 12720271))) 0) (<= (+ x2 (- (/ 1616947505 14009219))) 0)))
(check-sat)
(exit)