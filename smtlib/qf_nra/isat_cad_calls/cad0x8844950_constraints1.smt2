(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* x1 x0) (* x2 (* x1 x1) (* x0 x0) 2) (* x2 (* x1 x1) x0 2) (* x2 x1 x0 (- 14)) x1 (* x2 x1 (* x0 x0) 2) x0 1) 0) (< (+ (* x1 x0) (* x2 (* x1 x1) (* x0 x0) (/ 15 11)) (* x2 (* x1 x1) (* x0 x0 x0) (- (/ 1 11))) (* x2 (* x1 x1 x1) (* x0 x0) (- (/ 1 11))) (* x2 (* x1 x1 x1) (* x0 x0 x0) (- (/ 1 11))) (* x1 (* x0 x0) (- (/ 5 11))) (* (* x1 x1) (* x0 x0) (- (/ 5 11))) (* (* x1 x1) x0 (- (/ 5 11)))) 0) (>= (+ x2 (- (/ 4811259871 4811262))) 0) (<= (+ x2 (- 1000)) 0) (>= (+ x0 (/ 139905737 156076111)) 0) (< (+ x0 (/ 146155217 163230140)) 0) (>= (+ x1 (- (/ 3035922638 6587045))) 0) (< (+ x1 (- (/ 1883668703 4086994))) 0)))
(check-sat)
(exit)