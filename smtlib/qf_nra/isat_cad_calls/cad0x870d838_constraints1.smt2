(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* (* x0 x0) x2 (* x1 x1 x1) 2) (* (* x0 x0 x0) (* x2 x2 x2) (* x1 x1) (- 4)) (* (* x0 x0) (* x2 x2 x2 x2 x2 x2)) (* (* x0 x0 x0 x0) (* x2 x2 x2 x2 x2) x1 2) (* (* x0 x0 x0 x0) (* x1 x1 x1 x1)) (* (* x2 x2) (* x1 x1)) (* (* x0 x0 x0 x0 x0 x0) (* x2 x2 x2 x2) (* x1 x1)) (* x0 (* x2 x2 x2 x2) x1 (- 2)) (* (* x0 x0 x0 x0 x0) (* x2 x2) (* x1 x1 x1) (- 2))) 0) (> (+ x0 (- (/ 5112994885 6441978))) 0) (<= (+ x0 (- (/ 2746799889 3460751))) 0) (> (+ x1 (- (/ 5497177 843057756))) 0) (< (+ x1 (- (/ 8618723 1213951832))) 0) (>= (+ x2 1000) 0) (< (+ x2 (/ 3468713933 3468715)) 0)))
(check-sat)
(exit)