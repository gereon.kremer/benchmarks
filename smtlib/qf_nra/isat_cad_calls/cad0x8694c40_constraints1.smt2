(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* x2 (- (/ 400 441))) x1) 0) (< (+ (* x2 (- (/ 400 441))) x1) 0) (> (+ (* x0 x2 (- (/ 80 69))) x0 (* (* x0 x0) x1 (- (/ 1421 1150000))) (* (* x0 x0) (/ 29 30000)) (* x0 x1 (/ 147 115)) (* x2 (/ 800000 2001)) (* (* x0 x0) x2 (/ 29 25875)) (* x1 (- (/ 294000 667))) (/ 10000 29)) 0) (>= (+ x0 (- (/ 2065080360 2777651))) 0) (<= (+ x0 (- (/ 2661143402 3579385))) 0) (> (+ x1 (- (/ 3894116927 13070037))) 0) (<= (+ x1 (- (/ 1497828919 5027228))) 0) (>= (+ x2 (- (/ 4684358314 4684363))) 0) (<= (+ x2 (- 1000)) 0)))
(check-sat)
(exit)