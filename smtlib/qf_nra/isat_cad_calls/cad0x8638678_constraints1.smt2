(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0) (- 1440)) (* (* x0 x0 x0 x0 x0 x0) (- 32)) (* (* x0 x0 x0 x0) 420)) 0) (< (+ x0 (* x2 (- 1))) 0) (>= (+ x0 (- (/ 1 10))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (>= (+ x1 (* x2 (- 2)) (- (/ 2 5))) 0) (>= (+ x0 (- (/ 19749361 186286322))) 0) (< (+ x0 (- (/ 68308133 640756439))) 0) (>= (+ x2 (- (/ 127232119 92856126))) 0) (< (+ x2 (- (/ 215947220 157534137))) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)