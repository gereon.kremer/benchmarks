(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x2 (- (/ 400 441))) x1) 0) (>= (+ (* x0 x2 (- (/ 80 69))) x0 (* (* x0 x0) x1 (- (/ 1421 1150000))) (* (* x0 x0) (/ 29 30000)) (* x0 x1 (/ 147 115)) (* x2 (/ 800000 2001)) (* (* x0 x0) x2 (/ 29 25875)) (* x1 (- (/ 294000 667))) (/ 10000 29)) 0) (<= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (>= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (> (+ (* x2 (- (/ 400 441))) x1) 0) (<= (+ x0 (- (/ 1570 21))) 0) (>= x0 0) (<= x0 0) (>= (+ x1 (- (/ 662572553 978383586))) 0) (<= (+ x1 (- (/ 58661329 86612813))) 0) (>= (+ x2 (- (/ 126102863 171399873))) 0) (<= (+ x2 (- (/ 110149793 149684541))) 0)))
(check-sat)
(exit)