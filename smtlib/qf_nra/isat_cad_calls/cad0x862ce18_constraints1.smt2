(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (<= (+ (* x0 x1) x2 (* x2 x0 x1 (- 1)) x0 x1 (- 1)) 0) (< (+ x0 (* x1 (- 1))) 0) (< (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (> x2 0) (< (+ x2 (- (/ 28605896 4690873681))) 0) (> (+ x1 (/ 43 128)) 0) (< (+ x1 (/ 62083892 188224667)) 0) (> (+ x0 1) 0) (< (+ x0 (/ 51371024 51686217)) 0)))
(check-sat)
(exit)