(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x1 x1 x1) (* x0 x0 x0 x0 x0) 160) (* (* x1 x1 x1 x1) (* x0 x0 x0) (- (/ 1705 36))) (* (* x1 x1 x1) (* x0 x0 x0 x0) (- (/ 3140 9))) (* (* x1 x1 x1 x1 x1) (* x0 x0 x0)) (* (* x1 x1) (* x0 x0 x0 x0 x0) (- (/ 5200 9))) (* (* x1 x1) (* x0 x0 x0 x0 x0 x0) 256) (* x1 (* x0 x0 x0 x0 x0 x0) (- (/ 1120 9))) (* x1 (* x0 x0 x0 x0 x0 x0 x0) 128) (* (* x0 x0 x0 x0 x0 x0 x0) 160) (* (* x1 x1 x1 x1) (* x0 x0 x0 x0) 32)) 0) (<= (+ (* x0 x0) (* x2 (- 1)) (* x1 (- 1))) 0) (>= (+ (* x0 x0) (* x2 (- 1)) (* x1 (- 1))) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 14602799 14554316))) 0) (>= (+ x0 (- (/ 152756551 96611691))) 0) (<= (+ x0 (- (/ 108598627 68640336))) 0) (> (+ x1 (- (/ 3 2))) 0) (< (+ x1 (- (/ 180867006 120544601))) 0)))
(check-sat)
(exit)