(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x3 () Real)
(assert (and  (> (+ (* x0 x2 10) (* (* x2 x2) 2) (* x1 x0 (- 4)) (* x1 (- 3)) (* x1 (* x0 x0) 2) (* x1 x1) (* x0 (* x2 x2) 2) (* x1 (* x0 x0 x0)) (* (* x0 x0 x0) 2) (* x1 x2 (- 2)) (* (* x0 x0) 6) (* (* x1 x1) x0) (* (* x0 x0) x2 6) (* x2 2) (- 2)) 0) (< (+ (* x3 (- (/ 1 2))) x0) 0) (> (+ x3 (- (/ 15707963 5000000))) 0) (< (+ x3 (- (/ 31415927 10000000))) 0) (>= (+ (* x1 (- 1)) x0) 0) (>= (+ x1 1000) 0) (< (+ x1 (- (/ 304002716 193534137))) 0) (>= x2 0) (<= (+ x2 (- 1000)) 0) (>= x0 0) (< (+ x0 (- (/ 304002716 193534137))) 0) (> (+ x3 (- (/ 1064670109 338895027))) 0) (< (+ x3 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)