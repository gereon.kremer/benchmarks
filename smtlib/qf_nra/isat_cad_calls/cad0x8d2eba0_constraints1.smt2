(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* (* x0 x0 x0) (/ 1 3)) x2 (* x1 (* x0 x0) (/ 1 3)) (* x2 x1 x0 (- 1)) x1 (* x2 x1 (* x0 x0 x0) (- (/ 1 3))) (* x2 (* x0 x0) (/ 1 3)) x0) 0) (<= (+ (* x0 x0 x0) (* x2 3) (* x1 (* x0 x0)) (* x2 x1 x0 (- 3)) (* x1 3) (* x2 x1 (* x0 x0 x0) (- 1)) (* x2 (* x0 x0)) (* x0 3)) 0) (< (+ (* (* x0 x0) (- (/ 200 273))) (* (* x0 x0 x0) (/ 1 3)) x2 (* x1 (* x0 x0 x0) (/ 50 273)) (* x1 (* x0 x0) (/ 1 3)) (* x2 x1 x0 (- 1)) (* x2 x0 (- (/ 50 91))) x1 (* x2 x1 (* x0 x0 x0) (- (/ 1 3))) (* x2 (* x0 x0) (/ 1 3)) (* x2 x1 (* x0 x0) (/ 50 91)) x0 (- (/ 50 91))) 0) (>= (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ (* x1 (- 1)) x0) 0) (> (+ x2 (- (/ 2025295765 2025463))) 0) (< (+ x2 (- (/ 3434861613 3435136))) 0) (>= (+ x0 (/ 160033771 160034054)) 0) (<= (+ x0 (/ 202515938 202719277)) 0) (>= (+ x1 (/ 81865461 81927347)) 0) (< (+ x1 (/ 81173905 81317162)) 0)))
(check-sat)
(exit)