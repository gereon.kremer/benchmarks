(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x3 () Real)
(assert (and  (< (+ (* x0 x1 (/ 21 10)) (* (* x0 x0 x0) 10) (* (* x0 x0) (- 5)) x0 (* (* x0 x0 x0) x1 21) (* (* x0 x0) x1 (- (/ 1 2))) (* x1 (- (/ 1 120))) (- (/ 1 12))) 0) (> (+ x2 (- 2)) 0) (< (+ x2 (- 10)) 0) (> (+ x1 (- (/ 3302889718 3304531))) 0) (<= (+ x1 (- (/ 2634638254 2635927))) 0) (> x0 0) (< (+ x0 (- (/ 6043190 47160023721))) 0) (> x3 0) (<= (+ x3 (- 1000)) 0)))
(check-sat)
(exit)