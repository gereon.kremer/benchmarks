(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun height_0 () Real)
(declare-fun p_0 () Real)
(declare-fun p_1 () Real)
(declare-fun p_2 () Real)
(declare-fun delta_0 () Real)
(declare-fun height_2 () Real)
(declare-fun delta_2 () Real)
(declare-fun delta_1 () Real)
(declare-fun height_1 () Real)
(assert (and  (<= (+ delta_0 (- (/ 1 5))) 0) (>= (+ delta_0 (- (/ 1 10))) 0) (<= (+ (* delta_1 (* p_1 p_1) (- 1)) height_1) 0) (>= (+ (* delta_1 (* p_1 p_1) (- 1)) height_1) 0) (<= (+ delta_0 (* delta_1 (- (/ 10 11)))) 0) (>= (+ delta_0 (* delta_1 (- (/ 10 11)))) 0) (<= (+ height_0 (* height_1 (- (/ 5 4)))) 0) (>= (+ height_0 (* height_1 (- (/ 5 4)))) 0) (<= (+ (* p_1 3) p_0 (- 117)) 0) (<= (+ (* p_1 (/ 1 3)) p_0 (- 39)) 0) (< (+ (* p_1 (/ 1 3)) p_0 (- (/ 83 3))) 0) (>= (+ (* p_1 3) p_0 (- 83)) 0) (<= (+ (* (* p_2 p_2) delta_2 (- 1)) height_2) 0) (>= (+ (* (* p_2 p_2) delta_2 (- 1)) height_2) 0) (<= (+ delta_1 (* delta_2 (- (/ 10 11)))) 0) (>= (+ delta_1 (* delta_2 (- (/ 10 11)))) 0) (<= (+ (* height_2 (- (/ 5 4))) height_1) 0) (>= (+ (* height_2 (- (/ 5 4))) height_1) 0) (>= (+ height_0 (- (/ 583446187 5542828))) 0) (<= (+ height_0 (- (/ 6705310295 63700471))) 0) (>= p_0 0) (<= p_0 0) (>= (+ p_1 (- (/ 2224992673046185 80421421917332))) 0) (<= (+ p_1 (- (/ 2061301112 74504433))) 0) (>= (+ p_2 (- (/ 403109560 17117407))) 0) (<= (+ p_2 (- (/ 811710513 34467455))) 0) (>= (+ delta_0 (- (/ 655069036708436 6550690367084361))) 0) (<= (+ delta_0 (- (/ 14217287 141619470))) 0) (>= (+ height_2 (- (/ 882877308 13105421))) 0) (<= (+ height_2 (- (/ 506854687 7523627))) 0) (>= (+ delta_2 (- (/ 2321344216984 19184662950281))) 0) (<= (+ delta_2 (- (/ 47161968 388251179))) 0) (>= (+ delta_1 (- (/ 32220875382816 292917048934691))) 0) (<= (+ delta_1 (- (/ 29562382 267702837))) 0) (>= (+ height_1 (- (/ 583446187 6928535))) 0) (<= (+ height_1 (- (/ 2140004947 25412568))) 0)))
(check-sat)
(exit)