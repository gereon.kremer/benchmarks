(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* x0 x0) (* x1 (- 1)) (* x2 (- 1))) 0) (< (+ (* x0 x0) (* x1 (- 1)) (* x2 (- 1))) 0) (>= (+ x1 (- (/ 67167 65536))) 0) (< (+ x1 (- (/ 33 32))) 0) (> x0 0) (<= (+ x0 (- (/ 7510991 780928605))) 0) (> (+ x2 (- (/ 98399 65536))) 0) (<= (+ x2 (- (/ 193 128))) 0)))
(check-sat)
(exit)