(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x0 x1 (- 1)) (* x0 x0) (* x0 x2 (- (/ 207 100))) x2) 0) (> (+ x0 (* x1 (- 1))) 0) (> (+ x0 (- 1)) 0) (<= (+ x0 (- (/ 125265205 124899822))) 0) (>= (+ x2 (- (/ 9213091 1022042208))) 0) (< (+ x2 (- (/ 24327638 1883408869))) 0) (> (+ x1 (- (/ 133058392 133885311))) 0) (< (+ x1 (- (/ 128435821 128456178))) 0)))
(check-sat)
(exit)