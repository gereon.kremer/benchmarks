(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) x1 (- 6)) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* x1 x1) 12)) 0) (<= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (>= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- 1)) 0) (<= (+ x3 (- (/ 290278379 290145267))) 0) (> (+ x1 (- (/ 186444716 131836323))) 0) (<= (+ x1 (- (/ 103393789 73076931))) 0) (> x0 0) (<= (+ x0 (- (/ 7651046 1428827977))) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- (/ 146238119 146104122))) 0)))
(check-sat)
(exit)