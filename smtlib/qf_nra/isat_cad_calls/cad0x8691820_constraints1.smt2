(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* x1 x0) (* (* x0 x0) (- (/ 100 157))) (* x2 x1 (- (/ 100 157))) (* x1 (* x0 x0) (/ 100 157))) 0) (> (+ (* x2 (- 1)) x1) 0) (>= (+ x2 (- (/ 52942249 96578605))) 0) (<= (+ x2 (- (/ 98814796 178429209))) 0) (>= (+ x0 (- (/ 212513711 141265647))) 0) (<= (+ x0 (- (/ 144280886 95799219))) 0) (>= (+ x1 (- (/ 71154842 127978563))) 0) (<= (+ x1 (- (/ 83001163 148855984))) 0)))
(check-sat)
(exit)