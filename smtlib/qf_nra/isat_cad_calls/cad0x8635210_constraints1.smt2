(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* (* x0 x0) 360) (* x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0) (- 30)) (- 720)) 0) (> (+ x0 (* x1 (- 1))) 0) (>= (+ x1 (- (/ 1 20))) 0) (<= (+ x0 (* x2 (- (/ 1 2)))) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (<= (+ x0 (* x1 (- 10))) 0) (< (+ x0 (* x1 (- 10))) 0) (> (+ x0 (- (/ 126787601 80831067))) 0) (<= (+ x0 (- (/ 157605656 100406529))) 0) (> (+ x1 (- (/ 67494071 312783609))) 0) (<= (+ x1 (- (/ 43115703 198497231))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)