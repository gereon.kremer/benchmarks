(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x3 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x1 x3 (/ 1 5)) (* (* x0 x0) x2 (/ 2637651 2680000)) (* x0 x1 (/ 159 1000)) (* (* x0 x0) x1 x3 (/ 8427 5000000)) (* x0 x2 x3 (/ 49767 13400)) (* x2 (/ 7825 67)) (* x0 x1 x3 (/ 159 5000)) (* (* x0 x0) x2 x3 (/ 2637651 13400000)) x1 (* x0 x2 (/ 49767 2680)) (* (* x0 x0) x1 (/ 8427 1000000)) (* x2 x3 (/ 1565 67))) 0) (<= (+ (* x2 x2) (* x1 x1) (- 1)) 0) (>= (+ (* x2 x2) (* x1 x1) (- 1)) 0) (< (+ x3 (- (/ 151 50))) 0) (> (+ x0 (- (/ 174969255 41000564))) 0) (< (+ x0 (- (/ 215879830 50568863))) 0) (> (+ x3 (- (/ 2464897644 208944965849))) 0) (< (+ x3 (- (/ 11829172 973669271))) 0) (> (+ x1 (- (/ 427294849 427503642))) 0) (<= (+ x1 (- 1)) 0) (> (+ x2 (/ 4649543 527541198)) 0) (<= (+ x2 (/ 1 128)) 0)))
(check-sat)
(exit)