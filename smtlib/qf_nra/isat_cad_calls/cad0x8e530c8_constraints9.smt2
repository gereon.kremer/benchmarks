(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* x2 x2 x2 x2 x2 x2) (* x0 x0 x0 x0 x0 x0) (* (* x0 x0) (* x2 x2) (* x1 x1) (- 3)) (* x1 x1 x1 x1 x1 x1)) 0) (>= (+ x0 1000) 0) (<= (+ x0 (- 1000)) 0) (>= (+ x1 1000) 0) (<= (+ x1 (- 1000)) 0) (>= (+ x2 1000) 0) (<= (+ x2 (- 1000)) 0)))
(check-sat)
(exit)