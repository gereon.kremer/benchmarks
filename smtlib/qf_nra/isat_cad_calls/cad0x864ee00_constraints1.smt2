(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x1 x0) (* (* x0 x0) (/ 3 4)) (* x2 x1 (/ 3 2)) (* (* x2 x2) x1 x0 (- (/ 3 2))) (* x2 (* x1 x1) x0 (- (/ 3 2))) (* (* x2 x2) (/ 3 4)) (* x2 x0 (/ 3 2)) (* (* x1 x1) (* x0 x0) (/ 1 4)) (* (* x1 x1) (/ 3 4)) (* (* x2 x2) (* x1 x1) (* x0 x0) (/ 3 4)) (* x2 x1 (* x0 x0) (- (/ 3 2))) (/ 1 4)) 0) (> (+ (* x1 (- 1)) x0) 0) (< (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (> x2 0) (< (+ x2 (- (/ 8774387 1707349843))) 0) (> (+ x0 (/ 49170379 194391042)) 0) (<= (+ x0 (/ 16046230 63959653)) 0) (>= (+ x1 (/ 114040150 230815663)) 0) (< (+ x1 (/ 170860791 347271415)) 0)))
(check-sat)
(exit)