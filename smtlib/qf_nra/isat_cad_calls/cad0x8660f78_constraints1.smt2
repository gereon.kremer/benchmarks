(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x10 () Real)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x4 () Real)
(declare-fun x9 () Real)
(declare-fun x7 () Real)
(declare-fun x8 () Real)
(declare-fun x5 () Real)
(declare-fun x6 () Real)
(assert (and  (> (+ (* x4 (- 1)) x0 (* x2 x3)) 0) (>= (+ x4 (* x9 (- 1))) 0) (< (+ x7 (* x8 (- 1))) 0) (< (+ (* x6 (- 1)) x5) 0) (<= (+ (* x4 (- 1)) x0) 0) (<= (+ (* x2 x1) (* x4 (- 1)) x0) 0) (>= (+ (* x3 (- 1)) x1) 0) (>= (+ x0 (* x2 x3)) 0) (> (+ (* x2 x1) x0) 0) (>= (+ (* x2 x1) x0) 0) (> (+ x0 (* x2 x3)) 0) (> (+ x2 (- (/ 436455139 28014798))) 0) (<= (+ x2 (- (/ 409662726 26283803))) 0) (> x10 0) (<= (+ x10 (- 1000)) 0) (> x1 0) (< (+ x1 (- (/ 3446711 3529531757))) 0) (>= x3 0) (< (+ x3 (- (/ 5027226 5026739347))) 0) (> x0 0) (< (+ x0 (- (/ 125 16384))) 0) (> (+ x4 (- (/ 17360286 1088618147))) 0) (<= (+ x4 (- (/ 1879428 81300989))) 0) (> (+ x9 (- (/ 188707936 18559264655))) 0) (<= (+ x9 (- (/ 19640679 1180160092))) 0) (>= (+ x7 1000) 0) (<= (+ x7 (/ 16383875 16384)) 0) (>= (+ x8 1000) 0) (<= (+ x8 (/ 16383875 16384)) 0) (>= (+ x5 1000) 0) (<= (+ x5 (/ 16383875 16384)) 0) (>= (+ x6 1000) 0) (<= (+ x6 (/ 16383875 16384)) 0)))
(check-sat)
(exit)