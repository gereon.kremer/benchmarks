(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (< (+ (* (* x0 x0 x0) (/ 10 9)) (* x2 (* x0 x0 x0 x0) (/ 5 21)) (* x2 x1 (* x0 x0 x0 x0 x0) (- (/ 5 21))) x2 (* x1 (* x0 x0) (/ 10 9)) (* x2 x1 x0 (- 1)) (* x1 (* x0 x0 x0 x0) (/ 5 21)) x1 (* (* x0 x0 x0 x0 x0) (/ 5 21)) (* x2 x1 (* x0 x0 x0) (- (/ 10 9))) (* x2 (* x0 x0) (/ 10 9)) x0) 0) (> (+ (* x1 (- 1)) x0) 0) (>= (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (> x2 0) (< (+ x2 (- (/ 11393590 1337183171))) 0) (>= x0 0) (<= (+ x0 (- (/ 7350903 915384397))) 0) (>= (+ x1 (/ 3684633 468365501)) 0) (< (+ x1 (- (/ 108501749 166009672963))) 0)))
(check-sat)
(exit)