(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> (+ (* x1 (- 1)) x0) 0) (< (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> x2 0) (< (+ x2 (- (/ 2407763 456117971))) 0) (> (+ x0 1) 0) (< (+ x0 (/ 141088933 141837668)) 0) (> (+ x1 1) 0) (< (+ x1 (/ 141088933 141837668)) 0)))
(check-sat)
(exit)