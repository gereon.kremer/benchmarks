(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x0 x0) 3) 0) (< (+ (* x0 x1 (- (/ 1 4))) (* (* x1 x1) (/ 1 12)) (* x2 (* x0 x0) (* x1 x1) (/ 1 9)) (* (* x1 x1 x1) (/ 1 3)) (* x2 (* x0 x0 x0) x1 (- (/ 1 3))) x2 (* x2 (* x0 x0) (/ 1 3)) (* x0 (* x1 x1 x1) (- (/ 1 12))) (* x2 (* x0 x0 x0) (* x1 x1 x1) (- (/ 1 9))) (* x0 (* x1 x1)) (* (* x0 x0 x0) (/ 1 3)) (* x2 x0 x1 (- 1)) (* (* x0 x0) (* x1 x1) (/ 1 36)) (* (* x0 x0) (/ 1 12)) (* (* x0 x0) (* x1 x1 x1) (/ 4 9)) (* (* x0 x0 x0) x1 (- (/ 1 12))) (* (* x0 x0 x0) (* x1 x1) (/ 4 9)) (* x2 x0 (* x1 x1 x1) (- (/ 1 3))) (* (* x0 x0 x0) (* x1 x1 x1) (- (/ 1 36))) (* (* x0 x0) x1) (* x2 (* x1 x1) (/ 1 3)) (/ 1 4)) 0) (< (+ x0 (* x1 (- 1))) 0) (< (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (> x2 0) (<= (+ x2 (- (/ 7204544 1216715877))) 0) (>= (+ x1 (/ 46611179 175781764)) 0) (< (+ x1 (/ 40939727 157919829)) 0) (>= (+ x0 (/ 71916072 116233925)) 0) (< (+ x0 (/ 77016779 125680711)) 0)))
(check-sat)
(exit)