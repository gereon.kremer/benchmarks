(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x4 () Real)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(assert (and  (<= (+ (* x0 x0) (- 2)) 0) (>= (+ (* x0 x0) (- 2)) 0) (< (+ x2 (- (/ 4 5))) 0) (<= (+ (* x2 (- 1)) (* x1 x1) (- 1)) 0) (< (+ (* x2 (- 1)) (* x1 x1) (- 1)) 0) (> x2 0) (<= (+ x2 (- (/ 1725517098610 276082735777601))) 0) (>= x4 0) (<= (+ x4 (- 1000)) 0) (>= x1 0) (<= (+ x1 (- (/ 7238299 1381148041))) 0) (>= x3 0) (<= (+ x3 (- 1000)) 0) (>= (+ x0 (- (/ 186444716 131836323))) 0) (<= (+ x0 (- (/ 131836323 93222358))) 0)))
(check-sat)
(exit)