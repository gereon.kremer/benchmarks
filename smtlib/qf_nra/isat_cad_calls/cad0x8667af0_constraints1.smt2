(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (<= (+ x2 x0 x1 (- 2)) 0) (> (+ (* x0 x1 (- (/ 3 5))) (* (* x1 x1) (/ 1 5)) (* (* x1 x1 x1) (- (/ 1 60))) (* x2 x0 (- (/ 3 5))) (* (* x2 x2 x2) (- (/ 1 60))) (* (* x2 x2) x0 x1 (- (/ 1 20))) (* x2 (- 1)) (* x2 (* x0 x0) (/ 7 20)) (* x0 (* x1 x1 x1) (- (/ 1 60))) (* x2 (* x0 x0) x1 (- (/ 1 10))) (* (* x2 x2) x1 (- (/ 1 20))) (* (* x2 x2) (/ 1 5)) (* x0 (* x1 x1) (/ 3 20)) (* x2 (* x0 x0 x0) (- (/ 1 20))) (* (* x0 x0 x0 x0) (- (/ 1 60))) (* (* x0 x0 x0) (/ 11 60)) (* x2 x0 x1 (/ 3 10)) (* x2 x1 (/ 2 5)) (* (* x2 x2 x2) x0 (- (/ 1 60))) (* (* x0 x0) (* x1 x1) (- (/ 1 20))) (* (* x0 x0) (- (/ 4 5))) x0 (* (* x2 x2) (* x0 x0) (- (/ 1 20))) (* (* x0 x0 x0) x1 (- (/ 1 20))) (* (* x2 x2) x0 (/ 3 20)) (* (* x0 x0) x1 (/ 7 20)) (* x2 (* x1 x1) (- (/ 1 20))) (* x2 x0 (* x1 x1) (- (/ 1 20))) (* x1 (- 1)) 2) 0) (>= (+ x2 x0 x1 (- 2)) 0) (>= (+ x2 (- (/ 125 128))) 0) (< (+ x2 (- (/ 98636531 100899203))) 0) (> (+ x1 (- (/ 70812071 75817770))) 0) (<= (+ x1 (- (/ 109936501 117595533))) 0) (> (+ x0 (- (/ 40276149 460007752))) 0) (< (+ x0 (- (/ 172116982 1945971557))) 0)))
(check-sat)
(exit)