(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* (* x0 x0) (* x1 x1) (- (/ 3600060000000000000000000 3600119999))) (* (* x0 x0 x0 x0) (* x1 x1 x1 x1) (/ 900000000 3600119999)) (* (* x0 x0) (/ 120000000000000000000 3600119999)) (* (* x0 x0 x0 x0) (* x1 x1) (- (/ 3600060000 3600119999))) (* x0 x0 x0 x0) (* (* x0 x0) (* x1 x1 x1 x1) (/ 900000000000000000000000 3600119999)) (- (/ 55289999999999882000000000000057 205206839943))) 0) (> (+ (* (* x0 x0) (* x1 x1) (- 500000000000000)) (* (* x0 x0) 1000000000000000) (* (* x0 x0 x0 x0) (* x1 x1) (- (/ 1 2))) (* x0 x0 x0 x0)) 0) (>= (+ (* x2 (- (/ 1 4))) x1) 0) (<= (+ (* x2 (- (/ 1 3))) x1) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (>= (+ x0 (- 100)) 0) (<= (+ x0 (- (/ 469793424 4697915))) 0) (>= (+ x1 (- (/ 80106394 101071997))) 0) (< (+ x1 (- (/ 52514249 66173908))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)