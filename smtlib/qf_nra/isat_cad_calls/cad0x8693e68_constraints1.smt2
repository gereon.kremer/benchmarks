(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* (* x0 x0 x0) (/ 3 312500000)) x0 (* (* x0 x0) (- (/ 3 25000))) (* (* x0 x0 x0 x0 x0 x0) (- (/ 27 78125000000000000000000))) (* (* x0 x0 x0 x0 x0) (/ 27 1562500000000000000)) (* (* x0 x0 x0 x0) (- (/ 63 125000000000000))) (- (/ 12500 3))) 0) (<= (+ (* x2 (/ 235 42)) x1) 0) (< (+ (* x2 (/ 235 42)) x1) 0) (>= (+ x0 (/ 244693723 63503683)) 0) (<= (+ x0 (/ 283280301 73524377)) 0) (>= (+ x1 1000) 0) (<= (+ x1 (/ 2619182526 2619199)) 0) (>= (+ x2 1000) 0) (<= (+ x2 (/ 3516530047 3516534)) 0)))
(check-sat)
(exit)