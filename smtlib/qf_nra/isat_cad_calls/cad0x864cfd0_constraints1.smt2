(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x0 x1 2) (* (* x0 x0) (- 2)) x0 (- (/ 1 6))) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- 1000)) 0) (> (+ x1 (- (/ 11875736 179772081))) 0) (<= (+ x1 (- (/ 26911281 362809639))) 0) (> (+ x0 (- (/ 51500791 188612724))) 0) (< (+ x0 (- (/ 54173803 196468041))) 0)))
(check-sat)
(exit)