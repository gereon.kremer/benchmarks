(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ (* x1 (- (/ 441 400))) x0) 0) (< (+ (* x1 (- (/ 441 400))) x0) 0) (> x2 0) (<= (+ x2 (- 1000)) 0) (>= (+ x0 1000) 0) (<= (+ x0 (/ 16383875 16384)) 0) (>= (+ x1 (- (/ 3936469363 3936498))) 0) (<= (+ x1 (- 1000)) 0)))
(check-sat)
(exit)