(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (* x1 (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0)) 0) (<= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (>= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- 1)) 0) (< (+ x3 (- 5)) 0) (> (+ x0 (- (/ 186444716 131836323))) 0) (< (+ x0 (- (/ 251217123 102558961))) 0) (> x1 0) (< (+ x1 (- 2)) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 158114965 70711162))) 0)))
(check-sat)
(exit)