(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (>= (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (> (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (< (+ x0 (* x1 (- 1))) 0) (<= (+ (* x0 x1) x2 (* x2 x0 x1 (- 1)) x0 x1 (- 1)) 0) (<= (+ (* x2 (* x0 x0 x0) x1 (- 1)) (* x2 3) (* x2 (* x0 x0)) (* x0 (* x1 x1) 3) (* x0 x0 x0) (* x2 x0 x1 (- 3)) (* (* x0 x0 x0) (* x1 x1)) (* (* x0 x0) x1 3)) 0) (> x2 0) (< (+ x2 (- (/ 2847310 2950129019))) 0) (> (+ x1 (/ 2847310 2950129019)) 0) (< x1 0) (> (+ x0 (/ 2847310 2950129019)) 0) (< x0 0)))
(check-sat)
(exit)