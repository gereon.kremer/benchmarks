(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x1 x0 (- 12)) (* x1 (* x0 x0 x0) (- 120)) (* x1 (* x0 x0) 60) x1) 0) (<= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (>= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- 1)) 0) (< (+ x3 (- (/ 81736886 81686201))) 0) (> (+ x0 (- (/ 186444716 131836323))) 0) (< (+ x0 (- (/ 109283598 77263189))) 0) (> x1 0) (< (+ x1 (- (/ 917611 32360815683))) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 130658772 130618255))) 0)))
(check-sat)
(exit)