(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x0 x1 (- 6)) (* (* x1 x1) (- 1)) (* x0 (* x1 x1) 12) (* (* x0 x0 x0) 10) (* (* x0 x0) (* x1 x1) (- 60)) (* (* x0 x0) (- 5)) x0 (* (* x0 x0 x0) x1 (- 60)) (* (* x0 x0 x0) (* x1 x1) 120) (* (* x0 x0) x1 30) (* x1 (/ 1 2)) (- (/ 1 12))) 0) (< (+ (* (* x0 x0 x0) 10) (* (* x0 x0) (- 5)) x0 (- (/ 1 12))) 0) (> (+ (* x0 (* x1 x1) 12) (* (* x0 x0 x0) 10) x0 (* (* x0 x0 x0) (* x1 x1) 120) (* (* x0 x0) x1 (- 30)) (* x1 (- (/ 1 2)))) 0) (<= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (>= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- (/ 309816581 292750269))) 0) (< (+ x3 (- (/ 75076548 70864427))) 0) (> (+ x1 (- (/ 145579115 101527093))) 0) (< (+ x1 (- (/ 91884688 64000011))) 0) (> (+ x0 (- (/ 73246480 303365067))) 0) (< (+ x0 (- (/ 53891225 222908696))) 0) (> (+ x2 (- (/ 190859900 185528663))) 0) (< (+ x2 (- (/ 19970722 19386047))) 0)))
(check-sat)
(exit)