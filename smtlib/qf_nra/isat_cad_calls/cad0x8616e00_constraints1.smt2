(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (<= (+ x0 (* x1 x1) (- 1)) 0) (>= (+ x0 (* x1 x1) (- 1)) 0) (<= (+ (* x2 x2) (* x0 x0) (- 1)) 0) (< (+ (* x2 x2) (* x0 x0) (- 1)) 0) (>= (+ x0 (- (/ 31050487 87408688))) 0) (<= (+ x0 (- (/ 122934657 338111171))) 0) (>= x2 0) (<= (+ x2 (- (/ 12845227 2034338381))) 0) (>= (+ x1 (- (/ 60398917 75711435))) 0) (<= (+ x1 (- (/ 94087701 117174092))) 0)))
(check-sat)
(exit)