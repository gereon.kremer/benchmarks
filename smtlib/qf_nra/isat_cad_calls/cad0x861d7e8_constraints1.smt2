(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* x0 x0) (* (* x1 x1) (- 80)) (- 75)) 0) (< (+ (* x0 x0) (* (* x1 x1) (- 80)) (- 75)) 0) (>= (+ x1 (- (/ 18891113992 18891123))) 0) (<= (+ x1 (- 1000)) 0) (> x0 0) (<= (+ x0 (- (/ 5991599 1110625559))) 0) (> x2 0) (<= (+ x2 (- 1000)) 0)))
(check-sat)
(exit)