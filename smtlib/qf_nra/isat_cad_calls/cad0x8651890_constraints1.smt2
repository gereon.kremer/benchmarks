(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x0 x0) 360) (* x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0) (- 30)) (- 720)) 0) (> (+ x0 (* x2 (- 1))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (>= (+ x0 (/ 453831821 11682289)) 0) (<= (+ x0 (/ 489005153 12587847)) 0) (>= (+ x2 (/ 4537079075 4537103)) 0) (<= (+ x2 (/ 65535625 65536)) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)