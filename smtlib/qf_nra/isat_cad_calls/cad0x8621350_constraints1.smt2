(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) 11880) (* (* x0 x0) (- 239500800)) (* (* x0 x0 x0 x0 x0 x0) (- 665280)) (* (* x0 x0 x0 x0) 19958400) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 132)) 479001600) 0) (>= (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) (- 90)) (* (* x0 x0) 1814400) (* (* x0 x0 x0 x0 x0 x0) 5040) (* (* x0 x0 x0 x0) (- 151200)) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 3628800)) 0) (>= (+ (* (* x0 x0) 360) (* x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0) (- 30)) (- 720)) 0) (>= (+ (* x0 x0) (- 2)) 0) (> (+ x0 (* x1 (- 1))) 0) (>= (+ x1 (- (/ 1 20))) 0) (<= (+ x0 (* x2 (- (/ 1 2)))) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (< (+ x0 (* x1 (- 10))) 0) (<= (+ x0 (* x1 (- 10))) 0) (> (+ x0 (- (/ 150430877 95828361))) 0) (< (+ x0 (- (/ 304002716 193534137))) 0) (>= (+ x1 (- (/ 80734103 373777410))) 0) (<= (+ x1 (- (/ 104015258 478414739))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)