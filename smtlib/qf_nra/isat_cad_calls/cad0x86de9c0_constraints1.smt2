(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (<= (+ (* x1 x0) (* (* x1 x1) x0 x2 2) x1 (* x1 (* x0 x0) x2 2) (* x1 x0 x2 (- 14)) (* (* x1 x1) (* x0 x0) x2 2) x0 1) 0) (>= (+ (* (* x1 x1 x1) (* x0 x0) x2 (- (/ 1 11))) (* x1 x0) (* (* x1 x1 x1) (* x0 x0 x0) x2 (- (/ 1 11))) (* x1 (* x0 x0) (- (/ 5 11))) (* (* x1 x1) (* x0 x0) x2 (/ 15 11)) (* (* x1 x1) (* x0 x0) (- (/ 5 11))) (* (* x1 x1) x0 (- (/ 5 11))) (* (* x1 x1) (* x0 x0 x0) x2 (- (/ 1 11)))) 0) (> (+ x1 (- (/ 154155910 84797879))) 0) (<= (+ x1 (- (/ 193769711 106525287))) 0) (>= (+ x2 (- (/ 75472864 40011509))) 0) (<= (+ x2 (- (/ 325095963 172251259))) 0) (>= (+ x0 (- 1)) 0) (<= (+ x0 (- 1)) 0)))
(check-sat)
(exit)