(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x0 x1) x2 (* x2 x0 x1 (- 1)) x0 x1 (- 1)) 0) (< (+ x0 (* x1 (- 1))) 0) (> (+ x2 (- (/ 153774167 51800146))) 0) (< (+ x2 (- (/ 239990299 80583133))) 0) (> (+ x1 (/ 58153535 176443968)) 0) (< (+ x1 (/ 21 64)) 0) (> (+ x0 1) 0) (< (+ x0 (/ 88093055 88221984)) 0)))
(check-sat)
(exit)