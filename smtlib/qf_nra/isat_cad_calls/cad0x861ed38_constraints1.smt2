(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ x3 x1 (- 12)) 0) (>= (+ (* x3 (- 1)) x0 10) 0) (<= (+ x0 x1 (- 2)) 0) (>= (+ x3 (- 1)) 0) (<= (+ x3 (- (/ 2059 2048))) 0) (> (+ x1 (- (/ 22517 2048))) 0) (<= (+ x1 (- 11)) 0) (>= (+ x0 9) 0) (<= (+ x0 (/ 18421 2048)) 0) (>= (+ x2 (- 1)) 0) (<= (+ x2 (- 1)) 0)))
(check-sat)
(exit)