(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (>= (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (> (+ (* x2 (* x0 x0 x0) x1 (- 1)) (* x2 3) (* x2 (* x0 x0)) (* x0 (* x1 x1) 3) (* x0 x0 x0) (* x2 x0 x1 (- 3)) (* (* x0 x0 x0) (* x1 x1)) (* (* x0 x0) x1 3)) 0) (< (+ x0 (* x1 (- 1))) 0) (> (+ x2 (- (/ 218520714 439955171))) 0) (< (+ x2 (- (/ 67497990 135475751))) 0) (> (+ x1 (- (/ 33831719 101644032))) 0) (< (+ x1 (- (/ 54481051 162750684))) 0) (> (+ x0 1) 0) (< (+ x0 (/ 147702832 147861157)) 0)))
(check-sat)
(exit)