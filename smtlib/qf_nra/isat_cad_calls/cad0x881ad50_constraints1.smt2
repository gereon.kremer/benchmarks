(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x1 x1 x1) (* x0 x0 x0 x0) (- (/ 1 3))) (* x2 (* x1 x1) (* x0 x0) (/ 19 9)) (* (* x2 x2) x1 (* x0 x0) (/ 5 9)) (* x2 (* x1 x1 x1) x0 (/ 2 3)) x2 (* (* x2 x2) (* x1 x1 x1) (* x0 x0 x0 x0) (- (/ 1 9))) (* x2 (* x1 x1) (- (/ 2 3))) (* x2 (* x1 x1 x1) (* x0 x0 x0) (/ 2 9)) (* (* x2 x2) x0 (- (/ 1 3))) (* (* x1 x1 x1) (- (/ 1 3))) (* (* x2 x2) (* x1 x1 x1) (* x0 x0) (- (/ 1 3))) (* x2 x1 x0 (- (/ 10 3))) (* (* x2 x2) (* x1 x1) x0 (/ 2 3)) (* x1 (* x0 x0 x0 x0) (- (/ 4 9))) (* (* x1 x1) (* x0 x0 x0) (- (/ 8 9))) (* (* x1 x1 x1) (* x0 x0) (- (/ 10 9))) (* x2 (* x1 x1) (* x0 x0 x0 x0) (/ 5 9)) (* (* x2 x2) x1 (- (/ 1 3))) (* x2 x1 (* x0 x0 x0) (- (/ 2 9))) (* x2 (* x0 x0) (- (/ 1 3))) (* (* x2 x2) (* x1 x1) (* x0 x0 x0) (- (/ 1 9)))) 0) (< (+ (* x1 (- 1)) x0) 0) (>= (+ x2 (- (/ 5179549555 9475054))) 0) (< (+ x2 (- (/ 1212551481 2218142))) 0) (> (+ x0 1) 0) (< (+ x0 (/ 166712894 166814823)) 0) (>= (+ x1 (/ 59691937 138931790)) 0) (< (+ x1 (/ 57648120 134372489)) 0)))
(check-sat)
(exit)