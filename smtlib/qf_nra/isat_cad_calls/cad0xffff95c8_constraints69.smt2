(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x4 () Real)
(declare-fun x6 () Real)
(assert (and  (= (+ (* x2 (* x1 x1) (- (/ 1 2))) (* x4 2000) (* x6 x1)) 0) (>= (+ x1 (- (/ 4833523147 5272938))) 0) (<= (+ x1 (- (/ 2995850489 3268196))) 0) (>= (+ x2 (/ 103388231 47377957)) 0) (<= (+ x2 (/ 24834916 11386801)) 0) (>= (+ x4 (- (/ 3596289427 7846500))) 0) (<= (+ x4 (- (/ 7801840647 17022245))) 0) (>= (+ x6 (/ 1959497027 5878434)) 0) (<= (+ x6 (/ 566315537 1698969)) 0)))
(check-sat)
(exit)