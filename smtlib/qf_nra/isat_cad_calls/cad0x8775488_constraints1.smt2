(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ x1 (* x2 (/ 1 3))) 0) (>= (+ (* x0 x1 (- (/ 157 100))) (* x0 x2 (- (/ 157 300))) x1 (* (* x0 x0) x1 (/ 8 3)) (* x2 (/ 1 3))) 0) (<= (+ (* x2 x2) (* (* x0 x0) (- 80)) (- 75)) 0) (>= (+ (* x2 x2) (* (* x0 x0) (- 80)) (- 75)) 0) (<= (+ (* x1 x1) (- 3)) 0) (>= (+ (* x1 x1) (- 3)) 0) (>= (+ x0 (- (/ 245127703 71005200))) 0) (<= (+ x0 (- (/ 133352903 38615880))) 0) (>= (+ x2 (- (/ 273505500 8527039))) 0) (<= (+ x2 (- (/ 332374228 10362009))) 0) (>= (+ x1 (- (/ 138907099 80198051))) 0) (<= (+ x1 (- (/ 189750626 109552575))) 0)))
(check-sat)
(exit)