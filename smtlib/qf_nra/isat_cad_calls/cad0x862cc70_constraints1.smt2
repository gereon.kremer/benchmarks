(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* (* x0 x0) (* x1 x1)) (* x0 x2 (* x1 x1) (- 1))) 0) (< (+ x0 (* x2 (- 1))) 0) (> (+ x0 (- (/ 93 4096))) 0) (< (+ x0 (- (/ 3 128))) 0) (> (+ x1 (- (/ 3449389520 3498321))) 0) (<= (+ x1 (- (/ 4390111553 4452384))) 0) (> (+ x2 (- (/ 8189 4096))) 0) (< (+ x2 (- 2)) 0)))
(check-sat)
(exit)