(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x3 () Real)
(assert (and  (<= (+ (* x1 x1) (* x0 x2 (- 2)) (* x0 x0)) 0) (>= (+ x0 (- (/ 1894203948 3788423))) 0) (< (+ x0 (- (/ 2441331654 4882673))) 0) (> (+ x1 (/ 61261116 666039391)) 0) (<= (+ x1 (/ 29361979 340509363)) 0) (>= (+ x2 (- (/ 4331615499 4331617))) 0) (<= (+ x2 (- 1000)) 0) (>= x3 0) (<= (+ x3 (- 1000)) 0)))
(check-sat)
(exit)