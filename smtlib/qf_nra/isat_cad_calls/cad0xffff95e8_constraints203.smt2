(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x5 () Real)
(declare-fun x7 () Real)
(declare-fun x0 () Real)
(assert (and  (= (+ (* x7 (/ 10085907 10581889439)) (* x7 x5 (- 1)) (* x0 (/ 10085907 10581889439)) (- (/ 39582313130738706 77758761372518922047))) 0) (>= x5 0) (<= (+ x5 (- (/ 3017099 3706670203))) 0) (>= (+ x7 (- (/ 36777000151 5319756294842))) 0) (<= (+ x7 (- (/ 10402137 1286733674))) 0) (>= (+ x0 (- (/ 36777000151 5319756294842))) 0) (<= (+ x0 (- (/ 125 16384))) 0)))
(check-sat)
(exit)