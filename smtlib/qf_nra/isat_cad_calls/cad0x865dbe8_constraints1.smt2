(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ x0 (* (* x0 x0) (- (/ 259 30750))) (- (/ 37000 287))) 0) (< (+ (* x0 x2 (/ 4 315)) (* (* x0 x0) x1 (/ 49 750000)) (* x0 x1 (- (/ 7 500))) (* x2 (- (/ 400 441))) (* (* x0 x0) x2 (- (/ 1 16875))) x1) 0) (<= (+ (* x2 (- (/ 400 441))) x1) 0) (< (+ (* x2 (- (/ 400 441))) x1) 0) (> (+ x0 (/ 4097024387 8194051)) 0) (< (+ x0 (/ 3284276539 6568568)) 0) (>= (+ x1 1000) 0) (< (+ x1 (/ 7965184729 7965188)) 0) (>= (+ x2 (- (/ 4796218081 4796220))) 0) (<= (+ x2 (- 1000)) 0)))
(check-sat)
(exit)