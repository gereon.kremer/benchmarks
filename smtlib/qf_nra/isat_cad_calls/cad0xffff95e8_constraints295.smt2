(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x5 () Real)
(declare-fun x7 () Real)
(assert (and  (= (+ (* x5 (- 1)) (* x7 x5) (/ 3263295 7348287073)) 0) (>= x5 0) (<= (+ x5 (- (/ 3017099 3706670203))) 0) (>= (+ x7 (- (/ 36777000151 5319756294842))) 0) (<= (+ x7 (- (/ 10402137 1286733674))) 0)))
(check-sat)
(exit)