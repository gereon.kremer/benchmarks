(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) 11880) (* (* x0 x0) (- 239500800)) (* (* x0 x0 x0 x0 x0 x0) (- 665280)) (* (* x0 x0 x0 x0) 19958400) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 132)) 479001600) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (< (+ x0 (* x2 (- 1))) 0) (>= (+ x0 (/ 136239954 31163377)) 0) (<= (+ x0 (/ 304670621 69691104)) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0) (> (+ x2 (/ 136239954 31163377)) 0) (<= (+ x2 (/ 367556408 84220153)) 0)))
(check-sat)
(exit)