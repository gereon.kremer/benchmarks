(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x1 x0) (* (* x1 x1) x0 x2 2) x1 (* x1 (* x0 x0) x2 2) (* x1 x0 x2 (- 14)) (* (* x1 x1) (* x0 x0) x2 2) x0 1) 0) (> (+ x1 (- 1)) 0) (< (+ x1 (- (/ 118750268 118655541))) 0) (> (+ x2 (/ 71944234 71473149)) 0) (< (+ x2 (/ 30649839 30459314)) 0) (> (+ x0 (- (/ 60417264 60007805))) 0) (< (+ x0 (- (/ 132071 131072))) 0)))
(check-sat)
(exit)