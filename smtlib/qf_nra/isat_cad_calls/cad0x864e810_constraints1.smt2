(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x2 (* x0 x0) (* x1 x1) (/ 1 9)) (* (* x1 x1 x1) (/ 1 3)) (* x2 (* x0 x0 x0) x1 (- (/ 1 3))) x2 (* x2 (* x0 x0) (/ 1 3)) (* x2 (* x0 x0 x0) (* x1 x1 x1) (- (/ 1 9))) (* x0 (* x1 x1)) (* (* x0 x0 x0) (/ 1 3)) (* x2 x0 x1 (- 1)) (* (* x0 x0) (* x1 x1 x1) (/ 4 9)) (* (* x0 x0 x0) (* x1 x1) (/ 4 9)) (* x2 x0 (* x1 x1 x1) (- (/ 1 3))) (* (* x0 x0) x1) (* x2 (* x1 x1) (/ 1 3))) 0) (< (+ x0 (* x1 (- 1))) 0) (<= (+ (* x2 (* x0 x0 x0) x1 (- 1)) (* x2 3) (* x2 (* x0 x0)) (* x0 (* x1 x1) 3) (* x0 x0 x0) (* x2 x0 x1 (- 3)) (* (* x0 x0 x0) (* x1 x1)) (* (* x0 x0) x1 3)) 0) (< (+ x2 (* x2 x0 x1 (- 1)) x0 x1) 0) (<= (+ (* x0 x1) x2 (* x2 x0 x1 (- 1)) x0 x1 (- 1)) 0) (> (+ x2 (- (/ 45961841 1022980227))) 0) (< (+ x2 (- (/ 44680420 947665809))) 0) (> (+ x1 (- (/ 76018080 173109401))) 0) (<= (+ x1 (- (/ 78436855 177754298))) 0) (> (+ x0 1) 0) (< (+ x0 (/ 150734437 151099975)) 0)))
(check-sat)
(exit)