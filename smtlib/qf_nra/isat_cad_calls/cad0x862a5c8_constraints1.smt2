(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* x0 x0) (* x1 x1)) 0) (> (+ x0 x2) 0) (> (+ x0 (* x2 (- 1))) 0) (> x0 0) (<= (+ x0 (- (/ 125 16384))) 0) (> (+ x2 (/ 125 16384)) 0) (<= x2 0) (> (+ x1 (- 1)) 0) (<= (+ x1 (- (/ 54496881 54289597))) 0)))
(check-sat)
(exit)