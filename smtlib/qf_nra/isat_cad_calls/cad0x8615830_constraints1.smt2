(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ x2 (- (/ 15707963 5000000))) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (<= (+ (* x0 x0) (- 2)) 0) (< (+ (* x0 x0) (- 2)) 0) (> x1 0) (< (+ x1 (- 1)) 0) (>= (+ x0 (/ 21306286 241053109)) 0) (<= (+ x0 (/ 22138103 267161647)) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)