(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* (* x1 x1 x1) 10) (* (* x1 x1) (- 5)) x1 (- (/ 1 12))) 0) (<= (+ (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* x1 (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 12)) (* (* x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 60) (* (* x1 x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 120))) 0) (<= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (>= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- (/ 176715223 170736377))) 0) (< (+ x3 (- (/ 166031936 160271569))) 0) (>= (+ x0 (- (/ 284940173 199760152))) 0) (< (+ x0 (- (/ 64617643 45294660))) 0) (> (+ x1 (- (/ 61242013 323429042))) 0) (< (+ x1 (- (/ 24980632 131766979))) 0) (> (+ x2 (- (/ 70426381 69224754))) 0) (< (+ x2 (- (/ 76315167 74979629))) 0)))
(check-sat)
(exit)