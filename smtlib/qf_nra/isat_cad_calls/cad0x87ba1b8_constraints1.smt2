(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* x2 x1 (/ 2 5)) (* (* x1 x1) (/ 1 5)) (* (* x0 x0) (* x1 x1) (- (/ 1 20))) (* (* x1 x1 x1) (- (/ 1 60))) (* x0 x2 (- (/ 3 5))) (* (* x0 x0 x0) x1 (- (/ 1 20))) (* (* x0 x0 x0) (/ 11 60)) (* (* x0 x0) x2 x1 (- (/ 1 10))) x0 (* x0 (* x2 x2) (/ 3 20)) (* x0 (* x2 x2) x1 (- (/ 1 20))) (* (* x0 x0) x1 (/ 7 20)) (* (* x0 x0) (- (/ 4 5))) (* x2 (* x1 x1) (- (/ 1 20))) (* x0 (* x2 x2 x2) (- (/ 1 60))) (* (* x2 x2 x2) (- (/ 1 60))) (* x0 x2 x1 (/ 3 10)) (* x0 x1 (- (/ 3 5))) (* (* x0 x0 x0) x2 (- (/ 1 20))) (* (* x2 x2) (/ 1 5)) (* x2 (- 1)) (* (* x0 x0) (* x2 x2) (- (/ 1 20))) (* (* x0 x0 x0 x0) (- (/ 1 60))) (* (* x0 x0) x2 (/ 7 20)) (* x0 (* x1 x1 x1) (- (/ 1 60))) (* (* x2 x2) x1 (- (/ 1 20))) (* x0 (* x1 x1) (/ 3 20)) (* x0 x2 (* x1 x1) (- (/ 1 20))) (* x1 (- 1)) 2) 0) (> (+ x0 (/ 1340244415 1340254)) 0) (<= (+ x0 (/ 153400386639 153401557)) 0) (>= (+ x1 (- (/ 13535012546 13535019))) 0) (<= (+ x1 (- 1000)) 0) (>= (+ x2 (- (/ 1879047993 1879048))) 0) (< (+ x2 (- (/ 4294966984 4294967))) 0)))
(check-sat)
(exit)