(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(assert (and  (> (+ (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* x1 (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 12)) (* (* x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 60) (* (* x1 x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 120))) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- 5)) 0) (>= (+ x0 (- (/ 23125085 45696691))) 0) (<= (+ x0 (- (/ 212779250 415374769))) 0) (>= (+ x1 (- (/ 60204548 9014491))) 0) (<= (+ x1 (- (/ 233403079 34942261))) 0) (> x3 0) (<= (+ x3 (- 1000)) 0)))
(check-sat)
(exit)