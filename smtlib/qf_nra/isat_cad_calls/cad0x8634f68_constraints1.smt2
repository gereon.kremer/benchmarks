(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x3 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* x1 (- 1)) x0) 0) (<= (+ (* (* x1 x1) (* x0 x0) (- 1)) (* (* x2 x2) (* x0 x0) (- 1)) (* (* x1 x1) (* x2 x2) (- 1)) (* x3 x3) (* (* x0 x0 x0 x0) (- 1))) 0) (>= (+ (* (* x1 x1) (* x0 x0) (- 1)) (* (* x2 x2) (* x0 x0) (- 1)) (* (* x1 x1) (* x2 x2) (- 1)) (* x3 x3) (* (* x0 x0 x0 x0) (- 1))) 0) (< (+ x1 (* x2 (- 1))) 0) (> x1 0) (<= (+ x1 (- (/ 3234504 1653636613))) 0) (> x0 0) (<= (+ x0 (- (/ 84183373 8699865944))) 0) (>= x3 0) (<= (+ x3 (- (/ 12587175 1380335942))) 0) (> (+ x2 (- (/ 255 128))) 0) (<= (+ x2 (- (/ 182019007 91276785))) 0)))
(check-sat)
(exit)