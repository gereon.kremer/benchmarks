(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ x2 x0) 0) (> (+ (* x0 x0 x0) (* (* x1 x1) x0)) 0) (< (+ (* x2 (* x1 x1 x1) x0) (* (* x2 x2) x1 x0 (/ 10 3)) (* x1 (* x0 x0 x0) (/ 10 3)) (* x2 (* x1 x1) x0 (/ 157 15)) (* x2 (* x1 x1 x1) (- (/ 10 3))) (* (* x1 x1 x1) x0 (/ 10 3)) (* x2 x1 (* x0 x0 x0)) (* x2 (* x0 x0 x0) (/ 157 15)) (* x2 x1 (* x0 x0) (- (/ 20 3)))) 0) (> (+ (* x2 (- 1)) x0) 0) (>= (+ x2 (- (/ 821693 218333504))) 0) (< (+ x2 (- (/ 9478315 2451763021))) 0) (> (+ x0 (- (/ 821693 218333504))) 0) (<= (+ x0 (- (/ 9478315 2451763021))) 0) (> (+ x1 (- (/ 4139362078 4640207))) 0) (<= (+ x1 (- (/ 3251230233 3644611))) 0)))
(check-sat)
(exit)