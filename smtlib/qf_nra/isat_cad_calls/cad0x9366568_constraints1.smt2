(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x3 () Real)
(declare-fun x4 () Real)
(declare-fun x10 () Real)
(declare-fun x9 () Real)
(declare-fun x7 () Real)
(declare-fun x8 () Real)
(declare-fun x5 () Real)
(declare-fun x6 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (< (+ (* x4 (- 1)) x3) 0) (<= (+ (* x8 x8) (* x2 x5 2) (* (* x7 x7) (- 1)) (* x2 x6 (- 2))) 0) (<= (+ (* x1 x2 2) (* (* x3 x3) (- 1)) (* x2 x0 (- 2)) (* x4 x4)) 0) (<= (+ (* x1 (- 1)) x0) 0) (> (+ x2 (- (/ 4435393329 5574557))) 0) (< (+ x2 (- (/ 2928529677 3680674))) 0) (>= (+ x3 (- (/ 4095875 16384))) 0) (<= (+ x3 (- (/ 2522823553 10091596))) 0) (> (+ x4 (- (/ 1605927948 6423839))) 0) (<= (+ x4 (- (/ 1122199526 4488879))) 0) (>= x10 0) (<= (+ x10 (- 1000)) 0) (>= x9 0) (<= (+ x9 (- 1000)) 0) (>= x7 0) (<= (+ x7 (- (/ 125 16384))) 0) (> (+ x8 (/ 36641849 436106251)) 0) (<= (+ x8 (/ 21155601 268577126)) 0) (>= (+ x5 1000) 0) (< (+ x5 (/ 4852004684 4852005)) 0) (> (+ x6 (/ 237497179 475012)) 0) (<= (+ x6 (/ 3672727111 7345728)) 0) (> (+ x0 (/ 1132804697 3020901)) 0) (<= (+ x0 (/ 4672996056 12461729)) 0) (> (+ x1 (/ 2024295779 5398268)) 0) (<= (+ x1 (/ 2419593157 6452451)) 0)))
(check-sat)
(exit)