(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* (* x0 x0 x0) x1 (/ 1 128)) (* (* x0 x0 x0) (/ 1 16)) x0 (* (* x0 x0) x1 (/ 5 64)) (* (* x0 x0) (/ 15 32)) (* x0 x1 (/ 3 16)) (* (* x0 x0 x0 x0) (/ 1 1024)) (* x1 (/ 1 8)) (/ 5 8)) 0) (<= (+ (* x2 x1 (/ 957 1543)) (* x0 x2 (- (/ 957 3086))) x0 (* (* x0 x0) (/ 250 1543)) (* x0 x1 (/ 500 1543)) (* x2 (- (/ 957 1543))) (* x1 (/ 1914 1543)) (- (/ 1914 1543))) 0) (<= (+ (* x1 x1) x0 (- 1)) 0) (>= (+ (* x1 x1) x0 (- 1)) 0) (<= (+ (* x0 x0) (* x2 x2) (- 1)) 0) (>= (+ (* x0 x0) (* x2 x2) (- 1)) 0) (> (+ x0 (- (/ 166847674 167027733))) 0) (<= (+ x0 (- (/ 245751640 245767099))) 0) (>= (+ x1 (- (/ 4120753 519574414))) 0) (< (+ x1 (- (/ 14439010 1363169681))) 0) (> (+ x2 (- (/ 17883811 435148290))) 0) (< (+ x2 (- (/ 15670209 337569922))) 0)))
(check-sat)
(exit)