(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (< (* x1 x0) 0) (> (+ x0 (* x2 (- 1))) 0) (>= (+ x3 1000) 0) (< x3 0) (> x2 0) (<= (+ x2 (- (/ 1 128))) 0) (>= (+ x0 (- (/ 4095 2048))) 0) (< (+ x0 (- 2)) 0) (>= (+ x1 1000) 0) (< (+ x1 (/ 32767875 32768)) 0)))
(check-sat)
(exit)