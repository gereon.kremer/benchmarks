(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x0 x0) x2 x1) x0) 0) (< (+ (* (* x0 x0) x2 (* x1 x1) (/ 15 11)) (* (* x0 x0) x2 x1 (- (/ 5 11))) (* (* x0 x0 x0) (* x2 x2) (* x1 x1) (- (/ 1 11))) (* (* x0 x0) x1 (- (/ 5 11))) (* (* x0 x0 x0) x2 (* x1 x1) (- (/ 1 11))) (* x0 x2 x1 (- (/ 5 11))) (* x0 x1) (* (* x0 x0) (* x2 x2) (* x1 x1) (- (/ 1 11)))) 0) (< (+ (* (* x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* (* x0 x0) x2 (* x1 x1) (/ 15 11)) (* x0 x2 x1 (- (/ 5 11))) (* x0 x1) (* (* x0 x0) (* x2 x2) (* x1 x1) (- (/ 1 11))) (* x0 (* x1 x1) (- (/ 5 11))) (* x0 x2 (* x1 x1) (- (/ 5 11))) (* (* x0 x0) (* x2 x2) (* x1 x1 x1) (- (/ 1 11)))) 0) (<= (+ (* (* x0 x0) x2 (* x1 x1) 2) (* (* x0 x0) x2 x1 2) x0 (* x0 x2 x1 (- 14)) (* x0 x1) (* x0 x2 (* x1 x1) 2) x1 1) 0) (< (+ (* (* x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* (* x0 x0) (* x1 x1) (- (/ 5 11))) (* (* x0 x0) x2 (* x1 x1) (/ 15 11)) (* (* x0 x0) x1 (- (/ 5 11))) (* (* x0 x0 x0) x2 (* x1 x1) (- (/ 1 11))) (* x0 x1) (* (* x0 x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* x0 (* x1 x1) (- (/ 5 11)))) 0) (> (+ x0 (- (/ 1315331571 47229800))) 0) (<= (+ x0 (- (/ 605070761 21725254))) 0) (> (+ x1 (/ 694713 2723341282)) 0) (<= (+ x1 (- (/ 11643947 15534018978))) 0) (> (+ x2 (- (/ 1805675906 1805677))) 0) (<= (+ x2 (- 1000)) 0)))
(check-sat)
(exit)