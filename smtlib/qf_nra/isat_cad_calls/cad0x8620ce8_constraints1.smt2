(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* (* x0 x0 x0) (/ 3 312500000)) x0 (* (* x0 x0) (- (/ 3 25000))) (* (* x0 x0 x0 x0 x0 x0) (- (/ 27 78125000000000000000000))) (* (* x0 x0 x0 x0 x0) (/ 27 1562500000000000000)) (* (* x0 x0 x0 x0) (- (/ 63 125000000000000))) (- (/ 12500 3))) 0) (<= (+ (* x2 (/ 42 235)) x1) 0) (<= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (>= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (< (+ (* x2 (/ 42 235)) x1) 0) (> x0 0) (< (+ x0 (- (/ 12178919 1477840282))) 0) (>= (+ x1 (/ 119395757 164592156)) 0) (<= (+ x1 (/ 156549770 217197921)) 0) (>= (+ x2 (/ 31944781 46084782)) 0) (<= (+ x2 (/ 134370801 195214555)) 0)))
(check-sat)
(exit)