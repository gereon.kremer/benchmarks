(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* (* x1 x1 x1) (- (/ 1 6))) (* (* x0 x0 x0) (- (/ 1 6))) x0 (* x0 (* x2 x2) (- (/ 1 4))) (* (* x0 x0) x1 (- (/ 1 4))) (* x2 (* x1 x1) (- (/ 1 4))) (* (* x2 x2 x2) (- (/ 1 6))) (* x0 x2 x1 (- (/ 1 2))) x2 (* (* x0 x0) x2 (- (/ 1 4))) (* (* x2 x2) x1 (- (/ 1 4))) (* x0 (* x1 x1) (- (/ 1 4))) x1) 0) (> (+ x1 (- (/ 1 10))) 0) (> (+ x0 (- (/ 1 10))) 0) (> (+ x2 (- (/ 1 10))) 0) (> (+ x0 (- (/ 655069036708436 6550690367084361))) 0) (< (+ x0 (- (/ 646560642457 6119021237301))) 0) (> (+ x1 (- (/ 655069036708436 6550690367084361))) 0) (< (+ x1 (- (/ 646560642457 6119021237301))) 0) (> (+ x2 (- (/ 655069036708436 6550690367084361))) 0) (<= (+ x2 (- (/ 874758515743 8278675786699))) 0)))
(check-sat)
(exit)