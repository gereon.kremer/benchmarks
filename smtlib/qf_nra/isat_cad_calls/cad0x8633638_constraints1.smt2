(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* x0 x0) (* (* x0 x0 x0 x0) (/ 3 14)) (/ 9 10)) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (> (+ (* x1 (- 1)) x0) 0) (>= (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (> x2 0) (<= (+ x2 (- (/ 7527383 995688178))) 0) (>= x0 0) (< (+ x0 (- (/ 8989509 1348078708))) 0) (>= (+ x1 (/ 8989509 1348078708)) 0) (< x1 0)))
(check-sat)
(exit)