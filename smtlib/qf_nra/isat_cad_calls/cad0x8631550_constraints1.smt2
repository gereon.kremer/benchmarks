(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x3 () Real)
(declare-fun x4 () Real)
(declare-fun x0 () Real)
(assert (and  (< (+ x1 (* x4 (/ 570 13)) (* x3 (/ 49 65)) (* x2 200) (- (/ 12695 52))) 0) (> (+ x0 (- (/ 177 366500000))) 0) (>= (+ x1 1000) 0) (<= (+ x1 (/ 5721694897 5721726)) 0) (>= (+ x2 1000) 0) (<= (+ x2 (/ 2313447661 2313448)) 0) (>= (+ x3 1000) 0) (<= (+ x3 (/ 3244902601 3244926)) 0) (>= (+ x4 1000) 0) (<= (+ x4 (/ 1794676110 1794677)) 0) (> (+ x0 (- (/ 177 366500000))) 0) (<= (+ x0 (- (/ 6022169 789287773))) 0)))
(check-sat)
(exit)