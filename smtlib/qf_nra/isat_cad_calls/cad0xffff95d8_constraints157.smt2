(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x5 () Real)
(declare-fun x3 () Real)
(assert (and  (= (+ x5 (* x3 (- 1))) 0) (>= (+ x5 1000) 0) (<= (+ x5 (- 1000)) 0) (>= (+ x3 1000) 0) (<= (+ x3 (- 1000)) 0)))
(check-sat)
(exit)