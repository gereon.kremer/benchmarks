(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* x0 x0) (* (* x0 x0 x0 x0) (- (/ 1 12))) (- 2)) 0) (> (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) (- (/ 1 576))) (* x0 x0) (* (* x0 x0 x0 x0 x0 x0) (/ 1 24)) (* (* x0 x0 x0 x0) (- (/ 1 3)))) 0) (< (+ x0 (* x2 (- 1))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (>= (+ x0 (/ 21242563 13341959)) 0) (< (+ x0 (/ 26980399 16956573)) 0) (>= (+ x2 (/ 314189563 1011033402)) 0) (<= (+ x2 (/ 93426602 308111697)) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)