(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x0 x0) (* x1 x1) (- (/ 3600060000000000000000000 3600119999))) (* (* x0 x0 x0 x0) (* x1 x1 x1 x1) (/ 900000000 3600119999)) (* (* x0 x0) (/ 120000000000000000000 3600119999)) (* (* x0 x0 x0 x0) (* x1 x1) (- (/ 3600060000 3600119999))) (* x0 x0 x0 x0) (* (* x0 x0) (* x1 x1 x1 x1) (/ 900000000000000000000000 3600119999)) (- (/ 970000000000000000000000000000 3600119999))) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (> (+ x0 (/ 11856754 954803827)) 0) (<= (+ x0 (/ 5928377 954803827)) 0) (>= (+ x1 (/ 2589700470 2589719)) 0) (< (+ x1 (/ 2123739791 2123756)) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)