(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* (* x1 x1) x2 (* x0 x0) 2) (* (* x1 x1) x2 x0 2) x1 (* x1 x2 x0 (- 14)) (* x1 x0) (* x1 x2 (* x0 x0) 2) x0 1) 0) (>= (+ (* (* x1 x1) x2 (* x0 x0 x0) (- (/ 1 11))) (* (* x1 x1) (* x0 x0) (- (/ 5 11))) (* (* x1 x1) x2 (* x0 x0) (/ 15 11)) (* (* x1 x1) x0 (- (/ 5 11))) (* (* x1 x1 x1) x2 (* x0 x0) (- (/ 1 11))) (* x1 x0) (* (* x1 x1 x1) x2 (* x0 x0 x0) (- (/ 1 11))) (* x1 (* x0 x0) (- (/ 5 11)))) 0) (< (+ (* (* x1 x1) x2 (* x0 x0 x0) (- (/ 1 11))) (* (* x1 x1) x2 (* x0 x0) (/ 15 11)) (* x1 x2 x0 (- (/ 5 11))) (* x1 x0) (* (* x1 x1) (* x2 x2) (* x0 x0) (- (/ 1 11))) (* x1 (* x0 x0) (- (/ 5 11))) (* x1 x2 (* x0 x0) (- (/ 5 11))) (* (* x1 x1) (* x2 x2) (* x0 x0 x0) (- (/ 1 11)))) 0) (>= (+ x1 (- (/ 38433419 18594796))) 0) (< (+ x1 (- (/ 76930916 37203813))) 0) (>= (+ x0 (- (/ 147725037 92744833))) 0) (<= (+ x0 (- (/ 100668427 63161901))) 0) (> (+ x2 (- (/ 1140748749 51494753))) 0) (<= (+ x2 (- (/ 316738665 14297408))) 0)))
(check-sat)
(exit)