(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x0 x1 (- 12)) (* (* x1 x1) (- 5)) (* (* x1 x1 x1) 10) (* x0 (* x1 x1 x1) (- 120)) (* x0 (* x1 x1) 60) (* (* x0 x0 x0) 10) (* (* x0 x0) (* x1 x1) (- 300)) (* (* x0 x0) (- 5)) x0 (* (* x0 x0) (* x1 x1 x1) 600) (* (* x0 x0 x0) x1 (- 120)) (* (* x0 x0 x0) (* x1 x1) 600) (* (* x0 x0 x0) (* x1 x1 x1) (- 1200)) (* (* x0 x0) x1 60) x1 (- (/ 1 12))) 0) (> (+ (* (* x0 x0 x0) 10) (* (* x0 x0) (- 5)) x0 (- (/ 1 12))) 0) (<= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (>= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ x3 (- (/ 29982988 29253177))) 0) (< (+ x3 (- (/ 71725549 69889320))) 0) (> (+ x1 (- (/ 94897635 66705574))) 0) (< (+ x1 (- (/ 134464873 94462585))) 0) (> (+ x0 (- (/ 35456643 219951341))) 0) (<= (+ x0 (- (/ 29565182 183039637))) 0) (> (+ x2 (- (/ 153912539 152106483))) 0) (< (+ x2 (- (/ 57100261 56364617))) 0)))
(check-sat)
(exit)