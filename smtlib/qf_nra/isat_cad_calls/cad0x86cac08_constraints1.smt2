(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x0 x1 (- 12)) (* (* x1 x1) (- 5)) (* (* x1 x1 x1) 10) (* x0 (* x1 x1 x1) (- 120)) (* x0 (* x1 x1) 60) (* (* x0 x0 x0) 10) (* (* x0 x0) (* x1 x1) (- 300)) (* (* x0 x0) (- 5)) x0 (* (* x0 x0) (* x1 x1 x1) 600) (* (* x0 x0 x0) x1 (- 120)) (* (* x0 x0 x0) (* x1 x1) 600) (* (* x0 x0 x0) (* x1 x1 x1) (- 1200)) (* (* x0 x0) x1 60) x1 (- (/ 1 12))) 0) (< (+ (* (* x0 x0 x0) 10) (* (* x0 x0) (- 5)) x0 (- (/ 1 12))) 0) (<= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x1 x1) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (>= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- 1)) 0) (< (+ x3 (- (/ 256310043 256095916))) 0) (> (+ x1 (- (/ 186444716 131836323))) 0) (< (+ x1 (- (/ 392768771 277497537))) 0) (> (+ x0 (- (/ 8266207 1243762048))) 0) (< (+ x0 (- (/ 12710743 1758314426))) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 59990687 59965623))) 0)))
(check-sat)
(exit)