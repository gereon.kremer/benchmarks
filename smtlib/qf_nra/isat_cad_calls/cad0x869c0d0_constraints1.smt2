(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x1 x1 x1 x1) (* x0 x0 x0)) (* (* x1 x1 x1) (* x0 x0 x0 x0) 10) (* (* x1 x1 x1 x1) (* x0 x0) (- (/ 125 144))) (* (* x1 x1) (* x0 x0 x0 x0) (- (/ 1025 18))) (* (* x1 x1) (* x0 x0 x0 x0 x0) 24) (* x1 (* x0 x0 x0 x0 x0 x0) 16) (* x1 (* x0 x0 x0 x0 x0) (- (/ 230 9))) (* (* x1 x1 x1) (* x0 x0 x0) (- (/ 235 12))) (* (* x0 x0 x0 x0 x0 x0) 20)) 0) (<= (+ (* x0 x0) (* x2 (- 1)) (* x1 (- 1))) 0) (>= (+ (* x0 x0) (* x2 (- 1)) (* x1 (- 1))) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- (/ 18726363 18717104))) 0) (> (+ x0 (- (/ 101904649 64450159))) 0) (<= (+ x0 (- (/ 104852921 66272804))) 0) (>= (+ x1 (- (/ 191605907 127509837))) 0) (< (+ x1 (- (/ 260003441 172970064))) 0)))
(check-sat)
(exit)