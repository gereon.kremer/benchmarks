(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (< (+ (* x0 x0) (- 2)) 0) (> (+ x0 (* x1 (- 1))) 0) (>= (+ x1 (- (/ 1 20))) 0) (<= (+ x0 (* x2 (- (/ 1 2)))) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (<= (+ x0 (* x1 (- 10))) 0) (< (+ x0 (* x1 (- 10))) 0) (> (+ x0 (- (/ 33170321 581487597))) 0) (< (+ x0 (- (/ 8165287 140958851))) 0) (> (+ x1 (- (/ 33170321 581487597))) 0) (< (+ x1 (- (/ 8165287 140958851))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)