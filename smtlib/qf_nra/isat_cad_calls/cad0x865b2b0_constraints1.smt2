(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) 518918400) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0) (- 10461394944000)) (* (* x0 x0 x0 x0 x0 x0) (- 29059430400)) (* (* x0 x0 x0 x0) 871782912000) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 240)) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 43680) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 5765760)) 20922789888000) 0) (> (+ x0 (* x2 (- 1))) 0) (>= (+ x2 (- (/ 1 10))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (<= (+ x0 (* x1 (- (/ 1 2))) (/ 1 5)) 0) (> (+ x0 (- (/ 10190618 95449541))) 0) (< (+ x0 (- (/ 34751095 318702581))) 0) (>= (+ x2 (- (/ 655069036708436 6550690367084361))) 0) (< (+ x2 (- (/ 34751095 318702581))) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)