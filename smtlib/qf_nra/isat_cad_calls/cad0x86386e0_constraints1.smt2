(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 0) (> (+ (* (* x1 x1 x1) 10) (* (* x1 x1) (- 5)) x1 (- (/ 1 12))) 0) (<= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x0 x0) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (>= (+ (* x3 (- 1)) (* x1 x1) 1) 0) (<= (+ (* x2 x2) (* x3 (- 1))) 0) (>= (+ (* x2 x2) (* x3 (- 1))) 0) (> (+ x3 (- (/ 93562055 91747088))) 0) (< (+ x3 (- (/ 95381898 93328075))) 0) (> (+ x0 (- (/ 150012379 105554030))) 0) (< (+ x0 (- (/ 161477227 113558595))) 0) (> (+ x1 (- (/ 36928820 256244083))) 0) (< (+ x1 (- (/ 15260571 102871585))) 0) (> (+ x2 (- (/ 56961752 56406559))) 0) (< (+ x2 (- (/ 104678374 103477445))) 0)))
(check-sat)
(exit)