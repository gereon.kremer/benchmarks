(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x2 (- (/ 400 441))) x1) 0) (>= (+ x0 (* (* x0 x0) (/ 29 30000)) (/ 10000 29)) 0) (<= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (>= (+ (* x1 x1) (* x2 x2) (- 1)) 0) (> (+ (* x2 (- (/ 400 441))) x1) 0) (> (+ x0 (- (/ 1570 21))) 0) (> (+ x0 (- (/ 1570 21))) 0) (<= (+ x0 (- (/ 845850462 11313853))) 0) (> (+ x1 (/ 256187710 381049131)) 0) (<= (+ x1 (/ 45775087 68194243)) 0) (>= (+ x2 (/ 36896965 49777689)) 0) (< (+ x2 (/ 57892792 78228397)) 0)))
(check-sat)
(exit)