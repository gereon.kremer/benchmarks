(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ x1 x2) 0) (>= (+ (* x0 x0 x0) (* (* x1 x1) x0)) 0) (> (+ x1 (* x2 (- 1))) 0) (< (+ (* x1 (- 1)) x0) 0) (<= (+ (* (* x1 x1 x1) x2) (* x1 (* x0 x0 x0) (- (/ 157 100))) (* x0 x0 x0 x0) (* x1 (* x0 x0 x0 x0) (- (/ 3 10))) (* (* x1 x1 x1) x0 (- (/ 157 100))) (* (* x1 x1) (* x0 x0) 2) (* (* x1 x1 x1) (* x0 x0) (- (/ 3 10)))) 0) (> (+ x1 (- (/ 78222701 78024409))) 0) (<= (+ x1 (- (/ 229258853 228293194))) 0) (> (+ x2 (/ 119921509 130493770)) 0) (< (+ x2 (/ 157108955 171416163)) 0) (> (+ x0 (- (/ 78222701 78024409))) 0) (< (+ x0 (- (/ 18507584 18440489))) 0)))
(check-sat)
(exit)