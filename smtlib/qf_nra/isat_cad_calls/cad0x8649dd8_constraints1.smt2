(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (> (+ (* (* x0 x0) (* x2 x2)) (* (* x1 x1) (* x2 x2)) (* (* x0 x0) (* x1 x1)) (* x0 x0 x0 x0)) 0) (> (+ x1 (* x2 (- 1))) 0) (>= (+ x0 (/ 44395129 604366368)) 0) (<= (+ x0 (/ 21724965 337999454)) 0) (> x2 0) (<= (+ x2 (- (/ 13524383 1808702078))) 0) (> x1 0) (<= (+ x1 (- (/ 13524383 1808702078))) 0)))
(check-sat)
(exit)