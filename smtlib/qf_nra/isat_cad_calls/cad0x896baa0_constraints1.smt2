(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(assert (and  (< (+ (* (* x0 x0 x0 x0 x0 x0 x0 x0) 8229600) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (* (* x0 x0) (- 2612736000)) (* (* x0 x0 x0 x0 x0 x0) (- 116121600)) (* (* x0 x0 x0 x0) 870912000) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 120)) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) 8100) (* (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 335520))) 0) (> (+ x0 (* x2 (- 1))) 0) (< (+ x1 (- (/ 31415927 10000000))) 0) (> (+ x1 (- (/ 15707963 5000000))) 0) (> (+ x0 (- (/ 255344997 46065223))) 0) (<= (+ x0 (- (/ 263944279 47607978))) 0) (> (+ x2 (/ 1221291798 1837015)) 0) (<= (+ x2 (/ 3055619845 4596186)) 0) (> (+ x1 (- (/ 1064670109 338895027))) 0) (< (+ x1 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)