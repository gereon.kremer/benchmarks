(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* x0 x2 x1) 1) 0) (< (+ (* (* x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* (* x0 x0) (* x1 x1) (- (/ 5 11))) (* (* x0 x0) x2 (* x1 x1) (/ 15 11)) (* (* x0 x0) x1 (- (/ 5 11))) (* (* x0 x0 x0) x2 (* x1 x1) (- (/ 1 11))) (* x0 x1) (* (* x0 x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* x0 (* x1 x1) (- (/ 5 11)))) 0) (<= (+ (* (* x0 x0) x2 (* x1 x1) 2) (* (* x0 x0) x2 x1 2) x0 (* x0 x2 x1 (- 14)) (* x0 x1) (* x0 x2 (* x1 x1) 2) x1 1) 0) (>= (+ x0 1000) 0) (<= (+ x0 (/ 900942863 900943)) 0) (> (+ x1 (- (/ 169361 943062469325))) 0) (< (+ x1 (- (/ 257581 35493155459))) 0) (> (+ x2 (- (/ 3076520533 3076522))) 0) (<= (+ x2 (- 1000)) 0)))
(check-sat)
(exit)