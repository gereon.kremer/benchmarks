(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (< (+ (* x0 x1 (- (/ 72 205))) (* (* x0 x0) (- (/ 36 205))) x0 (* x1 (- (/ 18 41)))) 0) (> (+ x0 (* x1 2)) 0) (> (+ x2 (- 1)) 0) (< (+ x2 (- (/ 33 32))) 0) (>= (+ x1 (- (/ 43695461 65536))) 0) (< (+ x1 (- (/ 1905987809 2858667))) 0) (> (+ x0 (- (/ 306188555 203156423))) 0) (<= (+ x0 (- (/ 395217 262144))) 0)))
(check-sat)
(exit)