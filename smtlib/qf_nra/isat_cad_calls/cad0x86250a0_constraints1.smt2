(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (> (* x1 x3) 0) (<= (+ (* x3 x3) (* (* x1 x1) (* x2 x2) (- 1)) (* (* x0 x0 x0 x0) (- 1)) (* (* x1 x1) (* x0 x0) (- 1)) (* (* x2 x2) (* x0 x0) (- 1))) 0) (>= (+ (* x3 x3) (* (* x1 x1) (* x2 x2) (- 1)) (* (* x0 x0 x0 x0) (- 1)) (* (* x1 x1) (* x0 x0) (- 1)) (* (* x2 x2) (* x0 x0) (- 1))) 0) (> (+ x1 (* x2 (- 1))) 0) (> x1 0) (< (+ x1 (- (/ 125 16384))) 0) (> (+ x3 (- 1)) 0) (<= (+ x3 (- (/ 31232317 31151706))) 0) (> (+ x0 (- 1)) 0) (<= (+ x0 (- (/ 23973075 23932604))) 0) (> x2 0) (< (+ x2 (- (/ 125 16384))) 0)))
(check-sat)
(exit)