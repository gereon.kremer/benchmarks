(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x2 x0 (- 1)) (* x2 (- (/ 1 2))) (* x2 (* x0 x0) (- (/ 1 6))) (* (* x0 x0 x0) (/ 1 3)) x0 (* (* x0 x0) x1 (- (/ 1 3))) (* x1 (- 1))) 0) (> (+ x0 (* x1 (- 1))) 0) (> x2 0) (<= (+ x2 (- (/ 8758601 1002907183))) 0) (> x1 0) (<= (+ x1 (- (/ 7116083 1284074556))) 0) (> x0 0) (<= (+ x0 (- (/ 7116083 1284074556))) 0)))
(check-sat)
(exit)