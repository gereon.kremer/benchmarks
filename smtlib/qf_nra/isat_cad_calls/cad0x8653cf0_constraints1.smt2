(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (* x0 x0 x0 x0) 0) (> (+ (* (* x0 x0 x0 x0) (* x1 x1 x1 x1) 30) (* (* x0 x0 x0 x0 x0 x0) (- 720)) (* (* x0 x0 x0 x0) 2160) (* (* x0 x0) (* x1 x1 x1 x1) (- 30)) (* x1 x1 x1 x1 x1 x1)) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (< (+ (* x2 (- (/ 1 2))) x1 (/ 1 10000000)) 0) (> x0 0) (<= (+ x0 (- (/ 7272029 771469793))) 0) (> x1 0) (< (+ x1 (- (/ 9040475 1473368427))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)