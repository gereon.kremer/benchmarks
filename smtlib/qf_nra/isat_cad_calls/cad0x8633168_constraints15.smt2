(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x3 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (* x0 x2) 0) (> (+ (* x1 (- (/ 2 3))) (* x1 x0 (/ 2 3)) x0 (/ 1 3)) 0) (<= (+ (* x2 x2) (* x3 (- 1)) (- 1)) 0) (>= (+ (* x2 x2) (* x3 (- 1)) (- 1)) 0) (<= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (>= (+ (* x3 (- 1)) (* x0 x0) 1) 0) (<= (+ (* x1 x1) (* x3 (- 1))) 0) (>= (+ (* x1 x1) (* x3 (- 1))) 0) (> (+ x3 (- 1)) 0) (< (+ x3 (- 5)) 0) (> (+ x2 (- (/ 186444716 131836323))) 0) (< (+ x2 (- (/ 251217123 102558961))) 0) (> x0 0) (< (+ x0 (- 2)) 0) (> (+ x1 (- 1)) 0) (< (+ x1 (- (/ 158114965 70711162))) 0)))
(check-sat)
(exit)