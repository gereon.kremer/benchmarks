(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x0 x0) x2 (* x1 x1) 2) (* (* x0 x0) x2 x1 2) x0 (* x0 x2 x1 (- 14)) (* x0 x1) (* x0 x2 (* x1 x1) 2) x1 1) 0) (< (+ (* (* x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* (* x0 x0) (* x1 x1) (- (/ 5 11))) (* (* x0 x0) x2 (* x1 x1) (/ 15 11)) (* (* x0 x0) x1 (- (/ 5 11))) (* (* x0 x0 x0) x2 (* x1 x1) (- (/ 1 11))) (* x0 x1) (* (* x0 x0 x0) x2 (* x1 x1 x1) (- (/ 1 11))) (* x0 (* x1 x1) (- (/ 5 11)))) 0) (> (+ x0 (- (/ 30559001 15684939))) 0) (<= (+ x0 (- (/ 287229857 147425722))) 0) (> (+ x1 (- (/ 62562640 31346733))) 0) (< (+ x1 (- (/ 198249245 99331904))) 0) (>= (+ x2 (- 1)) 0) (<= (+ x2 (- 1)) 0)))
(check-sat)
(exit)