(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (<= (+ (* x0 x1) (- 1)) 0) (<= (+ x0 (* x0 x2 x1 (- 1)) (* x0 x1) x2 x1 (- 1)) 0) (< (+ x0 (* x1 (- 1))) 0) (>= (+ x0 (* x0 x2 x1 (- 1)) x2 x1) 0) (> (+ x0 1) 0) (< (+ x0 (/ 127 128)) 0) (> (+ x1 (- (/ 127 128))) 0) (< (+ x1 (- 1)) 0) (> x2 0) (< (+ x2 (- (/ 257 32768))) 0)))
(check-sat)
(exit)