(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(assert (and  (> (+ (* x0 x1) (* (* x0 x0 x0 x0) x1 (- 96)) (* (* x0 x0 x0 x0 x0 x0) (- 192)) (* (* x0 x0 x0 x0 x0) x1 192) (* (* x0 x0 x0 x0 x0) 192) (* (* x0 x0 x0 x0) (- 96)) (* (* x0 x0 x0) 32) (* (* x0 x0) (- 7)) x0 (* (* x0 x0 x0) x1 32) (* (* x0 x0) x1 (- 7)) (* x1 (- (/ 1 12))) (- (/ 1 12))) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- 1000)) 0) (>= (+ x1 (- (/ 2863137920263 2863312683))) 0) (<= (+ x1 (- (/ 3075325405 3075483))) 0) (> (+ x0 (- (/ 106881308 474140753))) 0) (<= (+ x0 (- (/ 145751533 644822960))) 0)))
(check-sat)
(exit)