(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x0 () Real)
(declare-fun x2 () Real)
(assert (and  (> (+ (* (* x1 x1 x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0) (- 132)) (* (* x1 x1 x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0 x0)) (* (* x1 x1 x1 x1) (* x0 x0 x0 x0 x0 x0) (- 665280)) (* (* x1 x1 x1 x1) (* x0 x0 x0 x0) 19958400) (* (* x1 x1) (* x0 x0 x0 x0) (- 19958400)) (* (* x1 x1 x1 x1) (* x0 x0 x0 x0 x0 x0 x0 x0) 11880) (* (* x0 x0 x0 x0 x0 x0) 665280)) 0) (< (+ x2 (- (/ 31415927 10000000))) 0) (> (+ x2 (- (/ 15707963 5000000))) 0) (< (+ (* x2 (- (/ 1 2))) x0 (/ 1 10000000)) 0) (> (+ x1 (- (/ 4797729187 4797734))) 0) (<= (+ x1 (- 1000)) 0) (> x0 0) (< (+ x0 (- (/ 3789992 734540681))) 0) (> (+ x2 (- (/ 1064670109 338895027))) 0) (< (+ x2 (- (/ 576589505 183534137))) 0)))
(check-sat)
(exit)