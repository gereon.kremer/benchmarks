(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(assert (and  (>= (* x1 x1) 0) (> (+ (* (* x0 x0 x0) 10) (* (* x0 x0) (- 5)) x0 (- (/ 1 12))) 0) (> (* x1 x1) 0) (> x1 0) (<= (+ x1 (- (/ 125 16384))) 0) (> (+ x2 (- 1)) 0) (<= (+ x2 (- 1000)) 0) (>= (+ x0 (- (/ 66724914 397889743))) 0) (< (+ x0 (- (/ 24728903 140912386))) 0)))
(check-sat)
(exit)