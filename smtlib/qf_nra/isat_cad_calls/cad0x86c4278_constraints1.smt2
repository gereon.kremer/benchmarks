(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(declare-fun x3 () Real)
(assert (and  (> (+ (* x0 x2 (/ 1 4)) (* (* x0 x0) x3 x2 (- (/ 63 80))) x0 (* (* x0 x0) x1 (/ 13 32)) (* (* x0 x0) (/ 1 20)) (* x3 x2 (/ 63 80)) (* x0 x1 (/ 1 4)) (* (* x0 x0) x3 x1 (/ 63 80)) (* x2 (/ 61 160)) (* (* x0 x0) x2 (- (/ 61 160))) (* x3 x1 (- (/ 63 80))) (* x1 (- (/ 13 32))) (- (/ 1 20))) 0) (> (+ x0 (- (/ 106895606 185172565))) 0) (<= (+ x0 (- (/ 68076553 117797016))) 0) (>= (+ x1 (- (/ 8191875 8192))) 0) (< (+ x1 (- (/ 7316867489 7316914))) 0) (>= (+ x2 (- (/ 2674667839 4012149))) 0) (< (+ x2 (- (/ 3249617467 4874540))) 0) (> (+ x3 (- (/ 10360930316 13089425141))) 0) (<= (+ x3 (- (/ 163841111 206781977))) 0)))
(check-sat)
(exit)