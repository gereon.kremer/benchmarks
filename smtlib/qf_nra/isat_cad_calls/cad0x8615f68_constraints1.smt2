(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(declare-fun x2 () Real)
(assert (and  (> (* x0 x1) 0) (> (+ x2 (* x3 (- 1))) 0) (>= (+ x0 (/ 25565281647 40210711152427)) 0) (< x0 0) (>= (+ x1 1000) 0) (<= (+ x1 (/ 16383875 16384)) 0) (> x3 0) (<= (+ x3 (- (/ 1 128))) 0) (> x2 0) (<= (+ x2 (- (/ 1 128))) 0)))
(check-sat)
(exit)