(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(declare-fun x2 () Real)
(assert (and  (>= (+ (* x2 3) x1) 0) (<= (+ (* x1 x1) (* (* x0 x0) (- 80)) (- 75)) 0) (>= (+ (* x1 x1) (* (* x0 x0) (- 80)) (- 75)) 0) (<= (+ (* x2 x2) (- 3)) 0) (>= (+ (* x2 x2) (- 3)) 0) (>= (+ x0 (- (/ 13724643 16965941))) 0) (<= (+ x0 (- (/ 54305421 67041553))) 0) (>= (+ x1 (- (/ 409804849 36304051))) 0) (<= (+ x1 (- (/ 584361347 51760712))) 0) (>= (+ x2 (- (/ 138907099 80198051))) 0) (<= (+ x2 (- (/ 189750626 109552575))) 0)))
(check-sat)
(exit)