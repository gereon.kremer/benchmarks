(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x0 () Real)
(declare-fun x1 () Real)
(assert (and  (>= (+ (* (* x2 x2) (* x1 x1) (* x0 x0 x0 x0) (/ 1 4)) (* x1 x0) (* (* x0 x0) (/ 5 6)) (* (* x2 x2) x1 (* x0 x0 x0) (- (/ 1 2))) (* (* x2 x2) (* x0 x0) (/ 1 4)) (* x2 x1 (* x0 x0 x0 x0) (- (/ 1 2))) (* x2 x1 (/ 3 2)) (* x2 (* x1 x1) (* x0 x0 x0) (- (/ 1 2))) (* (* x2 x2) x1 x0 (- (/ 3 2))) (* (* x1 x1) (* x0 x0 x0 x0) (/ 1 12)) (* x1 (* x0 x0 x0) (/ 1 3)) (* x2 (* x1 x1) x0 (- (/ 3 2))) (* (* x2 x2) (/ 3 4)) (* x2 x0 (/ 3 2)) (* (* x1 x1) (* x0 x0) (/ 1 2)) (* (* x1 x1) (/ 3 4)) (* (* x2 x2) (* x1 x1) (* x0 x0) (/ 3 4)) (* x2 (* x0 x0 x0) (/ 1 2)) (* (* x0 x0 x0 x0) (/ 1 4)) (* x2 x1 (* x0 x0) (- 1)) (/ 1 4)) 0) (> (+ (* x1 (- 1)) x0) 0) (< (+ x2 (* x2 x1 x0 (- 1)) x1 x0) 0) (<= (+ (* x1 x0) x2 (* x2 x1 x0 (- 1)) x1 x0 (- 1)) 0) (<= (+ (* x2 (* x1 x1 x1) x0 (- 1)) (* x2 3) (* x2 (* x1 x1)) (* x1 (* x0 x0) 3) (* x1 x1 x1) (* x2 x1 x0 (- 3)) (* (* x1 x1 x1) (* x0 x0)) (* (* x1 x1) x0 3)) 0) (> x2 0) (< (+ x2 (- (/ 4694448 1758974461))) 0) (> (+ x0 (/ 59732297 222533906)) 0) (< (+ x0 (/ 47215111 177696250)) 0) (> (+ x1 (/ 72233585 263889091)) 0) (< (+ x1 (/ 64949321 239652080)) 0)))
(check-sat)
(exit)