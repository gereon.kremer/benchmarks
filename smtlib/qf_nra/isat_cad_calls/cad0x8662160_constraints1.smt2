(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(declare-fun x10 () Real)
(declare-fun x1 () Real)
(declare-fun x3 () Real)
(declare-fun x0 () Real)
(declare-fun x5 () Real)
(declare-fun x6 () Real)
(declare-fun x4 () Real)
(declare-fun x9 () Real)
(declare-fun x7 () Real)
(declare-fun x8 () Real)
(assert (and  (>= (+ (* x3 x4 (- 1)) (* x3 x0) x5 (* x2 (* x3 x3) (/ 1 2)) (* x6 (- 1))) 0) (>= (+ x4 (* x9 (- 1))) 0) (< (+ (* x8 (- 1)) x7) 0) (< (+ x5 (* x6 (- 1))) 0) (<= (+ x0 (* x4 (- 1))) 0) (<= (+ (* x2 x1) x0 (* x4 (- 1))) 0) (>= (+ (* x3 (- 1)) x1) 0) (>= (+ x0 (* x2 x3)) 0) (> (+ (* x2 x1) x0) 0) (>= (+ (* x2 x1) x0) 0) (> (+ x0 (* x2 x3)) 0) (> x2 0) (<= (+ x2 (- (/ 125 16384))) 0) (> x10 0) (<= (+ x10 (- 1000)) 0) (> x1 0) (< (+ x1 (- (/ 11292877 1479827552))) 0) (>= x3 0) (< (+ x3 (- (/ 564816 310859996011))) 0) (> x0 0) (<= (+ x0 (- (/ 1706625 268435456))) 0) (>= (+ x5 1000) 0) (< (+ x5 (/ 2808590884 2808605)) 0) (>= (+ x6 1000) 0) (<= (+ x6 (/ 2808590884 2808605)) 0) (> (+ x4 (- (/ 5843609848 5843647))) 0) (<= (+ x4 (- 1000)) 0) (> (+ x9 (- (/ 2861419811 8584445))) 0) (<= (+ x9 (- (/ 2494332839 7482989))) 0) (>= (+ x7 1000) 0) (<= (+ x7 (/ 16383875 16384)) 0) (>= (+ x8 1000) 0) (<= (+ x8 (/ 16383875 16384)) 0)))
(check-sat)
(exit)