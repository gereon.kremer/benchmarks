#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

# Example input file generator smtlib 2.0.
# Example originally from (2004-04-14): http://hysat.informatik.uni-oldenburg.de/benchmarks/etcs_train_system.hys
# @author: Florian Corzilius
# @author: Ulrich Loup
# @version: 2010-12-13

#
# Constants
#

#
# Default values
#

alpha = "(/ 8 10)"; # relation between the heigth of a jump compared to the antecessor jump
beta = "(/ 1 10)";  # loss of the forward motion (slope gets more steep)
ldelta = "(/ 1 10)";    	# upper bound of the initial slope
udelta = "(/ 2 10)";    	# lower bound of the initial slope
lheight = 80;    	# upper bound of the initial height in cm
uheight = 200;    # lower bound of the initial height in cm
ballsize = 3		# radius of the ball in cm
goalsize = 20;    # radius of the hole in cm

#
# Functions
#

def ranges(m):
  formula = ""
  for k in range(2,m+1):
    # Horizontal position of the ball when it bounces the k+1th time must be after the
    # horizontal position of the ball when it bounced the kth time.
    formula += " (>= p_"+str(k)+" 0)"
  return formula

def init():
  # Conditions at the moment when ball is dropped.
  formula = " (> height_0 "+str(lheight)+") (<= height_0 "+str(uheight)+") (>= delta_0 "+str(ldelta)+") (<= delta_0 "+str(udelta)+") (= p_0 0) bounce_1"
  return formula

def transitionSteps(m,d):
  formula = ""
  for k in range(0,m):
    # A bounce occurs if the height is zero.
    formula += " (or (not bounce_"+str(k+1)+") (= height_"+str(k+1)+" (* "+str(alpha)+" height_"+str(k)+")))"
    formula += " (or (not bounce_"+str(k+1)+") (= delta_"+str(k+1)+" (* (+ 1 "+str(beta)+") delta_"+str(k)+")))"
    formula += " (or (not bounce_"+str(k+1)+") (= 0 (+ height_"+str(k+1)+" (* (- 1) delta_"+str(k+1)+" p_"+str(k+1)+" p_"+str(k+1)+"))))"
    # Hit the hole in the kth bounce or the hole is farther away and we may reach it with the next bounce.
    formula += " (or (>= (+ p_0 p_1"
    for j in range(2,m+1):
      formula += " (* 2 p_"+str(k)+")"
    formula += ") "+str(d-goalsize+ballsize)+") (not bounce_"+str(k+1)+") bounce_"+str(k+2)+")"
    formula += " (or (<= (+ p_0 p_1"
    for j in range(2,m+1):
      formula += " (* 2 p_"+str(k)+")"
    formula += ") "+str(d+goalsize-ballsize)+") (not bounce_"+str(k+1)+") bounce_"+str(k+2)+")"
  # Maximal number of bounces is m.
  formula += " (not bounce_"+str(m+1)+")"
  return formula

#
# Generate BMC formula up to length n.
#
def formula(m,d,l,u):
  assert( m>0 )
  assert( d>0 )
  assert( u>l )
  lower = ""
  if l < 0:
    lower = "(- " + str(abs(l)) + ")"
  else:
    lower = str(l)
  upper = ""
  if u < 0:
    upper = "(- " + str(abs(u)) + ")"
  else:
    upper = str(u)
  formula = "(set-logic QF_NRA)\n"
  formula += "(set-info :source |\n"
  formula += "Florian Corzilius <corzilius@cs.rwth-aachen.de>\n"
  formula += "\n"
  formula += "|)\n"
  formula += "(set-info :smt-lib-version 2.0)\n"
  formula += "(set-info :category \"industrial\")\n"
  # all Pseudo-Boolean variables
  for k in range(1,m+2):
    formula += "(declare-fun bounce_"+str(k)+" () Bool)\n"
  # all real variables
  for k in range(0,m+1):
    formula += "(declare-fun p_"+str(k)+" () Real)\n"
    formula += "(declare-fun height_"+str(k)+" () Real)\n"
    formula += "(declare-fun delta_"+str(k)+" () Real)\n"
  # all real variables
  for k in range(0,m+1):
    formula += "(assert (>= p_"+str(k)+" " + lower + "))\n"
    formula += "(assert (<= p_"+str(k)+" " + upper + "))\n"
    formula += "(assert (>= height_"+str(k)+" " + lower + "))\n"
    formula += "(assert (<= height_"+str(k)+" " + upper + "))\n"
    formula += "(assert (>= delta_"+str(k)+" " + lower + "))\n"
    formula += "(assert (<= delta_"+str(k)+" " + upper + "))\n"
  formula += "(assert "
  formula += "(and"
  formula += ranges(m)
  formula += init()
  formula += transitionSteps(m,d)
  formula += "))\n"
  formula += "(check-sat)\n"
  formula += "(exit)\n"
  return formula

# Print usage information
def printUsage():
  print( "Usage: python "+sys.argv[0]+" m d l u\n" )
  print( "            m: number (>0) being the number of allowed bounces" )
  print( "            d: number (>0) being the distance of the hole in cm" )
  print( "            l: number being the lower bound of all variables" )
  print( "            u: number (>l) being the upper bound of all variables" )
  print("\n")
  print( "Example: python generate_bouncing_ball_bounded_example_smtlib.py 3 600 -1000 1000" )

#
# Parse input.
#
i = 0
for entry in sys.argv:
  try:
    if i == 1:
      m = int(entry)
      if (not(isinstance(m,int)) or m<1): raise ValueError()
    if i == 2:
      d = int(entry)
      if (not(isinstance(d,int)) or d<1): raise ValueError()
    if i == 3:
      l = int(entry)
      if (not(isinstance(l,int))): raise ValueError()
    if i == 4:
      u = int(entry)
      if (not(isinstance(u,int)) or l>=u): raise ValueError()
  except ValueError:
    print( "Error:",entry, "should be an appropriate value at position %i." % i )
    printUsage()
    sys.exit(1)
  i += 1
if i != 5:
  print( "Error: Insufficient number of arguments." )
  printUsage()
  sys.exit(1)

#
# Run formula run!
#
print( formula(m,d,l,u) )
