(set-logic QF_NRA)
(set-info :smt-lib-version 2.0)
(declare-fun x2 () Real)
(assert (and  (= x2 0) (> (+ x2 (- (/ 106968223 217675119))) 0) (<= (+ x2 (- (/ 149077582 298768593))) 0)))
(check-sat)
(exit)