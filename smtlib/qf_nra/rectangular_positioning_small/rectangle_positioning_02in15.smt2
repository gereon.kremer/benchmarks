(set-logic QF_NRA)
(set-info :source |
Florian Corzilius <corzilius@cs.rwth-aachen.de>

|)
(set-info :smt-lib-version 2.0)
(set-info :category "industrial")
(declare-fun x_0 () Real)
(declare-fun y_0 () Real)
(declare-fun x_1 () Real)
(declare-fun y_1 () Real)
(declare-fun delta_x () Real)
(declare-fun delta_y () Real)
(assert 
(and (or (= delta_x (- (+ x_1 2) x_1)) (< (+ x_1 2) (+ x_0 2)) (> x_1 x_0)) (or (= delta_x (- (+ x_0 2) x_1)) (< (+ x_0 2) (+ x_1 2)) (> x_1 x_0)) (or (= delta_x (- (+ x_1 2) x_0)) (< (+ x_1 2) (+ x_0 2)) (> x_0 x_1)) (or (= delta_x (- (+ x_0 2) x_0)) (< (+ x_0 2) (+ x_1 2)) (> x_0 x_1)) (or (= delta_y (- (+ y_1 5) y_1)) (< (+ y_1 5) (+ y_0 3)) (> y_1 y_0)) (or (= delta_y (- (+ y_0 3) y_1)) (< (+ y_0 3) (+ y_1 5)) (> y_1 y_0)) (or (= delta_y (- (+ y_1 5) y_0)) (< (+ y_1 5) (+ y_0 3)) (> y_0 y_1)) (or (= delta_y (- (+ y_0 3) y_0)) (< (+ y_0 3) (+ y_1 5)) (> y_0 y_1)) (or (>= (- x_0 (+ x_1 2)) 0) (>= (- x_1 (+ x_0 2)) 0) (>= (- y_0 (+ y_1 5)) 0) (>= (- y_1 (+ y_0 3)) 0)) (<= (* delta_x delta_y) 15)))
(check-sat)
(exit)

